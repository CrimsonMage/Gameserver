/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.2.10-MariaDB : Database - df_shard
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`df_shard` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `df_shard`;

/*Table structure for table `ace_contract_tracker` */

DROP TABLE IF EXISTS `ace_contract_tracker`;

CREATE TABLE `ace_contract_tracker` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `contractId` int(10) unsigned NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `stage` int(10) unsigned NOT NULL,
  `timeWhenDone` bigint(20) unsigned NOT NULL,
  `timeWhenRepeats` bigint(20) unsigned NOT NULL,
  `deleteContract` int(10) unsigned NOT NULL,
  `setAsDisplayContract` int(10) unsigned NOT NULL,
  PRIMARY KEY (`aceObjectId`,`contractId`),
  KEY `ace_contract_aceObject` (`aceObjectId`),
  CONSTRAINT `fk_contract_ace_object` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object` */

DROP TABLE IF EXISTS `ace_object`;

CREATE TABLE `ace_object` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `aceObjectDescriptionFlags` int(10) unsigned NOT NULL,
  `weenieClassId` int(10) unsigned NOT NULL,
  `userModified` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'flag indicating whether or not this has record has been altered since deployment',
  `lastModifiedDate` bigint(20) DEFAULT NULL,
  `modifiedBy` text DEFAULT NULL,
  `weenieHeaderFlags` int(10) unsigned DEFAULT NULL,
  `weenieHeaderFlags2` int(10) unsigned DEFAULT NULL,
  `physicsDescriptionFlag` int(10) unsigned DEFAULT NULL,
  `currentMotionState` text DEFAULT NULL,
  PRIMARY KEY (`aceObjectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_animation_change` */

DROP TABLE IF EXISTS `ace_object_animation_change`;

CREATE TABLE `ace_object_animation_change` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `index` tinyint(3) unsigned NOT NULL,
  `animationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`aceObjectId`,`index`),
  CONSTRAINT `FK_ace_object_animation_changes__baseAceObjectId` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_palette_change` */

DROP TABLE IF EXISTS `ace_object_palette_change`;

CREATE TABLE `ace_object_palette_change` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `subPaletteId` int(10) unsigned NOT NULL,
  `offset` smallint(5) unsigned NOT NULL,
  `length` smallint(5) unsigned zerofill NOT NULL,
  PRIMARY KEY (`aceObjectId`,`subPaletteId`,`offset`,`length`),
  CONSTRAINT `FK_ace_object_palette_data__baseAceObjectId` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_attribute` */

DROP TABLE IF EXISTS `ace_object_properties_attribute`;

CREATE TABLE `ace_object_properties_attribute` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `attributeId` smallint(4) unsigned NOT NULL,
  `attributeBase` smallint(4) unsigned NOT NULL DEFAULT 0,
  `attributeRanks` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `attributeXpSpent` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`aceObjectId`,`attributeId`),
  UNIQUE KEY `ace_object__property_attribute_id` (`aceObjectId`,`attributeId`),
  CONSTRAINT `fk_Prop_Attribute_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_attribute2nd` */

DROP TABLE IF EXISTS `ace_object_properties_attribute2nd`;

CREATE TABLE `ace_object_properties_attribute2nd` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `attribute2ndId` smallint(4) unsigned NOT NULL,
  `attribute2ndValue` mediumint(7) unsigned NOT NULL DEFAULT 0,
  `attribute2ndRanks` smallint(5) unsigned NOT NULL DEFAULT 0,
  `attribute2ndXpSpent` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`aceObjectId`,`attribute2ndId`),
  UNIQUE KEY `ace_object__property_attribute2nd_id` (`aceObjectId`,`attribute2ndId`),
  CONSTRAINT `fk_Prop_Attribute2nd_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_bigint` */

DROP TABLE IF EXISTS `ace_object_properties_bigint`;

CREATE TABLE `ace_object_properties_bigint` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `bigIntPropertyId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `propertyIndex` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `propertyValue` bigint(20) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `ace_object__property_bigint_id` (`aceObjectId`,`bigIntPropertyId`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_BigInt_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_book` */

DROP TABLE IF EXISTS `ace_object_properties_book`;

CREATE TABLE `ace_object_properties_book` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `page` int(10) unsigned NOT NULL DEFAULT 0,
  `authorName` varchar(255) NOT NULL,
  `authorAccount` varchar(255) NOT NULL,
  `authorId` int(10) unsigned NOT NULL DEFAULT 0,
  `ignoreAuthor` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pageText` text NOT NULL,
  PRIMARY KEY (`aceObjectId`,`page`),
  UNIQUE KEY `ace_object__property_book_id` (`aceObjectId`,`page`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_Book_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_bool` */

DROP TABLE IF EXISTS `ace_object_properties_bool`;

CREATE TABLE `ace_object_properties_bool` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `boolPropertyId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `propertyIndex` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `propertyValue` tinyint(1) NOT NULL DEFAULT 0,
  UNIQUE KEY `ace_object__property_bool_id` (`aceObjectId`,`boolPropertyId`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_Bool_Ace_object` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_did` */

DROP TABLE IF EXISTS `ace_object_properties_did`;

CREATE TABLE `ace_object_properties_did` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `didPropertyId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `propertyIndex` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `propertyValue` int(10) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `ace_object__property_did_id` (`aceObjectId`,`didPropertyId`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_Did_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_double` */

DROP TABLE IF EXISTS `ace_object_properties_double`;

CREATE TABLE `ace_object_properties_double` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `dblPropertyId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `propertyIndex` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `propertyValue` double NOT NULL DEFAULT 0,
  UNIQUE KEY `ace_object__property_double_id` (`aceObjectId`,`dblPropertyId`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_Dbl_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_iid` */

DROP TABLE IF EXISTS `ace_object_properties_iid`;

CREATE TABLE `ace_object_properties_iid` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `iidPropertyId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `propertyIndex` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `propertyValue` int(10) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `ace_object__property_iid_id` (`aceObjectId`,`iidPropertyId`),
  KEY `aceObjectId` (`aceObjectId`),
  KEY `aopiid_propertyValue` (`propertyValue`),
  CONSTRAINT `fk_Prop_Iid_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_int` */

DROP TABLE IF EXISTS `ace_object_properties_int`;

CREATE TABLE `ace_object_properties_int` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `intPropertyId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `propertyIndex` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `propertyValue` int(10) NOT NULL DEFAULT 0,
  UNIQUE KEY `ace_object__property_int_id` (`aceObjectId`,`intPropertyId`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_Int_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_skill` */

DROP TABLE IF EXISTS `ace_object_properties_skill`;

CREATE TABLE `ace_object_properties_skill` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `skillId` tinyint(1) unsigned NOT NULL,
  `skillStatus` tinyint(1) unsigned NOT NULL,
  `skillPoints` smallint(2) unsigned NOT NULL DEFAULT 0,
  `skillXpSpent` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`aceObjectId`,`skillId`),
  CONSTRAINT `fk_Prop_Skill_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_spell` */

DROP TABLE IF EXISTS `ace_object_properties_spell`;

CREATE TABLE `ace_object_properties_spell` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `spellId` int(10) unsigned NOT NULL DEFAULT 0,
  `probability` float DEFAULT NULL,
  UNIQUE KEY `ace_object__property_spell_id` (`spellId`,`aceObjectId`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_Spell_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_spellbar_positions` */

DROP TABLE IF EXISTS `ace_object_properties_spellbar_positions`;

CREATE TABLE `ace_object_properties_spellbar_positions` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `spellId` int(10) unsigned NOT NULL DEFAULT 0,
  `spellBarId` int(10) unsigned NOT NULL DEFAULT 0,
  `spellBarPositionId` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`aceObjectId`,`spellId`,`spellBarId`),
  CONSTRAINT `fk_sb_ao` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_properties_string` */

DROP TABLE IF EXISTS `ace_object_properties_string`;

CREATE TABLE `ace_object_properties_string` (
  `aceObjectId` int(10) unsigned NOT NULL DEFAULT 0,
  `strPropertyId` smallint(5) unsigned NOT NULL DEFAULT 0,
  `propertyIndex` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `propertyValue` text NOT NULL,
  UNIQUE KEY `ace_object__property_string_id` (`aceObjectId`,`strPropertyId`),
  KEY `aceObjectId` (`aceObjectId`),
  CONSTRAINT `fk_Prop_Str_AceObject` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_object_texture_map_change` */

DROP TABLE IF EXISTS `ace_object_texture_map_change`;

CREATE TABLE `ace_object_texture_map_change` (
  `aceObjectId` int(10) unsigned NOT NULL,
  `index` tinyint(3) unsigned NOT NULL,
  `oldId` int(10) unsigned NOT NULL,
  `newId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`aceObjectId`,`index`,`oldId`),
  CONSTRAINT `FK_ace_object_texture_map_changes__baseAceObjectId` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `ace_position` */

DROP TABLE IF EXISTS `ace_position`;

CREATE TABLE `ace_position` (
  `positionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aceObjectId` int(10) unsigned NOT NULL,
  `positionType` smallint(5) unsigned NOT NULL,
  `landblockRaw` int(10) unsigned NOT NULL,
  `landblock` int(5) unsigned GENERATED ALWAYS AS (`landblockRaw` >> 16) VIRTUAL,
  `posX` float NOT NULL,
  `posY` float NOT NULL,
  `posZ` float NOT NULL,
  `qW` float NOT NULL,
  `qX` float NOT NULL,
  `qY` float NOT NULL,
  `qZ` float NOT NULL,
  PRIMARY KEY (`positionId`),
  KEY `idx_aceObjectId` (`aceObjectId`),
  KEY `idxPostionType` (`positionType`),
  KEY `idx_landblock_raw` (`landblockRaw`),
  KEY `idx_landblock` (`landblock`),
  CONSTRAINT `fk_position_ao` FOREIGN KEY (`aceObjectId`) REFERENCES `ace_object` (`aceObjectId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

/*Table structure for table `ace_weenie_class` */

DROP TABLE IF EXISTS `ace_weenie_class`;

CREATE TABLE `ace_weenie_class` (
  `weenieClassId` int(10) unsigned NOT NULL,
  `weenieClassDescription` text NOT NULL,
  PRIMARY KEY (`weenieClassId`),
  UNIQUE KEY `idx_weenieName` (`weenieClassDescription`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `vw_ace_character` */

DROP TABLE IF EXISTS `vw_ace_character`;

/*!50001 DROP VIEW IF EXISTS `vw_ace_character` */;
/*!50001 DROP TABLE IF EXISTS `vw_ace_character` */;

/*!50001 CREATE TABLE  `vw_ace_character`(
 `guid` int(10) unsigned ,
 `subscriptionId` int(10) unsigned ,
 `NAME` text ,
 `deleted` tinyint(1) ,
 `deleteTime` bigint(20) unsigned ,
 `weenieClassId` int(10) unsigned ,
 `aceObjectDescriptionFlags` int(10) unsigned ,
 `physicsDescriptionFlag` int(10) unsigned ,
 `weenieHeaderFlags` int(10) unsigned ,
 `itemType` int(10) ,
 `loginTimestamp` double
)*/;

/*Table structure for table `vw_ace_inventory_object` */

DROP TABLE IF EXISTS `vw_ace_inventory_object`;

/*!50001 DROP VIEW IF EXISTS `vw_ace_inventory_object` */;
/*!50001 DROP TABLE IF EXISTS `vw_ace_inventory_object` */;

/*!50001 CREATE TABLE  `vw_ace_inventory_object`(
 `containerId` int(10) unsigned ,
 `aceObjectId` int(10) unsigned ,
 `placement` int(10)
)*/;

/*Table structure for table `vw_ace_object` */

DROP TABLE IF EXISTS `vw_ace_object`;

/*!50001 DROP VIEW IF EXISTS `vw_ace_object` */;
/*!50001 DROP TABLE IF EXISTS `vw_ace_object` */;

/*!50001 CREATE TABLE  `vw_ace_object`(
 `aceObjectId` int(10) unsigned ,
 `name` text ,
 `weenieClassId` int(10) unsigned ,
 `currentMotionState` text ,
 `weenieClassDescription` text ,
 `aceObjectDescriptionFlags` int(10) unsigned ,
 `physicsDescriptionFlag` int(10) unsigned ,
 `weenieHeaderFlags` int(10) unsigned ,
 `itemType` int(10) ,
 `positionId` int(10) unsigned ,
 `positionType` smallint(5) unsigned ,
 `LandblockRaw` int(10) unsigned ,
 `landblock` int(5) unsigned ,
 `posX` float ,
 `posY` float ,
 `posZ` float ,
 `qW` float ,
 `qX` float ,
 `qY` float ,
 `qZ` float
)*/;

/*Table structure for table `vw_ace_wielded_object` */

DROP TABLE IF EXISTS `vw_ace_wielded_object`;

/*!50001 DROP VIEW IF EXISTS `vw_ace_wielded_object` */;
/*!50001 DROP TABLE IF EXISTS `vw_ace_wielded_object` */;

/*!50001 CREATE TABLE  `vw_ace_wielded_object`(
 `wielderId` int(10) unsigned ,
 `aceObjectId` int(10) unsigned ,
 `wieldedLocation` int(10)
)*/;

/*View structure for view vw_ace_character */

/*!50001 DROP TABLE IF EXISTS `vw_ace_character` */;
/*!50001 DROP VIEW IF EXISTS `vw_ace_character` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_ace_character` AS select `ao`.`aceObjectId` AS `guid`,`aopiidacc`.`propertyValue` AS `subscriptionId`,`aops`.`propertyValue` AS `NAME`,`aopb`.`propertyValue` AS `deleted`,`aopbi`.`propertyValue` AS `deleteTime`,`ao`.`weenieClassId` AS `weenieClassId`,`ao`.`aceObjectDescriptionFlags` AS `aceObjectDescriptionFlags`,`ao`.`physicsDescriptionFlag` AS `physicsDescriptionFlag`,`ao`.`weenieHeaderFlags` AS `weenieHeaderFlags`,`aopi`.`propertyValue` AS `itemType`,`aopd`.`propertyValue` AS `loginTimestamp` from ((((((`ace_object` `ao` join `ace_object_properties_string` `aops` on(`ao`.`aceObjectId` = `aops`.`aceObjectId` and `aops`.`strPropertyId` = 1)) join `ace_object_properties_bool` `aopb` on(`ao`.`aceObjectId` = `aopb`.`aceObjectId` and `aopb`.`boolPropertyId` = 9001)) join `ace_object_properties_int` `aopi` on(`ao`.`aceObjectId` = `aopi`.`aceObjectId` and `aopi`.`intPropertyId` = 1)) join `ace_object_properties_bigint` `aopbi` on(`ao`.`aceObjectId` = `aopbi`.`aceObjectId` and `aopbi`.`bigIntPropertyId` = 9001)) join `ace_object_properties_iid` `aopiidacc` on(`ao`.`aceObjectId` = `aopiidacc`.`aceObjectId` and `aopiidacc`.`iidPropertyId` = 9001)) left join `ace_object_properties_double` `aopd` on(`ao`.`aceObjectId` = `aopd`.`aceObjectId` and `aopd`.`dblPropertyId` = 48)) */;

/*View structure for view vw_ace_inventory_object */

/*!50001 DROP TABLE IF EXISTS `vw_ace_inventory_object` */;
/*!50001 DROP VIEW IF EXISTS `vw_ace_inventory_object` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_ace_inventory_object` AS (select `aopiid`.`propertyValue` AS `containerId`,`aopiid`.`aceObjectId` AS `aceObjectId`,`aopi`.`propertyValue` AS `placement` from (`ace_object_properties_iid` `aopiid` join `ace_object_properties_int` `aopi` on(`aopiid`.`aceObjectId` = `aopi`.`aceObjectId` and `aopi`.`intPropertyId` = 65)) where `aopiid`.`iidPropertyId` = 2 order by `aopiid`.`propertyValue`,`aopi`.`propertyValue`) */;

/*View structure for view vw_ace_object */

/*!50001 DROP TABLE IF EXISTS `vw_ace_object` */;
/*!50001 DROP VIEW IF EXISTS `vw_ace_object` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_ace_object` AS select `ao`.`aceObjectId` AS `aceObjectId`,`aops`.`propertyValue` AS `name`,`ao`.`weenieClassId` AS `weenieClassId`,`ao`.`currentMotionState` AS `currentMotionState`,`awc`.`weenieClassDescription` AS `weenieClassDescription`,`ao`.`aceObjectDescriptionFlags` AS `aceObjectDescriptionFlags`,`ao`.`physicsDescriptionFlag` AS `physicsDescriptionFlag`,`ao`.`weenieHeaderFlags` AS `weenieHeaderFlags`,`aopi`.`propertyValue` AS `itemType`,`ap`.`positionId` AS `positionId`,`ap`.`positionType` AS `positionType`,`ap`.`landblockRaw` AS `LandblockRaw`,`ap`.`landblock` AS `landblock`,`ap`.`posX` AS `posX`,`ap`.`posY` AS `posY`,`ap`.`posZ` AS `posZ`,`ap`.`qW` AS `qW`,`ap`.`qX` AS `qX`,`ap`.`qY` AS `qY`,`ap`.`qZ` AS `qZ` from ((((`ace_object` `ao` join `ace_weenie_class` `awc` on(`ao`.`weenieClassId` = `awc`.`weenieClassId`)) join `ace_object_properties_string` `aops` on(`ao`.`aceObjectId` = `aops`.`aceObjectId` and `aops`.`strPropertyId` = 1)) join `ace_object_properties_int` `aopi` on(`ao`.`aceObjectId` = `aopi`.`aceObjectId` and `aopi`.`intPropertyId` = 1)) join `ace_position` `ap` on(`ao`.`aceObjectId` = `ap`.`aceObjectId` and `ap`.`positionType` = 1)) */;

/*View structure for view vw_ace_wielded_object */

/*!50001 DROP TABLE IF EXISTS `vw_ace_wielded_object` */;
/*!50001 DROP VIEW IF EXISTS `vw_ace_wielded_object` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_ace_wielded_object` AS (select `aopiid`.`propertyValue` AS `wielderId`,`aopiid`.`aceObjectId` AS `aceObjectId`,`aopi`.`propertyValue` AS `wieldedLocation` from (`ace_object_properties_iid` `aopiid` join `ace_object_properties_int` `aopi` on(`aopiid`.`aceObjectId` = `aopi`.`aceObjectId` and `aopi`.`intPropertyId` = 10)) where `aopiid`.`iidPropertyId` = 3 order by `aopiid`.`propertyValue`,`aopi`.`propertyValue`) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
