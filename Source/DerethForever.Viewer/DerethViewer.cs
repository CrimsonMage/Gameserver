/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
************************************************************************/
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Entity;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameTime = Microsoft.Xna.Framework.GameTime;

namespace DerethForever.Viewer
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class DerethViewer : Game
    {
        GraphicsDeviceManager graphics;
        Effect effect;

        private static string _datFolder;
        private static string _cellDatFilename = "client_cell_1.dat";
        private static string _cellDat;
        private static string _portalDatFilename = "client_portal.dat";
        private static string _portalDat;
        private List<Landblock> _landblocks = new List<Landblock>();

        private VertexPositionColorNormal[] triangles = new VertexPositionColorNormal[0];
        private VertexPositionColor[] ground = new VertexPositionColor[0];
        private VertexPositionColor[] buildings = new VertexPositionColor[0];
        private Vector3 cameraPosition = new Vector3(1600, -50, 1000);
        private Vector3 cameraLookAtVector = new Vector3(1600, 500, 500);
        private float cameraDirection = 0f; // heading, in radians
        private float cameraDistance = 500f;

        public DerethViewer()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1440;
            graphics.PreferredBackBufferHeight = 900;

            Content.RootDirectory = "Content";

            _datFolder = ConfigurationManager.AppSettings["DatFolder"];
            _cellDat = Path.Combine(_datFolder, _cellDatFilename);
            _portalDat = Path.Combine(_datFolder, _portalDatFilename);

            CellDatReader.Initialize(_cellDat);
            PortalDatReader.Initialize(_portalDat);

            graphics.GraphicsProfile = GraphicsProfile.HiDef;
        }

        protected override void LoadContent()
        {
            byte[] effectsFile = File.ReadAllBytes("Content/effects.mgfxo");
            effect = new Effect(GraphicsDevice, effectsFile);
            base.LoadContent();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            for (uint x = 0; x < 20; x++)
            {
                for (uint y = 0; y < 20; y++)
                {
                    uint landblockId = (((x << 8) | y) << 16);
                    _landblocks.Add(Landblock.Load(landblockId));
                }
            }

            List<VertexPositionColor> tLines = new List<VertexPositionColor>();
            List<VertexPositionColorNormal> tTriangles = new List<VertexPositionColorNormal>();

            foreach (var lb in _landblocks)
            {
                System.Numerics.Vector3 vZero = System.Numerics.Vector3.Zero;
                int xCells, yCells;
                LandDefs.GetOutsideLCoord(lb.Id, vZero, out xCells, out yCells);

                Vector3 offset = new Vector3(xCells * 24f, yCells * 24f, 0f);

                // draw objects
                foreach (var poly in lb.Polygons)
                {
                    //for (int i = 0; i < (poly.Vertices.Count - 1); i++)
                    //{
                    //    tLines.Add(new VertexPositionColor() { Position = Vec(poly.Vertices[i]) + offset });
                    //    tLines.Add(new VertexPositionColor() { Position = Vec(poly.Vertices[i + 1]) + offset });
                    //}

                    //// manually add the last line back to 0
                    //tLines.Add(new VertexPositionColor() { Position = Vec(poly.Vertices[poly.Vertices.Count - 1]) + offset });
                    //tLines.Add(new VertexPositionColor() { Position = Vec(poly.Vertices[0]) + offset });

                    tTriangles.Add(new VertexPositionColorNormal() { Position = Vec(poly.Vertices[2]) + offset, Color = Color.White, Normal = Vec(poly.Plane.Normal) });
                    tTriangles.Add(new VertexPositionColorNormal() { Position = Vec(poly.Vertices[1]) + offset, Color = Color.White, Normal = Vec(poly.Plane.Normal) });
                    tTriangles.Add(new VertexPositionColorNormal() { Position = Vec(poly.Vertices[0]) + offset, Color = Color.White, Normal = Vec(poly.Plane.Normal) });
                }
            }

            ground = tLines.ToArray();
            triangles = tTriangles.ToArray();

            effect = new BasicEffect(graphics.GraphicsDevice);

            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            var state = Keyboard.GetState();

            if (state.IsKeyDown(Keys.E))
            {
                cameraPosition.X += 10 * (float)System.Math.Cos(cameraDirection);
                cameraPosition.Y += -10 * (float)System.Math.Sin(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Q))
            {
                cameraPosition.X -= 10 * (float)System.Math.Cos(cameraDirection);
                cameraPosition.Y -= -10 * (float)System.Math.Sin(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Up) || state.IsKeyDown(Keys.W))
            {
                cameraPosition.X += 10 * (float)System.Math.Sin(cameraDirection);
                cameraPosition.Y += 10 * (float)System.Math.Cos(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Down) || state.IsKeyDown(Keys.S))
            {
                cameraPosition.X -= 10 * (float)System.Math.Sin(cameraDirection);
                cameraPosition.Y -= 10 * (float)System.Math.Cos(cameraDirection);
            }

            if (state.IsKeyDown(Keys.Space))
                cameraPosition.Z += 10;
            if (state.IsKeyDown(Keys.LeftShift))
                cameraPosition.Z -= 10;

            if (state.IsKeyDown(Keys.Left) || state.IsKeyDown(Keys.A))
                cameraDirection -= 0.02f;
            if (state.IsKeyDown(Keys.Right) || state.IsKeyDown(Keys.D))
                cameraDirection += 0.02f;

            Vector3 tempOffset;
            tempOffset.X = (float)System.Math.Sin(cameraDirection) * cameraDistance;
            tempOffset.Y = (float)System.Math.Cos(cameraDirection) * cameraDistance;
            tempOffset.Z = -500f;
            cameraLookAtVector = cameraPosition + tempOffset;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            DrawTheThings();

            base.Draw(gameTime);
        }

        public void DrawTheThings()
        {
            // The assignment of effect.View and effect.Projection
            // are nearly identical to the code in the Model drawing code.
            var cameraUpVector = Vector3.UnitZ;


            float aspectRatio = graphics.PreferredBackBufferWidth / (float)graphics.PreferredBackBufferHeight;
            float fieldOfView = Microsoft.Xna.Framework.MathHelper.PiOver2;
            float nearClipPlane = 1;
            float farClipPlane = 5000;

            var projectionMatrix = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearClipPlane, farClipPlane);
            effect.Parameters["xProjection"].SetValue(projectionMatrix);

            var viewMatrix = Matrix.CreateLookAt(cameraPosition, cameraLookAtVector, cameraUpVector);
            effect.Parameters["xView"].SetValue(viewMatrix);
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);
            effect.Parameters["xAmbient"].SetValue(0.3f);
            effect.Parameters["xShowNormals"].SetValue(true);

            effect.CurrentTechnique = effect.Techniques["Colored"];

            effect.Parameters["xEnableLighting"].SetValue(true);
            Vector3 lightDirection = new Vector3(0.1f, 0.1f, -0.5f);
            effect.Parameters["xLightDirection"].SetValue(lightDirection);
            

            int triangleCount = triangles.Length / 3;
            int lineCount = ground.Length / 2;

            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                if (triangleCount > 0)
                    graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, triangles, 0, triangleCount, VertexPositionColorNormal.VertexDeclaration);

                if (lineCount > 0)
                    graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, ground, 0, lineCount);
            }

        }
        private Vector3 Vec(System.Numerics.Vector3 snVector)
        {
            return new Vector3(snVector.X, snVector.Y, snVector.Z);
        }
    }
}
