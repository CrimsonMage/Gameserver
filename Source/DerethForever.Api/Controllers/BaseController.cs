/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using log4net;

using DerethForever.Common;
using DerethForever.Database;
using DerethForever.DatLoader;
using System.Diagnostics;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    ///
    /// </summary>
    public class BaseController : ApiController
    {
        /// <summary>
        /// logging for all of the api methods
        /// </summary>
        protected static readonly ILog log = LogManager.GetLogger("ApiHost");

        static BaseController()
        {
        }
        
        /// <summary>
        /// list of observers with which to register/fire changes.  note, i'm not a huge fan of
        /// using a singleton list like this.  i'd much rather do dependency injection for each
        /// controller but setting that stuff up is a royal PITA.
        /// </summary>
        public static List<IApiObserver> ApiObservers { get; set; } = new List<IApiObserver>();
    }
}
