/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

using DerethForever.Api.Common;
using DerethForever.Database;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using Swashbuckle.Swagger.Annotations;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    /// Manages spawn map data
    /// </summary>
    public class SpawnController : BaseController
    {
        /// <summary>
        /// gets the first 100 landblock spawn maps matching the criteria
        /// </summary>
        [HttpPost]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<ushort>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Search(SearchSpawnMapsCriteria criteria)
        {
            if (criteria == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("Invalid search parameters."));
            }

            var spawnMaps = DatabaseManager.World.SearchSpawnMaps(criteria).Take(100);
            return Request.CreateResponse(HttpStatusCode.OK, spawnMaps ?? new List<ushort>());
        }

        /// <summary>
        /// gets a single spawn map
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<WeenieObjectInstance>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage GetInstancesByLandblock(int landblockId)
        {
            var spawnMap = DatabaseManager.World.GetSpawnMap((ushort)landblockId);
            if (spawnMap != null)
                return Request.CreateResponse(HttpStatusCode.OK, spawnMap);
            else
                return Request.CreateResponse(HttpStatusCode.OK, new List<WeenieObjectInstance>());
        }

        /// <summary>
        /// Gets an instance from a spawn map
        /// </summary>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(WeenieObjectInstance))]
        [SwaggerResponse(HttpStatusCode.NotFound, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage GetInstance(int instanceId)
        {
            var spawnMapId = DatabaseManager.World.SearchSpawnMaps(new SearchSpawnMapsCriteria() { InstanceId = instanceId }).FirstOrDefault();
            var instance = DatabaseManager.World.GetInstance(spawnMapId, instanceId);
            if (instance != null)
                return Request.CreateResponse(HttpStatusCode.OK, instance);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound, new SimpleMessage("Instance was not found."));
        }

        /// <summary>
        /// Adds an instance to a spawn map
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage UpdateInstance(ushort landblockId, WeenieObjectInstance instance)
        {
            DatabaseManager.World.UpdateInstance(landblockId, instance);
            return Request.CreateResponse(HttpStatusCode.OK, new SimpleMessage("Instance updated!"));
        }

        /// <summary>
        /// Removes an instance from a spawn map
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage RemoveInstance(ushort landblockId, WeenieObjectInstance instance)
        {
            DatabaseManager.World.RemoveInstance(landblockId, instance);
            return Request.CreateResponse(HttpStatusCode.OK, new SimpleMessage("Instance removed!"));
        }
    }
}
