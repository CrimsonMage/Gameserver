/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Web.Http;
using System.Net.Http;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using DerethForever.Api.Common;
using DerethForever.Api.Models;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Database;
using Newtonsoft.Json;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    ///
    /// </summary>
    public class WeenieController : BaseController
    {
        /// <summary>
        /// Searches for Weenies according to the given criteria.  All criteria are applied as "AND", logically.
        /// </summary>
        [HttpPost]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<WeenieSearchResult>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        public HttpResponseMessage Search([FromBody] SearchWeeniesCriteria request)
        {
            return Request.CreateResponse(HttpStatusCode.OK, DatabaseManager.World.SearchWeenies(request, 100));
        }

        /// <summary>
        /// fetches a single weenie object.  this method is not implemented yet.  do not code against it.
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, "success", typeof(DataObject))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "requested weenie id not found")]
        public HttpResponseMessage Get(uint weenieId)
        {
            DataObject weenie = DatabaseManager.World.GetWeenie(weenieId);
            return weenie != null ? Request.CreateResponse(HttpStatusCode.OK, weenie) : Request.CreateResponse(HttpStatusCode.NotFound);
        }

        /// <summary>
        /// updates a weenie.  this method is not implemented yet.  do not code against it.
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Envoy)]
        [SwaggerResponse(HttpStatusCode.OK, "weenie updated", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unable to save changes.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "existing object not found.")]
        public HttpResponseMessage Update([FromBody] DataObject request)
        {
            // if a request property is ever inexplicably missing or null, convert the method signature to this:
            // public HttpResponseMessage Update([RawBody] string raw)

            // then use this to create the DataWeenie.  This can happen any time there is a deserialization error
            // Newtonsoft tends to silently swallow deserialization errors on us.
            // DataWeenie request = JsonConvert.DeserializeObject<DataWeenie>(raw + additionalContent);

            // see if we already have this weenie.  has acceptable race condition error cases.
            var existingWeenie = DatabaseManager.World.GetWeenie(request.WeenieClassId);
            if (existingWeenie == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "no existing weenie object found to update." });

            // necessary to set this in order for it to not attempt an insert
            request.HasEverBeenSavedToDatabase = true;

            if (DatabaseManager.World.ReplaceWeenie(request))
            {
                uint wcid = request.WeenieClassId;

                Task.Run(() => {
                    ApiObservers.ForEach(o => o.WeenieChanged(wcid));
                });

                return Request.CreateResponse(HttpStatusCode.OK, new { message = "changes saved." });
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Unable to save changes." });
        }

        /// <summary>
        /// updates a weenie.  this method is not implemented yet.  do not code against it.
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Envoy)]
        [SwaggerResponse(HttpStatusCode.OK, "weenie updated", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unable to save changes.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "existing object not found.")]
        public HttpResponseMessage Preview([FromBody] DataObject request)
        {
            // if a request property is ever inexplicably missing or null, convert the method signature to this:
            // public HttpResponseMessage Update([RawBody] string raw)

            // then use this to create the DataWeenie.  This can happen any time there is a deserialization error
            // Newtonsoft tends to silently swallow deserialization errors on us.
            // DataWeenie request = JsonConvert.DeserializeObject<DataWeenie>(raw + additionalContent);

            // see if we already have this weenie.  has acceptable race condition error cases.
            var existingWeenie = DatabaseManager.World.GetWeenie(request.WeenieClassId);
            if (existingWeenie == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new { message = "no existing weenie object found to update." });

            // necessary to set this in order for it to not attempt an insert
            request.HasEverBeenSavedToDatabase = true;

            if (DatabaseManager.World.CacheOnlySaveWeenie(request))
            {
                uint wcid = request.WeenieClassId;

                Task.Run(() => {
                    ApiObservers.ForEach(o => o.WeenieChanged(wcid));
                });

                return Request.CreateResponse(HttpStatusCode.OK, new { message = "changes previewed." });
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Unable to preview changes." });
        }

        /// <summary>
        /// creates a new weenie.
        /// </summary>
        [HttpPost]
        [ApiAuthorize(AccessLevel.Envoy)]
        [SwaggerResponse(HttpStatusCode.OK, "weenie created", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unable to save changes.")]
        public HttpResponseMessage Create([FromBody] DataObject request)
        {
            // if a request property is ever inexplicably missing or null, convert the method signature to this:
            // public HttpResponseMessage Update([RawBody] string raw)

            // then use this to create the DataWeenie.  This can happen any time there is a deserialization error
            // Newtonsoft tends to silently swallow deserialization errors on us.
            // DataWeenie request = JsonConvert.DeserializeObject<DataWeenie>(raw + additionalContent);

            if (DatabaseManager.World.SaveWeenie(request))
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "changes saved." });

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Unable to save changes." });
        }

        /// <summary>
        /// deletes a weenie.
        /// </summary>
        [HttpDelete]
        [ApiAuthorize(AccessLevel.Developer)]
        [SwaggerResponse(HttpStatusCode.OK, "weenie deleted", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "missing or invalid authorization header.")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unable to save changes.")]
        public HttpResponseMessage Delete(uint weenieClassId)
        {
            DataObject w = DatabaseManager.World.GetWeenie(weenieClassId);

            if (DatabaseManager.World.DeleteWeenie(w))
                return Request.CreateResponse(HttpStatusCode.OK, new { message = "object deleted" });

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { message = "Unable to save changes." });
        }
    }
}
