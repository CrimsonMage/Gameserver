/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.DatLoader
{
    public class CellDatDatabase : DatDatabase
    {
        private static volatile CellDatDatabase _cellDatDatabase;

        private static object _cellDatDatabaseLock = new object();

        public CellDatDatabase(string filename) : base(filename, DatDatabaseType.Cell)
        {
            if (_cellDatDatabase == null)
            {
                lock (_cellDatDatabaseLock)
                {
                    if (_cellDatDatabase == null)
                    {
                        ReadDat();
                        _cellDatDatabase = this;
                    }
                }
            }
            else
            {
                AllFiles = _cellDatDatabase.AllFiles;
                RootDirectory = _cellDatDatabase.RootDirectory;
                SectorSize = _cellDatDatabase.SectorSize;
                FilePath = filename;
            }
        }

        public void ExtractLandblockContents(string path)
        {
            foreach (KeyValuePair<uint, DatFile> entry in this.AllFiles)
            {
                string thisFolder = Path.Combine(path, (entry.Value.ObjectId >> 16).ToString("X4"));

                if (!Directory.Exists(thisFolder))
                {
                    Directory.CreateDirectory(thisFolder);
                }

                // Use the DatReader to get the file data - file blocks can extend over block size.
                DatReader dr = this.GetReaderForFile(entry.Value.ObjectId);

                string hex = entry.Value.ObjectId.ToString("X8");
                string thisFile = Path.Combine(thisFolder, hex + ".bin");
                File.WriteAllBytes(thisFile, dr.Buffer);
            }
        }
    }
}
