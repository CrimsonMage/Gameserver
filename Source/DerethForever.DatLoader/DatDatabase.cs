/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using log4net;

namespace DerethForever.DatLoader
{
    public class DatDatabase
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DatDirectory RootDirectory { get; set; }

        public Dictionary<uint, DatFile> AllFiles { get; set; }

        // So we can cache the read files. The read methods in the FileTypes will handle the caching and casting.
        public Dictionary<uint, object> FileCache { get; set; } = new Dictionary<uint, object>();

        public DatDatabaseType DatType { get; set; }

        public string FilePath { get; set; }

        public uint SectorSize { get; set; }

        private static object _datDatabaseLock = new object();

        public DatDatabase(string filePath, DatDatabaseType type)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException(filePath);
            }

            this.FilePath = filePath;
            DatType = type;
        }

        public void ReadDat()
        {
            lock (_datDatabaseLock)
            {
                using (FileStream stream = new FileStream(FilePath, FileMode.Open))
                {
                    byte[] sectorSizeBuffer = new byte[4];
                    stream.Seek(0x144u, SeekOrigin.Begin);
                    stream.Read(sectorSizeBuffer, 0, sizeof(uint));
                    this.SectorSize = BitConverter.ToUInt32(sectorSizeBuffer, 0);

                    stream.Seek(0x160u, SeekOrigin.Begin);
                    byte[] firstDirBuffer = new byte[4];
                    stream.Read(firstDirBuffer, 0, sizeof(uint));
                    uint firstDirectoryOffset = BitConverter.ToUInt32(firstDirBuffer, 0);

                    RootDirectory = new DatDirectory(firstDirectoryOffset, Convert.ToInt32(this.SectorSize), stream, DatType);
                }

                AllFiles = new Dictionary<uint, DatFile>();
                RootDirectory.AddFilesToList(AllFiles);
            }
        }

        public DatReader GetReaderForFile(uint object_id)
        {
            if (AllFiles.ContainsKey(object_id))
            {
                DatReader dr = new DatReader(FilePath, AllFiles[object_id].FileOffset, AllFiles[object_id].FileSize, SectorSize);
                return dr;
            }
            else
            {
                log.InfoFormat("Unable to find object_id {0} in {1}", object_id.ToString(), Enum.GetName(typeof(DatDatabaseType), DatType));
                return null;
            }
        }
    }
}
