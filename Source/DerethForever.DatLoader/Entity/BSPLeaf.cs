﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity.Enum;

namespace DerethForever.DatLoader.Entity
{
    public class BSPLeaf : BSPNode
    {
        public int LeafIndex { get; set; }
        public int Solid { get; set; }

        public static BSPLeaf ReadLeaf(DatReader datReader, BSPType treeType)
        {
            BSPLeaf obj = new BSPLeaf();
            obj.Type = 0x4C454146; // LEAF
            obj.LeafIndex = datReader.ReadInt32();

            if (treeType == BSPType.Physics)
            {
                obj.Solid = datReader.ReadInt32();
                // Note that if Solid is equal to 0, these values will basically be null. Still read them, but they don't mean anything.
                obj.Sphere = CSphere.Read(datReader);

                uint numPolys = datReader.ReadUInt32();
                for (uint i = 0; i < numPolys; i++)
                    obj.InPolys.Add(datReader.ReadUInt16());
            }

            return obj;
        }
    }
}
