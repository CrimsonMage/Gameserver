﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.DatLoader.Entity
{
    public class SkyObject
    {
        public float BeginTime { get; set; }
        public float EndTime { get; set; }
        public float BeginAngle { get; set; }
        public float EndAngle { get; set; }
        public float TexVelocityX { get; set; }
        public float TexVelocityY { get; set; }
        public float TexVelocityZ { get; set; } = 0;
        public uint DefaultGFXObjectId { get; set; }
        public uint DefaultPESObjectId { get; set; }
        public uint Properties { get; set; }
        
        public static SkyObject Read(DatReader datReader)
        {
            SkyObject obj = new SkyObject();
            obj.BeginTime = datReader.ReadSingle();
            obj.EndTime = datReader.ReadSingle();
            obj.BeginAngle = datReader.ReadSingle();
            obj.EndAngle = datReader.ReadSingle();
            obj.TexVelocityX = datReader.ReadSingle();
            obj.TexVelocityY = datReader.ReadSingle();
            obj.DefaultGFXObjectId = datReader.ReadUInt32();
            obj.DefaultPESObjectId = datReader.ReadUInt32();
            obj.Properties = datReader.ReadUInt32();
            return obj;
        }
    }
}
