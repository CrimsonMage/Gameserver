/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Globalization;

using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity.Enum;

namespace DerethForever.DatLoader.Entity
{
    public class SpellBase
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public MagicSchool School { get; set; }
        public uint Icon { get; set; }
        public uint Category { get; set; } // All related levels of the same spell. Same category spells will not stack. (Strength Self I & Strength Self II)
        public uint Bitfield { get; set; }
        public uint BaseMana { get; set; } // Mana Cost
        public float BaseRangeConstant { get; set; }
        public float BaseRangeMod { get; set; }
        public uint Power { get; set; } // Used to determine which spell in the catgory is the strongest.
        public float SpellEconomyMod { get; set; } // A legacy of a bygone era
        public uint FormulaVersion { get; set; }
        public uint ComponentLoss { get; set; } // Burn rate
        public SpellType MetaSpellType { get; set; }
        public uint MetaSpellId { get; set; } // Just the spell id again

        // Only on EnchantmentSpell/FellowshipEnchantmentSpells
        public double Duration { get; set; }
        public float DegradeModifier { get; set; } // Unknown what this does
        public float DegradeLimit { get; set; }  // Unknown what this does

        public double PortalLifetime { get; set; } // Only for PortalSummon_SpellType

        public List<uint> Formula { get; set; } // UInt Values correspond to the SpellComponentsTable
        public List<uint> FociFormula { get; set; } // This is the formula if a player has the corresponding foci equipped
        public uint CasterEffect { get; set; }  // effect that playes on the caster of the casted spell (e.g. for buffs, protects, etc)
        public uint TargetEffect { get; set; } // effect that playes on the target of the casted spell (e.g. for debuffs, vulns, etc)
        public uint FizzleEffect { get; set; } // is always zero. All spells have the same fizzle effect.
        public double RecoveryInterval { get; set; } // is always zero
        public float RecoveryAmount { get; set; } // is always zero
        public uint DisplayOrder { get; set; } // for soring in the spell list in the client UI
        public uint NonComponentTargetType { get; set; } // Unknown what this does
        public uint ManaMod { get; set; } // Additional mana cost per target (e.g. "Incantation of Acid Bane" Mana Cost = 80 + 14 per target)

        // These are calculated for DerethForever. They are not in the client_portal.dat
        public string SpellWords { get; set; }
        public List<uint> WindUpMotions { get; set; } // The motions for when a player windws up to cast the spell. This is a List since there can be multiple, e.g. spells with 2xDiamond Scarab
        public float WindUpTime { get; set; } // The length of time the wind-up motions last (Could this be more accurately read from MotionTable/Animations?)
        public uint GestureMotion { get; set; } // The motion when actually casting the spell (occurs after wind-up) if successful

        public void SetSpellOptions()
        {
            WindUpMotions = new List<uint>();
            string spellWords = "";

            SpellComponentsTable c = SpellComponentsTable.ReadFromDat();
            foreach (uint comp in Formula)
            {
                if (!c.SpellComponents.ContainsKey(comp))
                    continue;

                if (c.SpellComponents[comp].Category == 0) // Scarab
                {
                    WindUpMotions.Add(c.SpellComponents[comp].Gesture);
                    WindUpTime += c.SpellComponents[comp].Time;
                }
                else if (c.SpellComponents[comp].Category == 4) // Talisman
                    GestureMotion = c.SpellComponents[comp].Gesture;

                // Check Spell Words
                if (c.SpellComponents[comp].Text == "")
                    continue;

                if (spellWords == "")
                    spellWords = c.SpellComponents[comp].Text + " ";
                else
                    spellWords += c.SpellComponents[comp].Text;
            }

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            SpellWords = textInfo.ToTitleCase(spellWords);
        }
    }
}
