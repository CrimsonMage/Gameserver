﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;

namespace DerethForever.DatLoader.Entity
{
    public class ObjectDesc
    {
        public uint ObjId { get; set; }
        public Position BaseLoc { get; set; }
        public float Freq { get; set; }
        public float DisplaceX { get; set; }
        public float DisplaceY { get; set; }
        public float MinScale { get; set; }
        public float MaxScale { get; set; }
        public float MaxRotation { get; set; }
        public float MinSlope { get; set; }
        public float MaxSlope { get; set; }
        public uint Align { get; set; }
        public uint Orient { get; set; }
        public uint WeenieObj { get; set; }

        public static ObjectDesc Read(DatReader datReader)
        {
            ObjectDesc obj = new ObjectDesc();

            obj.ObjId = datReader.ReadUInt32();
            obj.BaseLoc = PositionExtensions.ReadPosition(datReader);
            obj.Freq = datReader.ReadSingle();
            obj.DisplaceX = datReader.ReadSingle();
            obj.DisplaceY = datReader.ReadSingle();
            obj.MinScale = datReader.ReadSingle();
            obj.MaxScale = datReader.ReadSingle();
            obj.MaxRotation = datReader.ReadSingle();
            obj.MinSlope = datReader.ReadSingle();
            obj.MaxSlope = datReader.ReadSingle();
            obj.Align = datReader.ReadUInt32();
            obj.Orient = datReader.ReadUInt32();
            obj.WeenieObj = datReader.ReadUInt32();

            return obj;
        }
    }
}
