﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.Entity.Enum;

namespace DerethForever.DatLoader.Entity
{
    public class BSPPortal : BSPNode
    {
        public List<PortalPoly> InPortals { get; set; } = new List<PortalPoly>();

        public static BSPPortal ReadPortal(DatReader datReader, BSPType treeType)
        {
            BSPPortal obj = new BSPPortal();
            obj.Type = 0x504F5254; // PORT
            obj.SplittingPlane = Plane.Read(datReader);
            obj.PosNode = BSPNode.Read(datReader, treeType);
            obj.NegNode = BSPNode.Read(datReader, treeType);

            if (treeType == BSPType.Drawing)
            {
                obj.Sphere = CSphere.Read(datReader);

                uint numPolys = datReader.ReadUInt32();
                uint numPortals = datReader.ReadUInt32();

                for (uint i = 0; i < numPolys; i++)
                    obj.InPolys.Add(datReader.ReadUInt16());

                for (uint i = 0; i < numPortals; i++)
                    obj.InPortals.Add(PortalPoly.Read(datReader));
            }

            return obj;
        }
    }
}