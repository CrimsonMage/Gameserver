﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class HeritageGroupCG
    {
        public string Name { get; set; }
        public uint IconImage { get; set; }
        public uint SetupID { get; set; } // Basic character model
        public uint EnvironmentSetupID { get; set; } // This is the background environment during Character Creation
        public uint AttributeCredits { get; set; }
        public uint SkillCredits { get; set; }
        public List<int> PrimaryStartAreaList { get; set; } = new List<int>();
        public List<int> SecondaryStartAreaList { get; set; } = new List<int>();
        public List<SkillCG> SkillList { get; set; } = new List<SkillCG>();
        public List<TemplateCG> TemplateList { get; set; } = new List<TemplateCG>();
        public Dictionary<int, SexCG> SexList { get; set; } = new Dictionary<int, SexCG>();
    }
}
