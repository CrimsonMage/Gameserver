﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    /// <summary>
    /// This is actually different than just a "VertexArray" class.
    /// </summary>
    public class CVertexArray
    {
        public int VertexType { get; set; }
        public Dictionary<short, SWVertex> Vertices { get; set; } = new Dictionary<short, SWVertex>();

        public static CVertexArray Read(DatReader datReader)
        {
            CVertexArray obj = new CVertexArray();

            obj.VertexType = datReader.ReadInt32();

            uint num_vertices = datReader.ReadUInt32();

            if (obj.VertexType == 1)
            {
                for (uint i = 0; i < num_vertices; i++)
                {
                    short vert_id = datReader.ReadInt16();
                    obj.Vertices.Add(vert_id, SWVertex.Read(datReader));
                }
            }

            return obj;
        }
    }
}
