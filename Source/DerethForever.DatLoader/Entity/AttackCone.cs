﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.DatLoader.Entity
{
    public class AttackCone
    {
        public uint PartIndex { get; set; }
        
        // these Left and Right are technically Vec2D types
        public float LeftX { get; private set; }
        public float LeftY { get; private set; }
        public float RightX { get; private set; }
        public float RightY { get; private set; }

        public float Radius { get; private set; }
        public float Height { get; private set; }

        public static AttackCone Read(DatReader datReader)
        {
            AttackCone a = new AttackCone();

            a.PartIndex = datReader.ReadUInt32();

            a.LeftX = datReader.ReadSingle();
            a.LeftY = datReader.ReadSingle();

            a.RightX = datReader.ReadSingle();
            a.RightY = datReader.ReadSingle();

            a.Radius = datReader.ReadSingle();
            a.Height = datReader.ReadSingle();
            return a;
        }
    }
}
