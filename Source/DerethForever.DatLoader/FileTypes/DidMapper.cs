/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// These are client_portal.dat files starting with 0x25.
    /// This contains a list of DataIDs (a file in the .dat)
    /// </summary>
    public class DidMapper
    {
        public uint DidMapperId; // The "file" id
        public short DidUnknown, EnumUnknown, Did2Unknown, Enum2Unknown; // Unsure what these are
        public Dictionary<uint, uint> Did = new Dictionary<uint, uint>();
        public Dictionary<uint, string> Enum = new Dictionary<uint, string>();

        // It feels like these are server specific, but that is just a guess - IronGolem
        public Dictionary<int, uint> Did2 = new Dictionary<int, uint>();
        public Dictionary<int, string> Enum2 = new Dictionary<int, string>();
        public uint Unknown_2; // Unsure what this is

        // This is the mapped dictionary, compiled from the Did and Enum Dictionaries
        public Dictionary<uint, string> Enums = new Dictionary<uint, string>();

        public static DidMapper ReadFromDat(uint fileId)
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(fileId))
            {
                return (DidMapper)DatManager.PortalDat.FileCache[fileId];
            }
            else
            {
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(fileId);
                DidMapper obj = new DidMapper();

                obj.DidMapperId = datReader.ReadUInt32();

                obj.DidUnknown = datReader.ReadPackedByte();
                short numberDid = datReader.ReadPackedByte();
                for (int i = 0; i < numberDid; i++)
                {
                    uint key = datReader.ReadUInt32();
                    uint val = datReader.ReadUInt32();
                    obj.Did.Add(key, val);
                }

                obj.EnumUnknown = datReader.ReadPackedByte();
                short numberEnums = datReader.ReadPackedByte();
                for (int i = 0; i < numberEnums; i++)
                {
                    uint key = datReader.ReadUInt32();
                    int strLen = datReader.ReadByte();
                    string val = datReader.ReadString(strLen);
                    obj.Enum.Add(key, val);
                }

                obj.Did2Unknown = datReader.ReadPackedByte();
                short numberDid2 = datReader.ReadPackedByte();
                for (int i = 0; i < numberDid2; i++)
                {
                    int key = datReader.ReadInt32();
                    uint val = datReader.ReadUInt32();
                    obj.Did2.Add(key, val);
                }

                obj.Enum2Unknown = datReader.ReadPackedByte();
                short numberEnum2 = datReader.ReadPackedByte();
                for (int i = 0; i < numberEnum2; i++)
                {
                    int key = datReader.ReadInt32();
                    int strLen = datReader.ReadByte();
                    string val = datReader.ReadString(strLen);
                    obj.Enum2.Add(key, val);
                }

                // Map the Did to the Enum for easier, more practical use
                // Some have multiple mappings, so something to keep in mind!
                foreach (KeyValuePair<uint, uint> entry in obj.Did)
                {
                    if (!obj.Enums.ContainsKey(entry.Value))
                        obj.Enums.Add(entry.Value, obj.Enum[entry.Key]);
                }
                foreach (KeyValuePair<int, uint> entry in obj.Did2)
                {
                    if (!obj.Enums.ContainsKey(entry.Value))
                        obj.Enums.Add(entry.Value, obj.Enum2[entry.Key]);
                }

                // Store this object in the FileCache
                DatManager.PortalDat.FileCache[fileId] = obj;

                return obj;
            }
        }
    }
}
