﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.Linq;

using DerethForever.DatLoader.Entity;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// Class for reading the File 0x0E00000D from the portal.dat.
    /// Thanks alot Widgeon of Leafcull for his ACDataTools which helped understanding this structure.
    /// And thanks alot to Pea as well whos hard work surely helped in the creation of those Tools too.
    /// </summary>
    public static class GeneratorTable
    {
        public static Generator ReadFromDat()
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(0x0E00000D))
            {
                return (Generator)DatManager.PortalDat.FileCache[0x0E00000D];
            }
            else
            {
                Generator gen = new Generator();

                // Create the datReader for the proper file
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(0x0E00000D);

                gen.Id = datReader.ReadInt32();
                gen.Name = "0E00000D";
                gen.Count = 2;
                datReader.Offset = 16;

                Generator playDay = new Generator();
                Generator weenieObjects = new Generator();
                gen.Items.Add(playDay.GetNextGenerator(datReader)); // Parse and add PlayDay hierarchy
                gen.Items.Add(weenieObjects.GetNextGenerator(datReader)); // Parse and add WeenieObjects hierarchy

                // Store this object in the FileCache
                DatManager.PortalDat.FileCache[0x0E00000D] = gen;
                return gen;
            }
        }

        public static IEnumerable<Generator> ReadItems(this Generator root)
        {
            var nodes = new Stack<Generator>(new[] { root });
            while (nodes.Any())
            {
                Generator node = nodes.Pop();
                yield return node;
                foreach (var n in node.Items) nodes.Push(n);
            }
        }
    }
}
