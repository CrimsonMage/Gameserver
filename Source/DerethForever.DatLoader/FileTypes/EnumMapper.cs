/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// These are client_portal.dat files starting with 0x22. 
    /// </summary>
    public class EnumMapper
    {
        public uint EnumMapperId; // The "file" id
        public uint BaseEmp; // Unsure what this is
        public byte Unknown; // Unsure what this is
        public Dictionary<int, string> Enums = new Dictionary<int, string>();

        public static EnumMapper ReadFromDat(uint fileId)
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(fileId))
            {
                return (EnumMapper)DatManager.PortalDat.FileCache[fileId];
            }
            else
            {
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(fileId);
                EnumMapper obj = new EnumMapper();

                obj.EnumMapperId = datReader.ReadUInt32();
                obj.BaseEmp = datReader.ReadUInt32();
                obj.Unknown = datReader.ReadByte();

                short numberEnums = datReader.ReadPackedByte();
                for (int i = 0; i < numberEnums; i++)
                {
                    int key = datReader.ReadInt32();
                    int strLen = datReader.ReadByte();
                    string val = datReader.ReadString(strLen);
                    obj.Enums.Add(key, val);
                }

                // Store this object in the FileCache
                DatManager.PortalDat.FileCache[fileId] = obj;

                return obj;
            }
        }
    }
}
