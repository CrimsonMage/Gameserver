/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.IO;
using System.Drawing.Imaging;

namespace DerethForever.DatLoader.FileTypes
{
    /// <summary>
    /// These are all of the textures an related assets (light maps, etc) in the game. In the client_portal.dat, they are in the range of 0x06000000-0x07FFFFFF (though none are actually in the 0x07 range)
    /// </summary>
    public class RenderSurface
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public uint Id;
        public uint Unknown;
        public int Width;
        public int Height;
        public ImagePixelFormat Format;
        public List<int> Colors = new List<int>();

        public static RenderSurface ReadFromDat(uint fileId)
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(fileId))
            {
                return (RenderSurface)DatManager.PortalDat.FileCache[fileId];
            }
            else
            {
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(fileId);

                if (datReader == null)
                {
                    log.Error("File " + fileId.ToString("X8") + " returned null from the DatManager.");
                    return null;
                }

                RenderSurface obj = new RenderSurface();

                obj.Id = datReader.ReadUInt32();
                obj.Unknown = datReader.ReadUInt32();
                obj.Width = datReader.ReadInt32();
                obj.Height = datReader.ReadInt32();
                obj.Format = (ImagePixelFormat)datReader.ReadUInt32();

                obj.Colors = GetColorData(datReader, obj);

                if (datReader.Buffer.Length > datReader.Offset)
                    log.Error("File " + obj.Id.ToString("X8") + " was not completely read. This may cause issues.");

                // Store this object in the FileCache
                DatManager.PortalDat.FileCache[fileId] = obj;

                return obj;
            }
        }

        private static List<int> GetColorData(DatReader datReader, RenderSurface obj)
        {
            List<int> colors = new List<int>();
            uint byteLength = datReader.ReadUInt32();

            if (byteLength > 0)
            {
                switch (obj.Format)
                {
                    case ImagePixelFormat.PFID_R8G8B8: // RGB format. 3 bytes per pixel
                        for (uint i = 0; i < obj.Height; i++)
                            for (uint j = 0; j < obj.Width; j++)
                            {
                                byte b = datReader.ReadByte();
                                byte g = datReader.ReadByte();
                                byte r = datReader.ReadByte();
                                int color = (r << 16) + (g << 8) + b;
                                colors.Add(color);
                            }
                                
                        break;
                    case ImagePixelFormat.PFID_A8R8G8B8: // ARGB format... Most UI textures fall into this category
                        for (uint i = 0; i < obj.Height; i++)
                            for (uint j = 0; j < obj.Width; j++)
                                colors.Add(datReader.ReadInt32());
                        break;
                    default:
                        log.Error("Unhandled ImagePixelFormat " + obj.Format.ToString() + " in RenderSurface " + obj.Id.ToString("X8"));
                        break;
                }
            }
            return colors;
        }

        public byte[] GetByteArray()
        {
            var imageArray = new byte[] { };
            if (Colors.Count > 0)
            {
                var image = GetBitmap();
                using (MemoryStream ms = new MemoryStream())
                {
                    // Currently using PNG for use with mordern browsers.
                    image.Save(ms, ImageFormat.Png);
                    imageArray = ms.ToArray();
                }
            }
            return imageArray;
        }

        public Bitmap GetBitmap()
        {
            Bitmap image = new Bitmap(Width, Height);
            switch (this.Format)
            {
                case ImagePixelFormat.PFID_R8G8B8:
                    for (int i = 0; i < Height; i++)
                        for (int j = 0; j < Width; j++)
                        {
                            int idx = (i * Height) + j;
                            int r = (Colors[idx] & 0xFF0000) >> 16;
                            int g = (Colors[idx] & 0xFF00) >> 8;
                            int b = Colors[idx] & 0xFF;
                            image.SetPixel(j, i, Color.FromArgb(r, g, b));
                        }
                    break;
                case ImagePixelFormat.PFID_A8R8G8B8:
                    for (int i = 0; i < Height; i++)
                        for (int j = 0; j < Width; j++)
                        {
                            int idx = (i * Height) + j;
                            int a = (int)((Colors[idx] & 0xFF000000) >> 24);
                            int r = (Colors[idx] & 0xFF0000) >> 16;
                            int g = (Colors[idx] & 0xFF00) >> 8;
                            int b = Colors[idx] & 0xFF;
                            image.SetPixel(j, i, Color.FromArgb(a, r, g, b));
                        }
                    break;
            }
            return image;
        }

        public static RenderSurface FromBitmap(Bitmap surface, uint surfaceId)
        {
            RenderSurface obj = new RenderSurface();

            obj.Id = surfaceId;
            obj.Unknown = 6;
            obj.Width = surface.Width;
            obj.Height = surface.Height;
            obj.Format = ImagePixelFormat.PFID_A8R8G8B8;
            obj.Colors = GetBitmapColors(surface);

            return obj;
        }

        private static List<int> GetBitmapColors(Bitmap bmp)
        {
            List<int> Colors = new List<int>();

            int x, y, rgb, val;
            for (y = 0; y < bmp.Height; y++)
            {
                for (x = 0; x < bmp.Width; x++)
                {
                    Color c = bmp.GetPixel(x, y);
                    rgb = c.ToArgb();
                    if ((uint)rgb == 0xff000000) // if black
                    {
                        val = 0;
                    }
                    else val = rgb;
                    Colors.Add(val);
                }
            }
            return Colors;
        }
    }
}
