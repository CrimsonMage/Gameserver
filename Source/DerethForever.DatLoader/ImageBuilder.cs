/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using DerethForever.Database;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace DerethForever.DatLoader
{
    public class ImageBuilder
    {
        /// <summary>
        /// Returns a RenderSurface for an ItemType.
        /// </summary>
        public static RenderSurface GetBackgroundPngIcon(ItemType itemType)
        {
            // Lookup the background ID from the DIDMapper in the Dat file
            // Magic number 620757000 : 0x25000008 : UIBackgrounds
            var itemMap = DidMapper.ReadFromDat(0x25000008);
            uint backgroundId = 100667860;

            foreach (var enumMap in itemMap.Enum)
            {
                if (enumMap.Value == itemType.ToString())
                {
                    var didKey = enumMap.Key;
                    foreach (var didMap in itemMap.Did)
                    {
                        if (didMap.Key == didKey)
                        {
                            backgroundId = didMap.Value;
                            break;
                        }
                    }
                }
            }
            return RenderSurface.ReadFromDat(backgroundId);
        }

        /// <summary>
        /// Returns a RenderSurface for UiEffects.
        /// </summary>
        public static RenderSurface GetUIEffect(UiEffects uiEffects)
        {
            // Lookup the UIEffect ID from the DIDMapper in the Dat file
            // Ref: http://ac.yotesfan.com/25-DidMapper/?id=25000009
            // Magic number 620757001 : 0x25000009 : UIBackgrounds
            var itemMap = DidMapper.ReadFromDat(0x25000009);
            uint effectId = 0;

            // Compare names of the effect with the did mapper
            foreach(var enumName in itemMap.Enum)
            {
                if (enumName.Value.ToUpperInvariant() == uiEffects.ToString().ToUpperInvariant())
                {
                    var didKey = enumName.Key;
                    effectId = itemMap.Did[didKey];
                    break;
                }
            }

            if (effectId == 0)
                return null;

            return RenderSurface.ReadFromDat(effectId);
        }

        /// <summary>
        /// Returns RenderSurface for a weenieclass.
        /// </summary>
        public static RenderSurface GetFullyLayeredPngIcon(uint weenieClassId)
        {
            List<Bitmap> imageBuffer = new List<Bitmap>();
            var weenie = DatabaseManager.World.GetWeenie(weenieClassId);

            // Start with the background:
            var background = GetBackgroundPngIcon((ItemType)weenie.ItemType);
            imageBuffer.Add(background.GetBitmap());

            // This may not exist, member value is nullable.
            if (weenie.IconUnderlayDID != null)
            {
                // Add Underlay:
                var underlay = RenderSurface.ReadFromDat((uint)weenie.IconUnderlayDID);
                imageBuffer.Add(underlay.GetBitmap());
            }

            // Add Icon:
            var icon = GetPrimaryPngIcon((uint)weenie.IconDID);

            if (icon != null)
            {
                var effect = GetUIEffect(weenie.UiEffects != null ? (UiEffects)weenie.UiEffects : UiEffects.Default);
                if (effect != null)
                {
                    imageBuffer.Add(BlendEffect(icon.GetBitmap(), effect.GetBitmap()));
                }
                else
                    imageBuffer.Add(icon.GetBitmap());
            }

            if (weenie.IconOverlayDID != null)
            // This may not exist, member value is nullable.
            {
                // Add Overlay:
                var overlay = RenderSurface.ReadFromDat((uint)weenie.IconOverlayDID);
                if (overlay != null)
                    imageBuffer.Add(overlay.GetBitmap());
            }

            // Add Spell Indicator?
            // if (weenie.IconOverlaySecondary != null)
            // {
            //    // Add OverlaySecondary:
            //    var overlaySecondary = RenderSurface.ReadFromDat((uint)weenie.IconOverlaySecondary);
            //    imageBuffer.Add(overlay.GetBitmap());
            // }

            // Combine
            var finalImage = CombineImageList(imageBuffer);
            // Convert to bitmap before returning byte[]
            return RenderSurface.FromBitmap(finalImage, weenieClassId);
        }

        /// <summary>
        /// Returns RenderSurface for a bare Icon.
        /// </summary>
        public static RenderSurface GetPrimaryPngIcon(uint iconId)
        {
            // Convert RenderFile to bitmap with iconId
            var image = RenderSurface.ReadFromDat(iconId);
            return image;
        }

        /// <summary>
        /// Combines images.
        /// </summary>
        /// <returns>Bitmap containing all layers</returns>
        public static Bitmap CombineImageList(List<Bitmap> imageBuffer)
        {
            Bitmap iconImage = new Bitmap(32, 32, PixelFormat.Format32bppRgb);
            //iconImage.MakeTransparent();
            using (Graphics g = Graphics.FromImage(iconImage))
            {
                Brush effectBrush = new SolidBrush(Color.Black);
                // Allow composting ontop?
                g.CompositingMode = CompositingMode.SourceOver;       
                foreach (Bitmap image in imageBuffer)
                {
                    //image.MakeTransparent(Color.Transparent);
                    // add the bitmap
                    g.DrawImage(image, 0, 0);
                }
                return iconImage;
            }
        }

        public static Bitmap BlendEffect(Bitmap baseImage, Bitmap effect)
        {
            Bitmap iconImage = new Bitmap(32, 32, PixelFormat.Format32bppPArgb);

            iconImage.MakeTransparent();

            for (int y = 0; y < baseImage.Height; y++)
            {
                for (int x = 0; x < baseImage.Width; x++)
                {
                    var pixel = baseImage.GetPixel(x, y);

                    if (pixel.A > 0)
                    {
                        iconImage.SetPixel(x, y, pixel);
                    }

                    if (pixel.A == 255 && pixel.R == 255 && pixel.G == 255 && pixel.B == 255)
                    {
                        iconImage.SetPixel(x, y, effect.GetPixel(x, y));                            
                    }
                }
            }

            baseImage.Dispose();
            effect.Dispose();
            
            return iconImage;
        }
    }
}
