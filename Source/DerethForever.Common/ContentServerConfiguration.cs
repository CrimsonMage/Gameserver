/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;

using System.ComponentModel;

namespace DerethForever.Common
{
    public class ContentServerConfiguration
    {
        /// <summary>
        /// Token or key too authorizae system access.
        /// </summary>
        [DefaultValue("")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string RemoteSyncToken { get; set; }

        /// <summary>
        /// Branch to pull release info and archive from. Should be master unless you're testing a specific branch.
        /// </summary>
        [DefaultValue("master")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string GitlabWorldReleaseBranch { get; set; }

        /// <summary>
        /// Api URL to current Database root Folder.
        /// </summary>
        [DefaultValue("https://gitlab.com/api/v4/projects/5041735/repository/tree?path=Database")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string GitlabDatabaseUrl { get; set; }

        /// <summary>
        /// Url to download the latest release archive of the Game World.
        /// </summary>
        [DefaultValue("https://gitlab.com/api/v4/projects/5042419/repository/files/current_release.json")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string GitlabWorldArchivePointer { get; set; }

        /// <summary>
        /// Url to download the latest release archive of DerethForever.
        /// </summary>
        [DefaultValue("https://gitlab.com/api/v4/projects/5042419/repository/files/")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string GitlabWorldArchiveBaseUrl { get; set; }

        /// <summary>
        /// Local hard disk path that will be used to save the database files and downloaded json content.
        /// </summary>
        [DefaultValue("Content\\")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string LocalContentPath { get; set; }

        /// <summary>
        /// Local hard disk path that will be used to backup the database files.
        /// </summary>
        [DefaultValue("Backup\\")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string LocalBackupPath { get; set; }

        /// <summary>
        /// Setting that determines if the server will keep data backups when redeploying too a database.
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public bool KeepDataBackups { get; set; }

        /// <summary>
        /// Setting that determines if the server will keep data backups when redeploying too a database.
        /// </summary>
        [DefaultValue("http://www.derethforever.com/Resources/CurrentWorldRelease/NotImplementedYet")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public string CurrentReleaseInfo { get; set; }
    }
}
