﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity.Enum;

using MySql.Data.MySqlClient;

namespace DerethForever.Entity
{
    public class SearchWeenieProperty
    {
        public ObjectPropertyType PropertyType { get; set; }

        public uint PropertyId { get; set; }

        public string PropertyValue { get; set; }

        public MySqlDbType GetMySqlDbType()
        {
            switch (PropertyType)
            {
                case ObjectPropertyType.PropertyBool:
                    return MySqlDbType.Bit;
                case ObjectPropertyType.PropertyDataId:
                case ObjectPropertyType.PropertyInstanceId:
                case ObjectPropertyType.PropertyInt:
                    return MySqlDbType.UInt32;
                case ObjectPropertyType.PropertyInt64:
                    return MySqlDbType.UInt64;
                case ObjectPropertyType.PropertyString:
                    return MySqlDbType.Text;
                case ObjectPropertyType.PropertyDouble:
                    return MySqlDbType.Float;
                default:
                    return MySqlDbType.Text;
            }
        }
    }
}
