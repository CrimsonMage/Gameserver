﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Common;

using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("ace_content_landblock")]
    public class ContentLandblock : ChangeTrackedObject
    {
        public ContentLandblock()
        {
            ContentLandblockGuid = Guid.NewGuid();
            IsDirty = true;
            HasEverBeenSavedToDatabase = false;
        }

        [JsonProperty("contentLandblockGuid")]
        public Guid? ContentLandblockGuid { get; set; }
        
        [JsonIgnore]
        [DbField("contentLandblockGuid", (int)MySqlDbType.Binary, Update = false, IsCriteria = true)]
        public byte[] ContentLandblockGuid_Binder
        {
            get { return ContentLandblockGuid.Value.ToByteArray(); }
            set { ContentLandblockGuid = new Guid(value); }
        }

        /// <summary>
        /// content identifier
        /// </summary>
        [JsonIgnore]
        public Guid ContentGuid { get; set; }

        [JsonIgnore]
        [DbField("contentGuid", (int)MySqlDbType.Binary, Update = false, ListGet = true, ListDelete = true)]
        public byte[] ContentGuid_Binder
        {
            get { return ContentGuid.ToByteArray(); }
            set { ContentGuid = new Guid(value); }
        }

        [JsonIgnore]
        public LandblockId LandblockId { get; set; }

        /// <summary>
        /// this value is functionally immutable.  do not set expecting it to be updated in the database, only ever inserted.
        /// </summary>
        [JsonProperty("landblockId")]
        [DbField("landblockId", (int)MySqlDbType.UInt32, Update = false)]
        public uint LandblockId_Binder
        {
            get { return LandblockId.Raw; }
            set { this.LandblockId = new LandblockId(value); }
        }

        private string _comment;

        [JsonProperty("comment")]
        [DbField("comment", (int)MySqlDbType.Text)]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; IsDirty = true; }
        }
    }
}
