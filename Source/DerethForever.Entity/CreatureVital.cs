/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Entity.Enum;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DerethForever.Entity
{
    public class CreatureVital : ICloneable, ICreatureXpSpendableStat
    {
        private DataObjectPropertiesAttribute2nd _backer;

        // because health/stam/mana values are determined from stats, we need a reference to the WeenieCreatureData
        // so we can calculate.  this could be refactored into a better pattern, but it will do for now.
        private ICreatureStats creature;

        private double lastTick = double.NegativeInfinity;

        [JsonIgnore]
        public double RegenRate { set; get; }

        [JsonProperty("ability")]
        public Ability Ability
        {
            get { return (Ability)_backer.Attribute2ndId; }
            set { _backer.Attribute2ndId = (ushort)value; }
        }

        [JsonProperty("startingValue")]
        public uint Base { get; set; }

        [JsonProperty("ranks")]
        public uint Ranks
        {
            get { return _backer.Attribute2ndRanks; }
            set
            {
                _backer.Attribute2ndRanks = (ushort)value;
                _backer.IsDirty = true;
            }
        }

        /// <summary>
        /// Total Experience Spent on an ability
        /// </summary>
        [JsonProperty("experienceSpent")]
        public uint ExperienceSpent
        {
            get { return _backer.Attribute2ndXpSpent; }
            set
            {
                _backer.Attribute2ndXpSpent = value;
                _backer.IsDirty = true;
            }
        }

        [JsonProperty("baseValue")]
        public uint ValueFromAttributes
        {
            get
            {
                var formula = this.Ability.GetFormula();

                uint derivationTotal = 0;
                uint abilityTotal = 0;

                if (formula != null)
                {
                    // restricted to endurance and self because those are the only 2 used by abilities

                    Ability abilities = formula.Abilities;
                    uint end = (uint)((abilities & Ability.endurance) > 0 ? 1 : 0);
                    uint wil = (uint)((abilities & Ability.self) > 0 ? 1 : 0);

                    derivationTotal += end * this.creature?.Endurance ?? 10;
                    derivationTotal += wil * this.creature?.Self ?? 10;

                    derivationTotal *= formula.AbilityMultiplier;
                    abilityTotal = (uint)Math.Ceiling((double)derivationTotal / (double)formula.Divisor);
                }
                
                return abilityTotal;
            }
            set
            {
            }
        }

        [JsonIgnore]
        public uint UnbuffedValue
        {
            get
            {
                // TODO: buffs?  not sure where they will go
                // TODO: check if this can be replaced with Ability.GetFormula().CalcBase(creature)
                var formula = this.Ability.GetFormula();

                uint derivationTotal = 0;
                uint abilityTotal = 0;

                if (formula != null)
                {
                    // restricted to endurance and self because those are the only 2 used by abilities

                    Ability abilities = formula.Abilities;
                    uint end = (uint)((abilities & Ability.endurance) > 0 ? 1 : 0);
                    uint wil = (uint)((abilities & Ability.self) > 0 ? 1 : 0);

                    derivationTotal += end * this.creature?.Endurance ?? 10;
                    derivationTotal += wil * this.creature?.Self ?? 10;

                    derivationTotal *= formula.AbilityMultiplier;
                    abilityTotal = (uint)Math.Ceiling((double)derivationTotal / (double)formula.Divisor);
                }

                abilityTotal += this.Ranks + this.Base;

                return abilityTotal;
            }
        }

        [JsonIgnore]
        public uint MaxValue { get { return UnbuffedValue; } }

        /// <summary>
        /// only applies to Health/Stam/Mana
        /// </summary>
        [JsonIgnore]
        public uint Current
        {
            get { return _backer.Attribute2ndValue; }
            set
            {
                _backer.Attribute2ndValue = (ushort)value;
                _backer.IsDirty = true;
            }
        }

        [JsonIgnore]
        public double NextTickTime {
            get
            {
                if (lastTick == double.NegativeInfinity)
                {
                    return double.NegativeInfinity;
                }
                return lastTick + RegenRate;
            }
        }

        /// <summary>
        /// Used to determine if health/stamina/mana updates need to be sent periodically
        /// Returns the "last time" the vitals were updated
        /// Takes the vital to update, the lastTime it was updated, and the update rate
        /// </summary>
        public void Tick(double tickTime)
        {
            if (lastTick == double.NegativeInfinity)
            {
                lastTick = tickTime;
                return;
            }

            // This shouldn't happen?  maybe?
            if (tickTime <= lastTick)
            {
                return;
            }

            double timeDiff = tickTime - lastTick;

            uint numTicks = (uint)(timeDiff * RegenRate);

            if (numTicks > 0)
            {
                // lastTime is the time at which the last tick would have happened
                lastTick = lastTick + numTicks / RegenRate;

                // Now, update our value
                Current = System.Math.Min(MaxValue, Current + numTicks);

                // Reset last tick, so when we resume we start ticking properly
                if (Current == MaxValue)
                {
                    lastTick = double.NegativeInfinity;
                }
            }
        }

        /// <summary>
        /// DO NOT USE - this constructor only exists to allow Weenie objects to be edited
        /// via the API
        /// </summary>
        public CreatureVital()
        {
            this._backer = new DataObjectPropertiesAttribute2nd();
        }

        public CreatureVital(ICreatureStats creature, Ability ability, double regenRate)
        {
            this._backer = new DataObjectPropertiesAttribute2nd();
            this.creature = creature;
            this.RegenRate = regenRate;
            Ability = ability;
        }

        public CreatureVital(ICreatureStats creature, DataObjectPropertiesAttribute2nd props) 
        {
            this._backer = props;
            this.creature = creature;
            this.RegenRate = Ability.GetRegenRate();
        }

        public DataObjectPropertiesAttribute2nd GetVital()
        {
            return _backer;
        }

        public void ClearDirtyFlags()
        {
            _backer.IsDirty = false;
            _backer.HasEverBeenSavedToDatabase = true;
        }

        public void SetDirtyFlags()
        {
            _backer.IsDirty = true;
            _backer.HasEverBeenSavedToDatabase = false;
        }

        public object Clone()
        {
            return new CreatureVital(creature, (DataObjectPropertiesAttribute2nd)_backer.Clone());
        }
    }
}
