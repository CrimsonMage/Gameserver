/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using DerethForever.Entity.Enum;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    public class GeneratorTable
    {
        [JsonProperty("probability")]
        public double Probability { get; set; }

        [JsonProperty("weenieClassId")]
        public uint WeenieClassId { get; set; }

        [JsonProperty("delay")]
        public float Delay { get; set; }

        [JsonProperty("initCreate")]
        public uint InitCreate { get; set; }

        [JsonProperty("maxNumber")]
        public uint MaxNumber { get; set; }

        [JsonProperty("whenCreate")]
        public RegenerationType WhenCreate { get; set; }

        [JsonProperty("whereCreate")]
        public RegenerationLocation WhereCreate { get; set; }

        [JsonProperty("stackSize")]
        public int StackSize { get; set; }

        [JsonProperty("paletteId")]
        public uint PaletteId { get; set; }

        [JsonProperty("slot")]
        public uint Slot { get; set; }

        [JsonProperty("objectCell")]
        public uint ObjectCell { get; set; }

        [JsonProperty("shade")]
        public float Shade { get; set; }

        public Frame Frame { get; set; } = new Frame();
    }
}
