/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using DerethForever.Entity.Enum;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Entity
{
    [DbTable("weenie_body_armor")]
    public class BodyPart
    {
        [JsonIgnore]
        [DbField("weenieClassId", (int)MySqlDbType.UInt32, IsCriteria = true, Update = false, ListDelete = true, ListGet = true)]
        public uint WeenieClassId { get; set; }

        [JsonIgnore]
        [DbField("bodyPartType", (int)MySqlDbType.Int16, IsCriteria = true, Update = false)]
        public int BodyPartType_Binder { get; set; }

        [JsonProperty("bodyPartType")]
        public BodyPartType BodyPartType
        {
            get { return (BodyPartType)BodyPartType_Binder; }
            set { BodyPartType_Binder = (int)value; }
        }

        [JsonProperty("targetingData")]
        public BodyPartSelectionData TargetingData { get; } = new BodyPartSelectionData();

        [JsonProperty("armorValues")]
        public TypedArmorRatings ArmorValues { get; } = new TypedArmorRatings();

        [JsonProperty("bodyHeight")]
        [DbField("bodyHeight", (int)MySqlDbType.Int32)]
        public int BodyHeight { get; set; }

        [JsonProperty("damage")]
        [DbField("damage", (int)MySqlDbType.Int32)]
        public int Damage { get; set; }

        [JsonProperty("damageType")]
        [DbField("damageType", (int)MySqlDbType.Int32)]
        public int DamageType { get; set; }

        [JsonProperty("damageVariance")]
        [DbField("damageVariance", (int)MySqlDbType.Double)]
        public double DamageVariance { get; set; }

        // below this point is all the nested properties that we need to expose here for the ORM utilities to work

        [JsonIgnore]
        [DbField("bpsdHighLeftBack", (int)MySqlDbType.Double)]
        public double HighLeftBack_Binder
        {
            get { return TargetingData.HighLeftBack; }
            set { TargetingData.HighLeftBack = value; }
        }

        [JsonIgnore]
        [DbField("bpsdHighLeftFront", (int)MySqlDbType.Double)]
        public double HighLeftFront_Binder
        {
            get { return TargetingData.HighLeftFront; }
            set { TargetingData.HighLeftFront = value; }
        }

        [JsonIgnore]
        [DbField("bpsdHighRightBack", (int)MySqlDbType.Double)]
        public double HighRightBack_Binder
        {
            get { return TargetingData.HighRightBack; }
            set { TargetingData.HighRightBack = value; }
        }

        [JsonIgnore]
        [DbField("bpsdHighRightFront", (int)MySqlDbType.Double)]
        public double HighRightFront_Binder
        {
            get { return TargetingData.HighRightFront; }
            set { TargetingData.HighRightFront = value; }
        }

        [JsonIgnore]
        [DbField("bpsdLowLeftBack", (int)MySqlDbType.Double)]
        public double LowLeftBack_Binder
        {
            get { return TargetingData.LowLeftBack; }
            set { TargetingData.LowLeftBack = value; }
        }

        [JsonIgnore]
        [DbField("bpsdLowLeftFront", (int)MySqlDbType.Double)]
        public double LowLeftFront_Binder
        {
            get { return TargetingData.LowLeftFront; }
            set { TargetingData.LowLeftFront = value; }
        }

        [JsonIgnore]
        [DbField("bpsdLowRightBack", (int)MySqlDbType.Double)]
        public double LowRightBack_Binder
        {
            get { return TargetingData.LowRightBack; }
            set { TargetingData.LowRightBack = value; }
        }

        [JsonIgnore]
        [DbField("bpsdLowRightFront", (int)MySqlDbType.Double)]
        public double LowRightFront_Binder
        {
            get { return TargetingData.LowRightFront; }
            set { TargetingData.LowRightFront = value; }
        }

        [JsonIgnore]
        [DbField("bpsdMidLeftBack", (int)MySqlDbType.Double)]
        public double MidLeftBack_Binder
        {
            get { return TargetingData.MidLeftBack; }
            set { TargetingData.MidLeftBack = value; }
        }

        [JsonIgnore]
        [DbField("bpsdMidLeftFront", (int)MySqlDbType.Double)]
        public double MidLeftFront_Binder
        {
            get { return TargetingData.MidLeftFront; }
            set { TargetingData.MidLeftFront = value; }
        }

        [JsonIgnore]
        [DbField("bpsdMidRightBack", (int)MySqlDbType.Double)]
        public double MidRightBack_Binder
        {
            get { return TargetingData.MidRightBack; }
            set { TargetingData.MidRightBack = value; }
        }

        [JsonIgnore]
        [DbField("bpsdMidRightFront", (int)MySqlDbType.Double)]
        public double MidRightFront_Binder
        {
            get { return TargetingData.MidRightFront; }
            set { TargetingData.MidRightFront = value; }
        }

        [JsonIgnore]
        [DbField("armorVsAcid", (int)MySqlDbType.Int32)]
        public int Acid_Binder
        {
            get { return ArmorValues.Acid; }
            set { ArmorValues.Acid = value; }
        }

        [JsonIgnore]
        [DbField("armorVsBludgeon", (int)MySqlDbType.Int32)]
        public int Bludgeon_Binder
        {
            get { return ArmorValues.Bludgeon; }
            set { ArmorValues.Bludgeon = value; }
        }

        [JsonIgnore]
        [DbField("armorVsCold", (int)MySqlDbType.Int32)]
        public int Cold_Binder
        {
            get { return ArmorValues.Cold; }
            set { ArmorValues.Cold = value; }
        }

        [JsonIgnore]
        [DbField("armorVsElectric", (int)MySqlDbType.Int32)]
        public int Electric_Binder
        {
            get { return ArmorValues.Electric; }
            set { ArmorValues.Electric = value; }
        }

        [JsonIgnore]
        [DbField("armorVsFire", (int)MySqlDbType.Int32)]
        public int Fire_Binder
        {
            get { return ArmorValues.Fire; }
            set { ArmorValues.Fire = value; }
        }

        [JsonIgnore]
        [DbField("armorVsNether", (int)MySqlDbType.Int32)]
        public int Nether_Binder
        {
            get { return ArmorValues.Nether; }
            set { ArmorValues.Nether = value; }
        }

        [JsonIgnore]
        [DbField("armorVsPierce", (int)MySqlDbType.Int32)]
        public int Pierce_Binder
        {
            get { return ArmorValues.Pierce; }
            set { ArmorValues.Pierce = value; }
        }

        [JsonIgnore]
        [DbField("armorVsSlash", (int)MySqlDbType.Int32)]
        public int Slash_Binder
        {
            get { return ArmorValues.Slash; }
            set { ArmorValues.Slash = value; }
        }

        [JsonIgnore]
        [DbField("baseArmor", (int)MySqlDbType.Int32)]
        public int Base_Binder
        {
            get { return ArmorValues.Base; }
            set { ArmorValues.Base = value; }
        }
    }
}
