/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;

using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("vw_teleport_location")]
    public class TeleportLocation
    {
        [JsonProperty("name")]
        [DbField("name", (int)MySqlDbType.Text)]
        public string Location { get; set; }

        [JsonProperty("landblock")]
        [DbField("landblock", (int)MySqlDbType.UInt16)]
        public uint LandblockRaw { get; set; }

        [JsonProperty("x")]
        [DbField("posX", (int)MySqlDbType.Float)]
        public float PosX { get; set; }

        [JsonProperty("y")]
        [DbField("posY", (int)MySqlDbType.Float)]
        public float PosY { get; set; }

        [JsonProperty("z")]
        [DbField("posZ", (int)MySqlDbType.Float)]
        public float PosZ { get; set; }

        [JsonProperty("qw")]
        [DbField("qW", (int)MySqlDbType.Float)]
        public float QW { get; set; }

        [JsonProperty("qx")]
        [DbField("qX", (int)MySqlDbType.Float)]
        public float QX { get; set; }

        [JsonProperty("qy")]
        [DbField("qY", (int)MySqlDbType.Float)]
        public float QY { get; set; }

        [JsonProperty("qz")]
        [DbField("qZ", (int)MySqlDbType.Float)]
        public float QZ { get; set; }

        [JsonIgnore]
        public Position Position
        {
            get { return new Position(LandblockRaw, PosX, PosY, PosZ, QX, QY, QZ, QW); }
        }
    }
}
