/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using DerethForever.Entity.Enum;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    public abstract class BaseDataProperty : IDirty
    {
        [JsonIgnore]
        [DbField("aceObjectId", (int)MySqlDbType.UInt32, IsCriteria = true, ListGet = true, ListDelete = true, Update = false)]
        public uint DataObjectId { get; set; }

        [JsonIgnore]
        public bool IsDirty { get; set; } = false;

        [JsonIgnore]
        public bool HasEverBeenSavedToDatabase { get; set; } = false;

        public void ClearDirtyFlags()
        {
            IsDirty = false;
            HasEverBeenSavedToDatabase = true;
        }

        public void SetDirtyFlags()
        {
            IsDirty = true;
            HasEverBeenSavedToDatabase = false;
        }

        [JsonIgnore]
        public virtual uint PropertyId { get; set; }

        [JsonIgnore]
        public virtual ObjectPropertyType PropertyType { get; }
    }
}
