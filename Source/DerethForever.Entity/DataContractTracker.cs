﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Common;
using MySql.Data.MySqlClient;

namespace DerethForever.Entity
{
    [DbTable("ace_contract_tracker")]
    public class DataContractTracker : ICloneable
    {
        [DbField("aceObjectId", (int)MySqlDbType.UInt32, IsCriteria = true, ListGet = true, ListDelete = true, Update = false)]
        public uint DataObjectId { get; set; }

        [DbField("contractId", (int)MySqlDbType.UInt32, IsCriteria = true, Update = false)]
        public uint ContractId { get; set; }

        [DbField("version", (int)MySqlDbType.UInt32)]
        public uint Version { get; set; } = 0;

        [DbField("stage", (int)MySqlDbType.UInt32)]
        public uint Stage { get; set; } = 0;

        [DbField("timeWhenDone", (int)MySqlDbType.UInt64)]
        public ulong TimeWhenDone { get; set; } = 0;

        [DbField("timeWhenRepeats", (int)MySqlDbType.UInt64)]
        public ulong TimeWhenRepeats { get; set; } = 0;

        [DbField("deleteContract", (int)MySqlDbType.UInt32)]
        public uint DeleteContract { get; set; } = 0;

        [DbField("setAsDisplayContract", (int)MySqlDbType.UInt32)]
        public uint SetAsDisplayContract { get; set; } = 0;

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
