﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    public enum AnimationHookType
    {
        Unknown = -1,
        NoOp = 0,
        Sound = 1,
        SoundTable = 2,
        Attack = 3,
        AnimationDone = 4,
        ReplaceObject = 5,
        Ethereal = 6,
        TransparentPart = 7,
        Luminous = 8,
        LuminousPart = 9,
        Diffuse = 10,
        DiffusePart = 11,
        Scale = 12,
        CreateParticle = 13,
        DestroyParticle = 14,
        StopParticle = 15,
        NoDraw = 16,
        DefaultScript = 17,
        DefaultScriptPart = 18,
        CallPES = 19, // Particle Emitter System
        Transparent = 20,
        SoundTweaked = 21,
        SetOmega = 22,
        TextureVelocity = 23,
        TextureVelocityPart = 24,
        SetLight = 25,
        CreateBlockingParticle = 26,
        ForceAnimationHook32Bit = 2147483647
    }
}
