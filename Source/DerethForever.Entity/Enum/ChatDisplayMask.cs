﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// The ChatDisplayMask identifies that types of chat that are displayed in each chat window.<para />
    /// Used by CharacterOptionData: The CharacterOptionData structure contains character options.
    /// </summary>
    public enum ChatDisplayMask : long
    {
        /// <summary>
        /// Gameplay (main chat window only)
        /// </summary>
        Gameplay        = 0x0000000003912021,

        /// <summary>
        /// Mandatory (main chat window only, cannot be disabled)
        /// </summary>
        Mandatory       = 0x000000000000c302,
     
        AreaChat        = 0x0000000000001004,
        Tells           = 0x0000000000000018,
        Combat          = 0x0000000000600040,
        Magic           = 0x0000000000020080,
        Allegiance      = 0x0000000000040c00,
        Fellowship      = 0x0000000000080000,
        Errors          = 0x0000000004000000,
        GeneralChannel  = 0x0000000008000000,
        TradeChannel    = 0x0000000010000000,
        LFGChannel      = 0x0000000020000000,
        RoleplayChannel = 0x0000000040000000,
    }
}
