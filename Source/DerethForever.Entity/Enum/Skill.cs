/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Linq;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// note: even though these are unnumbered, order is very important.  values of "none" or commented
    /// as retired or unused CANNOT be removed
    /// </summary>
    public enum Skill : ushort
    {
        None,
        Axe,                 /* Retired */
        Bow,                 /* Retired */
        Crossbow,            /* Retired */
        Dagger,              /* Retired */
        Mace,                /* Retired */

        [AbilityFormula(Ability.quickness | Ability.coordination, 3)]
        [SkillCost(10, 10)]
        [SkillUsableUntrained(true)]
        MeleeDefense,

        [AbilityFormula(Ability.quickness | Ability.coordination, 5)]
        [SkillCost(6, 4)]
        [SkillUsableUntrained(true)]
        MissileDefense,

        Sling,               /* Retired */
        Spear,               /* Retired */
        Staff,               /* Retired */
        Sword,               /* Retired */
        ThrownWeapon,        /* Retired */
        UnarmedCombat,       /* Retired */

        [AbilityFormula(Ability.focus, 3)]
        [SkillCost(0, 2, TrainsFree = true)]
        [SkillUsableUntrained(true)]
        ArcaneLore,

        [AbilityFormula(Ability.focus | Ability.self, 7)]
        [SkillCost(0, 12, TrainsFree = true)]
        [SkillUsableUntrained(true)]
        MagicDefense,

        [AbilityFormula(Ability.focus | Ability.self, 6)]
        [SkillCost(6, 6)]
        [SkillUsableUntrained(false)]
        ManaConversion,

        Spellcraft,          /* Unimplemented */

        [AbilityFormula(Ability.focus | Ability.self, 6)]
        [SkillCost(2, CanSpecialize = false)]
        [SkillUsableUntrained(false)]
        ItemTinkering,

        [AbilityFormula(Ability.None)]
        [SkillCost(2, 2)]
        [SkillUsableUntrained(false)]
        AssessPerson,

        [AbilityFormula(Ability.None)]
        [SkillCost(4, 2)]
        [SkillUsableUntrained(false)]
        Deception,

        [AbilityFormula(Ability.focus | Ability.coordination, 3)]
        [SkillCost(6, 4)]
        [SkillUsableUntrained(false)]
        Healing,

        [AbilityFormula(Ability.strength | Ability.coordination, 2)]
        [SkillCost(0, 4, TrainsFree = true)]
        [SkillUsableUntrained(true)]
        Jump,

        [AbilityFormula(Ability.focus | Ability.coordination, 3)]
        [SkillCost(6, 4)]
        [SkillUsableUntrained(false)]
        Lockpick,

        [AbilityFormula(Ability.quickness)]
        [SkillCost(0, 4, TrainsFree = true)]
        [SkillUsableUntrained(true)]
        Run,

        Awareness,           /* Unimplemented */
        ArmsAndArmorRepair,  /* Unimplemented */

        [AbilityFormula(Ability.None)]
        [SkillCost(4, 2)]
        [SkillUsableUntrained(false)]
        AssessCreature,

        [AbilityFormula(Ability.focus | Ability.strength, 2)]
        [SkillCost(4, CanSpecialize = false)]
        [SkillUsableUntrained(false)]
        WeaponTinkering,

        [AbilityFormula(Ability.focus | Ability.endurance, 2)]
        [SkillCost(4, CanSpecialize = false)]
        [SkillUsableUntrained(false)]
        ArmorTinkering,

        [AbilityFormula(Ability.focus)]
        [SkillCost(4, CanSpecialize = false)]
        [SkillUsableUntrained(false)]
        MagicItemTinkering,

        [AbilityFormula(Ability.focus | Ability.self, 4)]
        [SkillCost(8, 8)]
        [SkillUsableUntrained(false)]
        CreatureEnchantment,

        [AbilityFormula(Ability.focus | Ability.self, 4)]
        [SkillCost(8, 8)]
        [SkillUsableUntrained(false)]
        ItemEnchantment,

        [AbilityFormula(Ability.focus | Ability.self, 4)]
        [SkillCost(12, 8)]
        [SkillUsableUntrained(false)]
        LifeMagic,

        [AbilityFormula(Ability.focus | Ability.self, 4)]
        [SkillCost(16, 12)]
        [SkillUsableUntrained(false)]
        WarMagic,

        [AbilityFormula(Ability.None)]
        [SkillCost(4, 2)]
        [SkillUsableUntrained(true)]
        Leadership,

        [AbilityFormula(Ability.None)]
        [SkillCost(0, 2, TrainsFree = true)]
        [SkillUsableUntrained(true)]
        Loyalty,

        [AbilityFormula(Ability.coordination | Ability.focus, 3)]
        [SkillCost(4, 4)]
        [SkillUsableUntrained(false)]
        Fletching,

        [AbilityFormula(Ability.coordination | Ability.focus, 3)]
        [SkillCost(6, 6)]
        [SkillUsableUntrained(false)]
        Alchemy,

        [AbilityFormula(Ability.coordination | Ability.focus, 3)]
        [SkillCost(4, 4)]
        [SkillUsableUntrained(false)]
        Cooking,

        [AbilityFormula(Ability.None)]
        [SkillCost(0, CanSpecialize = false, TrainsFree = true)]
        [SkillUsableUntrained(true)]
        Salvaging,

        [AbilityFormula(Ability.strength | Ability.coordination, 3)]
        [SkillCost(8, 8)]
        [SkillUsableUntrained(true)]
        TwoHandedCombat,

        Gearcraft,           /* Retired */

        [AbilityFormula(Ability.focus | Ability.self, 4)]
        [SkillCost(16, 12)]
        [SkillUsableUntrained(false)]
        VoidMagic,

        [AbilityFormula(Ability.coordination | Ability.strength, 3)]
        [SkillCost(6, 6)]
        [SkillUsableUntrained(true)]
        HeavyWeapons,

        [AbilityFormula(Ability.coordination | Ability.strength, 3)]
        [SkillCost(4, 4)]
        [SkillUsableUntrained(true)]
        LightWeapons,

        [AbilityFormula(Ability.coordination | Ability.quickness, 3)]
        [SkillCost(4, 4)]
        [SkillUsableUntrained(true)]
        FinesseWeapons,

        [AbilityFormula(Ability.coordination, 2)]
        [SkillCost(6, 6)]
        [SkillUsableUntrained(true)]
        MissileWeapons,

        [AbilityFormula(Ability.coordination | Ability.strength, 2)]
        [SkillCost(2, 2)]
        [SkillUsableUntrained(true)]
        Shield,

        [AbilityFormula(Ability.coordination, 3, AbilityMultiplier = 2)]
        [SkillCost(2, 2)]
        [SkillUsableUntrained(false)]
        DualWield,

        [AbilityFormula(Ability.strength | Ability.quickness, 3)]
        [SkillCost(4, 2)]
        [SkillUsableUntrained(false)]
        Recklessness,

        [AbilityFormula(Ability.coordination | Ability.quickness, 3)]
        [SkillCost(4, 2)]
        [SkillUsableUntrained(false)]
        SneakAttack,

        [AbilityFormula(Ability.strength | Ability.coordination, 3)]
        [SkillCost(2, 2)]
        [SkillUsableUntrained(false)]
        DirtyFighting,

        Challenge,          /* Unimplemented */

        [AbilityFormula(Ability.endurance | Ability.self, 3)]
        [SkillCost(8, 4)]
        [SkillUsableUntrained(false)]
        Summoning
    }

    public static class SkillExtensions
    {
        public static AbilityFormulaAttribute GetFormula(this Skill skill)
        {
            return Enum.EnumHelper.GetAttributeOfType<AbilityFormulaAttribute>(skill);
        }

        public static SkillCostAttribute GetCost(this Skill skill)
        {
            return Enum.EnumHelper.GetAttributeOfType<SkillCostAttribute>(skill);
        }

        public static SkillUsableUntrainedAttribute GetUsability(this Skill skill)
        {
            return Enum.EnumHelper.GetAttributeOfType<SkillUsableUntrainedAttribute>(skill);
        }

        /// <summary>
        /// Will add a space infront of capital letter words in a string
        /// </summary>
        /// <param name="skill"></param>
        /// <returns>string with spaces infront of capital letters</returns>
        public static string ToSentence(this Skill skill)
        {
            return new string(skill.ToString().ToCharArray().SelectMany((c, i) => i > 0 && char.IsUpper(c) ? new char[] { ' ', c } : new char[] { c }).ToArray());
        }
    }

    public enum SkillStatus : uint
    {
        Inactive,
        Untrained,
        Trained,
        Specialized
    }
}
