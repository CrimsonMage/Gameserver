/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// DAMAGE_TYPE in the client
    /// </summary>
    [Flags]
    public enum DamageType
    {
        Slashing        = 0x001,
        Piercing        = 0x002,
        Bludgeoning     = 0x004,
        Cold            = 0x008,
        Fire            = 0x010,
        Acid            = 0x020,
        Electric        = 0x040,
        Health          = 0x080,
        Stamina         = 0x100,
        Mana            = 0x200,
        Nether          = 0x400,
        Prismatic       = 0x10000000
    }
}
