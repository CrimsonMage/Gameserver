﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// Used during Calculation of Damage
    /// This data is sent in the priority field of the iilst (equiped items) portion of the player discription event F7B0 - 0013 Og II
    /// </summary>
    /// 
    [Flags]
    public enum CoverageMask : uint
    {
        UnderwearUpperLegs      = 0x00000002,
        UnderwearLowerLegs      = 0x00000004,
        UnderwearChest          = 0x00000008,
        UnderwearAbdomen        = 0x00000010,
        UnderwearUpperArms      = 0x00000020,
        UnderwearLowerArms      = 0x00000040,
        OuterwearUpperLegs      = 0x00000100,
        OuterwearLowerLegs      = 0x00000200,
        OuterwearChest          = 0x00000400,
        OuterwearAbdomen        = 0x00000800,
        OuterwearUpperArms      = 0x00001000,
        OuterwearLowerArms      = 0x00002000,
        Head                    = 0x00004000,
        Hands                   = 0x00008000,
        Feet                    = 0x00010000,
    }
}