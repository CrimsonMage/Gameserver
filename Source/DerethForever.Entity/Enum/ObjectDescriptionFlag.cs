﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Entity.Enum
{
    [Flags]
    public enum ObjectDescriptionFlag
    {
        None                   = 0x00000000,
        Openable               = 0x00000001,
        Inscribable            = 0x00000002,
        Stuck                  = 0x00000004,
        Player                 = 0x00000008,
        Attackable             = 0x00000010,
        PlayerKiller           = 0x00000020,
        HiddenAdmin            = 0x00000040,
        UiHidden               = 0x00000080,
        Book                   = 0x00000100,
        Vendor                 = 0x00000200,
        PkSwitch               = 0x00000400,
        NpkSwitch              = 0x00000800,
        Door                   = 0x00001000,
        Corpse                 = 0x00002000,
        LifeStone              = 0x00004000,
        Food                   = 0x00008000,
        Healer                 = 0x00010000,
        Lockpick               = 0x00020000,
        Portal                 = 0x00040000,
        Admin                  = 0x00100000,
        FreePkStatus           = 0x00200000,
        ImmuneCellRestrictions = 0x00400000,
        RequiresPackSlot       = 0x00800000,
        Retained               = 0x01000000,
        PkLiteStatus           = 0x02000000,
        IncludesSecondHeader   = 0x04000000,
        BindStone              = 0x08000000,
        VolatileRare           = 0x10000000,
        WieldOnUse             = 0x20000000,
        WieldLeft              = 0x40000000,
    }
}
