/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    /// <summary>
    /// exported from the decompiled client.  actual usage of these is 100% speculative.
    /// </summary>
    public enum EmoteCategory
    {
        Invalid                   = 0,
        Refuse                    = 1,
        Vendor                    = 2,
        Death                     = 3,
        Portal                    = 4,
        HeartBeat                 = 5,
        Give                      = 6,
        Use                       = 7,
        Activation                = 8,
        Generation                = 9,
        PickUp                    = 10,
        Drop                      = 11,
        QuestSuccess              = 12,
        QuestFailure              = 13,
        Taunt                     = 14,
        WoundedTaunt              = 15,
        KillTaunt                 = 16,
        NewEnemy                  = 17,
        Scream                    = 18,
        Homesick                  = 19,
        ReceiveCritical           = 20,
        ResistSpell               = 21,
        TestSuccess               = 22,
        TestFailure               = 23,
        HearChat                  = 24,
        Wield                     = 25,
        UnWield                   = 26,
        EventSuccess              = 27,
        EventFailure              = 28,
        TestNoQuality             = 29,
        QuestNoFellow             = 30,
        TestNoFellow              = 31,
        GotoSet                   = 32,
        NumFellowsSuccess         = 33,
        NumFellowsFailure         = 34,
        NumCharacterTitlesSuccess = 35,
        NumCharacterTitlesFailure = 36,
        ReceiveLocalSignal        = 37,
        ReceiveTalkDirect         = 38
    }
}
