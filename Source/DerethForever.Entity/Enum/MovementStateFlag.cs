﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    using System;

    /// <summary>
    /// used for building a set of flags to send to be used in GameMessageMotion
    /// </summary>
    [Flags]
    public enum MovementStateFlag : uint
    {
        NoMotionState = 0x0,
        CurrentStyle = 0x1,
        ForwardCommand = 0x2,
        ForwardSpeed = 0x4,
        SideStepCommand = 0x8,
        SideStepSpeed = 0x10,
        TurnCommand = 0x20,
        TurnSpeed = 0x40
    }
}
