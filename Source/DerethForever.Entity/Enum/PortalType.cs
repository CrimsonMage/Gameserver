﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Entity.Enum
{
    [Flags]
    public enum PortalType : uint
    {
        Purple      = 0x020001B3u,
        Blue        = 0x020005D2u,
        Green       = 0x020005D3u,
        Orange      = 0x020005D4u,
        Red         = 0x020005D5u,
        Yellow      = 0x020005D6u,
        White       = 0x020006F4u,
        Shadow      = 0x020008FDu,
        Broken      = 0x02000F2Eu,
        Destroyed   = 0x020019E4u
    }
}
