/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Common;
using DerethForever.Entity.Enum;

using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DerethForever.Entity
{
    [DbTable("vw_weenie_search")]
    public class WeenieSearchResult
    {
        [JsonProperty("weenieClassId")]
        [DbField("aceObjectId", (int)MySqlDbType.UInt32)]
        public uint WeenieClassId { get; set; }

        [JsonProperty("name")]
        [DbField("name", (int)MySqlDbType.Text)]
        public string Name { get; set; }

        [JsonProperty("description")]
        [DbField("weenieClassDescription", (int)MySqlDbType.Text)]
        public string Description { get; set; }

        [JsonProperty("weenieType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public WeenieType? WeenieType { get; set; }

        [JsonIgnore]
        [DbField("weenieType", (int)MySqlDbType.Int32)]
        public int? WeenieType_Binder
        {
            get { return (int?)WeenieType; }
            set { WeenieType = (WeenieType?)value; }
        }

        [JsonProperty("itemType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ItemType ItemType { get; set; }

        [JsonIgnore]
        [DbField("itemType", (int)MySqlDbType.Int32)]
        public int ItemType_Binder
        {
            get { return (int)ItemType; }
            set { ItemType = (ItemType)value; }
        }

        [JsonProperty("userModified")]
        [DbField("userModified", (int)MySqlDbType.Byte)]
        public bool UserModified { get; set; }
        
        [JsonProperty("lastModified")]
        public DateTime? LastModified { get; set; }

        [JsonIgnore]
        [DbField("lastModifiedDate", (int)MySqlDbType.Int64)]
        public long? LastModified_Binder
        {
            get { return LastModified == null ? (long?)null : LastModified.Value.Ticks; }
            set { LastModified = value == null ? (DateTime?)null : new DateTime(value.Value); }
        }
        
        [JsonProperty("modifiedBy")]
        [DbField("modifiedBy", (int)MySqlDbType.Text)]
        public string ModifiedBy { get; set; }
    }
}
