/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Entity.Enum;

namespace DerethForever.Entity
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class AbilityFormulaAttribute : Attribute
    {
        public Ability Abilities { get; set; }

        public byte Divisor { get; set; }

        public byte AbilityMultiplier { get; set; } = 1;

        public AbilityFormulaAttribute(Ability abilities)
            : this(abilities, 1)
        {
        }

        public AbilityFormulaAttribute(Ability abilities, byte divisor)
        {
            if (divisor == 0)
            {
                throw new ArgumentException("0 not a valid value for " + nameof(divisor));
            }

            Abilities = abilities;
            Divisor = divisor;
        }

        public uint CalcBase(ICreatureStats stats)
        {
            uint sum = 0;

            if (stats != null)
            {
                if (((uint)Abilities & (uint)Ability.coordination) != 0)
                {
                    sum += stats.Coordination ?? 0;
                }
                if (((uint)Abilities & (uint)Ability.endurance) != 0)
                {
                    sum += stats.Endurance ?? 0;
                }
                if (((uint)Abilities & (uint)Ability.focus) != 0)
                {
                    sum += stats.Focus ?? 0;
                }
                if (((uint)Abilities & (uint)Ability.quickness) != 0)
                {
                    sum += stats.Quickness ?? 0;
                }
                if (((uint)Abilities & (uint)Ability.self) != 0)
                {
                    sum += stats.Self ?? 0;
                }
                if (((uint)Abilities & (uint)Ability.strength) != 0)
                {
                    sum += stats.Strength ?? 0;
                }
            }

            return (uint)Math.Ceiling((double)(sum * AbilityMultiplier) / Divisor);
        }
    }
}
