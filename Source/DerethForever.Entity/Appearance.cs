/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;

namespace DerethForever.Entity
{
    public class Appearance
    {
        /// <summary>
        /// This should be moved back to the ACE project when time permits.
        /// </summary>
        public static Appearance FromNetwork(BinaryReader reader)
        {
            Appearance appearance = new Appearance()
            {
                Eyes = reader.ReadUInt32(),
                Nose = reader.ReadUInt32(),
                Mouth = reader.ReadUInt32(),
                HairColor = reader.ReadUInt32(),
                EyeColor = reader.ReadUInt32(),
                HairStyle = reader.ReadUInt32(),
                HeadgearStyle = reader.ReadUInt32(),
                HeadgearColor = reader.ReadInt32(),
                ShirtStyle = reader.ReadUInt32(),
                ShirtColor = reader.ReadInt32(),
                PantsStyle = reader.ReadUInt32(),
                PantsColor = reader.ReadInt32(),
                FootwearStyle = reader.ReadUInt32(),
                FootwearColor = reader.ReadInt32(),
                SkinHue = reader.ReadDouble(),
                HairHue = reader.ReadDouble(),
                HeadgearHue = reader.ReadDouble(),
                ShirtHue = reader.ReadDouble(),
                PantsHue = reader.ReadDouble(),
                FootwearHue = reader.ReadDouble()
            };
            return appearance;
        }

        public uint Eyes { get; set; }

        public uint Nose { get; set; }

        public uint Mouth { get; set; }

        public uint HairColor { get; set; }

        public uint EyeColor { get; set; }

        public uint HairStyle { get; set; }

        public uint HeadgearStyle { get; set; }

        public int HeadgearColor { get; set; }

        public uint ShirtStyle { get; set; }

        public int ShirtColor { get; set; }

        public uint PantsStyle { get; set; }

        public int PantsColor { get; set; }

        public uint FootwearStyle { get; set; }

        public int FootwearColor { get; set; }

        public double SkinHue { get; set; }

        public double HairHue { get; set; }

        public double HeadgearHue { get; set; }

        public double ShirtHue { get; set; }

        public double PantsHue { get; set; }

        public double FootwearHue { get; set; }
    }
}
