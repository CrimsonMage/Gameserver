/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Database
{
    public interface ISerializedShardDatabase
    {
        void GetObjectsByLandblock(ushort landblock, Action<List<DataObject>> callback);

        void GetObject(uint dataObjectId, Action<DataObject> callback);

        void SaveObject(DataObject dataObject, Action<bool> callback);

        void DeleteObject(DataObject dataObject, Action<bool> callback);

        void GetCharacters(uint subscriptionId, Action<List<CachedCharacter>> callback);

        /// <summary>
        /// Loads an object by name.  Primary use case: characters.
        /// </summary>
        void GetObjectInfoByName(string name, Action<ObjectInfo> callback);

        void DeleteFriend(uint characterId, uint friendCharacterId, Action callback);

        bool DeleteContract(DataContractTracker contract, Action<bool> callback);

        void AddFriend(uint characterId, uint friendCharacterId, Action callback);

        void RemoveAllFriends(uint characterId, Action callback);

        void IsCharacterNameAvailable(string name, Action<bool> callback);

        void DeleteOrRestore(ulong unixTime, uint id, Action<bool> callback);

        void DeleteCharacter(uint id, Action<bool> callback);

        void GetCurrentId(uint min, uint max, Action<uint> callback);

        void SetCharacterAccessLevelByName(string name, AccessLevel accessLevel, Action<uint> callback);

        void RenameCharacter(string currentName, string newName, Action<uint> callback);

        void GetCharacter(uint id, Action<DataCharacter> callback);
    }
}
