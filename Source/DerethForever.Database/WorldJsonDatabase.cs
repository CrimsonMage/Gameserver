/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using log4net;
using Newtonsoft.Json;
using DerethForever.Common;

namespace DerethForever.Database
{
    public class WorldJsonDatabase : IWorldDatabase
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string _sourceDirectory;

        private static readonly object _weenieMutex = new object();
        private static readonly object _recipeMutex = new object();
        private static readonly object _contentMutex = new object();
        private static readonly object _spawnMutex = new object();
        private static readonly object _poiMutex = new object();
        private static readonly object _dungeonNamesMutex = new object();

        // note all caches are of the serialized data.  there's no point in deserializing now
        // as it takes up more memory, more cpu to load, and still requires deserialization in order
        // to create cloned instances (primary use case)

        /// <summary>
        /// 1 file per weenie, filename to match uint of WeenieClassId
        /// </summary>
        private ConcurrentDictionary<uint, CachedJson<DataObject>> _weenieCache = null;

        /// <summary>
        /// 1 file per recipe, filename to match RecipeGuid.ToString().ToUpper()
        /// </summary>
        private ConcurrentDictionary<Guid, CachedJson<Recipe>> _recipeCache = null;

        /// <summary>
        /// 1 file per "content" object, filename to match ContentGuid.ToString().ToUpper()
        /// </summary>
        private ConcurrentDictionary<Guid, CachedJson<Content>> _contentCache = null;

        /// <summary>
        /// 1 file per POI, filename to match POI.Name
        /// </summary>
        private List<TeleportLocation> _poiCache = null;

        /// <summary>
        /// 1 file per landblock X/Y - will be saved as Hex of the landblock X/Y
        /// </summary>
        private ConcurrentDictionary<ushort, List<WeenieObjectInstance>> _spawnMap = null;

        /// <summary>
        /// Name database used when searching for landblocks.
        /// </summary>
        private DungeonNames _dungeonNames = null;

        /// <summary>
        /// instantiates the database.  warning: this loads lots of data and will take time and use up a
        /// lot of threads.
        /// </summary>
        public WorldJsonDatabase(string sourceDirectory)
        {
            if (string.IsNullOrWhiteSpace(sourceDirectory))
                throw new ArgumentNullException(nameof(sourceDirectory), "sourceDirectory must be provided");

            if (!Directory.Exists(sourceDirectory))
                throw new ArgumentException("Source directory does not exist.");

            _sourceDirectory = sourceDirectory;

            log.Info("Loading cache from world data folder...");
            ReloadCache();
            bool cacheFailure = CheckForCacheErrors();

            if (cacheFailure == true)
            {
                if (ConfigManager.Config.Database.EnableAutoRedeployment)
                {
                    var redeploy = Redeploy.RedeployDatabaseFromSource(DatabaseSelectionOption.World, ResourceSelectionOption.LocalDisk);
                    if (redeploy?.Length > 0)
                    {
                        log.Debug("First attempt at redeployment failed, now the server will download from the web and try again..");
                        log.Debug($"Error: {redeploy}");
                        if (redeploy == "Downloads were missing for World" || redeploy == "No data files were found on local disk or an unknown error has occurred.")
                        {
                            redeploy = Redeploy.RedeployDatabaseFromSource(DatabaseSelectionOption.World, ResourceSelectionOption.Gitlab);
                            if (redeploy?.Length == 0)
                            {
                                log.Error($"Redeployment error: {redeploy}");
                                Debug.Assert(redeploy == null, "Auto Redployment failed!");
                            }
                        }
                    }
                    ReloadCache();
                    cacheFailure = CheckForCacheErrors();
                }
                else
                    log.Info($"There was an error in the cache, set EnableAutoRedeployment true, if you would like too have the server try fixing this error.");
            }

            Debug.Assert(cacheFailure == false, "Caches are invalid!");

            log.Info("Cache loading complete.");
        }

        public void ReloadCache()
        {
            // load everything into cache - this will take some time, but we can
            LoadWeenieCache();
            LoadRecipeCache();
            LoadContentCache();
            LoadSpawnMap();
            LoadPoiCache();
            LoadDungeonNameCache();
        }

        public bool CheckForCacheErrors()
        {
            bool cacheFailure = false;

            if (_weenieCache == null || _weenieCache?.Count == 0)
            {
                log.Error($"WeenieCache is empty!");
                cacheFailure = true;
            }

            if (_spawnMap == null || _spawnMap?.Count == 0)
            {
                log.Error($"Spawn Map is empty!");
                cacheFailure = true;
            }

            if (_poiCache == null || _poiCache?.Count == 0)
            {
                log.Error($"POI (Point-of-interest) Cache is empty!");
                cacheFailure = true;
            }

            if (_recipeCache == null || _recipeCache?.Count == 0)
            {
                log.Error($"Recipe Cache is empty!");
                cacheFailure = true;
            }

            return cacheFailure;
        }

        /// <summary>
        /// loads all the weenie data.  use with caution.  this method allocates all the memory
        /// required for the full weenie cache, then reassigns the local cache afterwards.
        /// this also locks the writing mutex in case changes are attempted during load.
        /// </summary>
        private void LoadWeenieCache()
        {
            string weenieDir = Path.Combine(_sourceDirectory, "weenies");

            if (!Directory.Exists(weenieDir))
                Directory.CreateDirectory(weenieDir);

            lock (_weenieMutex)
            {
                var files = Directory.EnumerateFiles(weenieDir).ToList();

                ConcurrentDictionary<uint, CachedJson<DataObject>> newCache = new ConcurrentDictionary<uint, CachedJson<DataObject>>();

                Stopwatch sw = new Stopwatch();
                sw.Start();
                ThreadPool.SetMaxThreads(64, 64);
                Parallel.ForEach(files, file =>
                {
                    var content = File.ReadAllText(file);

                    // file is "{WeenieID}.json"
                    int begin = file.LastIndexOf("\\") + 1;
                    int end = file.IndexOf(".", begin);
                    string weenieIdString = file.Substring(begin, end - begin);
                    uint weenieId = uint.Parse(weenieIdString);

                    CachedJson<DataObject> cache = new CachedJson<DataObject>(content, weenieIdString);
                    RelinkObject(cache.Object);
                    newCache.TryAdd(weenieId, cache);
                });
                sw.Stop();
                log.Info($"{files.Count} weenies loaded in {sw.Elapsed}.");

                _weenieCache = newCache;
            }
        }

        private void LoadRecipeCache()
        {
            string recipeDir = Path.Combine(_sourceDirectory, "recipes");

            if (!Directory.Exists(recipeDir))
                Directory.CreateDirectory(recipeDir);

            lock (_recipeMutex)
            {
                var files = Directory.EnumerateFiles(recipeDir).ToList();

                ConcurrentDictionary<Guid, CachedJson<Recipe>> newCache = new ConcurrentDictionary<Guid, CachedJson<Recipe>>();

                Stopwatch sw = new Stopwatch();
                sw.Start();
                ThreadPool.SetMaxThreads(64, 64);
                Parallel.ForEach(files, file =>
                {
                    var content = File.ReadAllText(file);

                    // file is "{WeenieID}.json"
                    int begin = file.LastIndexOf("\\") + 1;
                    int end = file.IndexOf(".", begin);
                    string recipeGuidString = file.Substring(begin, end - begin);
                    Guid recipeGuid = Guid.Parse(recipeGuidString);

                    CachedJson<Recipe> cache = new CachedJson<Recipe>(content, recipeGuidString);
                    newCache.TryAdd(recipeGuid, cache);
                });
                sw.Stop();
                log.Info($"{files.Count} recipes loaded in {sw.Elapsed}.");

                _recipeCache = newCache;
            }
        }

        private void LoadPoiCache()
        {
            string poiDir = Path.Combine(_sourceDirectory, "poi");

            if (!Directory.Exists(poiDir))
                Directory.CreateDirectory(poiDir);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            lock (_poiMutex)
            {
                var path = Path.Combine(poiDir, "points-of-interest.json");
                if (!File.Exists(path))
                    return;

                string content = File.ReadAllText(path);
                List<TeleportLocation> cache = JsonConvert.DeserializeObject<List<TeleportLocation>>(content);
                _poiCache = cache;
            }
            sw.Stop();
            log.Info($"POI cache loaded in {sw.Elapsed}.");

        }

        private void LoadContentCache()
        {
            string contentDir = Path.Combine(_sourceDirectory, "content");

            if (!Directory.Exists(contentDir))
                Directory.CreateDirectory(contentDir);

            lock (_contentMutex)
            {
                var files = Directory.EnumerateFiles(contentDir).ToList();

                ConcurrentDictionary<Guid, CachedJson<Content>> newCache = new ConcurrentDictionary<Guid, CachedJson<Content>>();

                Stopwatch sw = new Stopwatch();
                sw.Start();
                ThreadPool.SetMaxThreads(64, 64);
                Parallel.ForEach(files, file =>
                {
                    var content = File.ReadAllText(file);

                    // file is "{WeenieID}.json"
                    int begin = file.LastIndexOf("\\") + 1;
                    int end = file.IndexOf(".", begin);
                    string contentGuidString = file.Substring(begin, end - begin);
                    Guid contentGuid = Guid.Parse(contentGuidString);

                    CachedJson<Content> cache = new CachedJson<Content>(content, contentGuidString);
                    newCache.TryAdd(contentGuid, cache);
                });
                sw.Stop();
                log.Info($"{files.Count} content objects loaded in {sw.Elapsed}.");

                _contentCache = newCache;
            }
        }

        private void LoadSpawnMap()
        {
            string contentDir = Path.Combine(_sourceDirectory, "spawnmaps");

            if (!Directory.Exists(contentDir))
                Directory.CreateDirectory(contentDir);

            lock (_spawnMutex)
            {
                var files = Directory.EnumerateFiles(contentDir).ToList();

                ConcurrentDictionary<ushort, List<WeenieObjectInstance>> newCache = new ConcurrentDictionary<ushort, List<WeenieObjectInstance>>();

                Stopwatch sw = new Stopwatch();
                sw.Start();
                ThreadPool.SetMaxThreads(64, 64);
                Parallel.ForEach(files, file =>
                {
                    var content = File.ReadAllText(file);

                    // file is "{WeenieID}.json"
                    int begin = file.LastIndexOf("\\") + 1;
                    int end = file.IndexOf(".", begin);
                    string spawnMapHex = file.Substring(begin, end - begin);
                    ushort spawnMapId = ushort.Parse(spawnMapHex, System.Globalization.NumberStyles.HexNumber);

                    List<WeenieObjectInstance> map = JsonConvert.DeserializeObject<List<WeenieObjectInstance>>(content);
                    newCache.TryAdd(spawnMapId, map);
                });
                sw.Stop();
                log.Info($"{newCache.Count} spawn map landblocks loaded in {sw.Elapsed}.");

                _spawnMap = newCache;
            }
        }

        private void LoadDungeonNameCache()
        {
            string nameDir = Path.Combine(_sourceDirectory, "dungeonnames");

            if (!Directory.Exists(nameDir))
                Directory.CreateDirectory(nameDir);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            lock (_dungeonNamesMutex)
            {
                var path = Path.Combine(nameDir, "dungeon-names.json");
                if (!File.Exists(path))
                    return;

                string content = File.ReadAllText(path);
                DungeonNames dungeons = JsonConvert.DeserializeObject<DungeonNames>(content);
                _dungeonNames = dungeons;
            }

            sw.Stop();
            log.Info($"Dungeon name cache loaded in {sw.Elapsed}.");
        }

        private void SaveSpawnMap(ushort spawnMapId)
        {
            string contentDir = Path.Combine(_sourceDirectory, "spawnmaps");

            if (!Directory.Exists(contentDir))
                Directory.CreateDirectory(contentDir);

            lock (_spawnMutex)
            {
                string landblockFile = spawnMapId.ToString("X4") + ".json";
                var file = Path.GetFullPath(Path.Combine(contentDir, landblockFile));

                Stopwatch sw = new Stopwatch();
                sw.Start();
                List<WeenieObjectInstance> map = _spawnMap[spawnMapId];
                string content = JsonConvert.SerializeObject(map);

                // reserialize to disk
                File.WriteAllText(file, content);

                sw.Stop();
                log.Info($"spawn map landblocks saved in {sw.Elapsed}.");
            }
        }

        public WeenieObjectInstance GetInstance(ushort spawnMapId, int instanceId)
        {
            WeenieObjectInstance instance = null;

            if (spawnMapId > 0)
            {
                List<WeenieObjectInstance> landblockInstances = new List<WeenieObjectInstance>();

                lock (_spawnMutex)
                {
                    if (_spawnMap.ContainsKey(spawnMapId))
                        landblockInstances.AddRange(_spawnMap[spawnMapId]);
                    else
                        log.Info($"Map for id: {spawnMapId} not found.");
                }

                if (landblockInstances?.Count > 0)
                {
                    var foundInstance = landblockInstances.FirstOrDefault(i => i.PreassignedGuid == instanceId);

                    if (foundInstance != null)
                    {
                        instance = foundInstance;
                    }
                    else
                    {
                        log.Info($"Could not find instance: {instanceId}.");
                    }
                }
                landblockInstances = null;
            }
            return instance;
        }

        public void UpdateInstance(ushort spawnMapId, WeenieObjectInstance instance)
        {
            if (spawnMapId > 0)
            {
                List<WeenieObjectInstance> landblockInstances = new List<WeenieObjectInstance>();

                lock (_spawnMutex)
                {
                    if (_spawnMap.ContainsKey(spawnMapId))
                        landblockInstances.AddRange(_spawnMap[spawnMapId]);
                    else
                        log.Info($"Landblock: {spawnMapId} not found, creating.");
                }

                if (landblockInstances.Contains(instance))
                {
                    landblockInstances.Remove(instance);
                    log.Info("Replacing instance data.");
                }

                log.Info("Adding instance.");
                landblockInstances.Add(instance);
                UpdateSpawnMap(spawnMapId, landblockInstances);
                landblockInstances = null;
            }
        }

        public void RemoveInstance(ushort spawnMapId, WeenieObjectInstance instance)
        {
            if (spawnMapId > 0)
            {
                List<WeenieObjectInstance> landblockInstances = new List<WeenieObjectInstance>();

                lock (_spawnMutex)
                {
                    landblockInstances.AddRange(_spawnMap[spawnMapId]);
                }

                var instances = landblockInstances.Where(l => l.PreassignedGuid == instance.PreassignedGuid).ToList();

                if (instances.Count() > 0)
                {
                    foreach (var inst in instances)
                    {
                        landblockInstances.Remove(inst);
                    }

                    log.Info("Replacing instance data.");
                }
                else
                    log.Error("Instance was missing from Spawn Map!");
                UpdateSpawnMap(spawnMapId, landblockInstances);
                landblockInstances = null;
            }
        }

        public List<WeenieObjectInstance> GetSpawnMap(ushort spawnMapId)
        {
            lock (_spawnMutex)
            {
                if (_spawnMap.ContainsKey(spawnMapId))
                {
                    return _spawnMap[spawnMapId];
                }
                else
                {
                    log.Info($"spawn map cache does not contain landblockid: {spawnMapId}.");
                }
            }
            return null;
        }

        private void UpdateSpawnMap(ushort spawnMapId, List<WeenieObjectInstance> map)
        {
            lock (_spawnMutex)
            {
                if (_spawnMap.ContainsKey(spawnMapId))
                {
                    var previousMap = _spawnMap[spawnMapId];
                    _spawnMap.TryUpdate(spawnMapId, map, previousMap);
                }
                else
                {
                    log.Info($"spawn map cache does not contain landblockid: {spawnMapId}.");
                    _spawnMap.TryAdd(spawnMapId, map);
                }
            }
            SaveSpawnMap(spawnMapId);
        }

        public List<ushort> SearchSpawnMaps(SearchSpawnMapsCriteria criteria)
        {
            if (criteria != null)
            {
                Dictionary<ushort, List<WeenieObjectInstance>> spawnMap = new Dictionary<ushort, List<WeenieObjectInstance>>();
                Dictionary<ushort, List<WeenieObjectInstance>> copy;

                lock (_spawnMutex)
                {
                    copy = new Dictionary<ushort, List<WeenieObjectInstance>>(_spawnMap);
                }

                if (criteria.PartialDungeonName != null)
                {
                    var matchingNames = _dungeonNames.Landblocks.Where(d => d.Name.ToLower().Contains(criteria.PartialDungeonName.ToLower()));
                    foreach (var match in matchingNames)
                    {
                        if (copy.ContainsKey((ushort)match.Landblock))
                            spawnMap.Add((ushort)match.Landblock, copy[(ushort)match.Landblock]);
                        else
                        {
                            log.Info($"Key Missing: {match.Landblock}");
                            spawnMap.Add((ushort)match.Landblock, new List<WeenieObjectInstance>());
                        }
                    }
                }

                foreach (KeyValuePair<ushort, List<WeenieObjectInstance>> i in copy)
                {
                    if (criteria.InstanceId != null && i.Value.Where(w => w.PreassignedGuid == criteria.InstanceId.Value).Count() > 0)
                    {
                        spawnMap.Add(i.Key, i.Value);
                        continue;
                    }

                    if (criteria.WeenieClassId != null && i.Value.Where(w => w.WeenieClassId == criteria.WeenieClassId.Value).Count() > 0)
                    {
                        spawnMap.Add(i.Key, i.Value);
                    }
                }
                return spawnMap.Keys.ToList();
            }
            return null;
        }

        private void SaveRecipe(Recipe recipe)
        {
            if (recipe.RecipeGuid == null)
                recipe.RecipeGuid = Guid.NewGuid();

            string content = JsonConvert.SerializeObject(recipe);
            string fileName = Path.Combine(_sourceDirectory, "recipes", $"{recipe.RecipeGuid}.json");

            CachedJson<Recipe> cache = new CachedJson<Recipe>(recipe, recipe.RecipeGuid.ToString());

            lock (_recipeMutex)
            {
                if (_recipeCache.ContainsKey(recipe.RecipeGuid.Value))
                    _recipeCache[recipe.RecipeGuid.Value] = cache;
                else
                    _recipeCache.TryAdd(recipe.RecipeGuid.Value, cache);

                // reserialize to disk
                File.WriteAllText(fileName, content);
            }
        }

        private void SaveContent(Content content)
        {
            if (content.ContentGuid == null)
                content.ContentGuid = Guid.NewGuid();

            string serialized = JsonConvert.SerializeObject(content);
            string fileName = Path.Combine(_sourceDirectory, "content", $"{content.ContentGuid}.json");

            CachedJson<Content> cache = new CachedJson<Content>(content, content.ContentGuid.ToString());

            lock (_contentMutex)
            {
                if (_contentCache.ContainsKey(content.ContentGuid.Value))
                    _contentCache[content.ContentGuid.Value] = cache;
                else
                    _contentCache.TryAdd(content.ContentGuid.Value, cache);

                // reserialize to disk
                File.WriteAllText(fileName, serialized);
            }
        }

        public void CreateContent(Content content)
        {
            SaveContent(content);
        }

        public void CreateRecipe(Recipe recipe)
        {
            SaveRecipe(recipe);
        }

        public void DeleteContent(Guid contentGuid)
        {
            string fileName = Path.Combine(_sourceDirectory, "content", $"{contentGuid}.json");

            lock (_contentMutex)
            {
                if (_contentCache.ContainsKey(contentGuid))
                    _contentCache.TryRemove(contentGuid, out CachedJson<Content> garbage);

                if (File.Exists(fileName))
                    File.Delete(fileName);
            }
        }

        public void DeleteRecipe(Guid recipeGuid)
        {
            string fileName = Path.Combine(_sourceDirectory, "recipes", $"{recipeGuid}.json");

            lock (_recipeMutex)
            {
                if (_recipeCache.ContainsKey(recipeGuid))
                    _recipeCache.TryRemove(recipeGuid, out CachedJson<Recipe> garbage);

                if (File.Exists(fileName))
                    File.Delete(fileName);
            }
        }

        public bool DeleteWeenie(DataObject weenie)
        {
            if (weenie == null)
                throw new ArgumentNullException(nameof(weenie));

            string fileName = Path.Combine(_sourceDirectory, "weenies", $"{weenie.WeenieClassId}.json");

            lock (_weenieMutex)
            {
                if (_weenieCache.ContainsKey(weenie.WeenieClassId))
                    _weenieCache.TryRemove(weenie.WeenieClassId, out CachedJson<DataObject> garbage);

                if (File.Exists(fileName))
                    File.Delete(fileName);
            }

            return true;
        }

        public List<string> ExportChangesToDisk(string rootPath)
        {
            // not sure what this is supposed to do.  all the data is on disk already.
            return new List<string>();
        }

        public List<Content> GetAllContent()
        {
            return _contentCache.Select(c => c.Value.Object).ToList();
        }

        public List<Recipe> GetAllRecipes()
        {
            return _recipeCache.Values.Select(r => r.Object).ToList();
        }

        public uint GetCurrentId(uint min, uint max)
        {
            return _weenieCache.Select(w => w.Value.Object.WeenieClassId).Max();
        }

        public DataObject GetDataObjectByWeenie(uint weenieClassId)
        {
            if (!_weenieCache.ContainsKey(weenieClassId))
                return null;

            var weenieContent = _weenieCache[weenieClassId].SerializedData;

            JsonSerializerSettings settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore };
            DataObject result = JsonConvert.DeserializeObject<DataObject>(weenieContent, settings);
            RelinkObject(result);

            if (result == null)
                log.Warn($"Failed to deserialize {weenieClassId} into DataObject");

            // the result here is that any properties that these object have in common will get copied.
            return result;
        }

        public List<TeleportLocation> GetPointsOfInterest()
        {
            return _poiCache.ToList();
        }

        public Recipe GetRecipe(Guid recipeGuid)
        {
            if (_recipeCache.ContainsKey(recipeGuid))
                return _recipeCache[recipeGuid].Object;

            return null;
        }

        public DataObject GetWeenie(uint weenieClassId)
        {
            return !_weenieCache.ContainsKey(weenieClassId) ? null : _weenieCache[weenieClassId].Object;
        }

        public List<DataObject> GetWeenieInstancesByLandblock(ushort landblock)
        {
            // reference the spawn map to get the weenies that should spawn, then convert them to DataObjects
            if (!_spawnMap.ContainsKey(landblock))
                return new List<DataObject>();

            var map = _spawnMap[landblock].ToList(); // ToList makes a copy to prevent collection modification errors

            ConcurrentQueue<DataObject> result = new ConcurrentQueue<DataObject>();
            Parallel.ForEach(map, m =>
            {
                DataObject dataO = GetDataObjectByWeenie(m.WeenieClassId);
                if (dataO != null)
                {
                    dataO.Location = new Position(m.LandblockRaw, m.PositionX, m.PositionY, m.PositionZ, m.RotationX, m.RotationY, m.RotationZ, m.RotationW);
                    dataO.DataObjectId = m.PreassignedGuid;
                    result.Enqueue(dataO);
                }
                else
                {
                    log.Warn($"Spawn map {landblock:X4} specifies missing weenie {m.WeenieClassId}");
                }
            });

            return result.ToList();
        }

        public List<DataObject> GetWeeniesByLandblock(ushort landblock)
        {
            return new List<DataObject>();
        }

        public bool ReplaceWeenie(DataObject weenie)
        {
            return SaveWeenie(weenie);
        }

        public int ResetUserModifiedFlags()
        {
            // user modified flags are irrelevant in the new database mechanism.
            return 0;
        }

        public bool SaveWeenie(DataObject weenie)
        {
            return SaveWeenieEx(weenie);
        }

        private bool SaveWeenieEx(DataObject weenie, bool cacheOnly = false)
        {
            if (weenie == null)
                throw new ArgumentNullException(nameof(weenie));

            string content = JsonConvert.SerializeObject(weenie);
            string fileName = Path.Combine(_sourceDirectory, "weenies", $"{weenie.WeenieClassId}.json");

            CachedJson<DataObject> cache = new CachedJson<DataObject>(weenie, weenie.WeenieClassId.ToString());

            lock (_weenieMutex)
            {
                if (_weenieCache.ContainsKey(weenie.WeenieClassId))
                    _weenieCache[weenie.WeenieClassId] = cache;
                else
                    _weenieCache.TryAdd(weenie.WeenieClassId, cache);

                if (!cacheOnly)
                {
                    // reserialize to disk
                    File.WriteAllText(fileName, content);
                }
            }

            return true;
        }

        public List<Recipe> SearchRecipes(SearchRecipesCriteria criteria)
        {
            List<Recipe> recipes = new List<Recipe>();
            List<Recipe> copy;

            lock (_recipeMutex)
            {
                copy = _recipeCache.Values.Select(s => s.Object).ToList();
            }

            foreach (Recipe r in copy)
            {
                if (criteria.RecipeType != null && r.RecipeType == (byte)criteria.RecipeType.Value)
                {
                    recipes.Add(r);
                    continue;
                }

                if (criteria.Skill != null && r.SkillId == (ushort)criteria.Skill.Value)
                {
                    recipes.Add(r);
                    continue;
                }

                if (criteria.SourceWcid != null && r.SourceWcid == criteria.SourceWcid.Value)
                {
                    recipes.Add(r);
                    continue;
                }

                if (criteria.TargetWcid != null && r.TargetWcid == criteria.TargetWcid.Value)
                    recipes.Add(r);
            }

            return recipes;
        }

        public List<WeenieSearchResult> SearchWeenies(SearchWeeniesCriteria criteria, int max = 100)
        {
            var copy = _weenieCache.Values.Select(x => x.Object).ToList();

            if (criteria?.WeenieClassId != null)
                copy = copy.Where(o => o.WeenieClassId == criteria.WeenieClassId).ToList();

            if (criteria?.ItemType != null)
                copy = copy.Where(o => o.ItemType == (int)criteria.ItemType.Value).ToList();

            if (criteria?.WeenieType != null)
                copy = copy.Where(o => o.WeenieType == (int)criteria.WeenieType).ToList();

            if (!string.IsNullOrWhiteSpace(criteria?.PartialName))
                copy = copy.Where(o => o.Name.ToLower().Contains(criteria.PartialName.ToLower())).ToList();

            if (criteria?.PropertyCriteria?.Count > 0)
            {
                criteria.PropertyCriteria.ForEach(pc =>
                {
                    switch (pc.PropertyType)
                    {
                        case ObjectPropertyType.PropertyInt:
                            copy = copy.Where(w => w.IntProperties.Any(ip => ip.PropertyId == pc.PropertyId && ip.PropertyValue == int.Parse(pc.PropertyValue))).ToList();
                            break;
                        case ObjectPropertyType.PropertyDataId:
                            copy = copy.Where(w => w.DataIdProperties.Any(ip => ip.PropertyId == pc.PropertyId && ip.PropertyValue == uint.Parse(pc.PropertyValue))).ToList();
                            break;
                        case ObjectPropertyType.PropertyDouble:
                            copy = copy.Where(w => w.DoubleProperties.Any(ip => ip.PropertyId == pc.PropertyId && ip.PropertyValue.Equals(double.Parse(pc.PropertyValue)))).ToList();
                            break;
                        case ObjectPropertyType.PropertyInt64:
                            copy = copy.Where(w => w.Int64Properties.Any(ip => ip.PropertyId == pc.PropertyId && ip.PropertyValue == ulong.Parse(pc.PropertyValue))).ToList();
                            break;
                        case ObjectPropertyType.PropertyString:
                            copy = copy.Where(w => w.StringProperties.Any(ip => ip.PropertyId == pc.PropertyId && ip.PropertyValue.Contains(pc.PropertyValue))).ToList();
                            break;
                        case ObjectPropertyType.PropertyBool:
                            copy = copy.Where(w => w.BoolProperties.Any(ip => ip.PropertyId == pc.PropertyId && ip.PropertyValue == bool.Parse(pc.PropertyValue))).ToList();
                            break;
                        default:
                            log.Warn($"Weenie search for unsupported property type {pc.PropertyType}");
                            break;
                    }
                });
            }

            var result = copy.Select(o => new WeenieSearchResult()
            {
                Description = o.ShortDesc,
                LastModified = o.LastModified,
                ModifiedBy = o.ModifiedBy,
                Name = o.Name,
                ItemType = (ItemType)o.ItemType,
                UserModified = o.UserModified,
                WeenieClassId = o.WeenieClassId,
                WeenieType = (WeenieType?)o.WeenieType
            }).Take(max).ToList();

            return result;
        }

        public void UpdateContent(Content content)
        {
            SaveContent(content);
        }

        public void UpdateRecipe(Recipe recipe)
        {
            SaveRecipe(recipe);
        }

        public bool UserModifiedFlagPresent()
        {
            return true;
        }

        private static void RelinkObject(DataObject weenie)
        {
            var oldSkills = weenie.DataObjectPropertiesSkills;
            var newSkills = new List<CreatureSkill>();
            oldSkills.ForEach(c => newSkills.Add(new CreatureSkill(weenie, c.GetDataObjectSkill())));
            weenie.DataObjectPropertiesSkills = newSkills;

            var oldAttributes = weenie.DataObjectPropertiesAttributes2nd.Values.ToList();
            var newAttributes = new List<CreatureVital>();
            oldAttributes.ForEach(a => newAttributes.Add(new CreatureVital(weenie, a.GetVital())));
            weenie.DataObjectPropertiesAttributes2nd = newAttributes.ToDictionary(a => a.Ability, a => a);
        }

        public bool CacheOnlySaveWeenie(DataObject weenie)
        {
            return SaveWeenieEx(weenie, true);
        }
    }
}
