/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using log4net;
using DerethForever.Common.Extensions;
using System.Reflection;
using System.Data.SqlClient;

using DerethForever.Common;
using DerethForever.Entity.Enum;

namespace DerethForever.Database
{
    public class Database
    {
        // This is a debug channel for the general debugging of the database.
        private ILog log = LogManager.GetLogger("Database");

        private static object propertyCacheMutex = new object();
        private static readonly Dictionary<Type, List<Tuple<PropertyInfo, DbFieldAttribute>>> PropertyCache = new Dictionary<Type, List<Tuple<PropertyInfo, DbFieldAttribute>>>();

        private static readonly Dictionary<Type, DbTableAttribute> DbTableCache = new Dictionary<Type, DbTableAttribute>();

        private static string capturedSqlError = string.Empty;

        public class DatabaseTransaction
        {
            // This logging function will log specific db transactions - this class may be instantiated outside of the database namespace
            private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            private readonly Database database;
            private readonly List<Tuple<StoredPreparedStatement, object[]>> queries = new List<Tuple<StoredPreparedStatement, object[]>>();

            public DatabaseTransaction(Database database) { this.database = database; }

            public void AddPreparedStatement<T>(T id, params object[] parameters)
            {
                if (!database.preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
                {
                    Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
                }

                queries.Add(new Tuple<StoredPreparedStatement, object[]>(preparedStatement, parameters));
            }

            public void AddPreparedDeleteListStatement<T1, T2>(T1 id, Dictionary<string, object> criteria)
            {
                List<Tuple<PropertyInfo, DbFieldAttribute>> propertyInfo = GetPropertyCache(typeof(T2));

                if (!database.preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
                {
                    Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
                }

                List<object> objects = new List<object>();
                foreach (Tuple<PropertyInfo, DbFieldAttribute> p in propertyInfo)
                {
                    if (p.Item2.ListDelete)
                    {
                        bool success = criteria.TryGetValue(p.Item2.DbFieldName, out object val);
                        Debug.Assert(success, "Criteria does not contain essential key");
                        objects.Add(val);
                    }
                }

                queries.Add(new Tuple<StoredPreparedStatement, object[]>(preparedStatement, objects.ToArray()));
            }

            public void AddPreparedInsertListStatement<T1, T2>(T1 id, List<T2> info)
            {
                if (info == null || info.Count < 1)
                    return;

                List<Tuple<PropertyInfo, DbFieldAttribute>> propertyInfo = GetPropertyCache(typeof(T2));

                if (!database.preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
                {
                    Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
                }

                foreach (T2 type in info)
                {
                    object[] parameters = new object[propertyInfo.Count];
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        // Reflection (woot woot)
                        parameters[i] = propertyInfo[i].Item1.GetValue(type);
                    }

                    queries.Add(new Tuple<StoredPreparedStatement, object[]>(preparedStatement, parameters));
                }
            }

            public void AddPreparedInsertStatement<T1, T2>(T1 id, T2 instance)
            {
                if (!database.preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
                {
                    Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
                }

                List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(typeof(T2));

                object[] parameters = new object[properties.Count];
                for (int i = 0; i < parameters.Length; i++)
                {
                    // Reflection (woot woot)
                    parameters[i] = properties[i].Item1.GetValue(instance);
                }
                queries.Add(new Tuple<StoredPreparedStatement, object[]>(preparedStatement, parameters));
            }

            public void AddPreparedUpdateStatement<T1, T2>(T1 id, T2 instance)
            {
                if (!database.preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
                {
                    Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
                }

                List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(typeof(T2));

                List<object> p = new List<object>();
                properties.Where(x => x.Item2.Update).ToList().ForEach(x => p.Add(x.Item1.GetValue(instance)));
                properties.Where(x => x.Item2.IsCriteria).ToList().ForEach(x => p.Add(x.Item1.GetValue(instance)));
                object[] parameters = p.ToArray();

                queries.Add(new Tuple<StoredPreparedStatement, object[]>(preparedStatement, parameters));
            }

            public void AddPreparedDeleteStatement<T1, T2>(T1 id, object instance)
            {
                // Debug.Assert(typeof(T1) == database.PreparedStatementType, "Invalid prepared statement type.");

                List<Tuple<PropertyInfo, DbFieldAttribute>> propertyInfo = GetPropertyCache(typeof(T2));

                if (!database.preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
                {
                    Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
                }

                List<object> objects = new List<object>();
                foreach (Tuple<PropertyInfo, DbFieldAttribute> p in propertyInfo)
                {
                    if (p.Item2.IsCriteria)
                    {
                        object val = p.Item1.GetValue(instance);
                        objects.Add(val);
                    }
                }
                queries.Add(new Tuple<StoredPreparedStatement, object[]>(preparedStatement, objects.ToArray()));
            }

            public async Task<bool> Commit()
            {
                if (queries.Count == 0)
                    return false;

                MySqlTransaction transaction = null;
                MySqlConnection connection = new MySqlConnection(database.connectionString);
                try
                {
                    await connection.OpenAsync();
                    return await Task.Run(() =>
                    {
                        transaction = connection.BeginTransaction();
                        int queryNum = 0; // used for debugging, nothing else.  needed to know which sequence the offending query came in

                        foreach (Tuple<StoredPreparedStatement, object[]> query in queries)
                        {
                            using (MySqlCommand command = new MySqlCommand(query.Item1.Query, connection, transaction))
                            {
                                for (int i = 0; i < query.Item2.Length; i++)
                                {
                                    command.Parameters.Add("", query.Item1.Types[i]).Value = query.Item2[i];
                                }
#if DBDEBUG
                                string debugString = "QUERY LINE - " + command.CommandText + " - ";
                                foreach (MySqlParameter p in command.Parameters)
                                {
                                    if (p?.Value != null)
                                        debugString += p.Value + " ";
                                }
                                Log.Debug(debugString);
#endif
                                command.ExecuteNonQuery();
                            }

                            queryNum++;
                        }

                        transaction.Commit();
                        return true;
                    });
                }
                catch (MySqlException transactionException)
                {
                    Log.Error($"An exception occurred while commiting a transaction of {queries.Count} queries, a rollback will be performed!");
                    Log.Error($"Exception: {transactionException.ToString()}");

                    try
                    {
                        // serious problem if rollback also fails
                        transaction?.Rollback();
                    }
                    catch (MySqlException rollbackException)
                    {
                        Log.Error("An exception occurred while rolling back transaction!");
                        Log.Error($"Exception: {rollbackException.ToString()}");
                        Debug.Assert(false, "Transaction was rolled back.");
                    }

                    return false;
                }
                finally
                {
                    queries.Clear();

                    // rollback will fail if connection or transaction is disposed before this
                    connection.Dispose();
                    transaction?.Dispose();
                }
            }
        }

        protected string connectionString;
        protected DatabaseSelectionOption selectedDatabase;

        private readonly Dictionary<uint, StoredPreparedStatement> preparedStatements = new Dictionary<uint, StoredPreparedStatement>();

        protected virtual Type PreparedStatementType { get; }

        public void Initialize(string host, uint port, string user, string password, DatabaseSelectionOption databaseSelection, bool autoReconnect = true)
        {
            string database = GetDatabaseName(databaseSelection);

            MySqlConnectionStringBuilder connectionBuilder = new MySqlConnectionStringBuilder()
            {
                Server = host,
                Port = port,
                UserID = user,
                Password = password,
                Database = database,
                IgnorePrepare = false,
                Pooling = true,
                AllowUserVariables = true,
                AllowZeroDateTime = true
            };

            connectionString = connectionBuilder.ToString();
            selectedDatabase = databaseSelection;

            for (; ; )
            {
                try
                {
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                        connection.Open();

                    log.Debug($"Successfully connected to {database} database on {host}:{port}.");
                    break;
                }
                catch (Exception exception)
                {
                    log.Error($"Exception: {exception.Message}");
                    log.Error($"Attempting to reconnect to {database} database on {host}:{port} in 5 seconds...");

                    if (exception.Message.Contains("Unknown database"))
                    {
                        connectionBuilder.Database = string.Empty;
                        connectionString = connectionBuilder.ToString();
                        CreateDatabase(database);
                        connectionBuilder.Database = database;
                        connectionString = connectionBuilder.ToString();
                    }
                    // if db is not found, attempt too create and bomb out of fail

                    if (autoReconnect)
                        Thread.Sleep(5000);
                    else
                        throw;
                }
            }

            InitializePreparedStatements();
        }

        public void ResetConnectionString(string host, uint port, string user, string password, string database, bool autoReconnect = true)
        {
            MySqlConnectionStringBuilder connectionBuilder = new MySqlConnectionStringBuilder()
            {
                Server = host,
                Port = port,
                UserID = user,
                Password = password,
                Database = database,
                IgnorePrepare = false,
                Pooling = true,
                AllowUserVariables = true,
                AllowZeroDateTime = true
            };

            connectionString = connectionBuilder.ToString();
        }

        public DatabaseTransaction BeginTransaction() { return new DatabaseTransaction(this); }

        protected virtual void InitializePreparedStatements() { }

        protected void AddPreparedStatement<T>(T id, string query, params MySqlDbType[] types)
        {
            // Debug.Assert(typeof(T) == PreparedStatementType, "Invalid prepared statement type.");
            Debug.Assert(types.Length == query.Count(c => c == '?'), "Invalid prepared statement parameter length.");

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        for (uint i = 0; i < types.Length; i++)
                            command.Parameters.Add("", types[i]);

                        command.Prepare();

                        uint uintId = Convert.ToUInt32(id);
                        preparedStatements.Add(uintId, new StoredPreparedStatement(uintId, query, types));
                    }
                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                log.Error($"An exception occurred while preparing statement {id}!");
                log.Error($"Exception: {exception.Message}");
                AutoRedeployment();                
            }
        }

        private void ConstructGetListStatement<T1>(T1 id, Type type)
        {
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);

            // ColumnNames
            HashSet<string> criteria = new HashSet<string>(properties.Where(x => x.Item2.ListGet).Select(x => x.Item2.DbFieldName));

            ConstructGetListStatement<T1>(id, type, criteria);
        }

        public void ConstructGetListStatement<T1>(T1 id, Type type, HashSet<string> columnNames)
        {
            uint statementId = Convert.ToUInt32(id);
            DbTableAttribute dbTable = GetDbTableAttribute(type);

            List<MySqlDbType> types = new List<MySqlDbType>();
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
            string tableName = dbTable.DbTableName;
            string selectList = null;
            string whereList = null;

            foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
            {
                if (p.Item2.Get)
                {
                    if (selectList != null)
                        selectList += ", ";
                    selectList += "`" + p.Item2.DbFieldName + "`";
                }

                if (columnNames.Contains(p.Item2.DbFieldName))
                {
                    if (whereList != null)
                        whereList += " AND ";
                    whereList += $"`{p.Item2.DbFieldName}` = ?";
                    types.Add((MySqlDbType)p.Item2.DbFieldType);
                }
            }

            string query = $"SELECT {selectList} FROM `{tableName}`";

            if (whereList != null)
                query = query + $" WHERE {whereList}";

            PrepareStatement(statementId, query, types);
        }

        private void ConstructDeleteListStatement<T1>(T1 id, Type type)
        {
            uint statementId = Convert.ToUInt32(id);
            DbTableAttribute dbTable = GetDbTableAttribute(type);

            List<MySqlDbType> types = new List<MySqlDbType>();
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
            string tableName = dbTable.DbTableName;
            string whereList = null;

            foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
            {
                if (p.Item2.ListDelete)
                {
                    if (whereList != null)
                        whereList += " AND ";
                    whereList += "`" + p.Item2.DbFieldName + "` = ?";
                    types.Add((MySqlDbType)p.Item2.DbFieldType);
                }
            }

            string query = $"DELETE FROM `{tableName}` WHERE {whereList}";

            PrepareStatement(statementId, query, types);
        }

        private void ConstructDeleteStatement<T1>(T1 id, Type type)
        {
            uint statementId = Convert.ToUInt32(id);
            DbTableAttribute dbTable = GetDbTableAttribute(type);

            List<MySqlDbType> types = new List<MySqlDbType>();
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
            string whereList = null;

            foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
            {
                if (p.Item2.IsCriteria)
                {
                    if (whereList != null)
                        whereList += " AND ";
                    whereList += "`" + p.Item2.DbFieldName + "` = ?";
                    types.Add((MySqlDbType)p.Item2.DbFieldType);
                }
            }

            string query = $"DELETE FROM `{dbTable.DbTableName}` WHERE {whereList}";

            PrepareStatement(statementId, query, types);
        }

        private void ConstructInsertListStatement<T1>(T1 id, Type type)
        {
            uint statementId = Convert.ToUInt32(id);
            DbTableAttribute dbTable = GetDbTableAttribute(type);

            List<MySqlDbType> types = new List<MySqlDbType>();
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
            string tableName = dbTable.DbTableName;
            string valueList = "";
            string fieldList = "";
            bool inserted = false;

            foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
            {
                if (inserted)
                {
                    valueList += ", ";
                    fieldList += ", ";
                }
                fieldList += "`" + p.Item2.DbFieldName + "`";

                valueList += "?";

                types.Add((MySqlDbType)p.Item2.DbFieldType);
                inserted = true;
            }

            string query = $"INSERT INTO `{tableName}` ( {fieldList} ) VALUES ( {valueList} )";

            PrepareStatement(statementId, query, types);
        }

        private void ConstructGetAggregateStatement<T1>(T1 id, Type type)
        {
            uint statementId = Convert.ToUInt32(id);
            DbTableAttribute dbTable = GetDbTableAttribute(type);
            DbGetAggregateAttribute getAggregate = type.GetCustomAttributes(false)?.OfType<DbGetAggregateAttribute>()?.FirstOrDefault(d => d.ConstructedStatementId == statementId);

            if (dbTable == null)
                Debug.Assert(false, $"Statement Construction failed for type {type}");

            if (getAggregate == null)
                Debug.Assert(false, $"Statement Construction failed for type {type}");

            List<MySqlDbType> types = new List<MySqlDbType>();
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
            string tableName = getAggregate.TableName;
            string aggregatFunction = getAggregate.AggregatFunction;
            string aggregateList = null;

            foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
            {
                if (getAggregate.ParameterFields.Contains(p.Item2.DbFieldName))
                {
                    if (aggregateList != null)
                        aggregateList += ", ";
                    aggregateList += aggregatFunction + "(`" + p.Item2.DbFieldName + "`)";
                    types.Add((MySqlDbType)p.Item2.DbFieldType);
                }
            }

            string query = $"SELECT {aggregateList} FROM `{tableName}`";

            PrepareStatement(statementId, query, types);
        }

        public void ConstructStatement<T1>(T1 id, Type type, ConstructedStatementType statementType)
        {
            if (statementType == ConstructedStatementType.GetList)
            {
                ConstructGetListStatement(id, type);
                return;
            }
            if (statementType == ConstructedStatementType.GetAggregate)
            {
                ConstructGetAggregateStatement(id, type);
                return;
            }
            if (statementType == ConstructedStatementType.DeleteList)
            {
                ConstructDeleteListStatement(id, type);
                return;
            }
            if (statementType == ConstructedStatementType.InsertList)
            {
                ConstructInsertListStatement(id, type);
                return;
            }

            if (statementType == ConstructedStatementType.Delete)
            {
                ConstructDeleteStatement(id, type);
                return;
            }

            DbTableAttribute dbTable = GetDbTableAttribute(type);

            string query = "";
            string tableName = dbTable.DbTableName;
            List<MySqlDbType> types = new List<MySqlDbType>();
            List<MySqlDbType> criteriaTypes = new List<MySqlDbType>();

            string updateList = null;
            string insertList = null;
            string insertValues = null;
            string selectList = null;
            string whereList = null;

            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
            foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
            {
#if DBDEBUG
                log.Debug("P1: " + p.Item1 + " P2: " + p.Item2);
#endif
                if (p.Item2.Get)
                {
                    if (selectList != null)
                        selectList += ", ";
                    selectList += "`" + p.Item2.DbFieldName + "`";
                }

                if (p.Item2.Update && statementType == ConstructedStatementType.Update)
                {
                    if (updateList != null)
                        updateList += ", ";
                    updateList += "`" + p.Item2.DbFieldName + "` = ?";
                    types.Add((MySqlDbType)p.Item2.DbFieldType);
                }

                if (p.Item2.Insert && statementType == ConstructedStatementType.Insert)
                {
                    if (insertList != null)
                        insertList += ", ";
                    insertList += "`" + p.Item2.DbFieldName + "`";

                    if (insertValues != null)
                        insertValues += ", ";
                    insertValues += "?";

                    types.Add((MySqlDbType)p.Item2.DbFieldType);
                }

                if (p.Item2.IsCriteria)
                {
                    if (whereList != null)
                        whereList += " AND ";
                    whereList += "`" + p.Item2.DbFieldName + "` = ?";

                    if (statementType == ConstructedStatementType.Get || statementType == ConstructedStatementType.Update)
                        criteriaTypes.Add((MySqlDbType)p.Item2.DbFieldType);
                }
            }

            types.AddRange(criteriaTypes);

            switch (statementType)
            {
                case ConstructedStatementType.Get:
                    query = $"SELECT {selectList} FROM `{tableName}` WHERE {whereList}";
                    break;
                case ConstructedStatementType.Insert:
                    query = $"INSERT INTO `{tableName}` ({insertList}) VALUES ({insertValues})";
                    break;
                case ConstructedStatementType.Update:
                    query = $"UPDATE `{tableName}` SET {updateList} WHERE {whereList}";
                    break;
                case ConstructedStatementType.Delete:
                    query = $"DELETE `{tableName}` WHERE {whereList}";
                    break;
            }
#if DBDEBUG
            log.Debug("Id: " + Convert.ToUInt32(id) + "Query: " + query);
            foreach (MySqlDbType name in types)
            {
                log.Debug("Types: + " + name.ToString());
            }
#endif
            PrepareStatement(Convert.ToUInt32(id), query, types);
        }

        public bool ExecuteConstructedGetStatement<T1, T2>(T2 id, Dictionary<string, object> criteria, object instance) where T1 : class
        {
            // Debug.Assert(typeof(T1) == preparedStatementType);

            StoredPreparedStatement preparedStatement;
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(typeof(T1));
                        foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
                        {
                            if (p.Item2.IsCriteria)
                            {
                                if (criteria.ContainsKey(p.Item2.DbFieldName))
                                    command.Parameters.Add("", (MySqlDbType)p.Item2.DbFieldType).Value = criteria[p.Item2.DbFieldName];
                                else
                                    command.Parameters.Add("", (MySqlDbType)p.Item2.DbFieldType).Value = DBNull.Value;
                            }
                        }

                        connection.Open();
                        using (MySqlDataReader commandReader = command.ExecuteReader(CommandBehavior.Default))
                        {
                            if (commandReader.Read())
                            {
                                ReadObject<T1>(commandReader, instance as T1);

                                return true;
                            }
                        }
                    }
                }
            }
            catch (MySqlException exception)
            {
                log.Error($"An exception occurred while executing prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
            }

            return false;
        }

        public List<T2> ExecuteConstructedGetListStatement<T1, T2>(T1 id, Dictionary<string, object> criteria) where T2 : class
        {
            List<T2> results = new List<T2>();

            StoredPreparedStatement preparedStatement;
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out preparedStatement))
            {
                AutoRedeployment();
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(typeof(T2));
                        foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
                        {
                            // Add field name and value parameters
                            if (criteria.ContainsKey(p.Item2.DbFieldName))
                            {
                                command.Parameters.Add("", (MySqlDbType)p.Item2.DbFieldType).Value = criteria[p.Item2.DbFieldName];
                            }
                        }
                        connection.Open();
                        using (MySqlDataReader commandReader = command.ExecuteReader(CommandBehavior.Default))
                        {
                            while (commandReader.Read())
                            {
                                results.Add(ReadObject<T2>(commandReader));
                            }
                        }
                    }
                }
            }
            catch (MySqlException exception)
            {
                log.Error($"An exception occurred while executing prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
            }

            return results;
        }

        protected T ReadObject<T>(MySqlDataReader commandReader, T o = null) where T : class
        {
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(typeof(T));

            if (o == null)
                o = Activator.CreateInstance<T>();

            foreach (Tuple<PropertyInfo, DbFieldAttribute> p in properties)
            {
                object assignable = commandReader[p.Item2.DbFieldName];
                if (Convert.IsDBNull(assignable))
                {
                    p.Item1.SetValue(o, null);
                }
                else
                {
                    p.Item1.SetValue(o, assignable);
                }
            }
            return o;
        }

        public T3 ExecuteConstructedGetAggregateStatement<T1, T2, T3>(T1 id)
        {
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        connection.Open();
                        using (MySqlDataReader commandReader = command.ExecuteReader(CommandBehavior.Default))
                        {
                            if (commandReader.Read())
                            {
                                // TODO: Extend this to read multiple Aggregate functions if/when there is a use case for this
                                if (commandReader[0] == DBNull.Value)
                                    return default(T3);
                                else
                                    return (T3)commandReader[0];
                            }
                        }
                    }
                }
            }
            catch (MySqlException exception)
            {
                log.Error($"An exception occurred while executing prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
            }

            return default(T3);
        }

        public bool ExecuteConstructedInsertStatement<T1>(T1 id, Type type, object instance)
        {
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
                        properties.Where(p => p.Item2.Insert).ToList().ForEach(p => command.Parameters.Add("", (MySqlDbType)p.Item2.DbFieldType).Value = p.Item1.GetValue(instance));

                        connection.Open();

                        DbTableAttribute dbTable = GetDbTableAttribute(type);
                        if (!dbTable.HasAutoGeneratedId)
                        {
                            return command.ExecuteNonQuery() > 0;
                        }
                        command.CommandText += "; SELECT LAST_INSERT_ID();";
                        // back fill the generated id
                        using (MySqlDataReader commandReader = command.ExecuteReader(CommandBehavior.Default))
                        {
                            if (commandReader.Read())
                            {
                                uint generatedId = Convert.ToUInt32(commandReader[0].ToString());
                                Tuple<PropertyInfo, DbFieldAttribute> prop = properties.FirstOrDefault(p => p.Item2.DbFieldName == dbTable.AutoGeneratedIdColumn);
                                prop?.Item1.SetValue(instance, generatedId);
                            }
                            else
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            }
            catch (MySqlException exception)
            {
                log.Error($"An exception occurred while executing prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
            }

            return false;
        }

        public bool ExecuteConstructedUpdateStatement<T1>(T1 id, Type type, object instance)
        {
            // Debug.Assert(typeof(T1) == preparedStatementType);

            StoredPreparedStatement preparedStatement;
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);

                        // add all the parameters to be updated
                        properties.Where(p => p.Item2.Update).ToList().ForEach(p => command.Parameters.Add("", (MySqlDbType)p.Item2.DbFieldType).Value = p.Item1.GetValue(instance));

                        // criteria (where clause) is at the end
                        properties.Where(p => p.Item2.IsCriteria).ToList().ForEach(p => command.Parameters.Add("", (MySqlDbType)p.Item2.DbFieldType).Value = p.Item1.GetValue(instance));

                        connection.Open();
                        return command.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (MySqlException exception)
            {
                log.Error($"An exception occurred while executing prepared statement {id}!");
                log.Error($"Exception: {exception.ToString()}");
            }

            return false;
        }

        public bool ExecuteConstructedDeleteStatement<T1>(T1 id, Type type, Dictionary<string, object> criteria)
        {
            StoredPreparedStatement preparedStatement;
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(type);
                        properties.Where(p => p.Item2.IsCriteria).ToList().ForEach(p => command.Parameters.Add("", (MySqlDbType)p.Item2.DbFieldType).Value = criteria[p.Item2.DbFieldName]);

                        connection.Open();
                        return command.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (MySqlException exception)
            {
                log.Error($"An exception occurred while executing prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
                throw;
            }
        }

        protected void ExecutePreparedStatement<T>(T id, params object[] parameters)
        {
            ExecutePreparedStatement(false, id, parameters);
        }

        protected async Task ExecutePreparedStatementAsync<T>(T id, params object[] parameters)
        {
            await Task.Run(() => ExecutePreparedStatement(true, id, parameters));
        }

        private async void ExecutePreparedStatement<T>(bool async, T id, params object[] parameters)
        {
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
#if DBDEBUG
                        log.Debug(preparedStatement.Query);
#endif
                        for (int i = 0; i < preparedStatement.Types.Count; i++)
                        {
#if DBDEBUG
                            log.Debug(preparedStatement.Types[i]);
                            foreach (MySqlParameter p in command.Parameters)
                            {
                                log.Debug(p.Value);
                            }
#endif
                            command.Parameters.Add("", preparedStatement.Types[i]).Value = parameters[i];
#if DBDEBUG
                            log.Debug(command.Parameters);
#endif
                        }
                        if (async)
                        {
                            await connection.OpenAsync();
                            await Task.Run(() => command.ExecuteNonQuery()); // by default ExecuteNonQueryAsync is blocking
                        }
                        else
                        {
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (MySqlException exception)
            {
                log.Error($"An exception occurred while executing prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
            }
        }

        protected MySqlResult SelectPreparedStatement<T>(T id, params object[] parameters)
        {
            // Debug.Assert(typeof(T) == PreparedStatementType, "Invalid prepared statement type.");

            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        for (int i = 0; i < preparedStatement.Types.Count; i++)
                            command.Parameters.Add("", preparedStatement.Types[i]).Value = parameters[i];

                        using (MySqlDataReader commandReader = command.ExecuteReader(CommandBehavior.Default))
                        {
                            using (MySqlResult result = new MySqlResult())
                            {
                                result.Load(commandReader);
                                result.Count = (uint)result.Rows.Count;
                                return result;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error($"An exception occurred while selecting prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
            }

            return null;
        }

        protected async Task<MySqlResult> SelectPreparedStatementAsync<T>(T id, params object[] parameters)
        {
            if (!preparedStatements.TryGetValue(Convert.ToUInt32(id), out StoredPreparedStatement preparedStatement))
            {
                Debug.Assert(preparedStatement != null, "Invalid prepared statement id.");
            }

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(preparedStatement.Query, connection))
                    {
                        for (int i = 0; i < preparedStatement.Types.Count; i++)
                            command.Parameters.Add("", preparedStatement.Types[i]).Value = parameters[i];

                        await connection.OpenAsync();
                        return await Task.Run(() =>
                        {
                            using (MySqlDataReader commandReader = command.ExecuteReader(CommandBehavior.Default))
                            {
                                using (MySqlResult result = new MySqlResult())
                                {
                                    result.Load(commandReader);
                                    result.Count = (uint)result.Rows.Count;
                                    return result;
                                }
                            }
                        });
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error($"An exception occurred while selecting prepared statement {id}!");
                log.Error($"Exception: {exception.Message}");
            }

            return null;
        }

        protected static List<Tuple<PropertyInfo, DbFieldAttribute>> GetPropertyCache(Type t)
        {
            if (PropertyCache.ContainsKey(t))
                return PropertyCache[t].ToList(); // always return a copy

            List<Tuple<PropertyInfo, DbFieldAttribute>> newValue;

            lock (propertyCacheMutex)
            {
                if (!PropertyCache.ContainsKey(t))
                {
                    newValue = new List<Tuple<PropertyInfo, DbFieldAttribute>>();
                    IEnumerable<PropertyInfo> properties = t.GetProperties().Where(prop => prop.IsDefined(typeof(DbFieldAttribute), false));
                    foreach (PropertyInfo p in properties)
                    {
                        DbFieldAttribute f = p.GetAttributeOfType<DbFieldAttribute>();
                        newValue.Add(new Tuple<PropertyInfo, DbFieldAttribute>(p, f));
                    }

                    PropertyCache.Add(t, newValue);
                }
            }

            newValue = PropertyCache[t].ToList();

            return newValue;
        }

        private void PrepareStatement(uint id, string query, List<MySqlDbType> types)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        types.ForEach(t => command.Parameters.Add("", t));
#if DBDEBUG
                        log.Debug(types);
                        log.Debug(query);
#endif
                        command.Prepare();

                        uint Id = Convert.ToUInt32(id);
                        preparedStatements.Add(Id, new StoredPreparedStatement(Id, query, types.ToArray()));
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error($"An exception occurred while preparing statement {id}!");
                log.Error($"Exception: {exception.Message}");
                AutoRedeployment();
            }
        }

        protected void AutoRedeployment()
        {
            if (ConfigManager.Config.Database.EnableAutoRedeployment)
            {
                if (Redeploy.CanDeploy)
                {
                    if (Redeploy.GetResources(ResourceSelectionOption.LocalDisk)?.Count >= 4 &&
                        capturedSqlError?.Length == 0)
                    {
                        Redeploy.RedeployDatabaseFromSource(selectedDatabase, ResourceSelectionOption.LocalDisk);
                    }
                    else
                    {
                        // Reset the failed error string:
                        capturedSqlError = string.Empty;
                        Redeploy.SetToken();

                        if (Redeploy.DownloadFailure != true)
                        {
                            var redeploymentResult =
                                Redeploy.RedeployDatabaseFromSource(selectedDatabase, ResourceSelectionOption.Gitlab);
                            Debug.Assert(redeploymentResult == null,
                                $"Issues with Auto Redeployment: {redeploymentResult}");
                        }
                    }
                }
            }
            else
            {
                string errorMsg =
                    "Cannot start database automation; for the application too attempt too correct itself, please check your config and set EnableAutoRedeployment, too true.";
                log.Error(errorMsg);
                Debug.Assert((ConfigManager.Config.Database.EnableAutoRedeployment == true),
                    $"Issues with Auto Redeployment: {errorMsg}");
            }            
        }

        protected DbTableAttribute GetDbTableAttribute(Type type)
        {
            if (!DbTableCache.ContainsKey(type))
            {
                DbTableAttribute dbTable = type.GetCustomAttributes(false)?.OfType<DbTableAttribute>()?.FirstOrDefault();
                Debug.Assert(dbTable != null, $"Statement Construction failed for type {type}");
                DbTableCache.Add(type, dbTable);
            }

            return DbTableCache[type];
        }

        protected string EscapeStringLiteral(string input)
        {
            string result = input.Replace(@"\", @"\\");
            result = result.Replace("'", @"\'");
            result = result.Replace("\"", "\\\"");
            result = result.Replace("%", "\\%");
            result = result.Replace("_", "\\_");
            result = result.Replace("\n", "\\n");
            result = result.Replace("\r", "\\r");
            result = result.Replace("\0", ""); // just remove null characters
            return result;
        }

        protected T ExecuteDynamicGet<T>(Dictionary<string, MySqlParameter> criteria) where T : class
        {
            List<Tuple<PropertyInfo, DbFieldAttribute>> properties = GetPropertyCache(typeof(T));
            DbTableAttribute dbTable = GetDbTableAttribute(typeof(T));
            string sql = "SELECT " + string.Join(", ", properties.Select(p => "`v`." + p.Item2.DbFieldName)) + " FROM " + dbTable.DbTableName + " `v`";

            string where = null;

            foreach (KeyValuePair<string, MySqlParameter> p in criteria)
            {
                where = where == null ? " WHERE " : where + " AND ";
                where += $"`{p.Key}`= ?";
            }

            sql += (where ?? "");

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(sql, connection))
                {
                    criteria.Values.ToList().ForEach(p => command.Parameters.Add(p));

                    connection.Open();
                    using (MySqlDataReader commandReader = command.ExecuteReader(CommandBehavior.Default))
                    {
                        if (commandReader.Read())
                        {
                            return ReadObject<T>(commandReader);
                        }
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Executs a single Sql query against a specific database.
        /// </summary>
        /// <remarks>This function needs to be converted to common database functions, like prepared/constructed statements.</remarks>
        public string ExecuteSqlQueryOrScript(string query, string databaseName, bool splitQuery)
        {
            string result = "";
            try
            {
                // This needs to be refactored, code no longer uses mysql script:
                if (splitQuery)
                    RunLargeQuery(query, databaseName);
                else
                    RunQuery(query, databaseName);
            }
            catch (SqlException ex)
            {
                string errMsg = $"SQL Error in the downloaded data: {ex.Message}";
#if DEBUG
                Console.WriteLine(errMsg);
#endif
                return errMsg;
            }
            catch (MySqlException ex)
            {
                string errMsg = "Error: ";
                // get the number from the inner exception
                if (ex.InnerException != null)
                {
                    int mySqlErrorNumber = GetExceptionNumber(ex);
                    // create a short error message for the console log / label
                    errMsg += $"{mySqlErrorNumber} : {ex.InnerException.Message}";
                }
                else
                {
                    errMsg += $"{ex.Message}";
                }
#if DEBUG
                // long message to console
                Console.WriteLine(errMsg);
#endif
                return errMsg;
            }
            catch (Exception e)
            {
                string errMsg = $"Error: {e.Message}";
#if DEBUG
                Console.WriteLine(errMsg);
#endif
                return errMsg;
            }

            if (capturedSqlError?.Length > 0)
            {
                log.Error($"Found error in script: {capturedSqlError}");
                var returnMessage = capturedSqlError;
                return returnMessage;
            }

            return $"{result}";
        }

        /// <summary>
        /// Executes a single query.
        /// </summary>
        private void RunQuery(string query, string databaseName)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            using (MySqlCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = query;
                MySqlDataReader reader = command.ExecuteReader();
                connection.Close();
            }
        }

        /// <summary>
        /// Splits a string containing multiple queries, into a list of individual queries.
        /// </summary>
        private List<string> SplitInsertsInQuery(string query, string insertLine)
        {
            var inserts = query.SubstringAfter("VALUES ", StringComparison.InvariantCultureIgnoreCase).Split('\r');
            List<string> newLines = new List<string>();
            int insertCount = 0;
            var lineBuffer = string.Empty;
            var schemaBuffer = string.Empty;
            int insertThreshold = ConfigManager.Config.Database?.DataImportSpeed ?? 1999;
            bool addingSchema = false;
            int totalLines = 0;

            foreach (var line in inserts)
            {
                if (line.Length > 1)
                {
                    // reset threshold.
                    if (line.Contains("\n/*Table"))
                    {
                        // Turn on "schema" mode
                        addingSchema = true;

                        // Capture buffer, if data is present:
                        if (lineBuffer?.Length > 0)
                        {
                            newLines.Add(lineBuffer);
                        }
                        lineBuffer = string.Empty;
                    }

                    // Add schema without affecting total, until we are finished.
                    if (addingSchema)
                    {
                        // Strip newline in begining
                        schemaBuffer += line.Replace("\n", "");
                        // Increase the total, not the lineCount
                        totalLines++;

                        // Reached end of schema, abort:
                        if (line.Contains("\n/*Data"))
                        {
                            newLines.Add(schemaBuffer);
                            // totalLines
                            totalLines += insertCount;
                            // Reset count too add insert:
                            insertCount = 0;
                            addingSchema = false;
                            // Clear Schema
                            schemaBuffer = string.Empty;
                            continue;
                        }

                        // Continue, only if we have more lines (able too continue check):
                        if (addingSchema && ((totalLines + 1) <= inserts?.Count()))
                            continue;
                    }

                    // Begining an Insert line
                    if (insertCount == 0)
                    {
                        if (lineBuffer.Length == 0)
                            lineBuffer = line.Replace("\n(", "INSERT INTO " + insertLine + " VALUES (");
                        else
                            lineBuffer += line;
                        insertCount++;
                    }
                    else if (insertCount < insertThreshold)
                    {
                        // Remove newlines at the begining of a query
                        lineBuffer += line.Replace("\n(", "(");
                        insertCount++;
                    }
                    else
                    {
                        // End the insert:
                        lineBuffer += (line.Replace("\n(", "(").Replace(@"),", ");"));
                        newLines.Add(lineBuffer);
                        // save total lines:
                        totalLines += insertCount;
                        // Clear buffer
                        insertCount = 0;
                        lineBuffer = string.Empty;
                    }
                }
            }

            // Capture last line
            if (lineBuffer.Length > 0)
                newLines.Add(lineBuffer.Replace("\n", string.Empty).Replace(@";/", ";" + Environment.NewLine + "/"));

            return newLines;
        }

        private void RunLargeQuery(string allQueries, string databaseName)
        {
            List<string> splitDataQueries = new List<string>();

            // Split up larger files:
            if (allQueries.Length > 14000000)
            {
                // Check if for the first insert:
                if (allQueries.IndexOf("insert", StringComparison.InvariantCultureIgnoreCase) > 0)
                {
                    log.Info("Splitting has begun.. please be patient, this may take a few moments...");
                    // Utilize split
                    foreach (var querySplit in allQueries.Split(new string[] { "INSERT INTO ", "insert  into ", "insert " }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        // Check if values exist in data split:
                        if (querySplit.IndexOf("VALUES ", StringComparison.InvariantCultureIgnoreCase) > 0)
                        {
                            var insertLine = querySplit.SubstringBefore("VALUES", StringComparison.InvariantCultureIgnoreCase);
                            var insertData = querySplit.SubstringAfter("VALUES", StringComparison.InvariantCultureIgnoreCase);

                            // Only split after the first query:
                            if (splitDataQueries.Count > 0)
                            {
                                var finalQuery = SplitInsertsInQuery(insertData, insertLine);
                                splitDataQueries.AddRange(finalQuery);
                            }
                            else
                                // Adds the first/initial query without splitting:
                                splitDataQueries.Add(querySplit);
                        }
                        else
                            // Issues locating value, add entire line:
                            splitDataQueries.Add(querySplit);
                    }
                }
                else
                {
                    // No insert present, default:
                    splitDataQueries.Add(allQueries);
                }
            }
            else
            {
                // File too small to split, default:
                splitDataQueries.Add(allQueries);
            }

            log.Info("Loading database...");

            // Load queries:
            if (splitDataQueries?.Count > 0)
            {
                foreach (var query in splitDataQueries)
                {
#if DBDEBUG
                    log.Debug("Query: " + query);
#endif
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        MySqlDataReader result = null;
                        try
                        {
                            connection.Open();
                            command.CommandText = query;
                            result = command.ExecuteReader();
                            if (result?.RecordsAffected > 0)
                            {
                                log.Info($"Added '{result.RecordsAffected}' new records.");
                            }
                            connection.Close();
                        }
                        catch (MySqlException ex)
                        {
                            capturedSqlError = ex.Message;
                            log.Error($"Error: {ex.Message}");
                        }
                        finally
                        {
                            if (result != null)
                                result.Close();
                        }
                    }
                }
            }
        }

        public string GetDatabaseName(DatabaseSelectionOption databaseSelection)
        {
            string name = string.Empty;
            switch (databaseSelection)
            {
                case DatabaseSelectionOption.None:
                case DatabaseSelectionOption.All:
                    break;
                case DatabaseSelectionOption.Authentication:
                    name = ConfigManager.Config.Database.Authentication.Database;
                    break;
                case DatabaseSelectionOption.Shard:
                    name = ConfigManager.Config.Database.Shard.Database;
                    break;
            }
            return name;
        }

        /// <summary>
        /// Attempts to Drop a database, with a name collected a Form Textbox.
        /// </summary>
        /// <remarks>This function must NOT set a database in the connection string.</remarks>
        public string DropDatabase(string databaseName)
        {
            log.Debug($"Dropping database {databaseName}!!!");
            string dbQuery = "DROP DATABASE IF EXISTS `" + databaseName + "`;";
            string result = ExecuteSqlQueryOrScript(dbQuery, databaseName, false);
            if (result.Length > 0 && result != "0")
            {
                return result;
            }
            return null;
        }

        /// <summary>
        /// Attempts to create a database, with a name collected a Form Textbox.
        /// </summary>
        /// <remarks>This function must NOT set a database in the connection string.</remarks>
        public string CreateDatabase(string databaseName)
        {
            log.Debug($"Creating database {databaseName}...");
            string dbQuery = "CREATE DATABASE IF NOT EXISTS `" + databaseName + "` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            string result = ExecuteSqlQueryOrScript(dbQuery, "", false);
            // Only show result strings
            if (result.Length > 3)
            {
                return result;
            }
            return null;
        }

        /// <summary>
        /// Returns inner MySql Exception number.
        /// </summary>
        public static int GetExceptionNumber(MySqlException my)
        {
            if (my != null)
            {
                int number = my.Number;
                // if the number is zero, try to get the number of the inner exception
                if (number == 0 && (my = my.InnerException as MySqlException) != null)
                {
                    number = my.Number;
                }
                return number;
            }
            return -1;
        }
    }
}
