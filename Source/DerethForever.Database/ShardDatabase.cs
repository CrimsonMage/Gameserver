/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using MySql.Data.MySqlClient;
using log4net;

namespace DerethForever.Database
{
    public class ShardDatabase : Database, IShardDatabase
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private enum ShardPreparedStatement
        {
            GetDataObjectPropertiesInt,
            GetDataObjectPropertiesBigInt,
            GetDataObjectPropertiesDouble,
            GetDataObjectPropertiesBool,
            GetDataObjectPropertiesString,
            GetDataObjectPropertiesDid,
            GetDataObjectPropertiesIid,
            GetDataObjectPropertiesPositions,
            GetDataObjectPropertiesAttributes,
            GetDataObjectPropertiesAttributes2nd,
            GetDataObjectPropertiesSkills,
            GetDataObjectPropertiesSpell,
            GetDataObjectPropertiesBook,

            GetDataObjectsByContainerId,
            GetDataObjectsByWielderId,

            SaveDataObject,
            DeleteDataObject,
            GetDataObject,
            UpdateDataObject,

            DeleteDataObjectPropertiesInt,
            DeleteDataObjectPropertiesBigInt,
            DeleteDataObjectPropertiesDouble,
            DeleteDataObjectPropertiesBool,
            DeleteDataObjectPropertiesString,
            DeleteDataObjectPropertiesDid,
            DeleteDataObjectPropertiesIid,
            DeleteDataObjectPropertiesPositions,
            DeleteDataObjectPropertiesAttributes,
            DeleteDataObjectPropertiesAttributes2nd,
            DeleteDataObjectPropertiesSkills,
            DeleteDataObjectPropertiesSpell,
            DeleteDataObjectPropertiesBook,

            InsertDataObjectPropertiesInt,
            InsertDataObjectPropertiesBigInt,
            InsertDataObjectPropertiesDouble,
            InsertDataObjectPropertiesBool,
            InsertDataObjectPropertiesString,
            InsertDataObjectPropertiesDid,
            InsertDataObjectPropertiesIid,
            InsertDataObjectPropertiesPositions,
            InsertDataObjectPropertiesAttributes,
            InsertDataObjectPropertiesAttributes2nd,
            InsertDataObjectPropertiesSkills,
            InsertDataObjectPropertiesSpells,
            InsertDataObjectPropertiesBook,

            // note, this section is all "Property" singular
            UpdateDataObjectPropertyInt,
            UpdateDataObjectPropertyBigInt,
            UpdateDataObjectPropertyDouble,
            UpdateDataObjectPropertyBool,
            UpdateDataObjectPropertyString,
            UpdateDataObjectPropertyDid,
            UpdateDataObjectPropertyIid,
            UpdateDataObjectPropertyPosition,
            UpdateDataObjectPropertyAttribute,
            UpdateDataObjectPropertyAttribute2nd,
            UpdateDataObjectPropertySkill,

            DeleteDataObjectPropertyInt,
            DeleteDataObjectPropertyBigInt,
            DeleteDataObjectPropertyDouble,
            DeleteDataObjectPropertyBool,
            DeleteDataObjectPropertyString,
            DeleteDataObjectPropertyDid,
            DeleteDataObjectPropertyIid,
            DeleteDataObjectPropertyPosition,
            DeleteDataObjectPropertyAttribute,
            DeleteDataObjectPropertyAttribute2nd,
            DeleteDataObjectPropertySkill,

            GetObjectsByLandblock,

            GetContractTracker,
            GetSpellBarPositions,

            GetCharacters,
            IsCharacterNameAvailable,

            DeleteContractTrackers,
            DeleteSpellBarPositions,

            InsertContractTracker,
            InsertSpellBarPositions,

            UpdateContractTracker,

            DeleteContractTracker,

            GetCurrentId,
        }

        private void ConstructMaxQueryStatement(ShardPreparedStatement id, string tableName, string columnName)
        {
            // NOTE: when moved to WordDatabase, ace_shard needs to be changed to ace_world
            AddPreparedStatement(id, $"SELECT MAX(`{columnName}`) FROM `{tableName}` WHERE `{columnName}` >= ? && `{columnName}` < ?",
                MySqlDbType.UInt32, MySqlDbType.UInt32);
        }

        protected override void InitializePreparedStatements()
        {
            base.InitializePreparedStatements();

            ConstructStatement(ShardPreparedStatement.DeleteDataObject, typeof(DataObject), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.SaveDataObject, typeof(DataObject), ConstructedStatementType.Insert);
            ConstructStatement(ShardPreparedStatement.GetDataObject, typeof(DataObject), ConstructedStatementType.Get);
            ConstructStatement(ShardPreparedStatement.UpdateDataObject, typeof(DataObject), ConstructedStatementType.Update);

            ConstructStatement(ShardPreparedStatement.GetDataObjectsByContainerId, typeof(CachedInventoryObject), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectsByWielderId, typeof(CachedWieldedObject), ConstructedStatementType.GetList);

            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesBigInt, typeof(DataObjectPropertiesInt64), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesInt, typeof(DataObjectPropertiesInt), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesBool, typeof(DataObjectPropertiesBool), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesDouble, typeof(DataObjectPropertiesDouble), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesString, typeof(DataObjectPropertiesString), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesIid, typeof(DataObjectPropertiesInstanceId), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesDid, typeof(DataObjectPropertiesDataId), ConstructedStatementType.GetList);

            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesPositions, typeof(DataObjectPropertiesPosition), ConstructedStatementType.GetList);

            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesAttributes, typeof(DataObjectPropertiesAttribute), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesAttributes2nd, typeof(DataObjectPropertiesAttribute2nd), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesSkills, typeof(DataObjectPropertiesSkill), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesSpell, typeof(DataObjectPropertiesSpell), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetDataObjectPropertiesBook, typeof(DataObjectPropertiesBook), ConstructedStatementType.GetList);

            // Delete statements
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesInt, typeof(DataObjectPropertiesInt), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesBigInt, typeof(DataObjectPropertiesInt64), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesBool, typeof(DataObjectPropertiesBool), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesDouble, typeof(DataObjectPropertiesDouble), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesString, typeof(DataObjectPropertiesString), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesIid, typeof(DataObjectPropertiesInstanceId), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesDid, typeof(DataObjectPropertiesDataId), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesPositions, typeof(DataObjectPropertiesPosition), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesSkills, typeof(DataObjectPropertiesSkill), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesAttributes, typeof(DataObjectPropertiesAttribute), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesAttributes2nd, typeof(DataObjectPropertiesAttribute2nd), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesSpell, typeof(DataObjectPropertiesSpell), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertiesBook, typeof(DataObjectPropertiesBook), ConstructedStatementType.DeleteList);

            // Insert statements
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesInt, typeof(DataObjectPropertiesInt), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesBigInt, typeof(DataObjectPropertiesInt64), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesBool, typeof(DataObjectPropertiesBool), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesDouble, typeof(DataObjectPropertiesDouble), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesString, typeof(DataObjectPropertiesString), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesIid, typeof(DataObjectPropertiesInstanceId), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesDid, typeof(DataObjectPropertiesDataId), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesPositions, typeof(DataObjectPropertiesPosition), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesSkills, typeof(DataObjectPropertiesSkill), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesAttributes, typeof(DataObjectPropertiesAttribute), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesAttributes2nd, typeof(DataObjectPropertiesAttribute2nd), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesSpells, typeof(DataObjectPropertiesSpell), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertDataObjectPropertiesBook, typeof(DataObjectPropertiesBook), ConstructedStatementType.InsertList);

            // Updates
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyInt, typeof(DataObjectPropertiesInt), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyBigInt, typeof(DataObjectPropertiesInt64), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyBool, typeof(DataObjectPropertiesBool), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyDouble, typeof(DataObjectPropertiesDouble), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyString, typeof(DataObjectPropertiesString), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyIid, typeof(DataObjectPropertiesInstanceId), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyDid, typeof(DataObjectPropertiesDataId), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyPosition, typeof(DataObjectPropertiesPosition), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertySkill, typeof(DataObjectPropertiesSkill), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyAttribute, typeof(DataObjectPropertiesAttribute), ConstructedStatementType.Update);
            ConstructStatement(ShardPreparedStatement.UpdateDataObjectPropertyAttribute2nd, typeof(DataObjectPropertiesAttribute2nd), ConstructedStatementType.Update);

            // deletes for properties
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyInt, typeof(DataObjectPropertiesInt), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyBigInt, typeof(DataObjectPropertiesInt64), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyBool, typeof(DataObjectPropertiesBool), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyDouble, typeof(DataObjectPropertiesDouble), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyString, typeof(DataObjectPropertiesString), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyIid, typeof(DataObjectPropertiesInstanceId), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyDid, typeof(DataObjectPropertiesDataId), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyPosition, typeof(DataObjectPropertiesPosition), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertySkill, typeof(DataObjectPropertiesSkill), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyAttribute, typeof(DataObjectPropertiesAttribute), ConstructedStatementType.Delete);
            ConstructStatement(ShardPreparedStatement.DeleteDataObjectPropertyAttribute2nd, typeof(DataObjectPropertiesAttribute2nd), ConstructedStatementType.Delete);

            ConstructStatement(ShardPreparedStatement.IsCharacterNameAvailable, typeof(CachedCharacter), ConstructedStatementType.Get);

            ConstructStatement(ShardPreparedStatement.GetCharacters, typeof(CachedCharacter), ConstructedStatementType.GetList);

            ConstructStatement(ShardPreparedStatement.GetContractTracker, typeof(DataContractTracker), ConstructedStatementType.GetList);
            ConstructStatement(ShardPreparedStatement.GetSpellBarPositions, typeof(DataObjectPropertiesSpellBarPositions), ConstructedStatementType.GetList);

            // Delete statements
            ConstructStatement(ShardPreparedStatement.DeleteContractTrackers, typeof(DataContractTracker), ConstructedStatementType.DeleteList);
            ConstructStatement(ShardPreparedStatement.DeleteSpellBarPositions, typeof(DataObjectPropertiesSpellBarPositions), ConstructedStatementType.DeleteList);

            // Insert statements
            ConstructStatement(ShardPreparedStatement.InsertContractTracker, typeof(DataContractTracker), ConstructedStatementType.InsertList);
            ConstructStatement(ShardPreparedStatement.InsertSpellBarPositions, typeof(DataObjectPropertiesSpellBarPositions), ConstructedStatementType.InsertList);

            // Updates
            ConstructStatement(ShardPreparedStatement.UpdateContractTracker, typeof(DataContractTracker), ConstructedStatementType.Update);

            // deletes for properties
            ConstructStatement(ShardPreparedStatement.DeleteContractTracker, typeof(DataContractTracker), ConstructedStatementType.Delete);

            // FIXME(ddevec): Use max/min values defined in factory -- this is just for demonstration purposes
            ConstructMaxQueryStatement(ShardPreparedStatement.GetCurrentId, "ace_object", "AceObjectId");
        }

        protected DataObject GetObjectOfType<T>(uint objId) where T : DataObject
        {
            T ret = (T)Activator.CreateInstance(typeof(T));

            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", objId } };
            if (!ExecuteConstructedGetStatement<DataObject, ShardPreparedStatement>(ShardPreparedStatement.GetDataObject, criteria, ret))
                return null;

            LoadIntoObject(ret);

            ret.ClearDirtyFlags();

            return ret;
        }

        public bool SaveObject(DataObject dataObject)
        {
            return SaveOrReplaceObject(dataObject);
        }

        protected bool SaveOrReplaceObject(DataObject dataObject, bool replace = true)
        {
            DatabaseTransaction transaction = BeginTransaction();

            SaveObjectInTransaction(transaction, dataObject, replace);

            return transaction.Commit().Result;
        }

        public bool DeleteObject(DataObject dataObject)
        {
            DatabaseTransaction transaction = BeginTransaction();

            DeleteObjectInTransaction(transaction, dataObject);

            return transaction.Commit().Result;
        }

        protected void LoadIntoObject(DataObject dataObject)
        {
            // this flag determines when the subsequent calls to "save" trigger an insert or an update
            dataObject.HasEverBeenSavedToDatabase = true;

            // TODO: still to implement - load spells, friends, allegiance info, spell comps
            dataObject.IntProperties = GetDataObjectPropertiesInt(dataObject.DataObjectId);
            dataObject.Int64Properties = GetDataObjectPropertiesBigInt(dataObject.DataObjectId);
            dataObject.BoolProperties = GetDataObjectPropertiesBool(dataObject.DataObjectId);
            dataObject.DoubleProperties = GetDataObjectPropertiesDouble(dataObject.DataObjectId);
            dataObject.StringProperties = GetDataObjectPropertiesString(dataObject.DataObjectId);
            dataObject.InstanceIdProperties = GetDataObjectPropertiesIid(dataObject.DataObjectId);
            dataObject.DataIdProperties = GetDataObjectPropertiesDid(dataObject.DataObjectId);
            dataObject.Positions = GetDataObjectPostions(dataObject.DataObjectId);
            dataObject.DataObjectPropertiesAttributes = GetDataObjectPropertiesAttribute(dataObject.DataObjectId).ToDictionary(x => (Ability)x.AttributeId,
                x => new CreatureAbility(x));
            dataObject.DataObjectPropertiesAttributes2nd = GetDataObjectPropertiesAttribute2nd(dataObject.DataObjectId).ToDictionary(x => (Ability)x.Attribute2ndId,
                x => new CreatureVital(dataObject, x));
            dataObject.DataObjectPropertiesSkills = GetDataObjectPropertiesSkill(dataObject.DataObjectId).Select(s => new CreatureSkill(dataObject, s)).ToList();
            dataObject.SpellIdProperties = GetDataObjectPropertiesSpell(dataObject.DataObjectId);
            dataObject.BookProperties = GetDataObjectPropertiesBook(dataObject.DataObjectId);

            dataObject.TrackedContracts = GetContractList(dataObject.DataObjectId).ToDictionary(x => x.ContractId, x => x);
            dataObject.SpellsInSpellBars = GetSpellBarPositions(dataObject.DataObjectId);

            dataObject.Inventory = GetInventoryByContainerId(dataObject.DataObjectId);

            foreach (KeyValuePair<ObjectGuid, DataObject> invItem in dataObject.Inventory)
            {
                if (invItem.Value.WeenieType == (uint)WeenieType.Container)
                    invItem.Value.Inventory = GetInventoryByContainerId(invItem.Key.Full);
            }

            dataObject.WieldedItems = GetItemsByWielderId(dataObject.DataObjectId);
        }

        protected virtual bool DeleteObjectInTransaction(DatabaseTransaction transaction, DataObject dataObject)
        {
            // TODO: Database is designed to cascade delete so in reality we only need to call DeleteDataObjectBase
            // We need to decide to either let the db do the work or if we are going to keep doing it on the application side
            // should we remove the cascade deletes? Og II
            DeleteDataObjectPropertiesInt(transaction, dataObject.DataObjectId);
            DeleteDataObjectPropertiesBigInt(transaction, dataObject.DataObjectId, dataObject.Int64Properties);
            DeleteDataObjectPropertiesBool(transaction, dataObject.DataObjectId, dataObject.BoolProperties);
            DeleteDataObjectPropertiesDouble(transaction, dataObject.DataObjectId, dataObject.DoubleProperties);
            DeleteDataObjectPropertiesString(transaction, dataObject.DataObjectId, dataObject.StringProperties);
            DeleteDataObjectPropertiesIid(transaction, dataObject.DataObjectId, dataObject.InstanceIdProperties);
            DeleteDataObjectPropertiesDid(transaction, dataObject.DataObjectId, dataObject.DataIdProperties);

            DeleteDataObjectPropertiesPositions(transaction, dataObject.DataObjectId);
            DeleteDataObjectPropertiesAttributes(transaction, dataObject.DataObjectId);
            DeleteDataObjectPropertiesAttribute2nd(transaction, dataObject.DataObjectId);
            DeleteDataObjectPropertiesSkill(transaction, dataObject.DataObjectId);
            DeleteDataObjectPropertiesSpells(transaction, dataObject.DataObjectId);

            if (!DeleteObjectDependencies(transaction, dataObject))
                return false;

            DeleteDataObjectBase(transaction, dataObject);
            return true;
        }

        protected bool DeleteObjectDependencies(DatabaseTransaction transaction, DataObject dataObject)
        {
            DeleteContractTrackers(transaction, dataObject.DataObjectId);
            DeleteSpellBarPositions(transaction, dataObject.DataObjectId);

            // Do we have any  - if not, we are done here?
            if (dataObject.Inventory.Count <= 0)
                return true;

            foreach (DataObject invItem in dataObject.Inventory.Values)
            {
                DeleteObjectInTransaction(transaction, invItem);
                // Was the item I just deleted a container?   If so, we need to delete the items in the container as well. Og II
                if (invItem.WeenieType != (uint)WeenieType.Container)
                    continue;

                foreach (DataObject contInvItem in invItem.Inventory.Values)
                {
                    DeleteObjectInTransaction(transaction, contInvItem);
                }
            }
            return true;
        }

        protected virtual void SaveObjectInTransaction(DatabaseTransaction transaction, DataObject dataObject, bool replace = true)
        {
            // save the base object as needed for referential integrity
            SaveDataObjectBase(transaction, dataObject);

            if (replace)
            {
                // the api requires the replace functionality - we only ever get purely dirty objects, which means
                // we can't effectively track changes to the object graph in detail.  therefore, we blow away the
                // entire current weenie, mark all the data as dirty, and save everything with inserts.

                DeleteDataObjectPropertiesInt(transaction, dataObject.DataObjectId);
                DeleteDataObjectPropertiesBigInt(transaction, dataObject.DataObjectId, dataObject.Int64Properties);
                DeleteDataObjectPropertiesBool(transaction, dataObject.DataObjectId, dataObject.BoolProperties);
                DeleteDataObjectPropertiesDouble(transaction, dataObject.DataObjectId, dataObject.DoubleProperties);
                DeleteDataObjectPropertiesString(transaction, dataObject.DataObjectId, dataObject.StringProperties);
                DeleteDataObjectPropertiesIid(transaction, dataObject.DataObjectId, dataObject.InstanceIdProperties);
                DeleteDataObjectPropertiesDid(transaction, dataObject.DataObjectId, dataObject.DataIdProperties);
                DeleteDataObjectPropertiesSpells(transaction, dataObject.DataObjectId);
                DeleteDataObjectPropertiesAttributes(transaction, dataObject.DataObjectId);
                DeleteDataObjectPropertiesAttribute2nd(transaction, dataObject.DataObjectId);
                DeleteDataObjectPropertiesSkill(transaction, dataObject.DataObjectId);

                dataObject.IntProperties.ForEach(p =>
                {
                    p.HasEverBeenSavedToDatabase = false;
                    p.IsDirty = true;
                });

                dataObject.Int64Properties.ForEach(p =>
                {
                    p.HasEverBeenSavedToDatabase = false;
                    p.IsDirty = true;
                });

                dataObject.BoolProperties.ForEach(p =>
                {
                    p.HasEverBeenSavedToDatabase = false;
                    p.IsDirty = true;
                });

                dataObject.DoubleProperties.ForEach(p =>
                {
                    p.HasEverBeenSavedToDatabase = false;
                    p.IsDirty = true;
                });

                dataObject.StringProperties.ForEach(p =>
                {
                    p.HasEverBeenSavedToDatabase = false;
                    p.IsDirty = true;
                });

                dataObject.InstanceIdProperties.ForEach(p =>
                {
                    p.HasEverBeenSavedToDatabase = false;
                    p.IsDirty = true;
                });

                dataObject.DataIdProperties.ForEach(p =>
                {
                    p.HasEverBeenSavedToDatabase = false;
                    p.IsDirty = true;
                });

                dataObject.DataObjectPropertiesAttributes.Values.ToList().ForEach(p =>
                {
                    p.GetAttribute().HasEverBeenSavedToDatabase = false;
                    p.GetAttribute().IsDirty = true;
                });

                dataObject.DataObjectPropertiesAttributes2nd.Values.ToList().ForEach(p =>
                {
                    p.GetVital().HasEverBeenSavedToDatabase = false;
                    p.GetVital().IsDirty = true;
                });

                dataObject.DataObjectPropertiesSkills.ForEach(p =>
                {
                    // this is a hacky little hack required for stuff that comes in over the API
                    p.GetDataObjectSkill().DataObjectId = dataObject.DataObjectId;

                    p.GetDataObjectSkill().HasEverBeenSavedToDatabase = false;
                    p.GetDataObjectSkill().IsDirty = true;
                });
            }

            // book pages, spells, and locations aren't tracked as dirty, just blow away and reinsert
            DeleteDataObjectPropertiesBook(transaction, dataObject.DataObjectId);
            DeleteDataObjectPropertiesPositions(transaction, dataObject.DataObjectId);
            DeleteDataObjectPropertiesSpells(transaction, dataObject.DataObjectId);

            SaveDataObjectPropertiesInt(transaction, dataObject.DataObjectId, dataObject.IntProperties);
            SaveDataObjectPropertiesBigInt(transaction, dataObject.DataObjectId, dataObject.Int64Properties);
            SaveDataObjectPropertiesBool(transaction, dataObject.DataObjectId, dataObject.BoolProperties);
            SaveDataObjectPropertiesDouble(transaction, dataObject.DataObjectId, dataObject.DoubleProperties);
            SaveDataObjectPropertiesString(transaction, dataObject.DataObjectId, dataObject.StringProperties);
            SaveDataObjectPropertiesIid(transaction, dataObject.DataObjectId, dataObject.InstanceIdProperties);
            SaveDataObjectPropertiesDid(transaction, dataObject.DataObjectId, dataObject.DataIdProperties);
            SaveDataObjectPropertiesSpells(transaction, dataObject.DataObjectId, dataObject.SpellIdProperties);
            SaveDataObjectPropertiesAttributes(transaction, dataObject.DataObjectId, dataObject.DataObjectPropertiesAttributes);
            SaveDataObjectPropertiesAttribute2nd(transaction, dataObject.DataObjectId, dataObject.DataObjectPropertiesAttributes2nd);
            SaveDataObjectPropertiesSkill(transaction, dataObject.DataObjectId, dataObject.DataObjectPropertiesSkills);
            SaveDataObjectPropertiesBook(transaction, dataObject.DataObjectId, dataObject.BookProperties);
            SaveDataObjectPropertiesPositions(transaction, dataObject.DataObjectId, dataObject.Positions);

            // call to save objects that may have been added to the graph via inheritance
            SaveObjectDependencies(transaction, dataObject);
        }

        protected bool SaveObjectDependencies(DatabaseTransaction transaction, DataObject dataObject)
        {
            DeleteContractTrackers(transaction, dataObject.DataObjectId);
            SaveContractTracker(transaction, dataObject.DataObjectId, dataObject.TrackedContracts.Select(x => x.Value).ToList());

            DeleteSpellBarPositions(transaction, dataObject.DataObjectId);
            SaveDataObjectPropertiesSpellBarPositions(transaction, dataObject.SpellsInSpellBars);

            // Do we have any inventory to save - if not, we are done here?
            if (dataObject.Inventory.Count > 0)
            {
                foreach (DataObject invItem in dataObject.Inventory.Values)
                {
                    if (invItem.IsDirty)
                    {
                        DeleteObjectInTransaction(transaction, invItem);
                        invItem.SetDirtyFlags();
                    }
                    SaveObjectInTransaction(transaction, invItem);

                    // Was the item I just saved a container?   If so, we need to save the items in the container as well. Og II
                    if (invItem.WeenieType == (uint)WeenieType.Container && invItem.Inventory.Count > 0)
                    {
                        foreach (DataObject contInvItem in invItem.Inventory.Values)
                        {
                            if (contInvItem.IsDirty)
                            {
                                DeleteObjectInTransaction(transaction, contInvItem);
                                contInvItem.SetDirtyFlags();
                            }
                            SaveObjectInTransaction(transaction, contInvItem);
                        }
                    }
                }
            }

            // Do we have any wielded items to save - if so, let's save them.
            if (dataObject.WieldedItems.Count > 0)
            {
                foreach (DataObject wieldedItem in dataObject.WieldedItems.Values)
                {
                    if (wieldedItem.IsDirty)
                    {
                        DeleteObjectInTransaction(transaction, wieldedItem);
                        wieldedItem.SetDirtyFlags();
                    }
                    SaveObjectInTransaction(transaction, wieldedItem);
                }
            }

            return true;
        }

        private List<DataObjectPropertiesPosition> GetDataObjectPostions(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesPosition> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesPosition>(ShardPreparedStatement.GetDataObjectPropertiesPositions, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesSkill> GetDataObjectPropertiesSkill(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesSkill> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesSkill>(ShardPreparedStatement.GetDataObjectPropertiesSkills, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesAttribute> GetDataObjectPropertiesAttribute(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesAttribute> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesAttribute>(ShardPreparedStatement.GetDataObjectPropertiesAttributes, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesAttribute2nd> GetDataObjectPropertiesAttribute2nd(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesAttribute2nd> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesAttribute2nd>(ShardPreparedStatement.GetDataObjectPropertiesAttributes2nd, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesInt> GetDataObjectPropertiesInt(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesInt> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesInt>(ShardPreparedStatement.GetDataObjectPropertiesInt, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesInt64> GetDataObjectPropertiesBigInt(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesInt64> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesInt64>(ShardPreparedStatement.GetDataObjectPropertiesBigInt, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesBool> GetDataObjectPropertiesBool(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesBool> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesBool>(ShardPreparedStatement.GetDataObjectPropertiesBool, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesDouble> GetDataObjectPropertiesDouble(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesDouble> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesDouble>(ShardPreparedStatement.GetDataObjectPropertiesDouble, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesString> GetDataObjectPropertiesString(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesString> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesString>(ShardPreparedStatement.GetDataObjectPropertiesString, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesDataId> GetDataObjectPropertiesDid(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesDataId> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesDataId>(ShardPreparedStatement.GetDataObjectPropertiesDid, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesInstanceId> GetDataObjectPropertiesIid(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesInstanceId> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesInstanceId>(ShardPreparedStatement.GetDataObjectPropertiesIid, criteria);
            objects.ForEach(o =>
            {
                o.HasEverBeenSavedToDatabase = true;
                o.IsDirty = false;
            });
            return objects;
        }

        private List<DataObjectPropertiesSpell> GetDataObjectPropertiesSpell(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesSpell> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesSpell>(ShardPreparedStatement.GetDataObjectPropertiesSpell, criteria);
            return objects;
        }

        private List<DataObjectPropertiesBook> GetDataObjectPropertiesBook(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesBook> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesBook>(ShardPreparedStatement.GetDataObjectPropertiesBook, criteria);
            return objects;
        }

        private static void SaveDataObjectBase(DatabaseTransaction transaction, DataObject obj)
        {
            if (!obj.IsDirty)
                return;

            if (!obj.HasEverBeenSavedToDatabase)
                transaction.AddPreparedInsertStatement(ShardPreparedStatement.SaveDataObject, obj);
            else
                transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObject, obj);
        }

        // FIXME(ddevec): These are a lot of functions that essentially do the same thing... but the SharedPreparedStatement.--- makes them a pain to template/reduce
        private static void SaveDataObjectPropertiesInt(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesInt> properties)
        {
            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesInt> theDirtyOnes = properties.Where(p => p.IsDirty).ToList();
            theDirtyOnes.ForEach(p => p.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesInt prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (prop.PropertyValue == null)
                    {
                        // delete it
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesInt>(ShardPreparedStatement.DeleteDataObjectPropertyInt, prop);
                        properties.Remove(prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyInt, prop);
                    }
                }
                else if (prop.PropertyValue != null)
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesInt, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesBigInt(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesInt64> properties)
        {
            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesInt64> theDirtyOnes = properties.Where(p => p.IsDirty).ToList();
            theDirtyOnes.ForEach(p => p.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesInt64 prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (prop.PropertyValue == null)
                    {
                        // delete it
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesInt64>(ShardPreparedStatement.DeleteDataObjectPropertyBigInt, prop);
                        properties.Remove(prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyBigInt, prop);
                    }
                }
                else if (prop.PropertyValue != null)
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesBigInt, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesBool(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesBool> properties)
        {
            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesBool> theDirtyOnes = properties.Where(p => p.IsDirty).ToList();
            theDirtyOnes.ForEach(p => p.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesBool prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (prop.PropertyValue == null)
                    {
                        // delete it
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesBool>(ShardPreparedStatement.DeleteDataObjectPropertyBool, prop);
                        properties.Remove(prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyBool, prop);
                    }
                }
                else if (prop.PropertyValue != null)
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesBool, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesDouble(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesDouble> properties)
        {
            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesDouble> theDirtyOnes = properties.Where(p => p.IsDirty).ToList();
            theDirtyOnes.ForEach(p => p.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesDouble prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (prop.PropertyValue == null)
                    {
                        // delete it
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesDouble>(ShardPreparedStatement.DeleteDataObjectPropertyDouble, prop);
                        properties.Remove(prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyDouble, prop);
                    }
                }
                else if (prop.PropertyValue != null)
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesDouble, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesString(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesString> properties)
        {
            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesString> theDirtyOnes = properties.Where(p => p.IsDirty).ToList();
            theDirtyOnes.ForEach(p => p.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesString prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (string.IsNullOrWhiteSpace(prop.PropertyValue))
                    {
                        // delete it
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesString>(ShardPreparedStatement.DeleteDataObjectPropertyString, prop);
                        properties.Remove(prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyString, prop);
                    }
                }
                else if (!string.IsNullOrWhiteSpace(prop.PropertyValue))
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesString, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesDid(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesDataId> properties)
        {
            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesDataId> theDirtyOnes = properties.Where(p => p.IsDirty).ToList();
            theDirtyOnes.ForEach(p => p.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesDataId prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (prop.PropertyValue == null)
                    {
                        // delete it
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesDataId>(ShardPreparedStatement.DeleteDataObjectPropertyDid, prop);
                        properties.Remove(prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyDid, prop);
                    }
                }
                else if (prop.PropertyValue != null)
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesDid, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesIid(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesInstanceId> properties)
        {
            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesInstanceId> theDirtyOnes = properties.Where(p => p.IsDirty).ToList();
            theDirtyOnes.ForEach(p => p.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesInstanceId prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (prop.PropertyValue == null)
                    {
                        // delete it
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesInstanceId>(ShardPreparedStatement.DeleteDataObjectPropertyIid, prop);
                        properties.Remove(prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyIid, prop);
                    }
                }
                else if (prop.PropertyValue != null)
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesIid, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesSpells(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesSpell> properties)
        {
            properties?.ForEach(a => a.DataObjectId = dataObjectId);
            transaction.AddPreparedInsertListStatement(ShardPreparedStatement.InsertDataObjectPropertiesSpells, properties);
        }

        private static void SaveDataObjectPropertiesPositions(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesPosition> properties)
        {
            properties?.ForEach(p => p.DataObjectId = dataObjectId);
            transaction.AddPreparedInsertListStatement(ShardPreparedStatement.InsertDataObjectPropertiesPositions, properties);
        }

        private static void SaveDataObjectPropertiesAttributes(DatabaseTransaction transaction, uint dataObjectId, Dictionary<Ability, CreatureAbility> attributes)
        {
            List<DataObjectPropertiesAttribute> attribs = attributes.Values.Select(x => x.GetAttribute()).ToList();

            // setting DataObjectId doesn't trigger dirty
            attribs.ForEach(a => a.DataObjectId = dataObjectId);

            List<DataObjectPropertiesAttribute> theDirtyOnes = attribs.Where(x => x.IsDirty).ToList();

            foreach (DataObjectPropertiesAttribute prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    // update it
                    transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyAttribute, prop);
                }
                else
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesAttributes, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesAttribute2nd(DatabaseTransaction transaction, uint dataObjectId, Dictionary<Ability, CreatureVital> properties)
        {
            List<DataObjectPropertiesAttribute2nd> attribs = properties.Values.Select(x => x.GetVital()).ToList();

            // setting DataObjectId doesn't trigger dirty
            attribs.ForEach(a => a.DataObjectId = dataObjectId);

            List<DataObjectPropertiesAttribute2nd> theDirtyOnes = attribs.Where(x => x.IsDirty).ToList();

            foreach (DataObjectPropertiesAttribute2nd prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    // update it
                    transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertyAttribute2nd, prop);
                }
                else
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesAttributes2nd, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesSkill(DatabaseTransaction transaction, uint dataObjectId, List<CreatureSkill> skills)
        {
            List<DataObjectPropertiesSkill> tempSkills = skills.Select(x => x.GetDataObjectSkill()).ToList();

            // setting DataObjectId doesn't trigger dirty
            tempSkills.ForEach(a => a.DataObjectId = dataObjectId);

            // calling ToList forces the enumerable evaluation so that we can call .Remove from within the loop
            List<DataObjectPropertiesSkill> theDirtyOnes = tempSkills.Where(x => x.IsDirty).ToList();

            foreach (DataObjectPropertiesSkill prop in theDirtyOnes)
            {
                if (prop.HasEverBeenSavedToDatabase)
                {
                    if (prop.SkillStatus == (ushort)SkillStatus.Untrained)
                    {
                        // delete it.  this is possibly (likely?) a no-op
                        transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObjectPropertiesSkill>(ShardPreparedStatement.DeleteDataObjectPropertiesSkills, prop);
                    }
                    else
                    {
                        // update it
                        transaction.AddPreparedUpdateStatement(ShardPreparedStatement.UpdateDataObjectPropertySkill, prop);
                    }
                }
                else
                {
                    transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesSkills, prop);
                }
            }
        }

        private static void SaveDataObjectPropertiesBook(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesBook> properties)
        {
            properties.ForEach(a => a.DataObjectId = dataObjectId);

            foreach (DataObjectPropertiesBook page in properties)
                transaction.AddPreparedInsertStatement(ShardPreparedStatement.InsertDataObjectPropertiesBook, page);
        }

        private static void DeleteDataObjectBase(DatabaseTransaction transaction, DataObject obj)
        {
            transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataObject>(ShardPreparedStatement.DeleteDataObject, obj);
        }

        // FIXME(ddevec): These are a lot of functions that essentially do the same thing... but the SharedPreparedStatement.--- makes them a pain to tempalte/reduce
        private static void DeleteDataObjectPropertiesInt(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesInt>(ShardPreparedStatement.DeleteDataObjectPropertiesInt, critera);
        }

        private static void DeleteDataObjectPropertiesSpells(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesSpell>(ShardPreparedStatement.DeleteDataObjectPropertiesSpell, criteria);
        }

        private static void DeleteDataObjectPropertiesBigInt(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesInt64> properties)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesInt64>(ShardPreparedStatement.DeleteDataObjectPropertiesBigInt, critera);
        }

        private static void DeleteDataObjectPropertiesBool(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesBool> properties)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesBool>(ShardPreparedStatement.DeleteDataObjectPropertiesBool, critera);
        }

        private static void DeleteDataObjectPropertiesDouble(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesDouble> properties)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesDouble>(ShardPreparedStatement.DeleteDataObjectPropertiesDouble, critera);
        }

        private static void DeleteDataObjectPropertiesString(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesString> properties)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesString>(ShardPreparedStatement.DeleteDataObjectPropertiesString, critera);
        }

        private static void DeleteDataObjectPropertiesDid(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesDataId> properties)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesDataId>(ShardPreparedStatement.DeleteDataObjectPropertiesDid, critera);
        }

        private static void DeleteDataObjectPropertiesIid(DatabaseTransaction transaction, uint dataObjectId, List<DataObjectPropertiesInstanceId> properties)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesInstanceId>(ShardPreparedStatement.DeleteDataObjectPropertiesIid, critera);
        }

        private static void DeleteDataObjectPropertiesPositions(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesPosition>(ShardPreparedStatement.DeleteDataObjectPropertiesPositions, critera);
        }

        private static void DeleteDataObjectPropertiesAttributes(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesAttribute>(ShardPreparedStatement.DeleteDataObjectPropertiesAttributes, critera);
        }

        private static void DeleteDataObjectPropertiesAttribute2nd(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesAttribute2nd>(ShardPreparedStatement.DeleteDataObjectPropertiesAttributes2nd, critera);
        }

        private static void DeleteDataObjectPropertiesSkill(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> critera = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesSkill>(ShardPreparedStatement.DeleteDataObjectPropertiesSkills, critera);
        }

        private static void DeleteDataObjectPropertiesBook(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesBook>(ShardPreparedStatement.DeleteDataObjectPropertiesBook, criteria);
        }

        private uint GetMaxGuid(ShardPreparedStatement id, uint min, uint max)
        {
            object[] criteria = { min, max };
            MySqlResult res = SelectPreparedStatement(id, criteria);
            object ret = res.Rows[0][0];
            if (ret is DBNull)
            {
                return uint.MaxValue;
            }

            return (uint)res.Rows[0][0];
        }

        public uint GetCurrentId(uint min, uint max)
        {
            return GetMaxGuid(ShardPreparedStatement.GetCurrentId, min, max);
        }

        public void AddFriend(uint characterId, uint friendCharacterId)
        {
            throw new NotImplementedException();
        }

        public void DeleteFriend(uint characterId, uint friendCharacterId)
        {
            throw new NotImplementedException();
        }

        public bool DeleteContract(DataContractTracker contract)
        {
            DatabaseTransaction transaction = BeginTransaction();
            DeleteContractTracker(transaction, contract);
            return transaction.Commit().Result;
        }

        public void RemoveAllFriends(uint characterId)
        {
            throw new NotImplementedException();
        }

        public bool DeleteOrRestore(ulong unixTime, uint dataObjectId)
        {
            DataCharacter dataCharacter = new DataCharacter(dataObjectId);
            LoadIntoObject(dataCharacter);
            dataCharacter.DeleteTime = unixTime;

            dataCharacter.Deleted = false;  // This is a reminder - the DB will set this 1 hour after deletion.

            SaveObject(dataCharacter);

            return true;
        }

        public bool DeleteCharacter(uint dataObjectId)
        {
            DataCharacter dataCharacter = new DataCharacter(dataObjectId);
            LoadIntoObject(dataCharacter);
            dataCharacter.Deleted = true;

            SaveObject(dataCharacter);

            return true;
        }

        public List<CachedCharacter> GetCharacters(uint subscriptionId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "subscriptionId", subscriptionId }, { "deleted", 0 } };
            List<CachedCharacter> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, CachedCharacter>(ShardPreparedStatement.GetCharacters, criteria);

            return objects;
        }

        public DataCharacter GetCharacter(uint id)
        {
            DataCharacter character = new DataCharacter(id);

            // load common stuff here
            LoadIntoObject(character);

            // fetch common stuff here (is there any?)

            return character;
        }

        private List<DataContractTracker> GetContractList(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataContractTracker> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataContractTracker>(ShardPreparedStatement.GetContractTracker, criteria);
            return objects;
        }

        private List<DataObjectPropertiesSpellBarPositions> GetSpellBarPositions(uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            List<DataObjectPropertiesSpellBarPositions> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, DataObjectPropertiesSpellBarPositions>(ShardPreparedStatement.GetSpellBarPositions, criteria);
            return objects;
        }

        public ObjectInfo GetObjectInfoByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<DataObject> GetObjectsByLandblock(ushort landblock)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "landblock", landblock } };
            List<CachedWorldObject> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, CachedWorldObject>(ShardPreparedStatement.GetObjectsByLandblock, criteria);
            List<DataObject> ret = new List<DataObject>();
            objects.ForEach(cwo =>
            {
                DataObject o = GetObject(cwo.DataObjectId);
                ret.Add(o);
            });
            return ret;
        }

        public bool IsCharacterNameAvailable(string name)
        {
            CachedCharacter cc = new CachedCharacter();
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "name", name } };
            return !(ExecuteConstructedGetStatement<CachedCharacter, ShardPreparedStatement>(ShardPreparedStatement.IsCharacterNameAvailable, criteria, cc));
        }

        public uint RenameCharacter(string currentName, string newName)
        {
            throw new NotImplementedException();
        }

        public uint SetCharacterAccessLevelByName(string name, AccessLevel accessLevel)
        {
            throw new NotImplementedException();
        }


        public DataObject GetObject(uint objId)
        {
            return GetObjectOfType<DataObject>(objId);
        }

        public Dictionary<ObjectGuid, DataObject> GetInventoryByContainerId(uint containerId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "containerId", containerId } };
            List<CachedInventoryObject> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, CachedInventoryObject>(ShardPreparedStatement.GetDataObjectsByContainerId, criteria);
            return objects.ToDictionary(x => new ObjectGuid(x.DataObjectId), x => GetObject(x.DataObjectId));
        }

        public Dictionary<ObjectGuid, DataObject> GetItemsByWielderId(uint wielderId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "wielderId", wielderId } };
            List<CachedWieldedObject> objects = ExecuteConstructedGetListStatement<ShardPreparedStatement, CachedWieldedObject>(ShardPreparedStatement.GetDataObjectsByWielderId, criteria);
            return objects.ToDictionary(x => new ObjectGuid(x.DataObjectId), x => GetObject(x.DataObjectId));
        }

        private static void SaveContractTracker(DatabaseTransaction transaction, uint dataObjectId, List<DataContractTracker> properties)
        {
            properties.ForEach(a => a.DataObjectId = dataObjectId);
            transaction.AddPreparedInsertListStatement(ShardPreparedStatement.InsertContractTracker, properties);
        }

        private static void SaveDataObjectPropertiesSpellBarPositions(DatabaseTransaction transaction, List<DataObjectPropertiesSpellBarPositions> properties)
        {
            transaction.AddPreparedInsertListStatement(ShardPreparedStatement.InsertSpellBarPositions, properties);
        }

        private bool DeleteDataContractTrackers(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataContractTracker>(ShardPreparedStatement.DeleteContractTrackers, criteria);
            return true;
        }

        private static void DeleteSpellBarPositions(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataObjectPropertiesSpellBarPositions>(ShardPreparedStatement.DeleteSpellBarPositions, criteria);
        }

        private static void DeleteContractTracker(DatabaseTransaction transaction, DataContractTracker contract)
        {
            transaction.AddPreparedDeleteStatement<ShardPreparedStatement, DataContractTracker>(ShardPreparedStatement.DeleteContractTracker, contract);
        }

        private static void DeleteContractTrackers(DatabaseTransaction transaction, uint dataObjectId)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object> { { "aceObjectId", dataObjectId } };
            transaction.AddPreparedDeleteListStatement<ShardPreparedStatement, DataContractTracker>(ShardPreparedStatement.DeleteContractTrackers, criteria);
        }
    }
}
