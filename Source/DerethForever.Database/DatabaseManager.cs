/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using DerethForever.Entity.Enum;
using log4net;
using System;

namespace DerethForever.Database
{
    public class DatabaseManager
    {
        protected static readonly ILog log = LogManager.GetLogger("DatabaseManager");

        private static volatile bool _initialized = false;
        private static object _initializerMutex = new object();

        private static SerializedShardDatabase serializedShardDb;

        public static IAuthenticationDatabase Authentication { get; set; }

        public static ISerializedShardDatabase Shard { get; set; }

        public static IWorldDatabase World { get; set; }

        public static void Initialize(bool autoRetry = true)
        {
            if (_initialized)
                return;

            lock (_initializerMutex)
            {
                if (_initialized)
                    return;

                try
                {
                    var config = ConfigManager.Config.Database;

                    var authDb = new AuthenticationDatabase();
                    authDb.Initialize(config.Authentication.Host, config.Authentication.Port, config.Authentication.Username, config.Authentication.Password, DatabaseSelectionOption.Authentication, autoRetry);
                    Authentication = authDb;

                    var shardDb = new ShardDatabase();
                    shardDb.Initialize(config.Shard.Host, config.Shard.Port, config.Shard.Username, config.Shard.Password, DatabaseSelectionOption.Shard, autoRetry);
                    serializedShardDb = new SerializedShardDatabase(shardDb);
                    Shard = serializedShardDb;

                    var worldDb = new WorldJsonDatabase(config.World.Folder);
                    World = worldDb;

                    _initialized = true;
                }
                catch (Exception ex)
                {
                    log.Error($"Error during database initialization: {ex}.");
                    throw;
                }
            }
        }

        public static void Start()
        {
            serializedShardDb.Start();
        }

        public static void Stop()
        {
            serializedShardDb.Stop();
        }
    }
}
