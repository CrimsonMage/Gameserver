/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System;

using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Database
{
    public interface IAuthenticationDatabase
    {
        void CreateAccount(Account account);
        
        Account GetAccountByName(string accountName);

        Account GetAccountById(uint accountId);

        Account GetAccountByGuid(Guid accountGuid);

        void GetAccountIdByName(string accountName, out uint id);

        void UpdateAccount(Account account);

        void CreateSubscription(Subscription sub);

        void UpdateSubscription(Subscription sub);

        void UpdateSubscriptionAccessLevel(uint subscriptionId, AccessLevel accessLevel);

        Subscription GetSubscriptionById(uint subscriptionId);

        Subscription GetSubscriptionByGuid(Guid subscriptionGuid);

        List<Subscription> GetSubscriptionsByAccount(Guid accountGuid);

        List<ManagedWorld> GetManagedWorlds(Guid accountGuid);

        bool CreateManagedWorld(ManagedWorld managedWorld);

        bool UpdateManagedWorld(ManagedWorld managedWorld);

        bool DeleteManagedWorld(Guid managedWorldGuid);

        List<Account> GetAll();
    }
}
