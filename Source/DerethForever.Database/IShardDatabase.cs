/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Database
{
    internal interface IShardDatabase
    {
        List<DataObject> GetObjectsByLandblock(ushort landblock);

        bool SaveObject(DataObject dataObject);

        bool DeleteObject(DataObject dataObject);

        DataObject GetObject(uint objId);

        List<CachedCharacter> GetCharacters(uint subscriptionId);

        /// <summary>
        /// Loads an object by name.  Primary use case: characters.
        /// </summary>
        ObjectInfo GetObjectInfoByName(string name);

        void DeleteFriend(uint characterId, uint friendCharacterId);

        bool DeleteContract(DataContractTracker contract);

        void AddFriend(uint characterId, uint friendCharacterId);

        void RemoveAllFriends(uint characterId);

        bool IsCharacterNameAvailable(string name);

        bool DeleteOrRestore(ulong unixTime, uint id);

        bool DeleteCharacter(uint id);

        uint SetCharacterAccessLevelByName(string name, AccessLevel accessLevel);

        uint RenameCharacter(string currentName, string newName);

        uint GetCurrentId(uint min, uint max);

        DataCharacter GetCharacter(uint id);
    }
}
