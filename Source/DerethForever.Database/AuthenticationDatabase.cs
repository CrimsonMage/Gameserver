/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using DerethForever.Entity;
using DerethForever.Entity.Enum;

using MySql.Data.MySqlClient;

namespace DerethForever.Database
{
    public class AuthenticationDatabase : Database, IAuthenticationDatabase
    {
        private enum AuthenticationPreparedStatement
        {
            AccountInsert,
            AccountSelect,
            AccountUpdate,
            AccountSelectByName,
            AccountSelectByGuid,
            AccountGetList,

            SubscriptionInsert,
            SubscriptionSelect,
            SubscriptionUpdate,
            SubscriptionGet,
            SubscriptionGetByAccount,

            ManagedWorldInsert,
            ManagedWorldUpdate,
            ManagedWorldDelete,
            ManagedWorldGetList
        }

        protected override Type PreparedStatementType => typeof(AuthenticationPreparedStatement);

        protected override void InitializePreparedStatements()
        {
            ConstructStatement(AuthenticationPreparedStatement.AccountSelect, typeof(Account), ConstructedStatementType.Get);
            ConstructStatement(AuthenticationPreparedStatement.AccountGetList, typeof(Account), ConstructedStatementType.GetList);
            ConstructStatement(AuthenticationPreparedStatement.AccountInsert, typeof(Account), ConstructedStatementType.Insert);
            ConstructStatement(AuthenticationPreparedStatement.AccountUpdate, typeof(Account), ConstructedStatementType.Update);
            ConstructStatement(AuthenticationPreparedStatement.AccountSelectByName, typeof(AccountByName), ConstructedStatementType.Get);
            ConstructStatement(AuthenticationPreparedStatement.AccountSelectByGuid, typeof(AccountByGuid), ConstructedStatementType.Get);

            ConstructStatement(AuthenticationPreparedStatement.SubscriptionInsert, typeof(Subscription), ConstructedStatementType.Insert);
            ConstructStatement(AuthenticationPreparedStatement.SubscriptionSelect, typeof(Subscription), ConstructedStatementType.Get);
            ConstructStatement(AuthenticationPreparedStatement.SubscriptionUpdate, typeof(Subscription), ConstructedStatementType.Update);
            ConstructStatement(AuthenticationPreparedStatement.SubscriptionGet, typeof(Subscription), ConstructedStatementType.Get);
            ConstructStatement(AuthenticationPreparedStatement.SubscriptionGetByAccount, typeof(Subscription), ConstructedStatementType.GetList);

            ConstructStatement(AuthenticationPreparedStatement.ManagedWorldInsert, typeof(ManagedWorld), ConstructedStatementType.Insert);
            ConstructStatement(AuthenticationPreparedStatement.ManagedWorldUpdate, typeof(ManagedWorld), ConstructedStatementType.Update);
            ConstructStatement(AuthenticationPreparedStatement.ManagedWorldDelete, typeof(ManagedWorld), ConstructedStatementType.Delete);
            ConstructStatement(AuthenticationPreparedStatement.ManagedWorldGetList, typeof(ManagedWorld), ConstructedStatementType.GetList);
        }
        
        public void CreateAccount(Account account)
        {
            ExecuteConstructedInsertStatement(AuthenticationPreparedStatement.AccountInsert, typeof(Account), account);
        }

        public void CreateSubscription(Subscription sub)
        {
            ExecuteConstructedInsertStatement(AuthenticationPreparedStatement.SubscriptionInsert, typeof(Subscription), sub);
        }

        public void UpdateSubscriptionAccessLevel(uint subscriptionId, AccessLevel accessLevel)
        {
            var sub = GetSubscriptionById(subscriptionId);
            sub.AccessLevel = accessLevel;
            UpdateSubscription(sub);
        }

        public void UpdateAccount(Account account)
        {
            var oldManagedWorlds = GetManagedWorlds(account.AccountGuid);

            ExecuteConstructedUpdateStatement(AuthenticationPreparedStatement.AccountUpdate, typeof(Account), account);

            foreach(var world in account.ManagedWorlds)
            {
                world.AccountGuid = account.AccountGuid;
                if (world.WorldGuid == null)
                {
                    world.WorldGuid = Guid.NewGuid();
                    CreateManagedWorld(world);
                }
                else
                {
                    UpdateManagedWorld(world);
                }
            }

            foreach(var world in oldManagedWorlds)
            {
                if (!account.ManagedWorlds.Any(w => w.WorldGuid == world.WorldGuid))
                    DeleteManagedWorld(world.WorldGuid.Value);
            }
        }

        public void UpdateSubscription(Subscription sub)
        {
            ExecuteConstructedUpdateStatement(AuthenticationPreparedStatement.SubscriptionUpdate, typeof(Subscription), sub);
        }

        public List<Account> GetAll()
        {
            return ExecuteConstructedGetListStatement<AuthenticationPreparedStatement, Account>(AuthenticationPreparedStatement.AccountGetList, new Dictionary<string, object>());
        }

        public Account GetAccountById(uint accountId)
        {
            Account ret = new Account();
            var criteria = new Dictionary<string, object> { { "accountId", accountId } };
            bool success = ExecuteConstructedGetStatement<Account, AuthenticationPreparedStatement>(AuthenticationPreparedStatement.AccountSelect, criteria, ret);

            if (success)
                ret.ManagedWorlds = GetManagedWorlds(ret.AccountGuid);

            return ret;
        }

        public Subscription GetSubscriptionById(uint subscriptionId)
        {
            Subscription ret = new Subscription();
            var criteria = new Dictionary<string, object> { { "subscriptionId", subscriptionId } };
            bool success = ExecuteConstructedGetStatement<Subscription, AuthenticationPreparedStatement>(AuthenticationPreparedStatement.SubscriptionGet, criteria, ret);
            return ret;
        }

        public Subscription GetSubscriptionByGuid(Guid subscriptionGuid)
        {
            Dictionary<string, MySqlParameter> criteria = new Dictionary<string, MySqlParameter>();
            criteria.Add("subscriptionGuid", new MySqlParameter("", MySqlDbType.Binary) { Value = subscriptionGuid.ToByteArray() });
            return ExecuteDynamicGet<Subscription>(criteria);
        }

        public List<Subscription> GetSubscriptionsByAccount(Guid accountGuid)
        {
            var criteria = new Dictionary<string, object> { { "accountGuid", accountGuid.ToByteArray() } };
            var result = ExecuteConstructedGetListStatement<AuthenticationPreparedStatement, Subscription>(AuthenticationPreparedStatement.SubscriptionGetByAccount, criteria);
            return result;
        }

        public Account GetAccountByName(string accountName)
        {
            AccountByName ret = new AccountByName();
            var criteria = new Dictionary<string, object> { { "accountName", accountName } };
            bool success = ExecuteConstructedGetStatement<AccountByName, AuthenticationPreparedStatement>(AuthenticationPreparedStatement.AccountSelectByName, criteria, ret);

            if (success)
            {
                return GetAccountById(ret.AccountId);
            }

            return null;
        }
        
        public void GetAccountIdByName(string accountName, out uint id)
        {
            AccountByName ret = new AccountByName();
            var criteria = new Dictionary<string, object> { { "accountName", accountName } };
            ExecuteConstructedGetStatement<AccountByName, AuthenticationPreparedStatement>(AuthenticationPreparedStatement.AccountSelectByName, criteria, ret);
            id = ret.AccountId;
        }

        public List<ManagedWorld> GetManagedWorlds(Guid accountGuid)
        {
            var criteria = new Dictionary<string, object> { { "accountGuid", accountGuid.ToByteArray() } };
            var result = ExecuteConstructedGetListStatement<AuthenticationPreparedStatement, ManagedWorld>(AuthenticationPreparedStatement.ManagedWorldGetList, criteria);
            return result;
        }

        public bool CreateManagedWorld(ManagedWorld managedWorld)
        {
            return ExecuteConstructedInsertStatement(AuthenticationPreparedStatement.ManagedWorldInsert, typeof(ManagedWorld), managedWorld);
        }

        public bool UpdateManagedWorld(ManagedWorld managedWorld)
        {
            return ExecuteConstructedUpdateStatement(AuthenticationPreparedStatement.ManagedWorldUpdate, typeof(ManagedWorld), managedWorld);
        }

        public bool DeleteManagedWorld(Guid managedWorldGuid)
        {
            var criteria = new Dictionary<string, object> { { "worldGuid", managedWorldGuid.ToByteArray() } };
            return ExecuteConstructedDeleteStatement(AuthenticationPreparedStatement.ManagedWorldDelete, typeof(ManagedWorld), criteria);
        }

        public Account GetAccountByGuid(Guid accountGuid)
        {
            AccountByGuid ret = new AccountByGuid();
            var criteria = new Dictionary<string, object> { { "accountGuid", accountGuid.ToByteArray() } };
            bool success = ExecuteConstructedGetStatement<AccountByGuid, AuthenticationPreparedStatement>(AuthenticationPreparedStatement.AccountSelectByGuid, criteria, ret);

            if (success)
            {
                return GetAccountById(ret.AccountId);
            }

            return null;
        }
    }
}
