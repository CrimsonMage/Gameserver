/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DerethForever.Api.Common
{
    /// <summary>
    /// this class exists because the alternative is playing version detection and management in about a dozen nuget packages
    /// that i really don't care to bother.  this is a simple-enough manual/explicit HS256 JWT validator
    /// </summary>
    public class JwtTokenHandler : DelegatingHandler
    {
        private const string authenticationType = "bearer";
        
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // if this is not a bearer header, we do not know how to process it, so just return
            var authorizationHeader = request.Headers.Authorization;

            if (string.IsNullOrWhiteSpace(authorizationHeader?.Scheme) || !authorizationHeader.Scheme.Equals(authenticationType, StringComparison.InvariantCultureIgnoreCase))
                return base.SendAsync(request, cancellationToken);

            try
            {
                // get the actual token
                var jwtToken = request.Headers.Authorization.Parameter;

                // parse the header to find out what algorithm to use
                var parts = jwtToken.Split('.');
                var headerBase64 = parts[0];
                var headerJson = Encoding.UTF8.GetString(JwtUtil.Base64UrlDecode(headerBase64));
                var headerData = JObject.Parse(headerJson);
                var principal = JwtManager.GetPrincipal(jwtToken);
                
                // setup context principal
                request.GetRequestContext().Principal = principal;
                
                return base.SendAsync(request, cancellationToken);
            }
            catch (Exception ex)
            {
                return Task.Run(() => request.CreateResponse(HttpStatusCode.Unauthorized, new { message = ex.Message }));
            }
        }

    }
}
