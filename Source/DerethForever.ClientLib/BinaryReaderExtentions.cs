/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib
{
    public static class BinaryReaderExtentions
    {
        /// <summary>
        /// this currently only accounts for strings of length less than 64k
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static string ReadPString(this BinaryReader reader, bool align = true)
        {
            ushort len16 = reader.ReadUInt16();
            uint length = len16;

            if (len16 == 0xFFFF)
            {
                length = reader.ReadUInt32();
            }

            byte[] str = reader.ReadBytes((int)length);

            if (align)
            {
                int extraBytes = (sizeof(ushort) + (int)length) & 3;
                if (extraBytes > 0)
                    reader.ReadBytes(4 - extraBytes);
            }

            return Encoding.Default.GetString(str);
        }

        public static string ReadTabooString(this BinaryReader reader)
        {
            ushort len = reader.ReadByte();
            byte[] str = reader.ReadBytes(len);
            return Encoding.Default.GetString(str);
        }

        public static uint ReadPackedUint32(this BinaryReader reader, out uint bytesRead)
        {
            short data = reader.ReadByte();
            bytesRead = 1;

            if ((data & 0x80) > 0)
            {
                short lowbyte = reader.ReadByte();
                short hiByte = (short)((data & 0x7f) << 8);
                data = (short)(hiByte | lowbyte);
                bytesRead = 2;
            }

            return (uint)data;
        }

        public static uint ReadPackedUint16(this BinaryReader reader, out uint bytesRead)
        {
            ushort d1 = reader.ReadByte();
            ushort result = d1;
            bytesRead = 1;

            while ((d1 & 0x80u) != 0 && bytesRead < 2)
            {
                result = (ushort)(d1 & 0x7F);
                result = (ushort)(result << 7);
                result |= d1;
                d1 = reader.ReadByte();
                bytesRead++;
            }

            return result;
        }

        public static Plane ReadPlane(this BinaryReader reader)
        {
            Plane p;
            p.Normal = reader.ReadVector();
            p.D = reader.ReadSingle();
            return p;
        }

        /// <summary>
        /// ----- (00516540) --------------------------------------------------------
        /// int __thiscall AC1Legacy::Vector3::UnPack(AC1Legacy::Vector3 *this, void **addr, unsigned int size)
        /// acclient.c 323540
        /// </summary>
        public static Vector3 ReadVector(this BinaryReader reader)
        {
            Vector3 result;
            result.X = reader.ReadSingle();
            result.Y = reader.ReadSingle();
            result.Z = reader.ReadSingle();
            return result;
        }
        
        public static Sphere ReadSphere(this BinaryReader datReader)
        {
            Sphere obj = new Sphere();
            obj.Origin = new Vector3(datReader.ReadSingle(), datReader.ReadSingle(), datReader.ReadSingle());
            obj.Radius = datReader.ReadSingle();
            return obj;
        }
    }
}
