/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib
{
    public static class Vector3Extentions
    {
        public static void Normalize(this Vector3 vector)
        {
            float mag = 1 / vector.Magnitude();
            vector.X *= mag;
            vector.Y *= mag;
            vector.Z *= mag;
        }

        public static bool IsInsignificant(this Vector3 normal)
        {
            float mag = normal.Magnitude();
            return (mag < Constants.TOLERANCE);
        }

        public static float Magnitude(this Vector3 vector)
        {
            return (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z);
        }

        public static float GetHeading(this Vector3 vector)
        {
            Vector3 heading = new Vector3(vector.X, vector.Y, 0);

            if (heading.IsInsignificant())
                return 0f;

            heading.Normalize();

            double radians = ((450d - Math.Atan2(heading.X, heading.Y) * Constants.DEGREES_PER_RADIAN) % 360d) / Constants.DEGREES_PER_RADIAN;
            return (float)radians;
        }

        public static Vector3 CreateVelocity(this Vector3 vector, float timeToUse)
        {
            if (timeToUse < Constants.TOLERANCE)
                throw new ArgumentException("can not scale a vector by 0.");

            Vector3 result;

            result.X = vector.X / timeToUse;
            result.Y = vector.Y / timeToUse;
            result.Z = vector.Z / timeToUse;

            return result;
        }
    }
}
