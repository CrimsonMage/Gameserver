/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Enum.LandDefs
{
    /// <summary>
    /// LandDefs::Direction in the client.  it's not REALLY an enumeration, but rather an
    /// index into a static table.  The values do mean something, though, so we decided
    /// to put an enumeration on it.
    /// </summary>
    public enum Direction
    {
        Default         = 0,
        InViewerBlock   = 0,

        Dir1            = 1,
        North           = 1,

        Dir2            = 2,
        South           = 2,

        Dir3            = 3,
        East            = 3,

        Dir4            = 4,
        West            = 4,

        Dir5            = 5,
        NorthWest       = 5,

        Dir6            = 6,
        SouthWest       = 6,

        Dir7            = 7,
        NorthEast       = 7,

        Dir8            = 8,
        SouthEast       = 8,

        Unknown         = 9
    }
}
