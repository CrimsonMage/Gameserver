/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Enum
{
    [Flags]
    public enum SurfaceType : uint
    {
        BaseSolid       = 0x1,
        BaseImage       = 0x2,
        BaseClipMap     = 0x4,
        Translucent     = 0x10,
        Diffuse         = 0x20,
        Luminous        = 0x40,
        Alpha           = 0x100,
        InverseAlpha    = 0x200,
        Additive        = 0x10000,
        Detail          = 0x20000,
        Gouraud         = 0x10000000,
        Stippled        = 0x40000000,
        Perspective     = 0x80000000
    }
}
