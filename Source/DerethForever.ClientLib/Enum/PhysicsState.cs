/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.ClientLib.Enum
{
    [Flags]
    public enum PhysicsState
    {
        Static                      = 0x00000001,
        Unused1                     = 0x00000002,
        Ethereal                    = 0x00000004,
        ReportCollision             = 0x00000008,
        IgnoreCollision             = 0x00000010,
        NoDraw                      = 0x00000020,
        Missile                     = 0x00000040,
        Pushable                    = 0x00000080,
        AlignPath                   = 0x00000100,
        PathClipped                 = 0x00000200,
        Gravity                     = 0x00000400,
        LightingOn                  = 0x00000800,
        ParticleEmitter             = 0x00001000,
        Unused2                     = 0x00002000,
        Hidden                      = 0x00004000,
        ScriptedCollision           = 0x00008000,
        HasPhysicsBsp               = 0x00010000,
        Inelastic                   = 0x00020000,
        HasDefaultAnim              = 0x00040000,
        HasDefaultScript            = 0x00080000,
        Cloaked                     = 0x00100000,
        ReportCollisionAsEnviroment = 0x00200000,
        EdgeSlide                   = 0x00400000,
        Sledding                    = 0x00800000,
        Frozen                      = 0x01000000,
    }
}
