/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Entity;

namespace DerethForever.ClientLib.DatUtil
{
    /// <summary>
    /// wrapper for reading portal.dat
    /// </summary>
    public class PortalDatReader : DatReader
    {
        private static object _mutex = new object();
        private static PortalDatReader _singleton;

        public static PortalDatReader Current
        { get { return _singleton; } }

        private PortalDatReader(string filePath) : base(filePath)
        {
            LoadRegionDescription();
        }

        /// <summary>
        /// region's file id is constant
        /// </summary>
        private const uint REGION_ID = 0x13000000;
        
        public RegionDescription CurrentRegion;

        private void LoadRegionDescription()
        {
            CurrentRegion = Unpack<RegionDescription>(REGION_ID);
        }

        public static void Initialize(string filePath)
        {
            if (_singleton == null)
            {
                lock (_mutex)
                {
                    if (_singleton == null)
                    {
                        _singleton = new PortalDatReader(filePath);
                    }
                }
            }
        }
    }
}
