/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib
{
    /// <summary>
    /// Psuedo-random number generator
    /// </summary>
    public static class Prng
    {
        /// <summary>
        /// this function doesn't exist in the client, but rather is inlined all over the place.  This
        /// is probably a compiler optimization.  Seed values are set as constants in the Seeds subclass.
        /// 
        /// examples: acclient.c 352640, 352664
        /// </summary>
        public static double Generate(uint a, uint b, uint seed)
        {
            //          1813693831       1109124029               1360117743           1888038839
            uint ival = 0x6C1AC587 * b - 0x421BE3BD * a - seed * (0x5111BFEF * b * a + 0x70892FB7);

            return ((double)ival / (double)uint.MaxValue);
        }

        public static double NeSwCut(uint a, uint b)
        {
            uint val = b * (214614067 * a + 1813693831) - 1109124029 * a - 1369149221;

            return ((double)val / (double)uint.MaxValue);
        }

        public static class Seeds
        {
            public const uint InvertCellTriangles = 0x00000003;

            public const uint SceneSubIndex = 0x00002bf9;

            public const uint Frequency = 0x00005b67;

            public const uint DisplacementX = 0x0000b2cd;

            public const uint DisplacementY = 0x00011c0f;

            public const uint ObjectScale = 0x00007f51;

            public const uint AltObjectScale = 0x000096a7;

            public const uint SceneRotation = 0x0000e7eb;
            
            public const uint ObjectRotation = 0x0000f697;
            
            public const uint LandblockRotation = 0x0000000d;

            public const uint RND_LAND_TEX = 0x00000011;
            public const uint RND_LAND_ALPHA2 = 0x00012071;
            public const uint RND_LAND_ALPHA1 = 0x000002db;
            public const uint RND_SKILL_APPRAISE = 0x00000013;
            public const uint RND_TIME_WEATH1 = 0x0000a883;
            public const uint RND_TIME_WEATH2 = 0x00013255;
        }
    }
}
