/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// TerrainTex in the client
    /// </summary>
    public class TerrainTexture
    {
        /// <summary>
        /// 0x000 tex_gid : IDClass
        /// </summary>
        public uint Id;

        /// <summary>
        /// 0x004 base_texture : Ptr32 ImgTex
        /// </summary>
        public uint BaseTexture;

        /// <summary>
        /// 0x008 min_slope : Float
        /// </summary>
        public float MinimumSlope;

        /// <summary>
        /// 0x00c tex_tiling : Uint4B
        /// </summary>
        public uint TextureTiling;

        /// <summary>
        /// 0x010 max_vert_bright : Uint4B
        /// </summary>
        public uint MaxVertexBrightness;

        /// <summary>
        /// 0x014 min_vert_bright : Uint4B
        /// </summary>
        public uint MinVertexBrightness;

        /// <summary>
        /// 0x018 max_vert_saturate : Uint4B
        /// </summary>
        public uint MaxVertexSaturation;

        /// <summary>
        /// 0x01c min_vert_saturate : Uint4B
        /// </summary>
        public uint MinVertexSaturation;

        /// <summary>
        /// 0x020 max_vert_hue : Uint4B
        /// </summary>
        public uint MaxVertexHue;

        /// <summary>
        /// 0x024 min_vert_hue : Uint4B
        /// </summary>
        public uint MinVertexHue;

        /// <summary>
        /// 0x028 detail_tex_tiling : Uint4B
        /// </summary>
        public uint DetailTextureTiling;

        /// <summary>
        /// 0x02c detail_tex_gid : IDClass
        /// </summary>
        public uint DetailTextureId;

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private TerrainTexture()
        {
        }

        /// <summary>
        /// //----- (00503B40) --------------------------------------------------------
        /// int __thiscall TerrainTex::UnPack(TerrainTex *this, void **addr, unsigned int *size)
        /// acclient.c 305054
        /// </summary>
        public static TerrainTexture Unpack(BinaryReader reader)
        {
            TerrainTexture tt = new TerrainTexture();

            tt.Id = reader.ReadUInt32();
            tt.TextureTiling = reader.ReadUInt32();
            tt.MaxVertexBrightness = reader.ReadUInt32();
            tt.MinVertexBrightness = reader.ReadUInt32();
            tt.MaxVertexSaturation = reader.ReadUInt32();
            tt.MinVertexSaturation = reader.ReadUInt32();
            tt.MaxVertexHue = reader.ReadUInt32();
            tt.MinVertexHue = reader.ReadUInt32();
            tt.DetailTextureTiling = reader.ReadUInt32();
            tt.DetailTextureId = reader.ReadUInt32();

            return tt;
        }
    }
}
