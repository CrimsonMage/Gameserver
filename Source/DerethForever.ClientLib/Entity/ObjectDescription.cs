/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// ObjectDesc in the client
    /// </summary>
    public class ObjectDescription
    {
        /// <summary>
        /// +0x000 obj_id           : IDClass<_tagDataID,32,0>
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// +0x004 base_loc         : Frame
        /// </summary>
        public Frame BaseLocation;

        /// <summary>
        /// +0x044 freq             : Float
        /// </summary>
        public float Freq;

        /// <summary>
        /// +0x048 displace_x       : Float
        /// </summary>
        public float DisplacementX;

        /// <summary>
        /// +0x04c displace_y       : Float
        /// </summary>
        public float DisplacementY;

        /// <summary>
        /// +0x050 min_scale        : Float
        /// </summary>
        public float MinScale;

        /// <summary>
        /// +0x054 max_scale        : Float
        /// </summary>
        public float MaxScale;

        /// <summary>
        /// +0x058 max_rot          : Float
        /// </summary>
        public float MaxRotation;

        /// <summary>
        /// +0x05c min_slope        : Float
        /// </summary>
        public float MinSlope;

        /// <summary>
        /// +0x060 max_slope        : Float
        /// </summary>
        public float MaxSlope;

        /// <summary>
        /// +0x064 align            : Int4B
        /// </summary>
        public int Alignment;

        /// <summary>
        /// +0x068 orient           : Int4B
        /// </summary>
        public int Orientation;

        /// <summary>
        /// +0x06c weenie_obj       : Int4B
        /// </summary>
        public int WeenieObject;

        /// <summary>
        /// //----- (005A6110) --------------------------------------------------------
        /// int __thiscall ObjectDesc::UnPack(ObjectDesc *this, void **addr, unsigned int size)
        /// acclient.c 462930
        /// </summary>
        public static ObjectDescription Unpack(BinaryReader reader)
        {
            ObjectDescription result = new ObjectDescription();

            result.ObjectId = reader.ReadUInt32();
            result.BaseLocation = Frame.Unpack(reader);
            result.Freq = reader.ReadSingle();
            result.DisplacementX = reader.ReadSingle();
            result.DisplacementY = reader.ReadSingle();
            result.MinScale = reader.ReadSingle();
            result.MaxScale = reader.ReadSingle();
            result.MaxRotation = reader.ReadSingle();
            result.MinSlope = reader.ReadSingle();
            result.MaxSlope = reader.ReadSingle();
            result.Alignment = reader.ReadInt32();
            result.Orientation = reader.ReadInt32();
            result.WeenieObject = reader.ReadInt32();
            return result;
        }
    }
}
