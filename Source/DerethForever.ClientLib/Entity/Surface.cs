/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSurface in the client
    /// </summary>
    public class Surface : DatabaseObject, GraphicsResource
    {
        /// <summary>
        /// private because all valid instances come from Unpack (for now)
        /// </summary>
        private Surface()
        {
        }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x008 m_bIsLost : Bool
        /// </summary>
        public bool IsLost { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x010 m_TimeUsed : Float
        /// </summary>
        public float TimeUsed { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x018 m_FrameUsed : Uint4B
        /// </summary>
        public uint FrameUsed { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x01c m_bIsThrashable : Bool
        /// </summary>
        public bool IsThrashable { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x01d m_AutoRestore : Bool
        /// </summary>
        public bool AutoRestore { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x020 m_nResourceSize
        /// </summary>
        public uint ResourceSize { get; set; }

        /// <summary>
        /// GraphicsResource Offset + 0x030
        /// +0x024 m_ListIndex
        /// </summary>
        public uint ListIndex { get; set; }

        /// <summary>
        /// 0x058 type : Uint4B
        /// </summary>
        public SurfaceType Type { get; set; }

        /// <summary>
        /// 0x05c handler : SurfaceHandlerEnum
        /// </summary>
        public SurfaceHandler Handler { get; set; }

        /// <summary>
        /// 0x060 color_value : Uint4B
        /// </summary>
        public uint ColorValue { get; set; }

        /// <summary>
        /// 0x064 solid_index : Int4B
        /// </summary>
        public int SolidIndex { get; set; }

        /// <summary>
        /// 0x068 indexed_texture_id : IDClass
        /// </summary>
        public uint IndexedTextureId { get; set; }

        /// <summary>
        /// 0x06c base1map : Ptr32 ImgTex
        /// </summary>
        public object BaseMap { get; set; }

        /// <summary>
        /// 0x070 base1pal : Ptr32 Palette
        /// </summary>
        public Palette BasePalette { get; set; }

        /// <summary>
        /// 0x074 translucency : Float
        /// </summary>
        public float Translucency { get; set; }

        /// <summary>
        /// 0x078 Luminosity : Float
        /// </summary>
        public float Luminosity { get; set; }

        /// <summary>
        /// 0x07c diffuse : Float
        /// </summary>
        public float Diffuse { get; set; }

        /// <summary>
        /// 0x080 orig_texture_id : IDClass
        /// </summary>
        public uint OriginalTextureId { get; set; }

        /// <summary>
        /// 0x084 orig_palette_id : IDClass
        /// </summary>
        public uint OriginalPaletteId { get; set; }

        /// <summary>
        /// 0x088 orig_luminosity : Float
        /// </summary>
        public float OriginalLuminosity { get; set; }

        /// <summary>
        /// 0x08c orig_diffuse : Float
        /// </summary>
        public float OriginalDiffuse { get; set; }

        /// <summary>
        /// CSurface doesn't have a literal unpack method.  we're adding one for 
        /// consistency purpose.  logic here was REed from CSurface::Serialize
        /// 
        /// //----- (005365F0) --------------------------------------------------------
        /// void __thiscall CSurface::Serialize(CSurface *this, Archive *io_archive)
        /// acclient.c 358266
        /// </summary>
        public static Surface Unpack(BinaryReader reader)
        {
            Surface s = new Surface();

            s.Type = (SurfaceType)reader.ReadUInt32();

            if (s.Type.HasFlag(SurfaceType.BaseImage) && s.Type.HasFlag(SurfaceType.BaseClipMap))
            {
                s.OriginalTextureId = reader.ReadUInt32();
                s.OriginalPaletteId = reader.ReadUInt32();
            }
            else
            {
                s.ColorValue = reader.ReadUInt32();
            }

            s.Translucency = reader.ReadSingle();
            s.Luminosity = reader.ReadSingle();
            s.Diffuse = reader.ReadSingle();

            return s;
        }
    }
}
