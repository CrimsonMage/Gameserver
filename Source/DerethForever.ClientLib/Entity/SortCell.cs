/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSortCell in the client
    /// </summary>
    public class SortCell : ObjectCell
    {
        /// <summary>
        /// 0x0f8 building : Ptr32 CBuildingObj
        /// </summary>
        public Building Building;

        /// <summary>
        /// //----- (005340A0) --------------------------------------------------------
        /// signed int __thiscall CSortCell::find_collisions(CSortCell *this, CTransition *transition)
        /// acclient.c 356107
        /// </summary>
        public override TransitionState FindCollisions(Transition trans)
        {
            // TODO implement SortCell.FindCollisions
            return TransitionState.OK;
        }

        /// <summary>
        /// //----- (00534030) --------------------------------------------------------
        /// void __thiscall CSortCell::add_building(CSortCell *this, CBuildingObj *_object)
        /// acclient.c 356074
        /// </summary>
        public void AddBuilding(Building building)
        {
            if (Building == null)
                Building = building;
        }
    }
}
