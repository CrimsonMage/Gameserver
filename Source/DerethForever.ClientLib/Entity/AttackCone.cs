/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{    
    public class AttackCone
    {
        /// <summary>
        /// +0x000 part_index       : Int4B
        /// </summary>
        public uint PartIndex { get; set; }

        /// <summary>
        /// +0x004 left             : Vec2D
        /// </summary>
        public Vector2 Left;

        /// <summary>
        /// +0x00c right            : Vec2D
        /// </summary>
        public Vector2 Right;

        /// <summary>
        /// +0x014 radius           : Float
        /// </summary>
        public float Radius { get; private set; }

        /// <summary>
        /// +0x018 height           : Float
        /// </summary>
        public float Height { get; private set; }

        /// <summary>
        /// ----- (00527060) --------------------------------------------------------
        /// int __thiscall AttackCone::UnPack(AttackCone *this, void **addr, unsigned int size)
        /// acclient.c 342622
        /// </summary>
        public static AttackCone Unpack(BinaryReader reader)
        {
            AttackCone a = new AttackCone
            {
                PartIndex = reader.ReadUInt32(),
                Left =
                {
                    X = reader.ReadSingle(),
                    Y = reader.ReadSingle()
                },
                Right =
                {
                    X = reader.ReadSingle(),
                    Y = reader.ReadSingle()
                },
                Radius = reader.ReadSingle(),
                Height = reader.ReadSingle()
            };

            // TODO validate if this is done or not

            return a;
        }
    }
}
