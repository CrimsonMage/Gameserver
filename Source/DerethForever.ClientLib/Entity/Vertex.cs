/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// represents both CVertex and CSWVertex.  The 2 types are interchangable
    /// for the most part.  CSWVertex was (speculatively) used for software
    /// rendering pre-ToD.  software rendering was dropped with ToD, which
    /// coincides with the appearance of CVertex in the client.  CVertex has
    /// dummy "Reserve" uints to take up the same data space as CSWVertex did,
    /// 32 bytes.  However, only the first 12 bytes are relevenat, at which
    /// point "Vertex" becomes synonymous with "Vector3".
    /// </summary>
    public class Vertex
    {
        /// <summary>
        /// +0x000 vertex           : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Vector = Vector3.Zero;

        /// <summary>
        /// +0x00c vert_id          : Int2B
        /// </summary>
        public short VertexId { get; set; }

        /// <summary>
        /// +0x014 normal           : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Normal { get; set; }

        /// <summary>
        /// +0x010 uvs              : Ptr32 CVec2Duv
        /// </summary>
        public List<UvMapping> UVs { get; set; } = new List<UvMapping>();

        /// <summary>
        /// //----- (0053DE50) --------------------------------------------------------
        /// int __thiscall CSWVertex::UnPack(CSWVertex *this, void **addr, unsigned int size)
        /// acclient.c 364861
        /// </summary>
        public static Vertex Unpack(BinaryReader reader)
        {
            Vertex v = new Vertex();

            v.VertexId = reader.ReadInt16();
            short uvCount = reader.ReadInt16();

            v.Vector = reader.ReadVector();
            v.Normal = reader.ReadVector();

            for (short i = 0; i < uvCount; i++)
                v.UVs.Add(UvMapping.Unpack(reader));

            return v;
        }
    }
}
