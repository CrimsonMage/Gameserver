/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// GfxObjInfo in the client
    /// </summary>
    public class GraphicsObjectInfo
    {
        /// <summary>
        /// 0x000 gfxobj_id : IDClass
        /// </summary>
        public uint GraphicsObjectId { get; private set; }

        /// <summary>
        /// 0x004 degrade_mode : Int4B
        /// </summary>
        public int DegradeMode { get; set; }

        /// <summary>
        /// 0x008 min_dist : Float
        /// </summary>
        public float MinimumDistance { get; set; }

        /// <summary>
        /// 0x00c ideal_dist : Float
        /// </summary>
        public float IdealDistance { get; set; }

        /// <summary>
        /// 0x010 max_dist : Float
        /// </summary>
        public float MaxDistance { get; set; }

        /// <summary>
        /// reference needed
        /// </summary>
        public static GraphicsObjectInfo Unpack(BinaryReader reader)
        {
            // todo cite and implement GraphicsObjectInfo.Unpack
            return null;
        }
    }
}
