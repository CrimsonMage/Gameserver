/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AmbientSoundDesc in the client
    /// </summary>
    public class AmbientSound
    {
        /// <summary>
        /// private because unpack is the only valid source of this class
        /// </summary>
        private AmbientSound()
        {
        }

        /// <summary>
        /// 0x000 stype : SoundType
        /// </summary>
        public Enum.SoundType SoundType;

        /// <summary>
        /// 0x004 is_continuous : Int4B
        /// </summary>
        public bool IsContinuous;

        /// <summary>
        /// 0x008 volume : Float
        /// </summary>
        public float Volume;

        /// <summary>
        /// 0x00c base_chance : Float
        /// </summary>
        public float BaseChance;

        /// <summary>
        /// 0x010 min_rate : Float
        /// </summary>
        public float MinRate;

        /// <summary>
        /// 0x014 max_rate : Float
        /// </summary>
        public float MaxRate;

        /// <summary>
        /// this method does not explicitly exist in the client, but is inlined with 
        /// it's parent's (AmbientSoundTable) unpack, lines 384573 - 384624
        /// </summary>
        public static AmbientSound Unpack(BinaryReader reader)
        {
            AmbientSound s = new AmbientSound();
            
            s.SoundType = (Enum.SoundType)reader.ReadUInt32();
            s.Volume = reader.ReadSingle();
            s.BaseChance = reader.ReadSingle();
            s.MinRate = reader.ReadSingle();
            s.MaxRate = reader.ReadSingle();

            return s;
        }
    }
}
