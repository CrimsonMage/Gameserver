/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CTerrainDesc in the client
    /// </summary>
    public class TerrainDescription
    {
        /// <summary>
        /// 0x000 land_surfaces : Ptr32 LandSurf
        /// </summary>
        public LandSurface LandSurface;

        /// <summary>
        /// 0x004 terrain_types : AC1Legacy::SmartArray&lt;CTerrainType&gt;
        /// </summary>
        public List<TerrainType> TerrainTypes = new List<TerrainType>();

        /// <summary>
        /// internal because only this project can unpack them (in RegionDescription)
        /// </summary>
        internal TerrainDescription()
        {
        }
    }
}
