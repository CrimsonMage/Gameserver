/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BuildInfo from the client
    /// </summary>
    public class BuildingInfo
    {
        /// <summary>
        /// +0x000 building_id      : IDClass&lt;_tagDataID,32,0&gt;
        /// </summary>
        public uint Id;

        /// <summary>
        /// +0x004 building_frame   : Frame
        /// </summary>
        public Frame Frame;

        /// <summary>
        /// +0x044 num_leaves       : Uint4B
        /// </summary>
        public uint NumLeaves;

        /// <summary>
        /// +0x048 num_portals      : Uint4B
        /// </summary>
        public uint NumPortals => (uint)Portals.Count;

        /// <summary>
        /// +0x04c portals          : Ptr32 Ptr32 CBldPortal
        /// </summary>
        public List<BuildingPortal> Portals = new List<BuildingPortal>();

        /// <summary>
        /// this method is not explicitly defined in the client.  it is inlined in 
        /// CLandblockInfo::Unpack from lines 351158 - 351211
        /// </summary>
        public static BuildingInfo Unpack(BinaryReader reader)
        {
            throw new NotSupportedException();
        }
    }
}
