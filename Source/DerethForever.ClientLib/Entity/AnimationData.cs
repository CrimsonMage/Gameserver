/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AnimData in the client
    /// </summary>
    public class AnimationData
    {
        /// <summary>
        /// +0x004 anim_id          : IDClass
        /// </summary>
        public uint AnimationId { get; set; }

        /// <summary>
        /// +0x008 low_frame        : Int4B
        /// </summary>
        public int LowFrame { get; set; }

        /// <summary>
        /// +0x00c high_frame       : Int4B
        /// </summary>
        public int HighFrame { get; set; }

        /// <summary>
        /// +0x010 framerate        : Float
        /// </summary>
        public float Framerate { get; set; }
    }
}
