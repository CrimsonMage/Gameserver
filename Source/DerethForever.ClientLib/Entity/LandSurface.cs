/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// LandSurf in the client
    /// </summary>
    public class LandSurface
    {
        /// <summary>
        /// 0x000 pal_shift : Ptr32 PalShift
        /// </summary>
        public PaletteShift PaletteShift = null;

        /// <summary>
        /// 0x004 tex_merge : Ptr32 TexMerge
        /// </summary>
        public TextureMerge TextureMerge;

        /// <summary>
        /// data type unknown.  8 bytes in the type dump, so ulong was used as a placeholder
        /// 0x008 surf_info : Ptr32 LongNIValHash(SurfInfo *)
        /// </summary>
        public ulong SurfaceInfo;

        /// <summary>
        /// 0x00c num_lsurf : Uint4B
        /// </summary>
        public uint NumLandSurfaces => (uint)Surfaces.Count;

        /// <summary>
        /// 0x010 lsurf : Ptr32 Ptr32 CSurface
        /// </summary>
        public List<Surface> Surfaces = new List<Surface>();

        /// <summary>
        /// 0x014 num_unique_surfaces : Uint4B
        /// </summary>
        public uint NumUniqueSurfaces;

        /// <summary>
        /// 0x018 num_block_surfs : Uint4B
        /// </summary>
        public uint NumBlockSurfaces;

        /// <summary>
        /// 0x01c block_surf_array : AC1Legacy::SmartArray(CSurface *)
        /// </summary>
        public List<Surface> BlockSurfaces = new List<Surface>();

        /// <summary>
        /// 0x028 curr_tex : Ptr32 UChar (void pointer?)
        /// </summary>
        public uint CurrentTexture;

        /// <summary>
        /// private because unpack is the only source of this class
        /// </summary>
        private LandSurface()
        {
        }

        /// <summary>
        /// //----- (00502C20) --------------------------------------------------------
        /// int __thiscall LandSurf::UnPack(LandSurf *this, void **addr, unsigned int *size)
        /// acclient.c 303985
        /// </summary>
        public static LandSurface Unpack(BinaryReader reader)
        {
            LandSurface ls = new LandSurface();

            uint hasPaletteShift = reader.ReadUInt32();

            // this is always 0 for the current AC dat files, so why bother?
            if (hasPaletteShift != 0)
                throw new NotImplementedException();
            
            ls.TextureMerge = TextureMerge.Unpack(reader);

            return ls;
        }
    }
}
