/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CCellPortal from the client    
    /// </summary>
    public class CellPortal
    {
        public CellPortalState Flags { get; set; }

        /// <summary>
        /// CCellPortal->other_cell_id
        /// +0x000 other_cell_id    : Uint4B
        /// </summary>
        public uint OtherCellId { get; set; }

        /// <summary>
        /// +0x004 other_cell_ptr   : Ptr32 CEnvCell
        /// </summary>
        public EnvironmentCell OtherCell { get; set; }

        /// <summary>
        /// +0x008 portal           : Ptr32 CPolygon
        /// </summary>
        public Polygon Portal { get; set; }

        /// <summary>
        /// CCellPortal->portal_side
        /// +0x00c portal_side      : Int4B
        /// this->portal_side = ((unsigned int)(unsigned __int8)~(_BYTE)v5 >> 1) & 1;
        /// </summary>
        public bool PortalSide
        {
            get { return Flags.HasFlag(CellPortalState.PortalSide); }
        }

        /// <summary>
        /// CCellPortal->other_portal_id
        /// +0x010 other_portal_id  : Int4B
        /// </summary>
        public int OtherPortalId { get; set; }

        /// <summary>
        /// CCellPortal->exact_match
        /// +0x014 exact_match      : Int4B
        ///  this->exact_match = v5 & 1;
        /// </summary>
        public bool ExactMatch
        {
            get { return Flags.HasFlag(CellPortalState.ExactMatch); }
        }

        /// <summary>
        /// ----- (0053BAB0) --------------------------------------------------------
        /// int __thiscall CCellPortal::UnPack(CCellPortal*this, unsigned int block_mask, unsigned __int16 *poly_id, void** addr, unsigned int size)
        /// acclient.c 362379
        /// </summary>
        public static CellPortal Unpack(BinaryReader reader, uint blockMask)
        {
            CellPortal cp = new CellPortal();
            cp.Flags = (CellPortalState)reader.ReadUInt16();
            ushort envId = reader.ReadUInt16();
            cp.OtherCellId = reader.ReadUInt16();
            cp.OtherPortalId = reader.ReadUInt16();

            if ((envId & 4) > 0)
                cp.OtherCellId = uint.MaxValue; // client uses -1 here.  given 2s compliment, it's the same as MaxValue - thanks Mud!
            else
                cp.OtherCellId |= blockMask;

            return cp;
        }
    }
}
