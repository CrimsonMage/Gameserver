/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// TexMerge in the client
    /// </summary>
    public class TextureMerge
    {
        /// <summary>
        /// 0x000 base_tex_size : Uint4B
        /// </summary>
        public uint BaseTextureSize;

        /// <summary>
        /// 0x004 corner_terrain_maps : AC1Legacy::SmartArray(TerrainAlphaMap *)
        /// </summary>
        public List<TerrainAlphaMap> CornerTerrainMaps = new List<TerrainAlphaMap>();

        /// <summary>
        /// 0x010 side_terrain_maps : AC1Legacy::SmartArray(TerrainAlphaMap *)
        /// </summary>
        public List<TerrainAlphaMap> SideTerrainMaps = new List<TerrainAlphaMap>();

        /// <summary>
        /// 0x01c road_maps : AC1Legacy::SmartArray(RoadAlphaMap *)
        /// </summary>
        public List<RoadAlphaMap> RoadMaps = new List<RoadAlphaMap>();

        /// <summary>
        /// 0x028 terrain_desc : AC1Legacy::SmartArray(TMTerrainDesc *)
        /// </summary>
        public List<TextureMergeTerrainDescription> TerrainDescriptions = new List<TextureMergeTerrainDescription>();

        /// <summary>
        /// private because Unpack is the only valid source of instances
        /// </summary>
        private TextureMerge()
        {
        }

        /// <summary>
        /// //----- (005045B0) --------------------------------------------------------
        /// int __thiscall TexMerge::UnPack(TexMerge *this, void **addr, unsigned int *size)
        /// acclient.c 305986
        /// </summary>
        public static TextureMerge Unpack(BinaryReader reader)
        {
            TextureMerge tm = new TextureMerge();

            tm.BaseTextureSize = reader.ReadUInt32();

            uint num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
            {
                TerrainAlphaMap m = new TerrainAlphaMap();
                m.TerrainCode = reader.ReadUInt32();
                m.TextureId = reader.ReadUInt32();
                tm.CornerTerrainMaps.Add(m);
            }

            num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
            {
                TerrainAlphaMap m = new TerrainAlphaMap();
                m.TerrainCode = reader.ReadUInt32();
                m.TextureId = reader.ReadUInt32();
                tm.SideTerrainMaps.Add(m);
            }

            num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
            {
                RoadAlphaMap m = new RoadAlphaMap();
                m.RoadCode = reader.ReadUInt32();
                m.RoadTextureId = reader.ReadUInt32();
                tm.RoadMaps.Add(m);
            }

            num = reader.ReadUInt32();
            for (int i = 0; i < num; i++)
                tm.TerrainDescriptions.Add(TextureMergeTerrainDescription.Unpack(reader));

            return tm;
        }
    }
}
