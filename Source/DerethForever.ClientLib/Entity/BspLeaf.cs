/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BSPLEAF in the client
    /// </summary>
    public class BspLeaf : BspNode
    {
        internal BspLeaf()
        {
            Type = BspNodeType.Leaf;
        }

        /// <summary>
        /// 0x038 leaf_index : Uint4B
        /// </summary>
        public uint LeafIndex { get; set; }

        /// <summary>
        /// 0x03c solid : Int4B
        /// </summary>
        public int Solid { get; set; }

        /// <summary>
        /// //----- (0053D4A0) --------------------------------------------------------
        /// int __thiscall BSPLEAF::UnPackLeaf(BSPLEAF *this, void **addr, unsigned int size)
        /// acclient.c 364133
        /// </summary>
        public void UnpackLeaf(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            LeafIndex = reader.ReadUInt32();

            if (treeType == BspTreeType.Physics)
            {
                Solid = reader.ReadInt32();
                Sphere = Sphere.Unpack(reader);

                var num = reader.ReadUInt32();

                for (int i = 0; i < num; i++)
                {
                    ushort polyIndex = reader.ReadUInt16();
                    InPolygons.Add(polygons[polyIndex]);
                }
            }
        }
    }
}
