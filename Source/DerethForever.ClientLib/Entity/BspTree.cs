/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BSPTREE in the client
    /// </summary>
    public class BspTree
    {
        /// <summary>
        /// 0x000 root_node : Ptr32 BSPNODE
        /// </summary>
        public BspNode RootNode { get; set; }

        /// <summary>
        /// //----- (00539830) --------------------------------------------------------
        /// int __thiscall BSPTREE::UnPack(BSPTREE *this, void **addr, unsigned int size)
        /// acclient.c 360686
        /// </summary>
        public void Unpack(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            RootNode = BspNode.UnpackChild(reader, treeType, polygons);
        }
    }
}
