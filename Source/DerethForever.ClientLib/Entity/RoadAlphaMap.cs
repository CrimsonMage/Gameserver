/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// RoadAlphaMap in the client
    /// </summary>
    public class RoadAlphaMap
    {
        /// <summary>
        /// 0x000 rcode : Uint4B
        /// </summary>
        public uint RoadCode;

        /// <summary>
        /// 0x004 road_tex_guid : IDClass
        /// </summary>
        public uint RoadTextureId;

        /// <summary>
        /// not loaded by the dat, so currently unused.
        /// 0x008 texture : Ptr32 ImgTex
        /// </summary>
        public uint Texture;

        /// <summary>
        /// internal because TextureMerge.Unpack is the only valid source of instances
        /// </summary>
        internal RoadAlphaMap()
        {
        }
    }
}
