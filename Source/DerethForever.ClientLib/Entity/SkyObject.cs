/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyObject in the client
    /// </summary>
    public class SkyObject
    {
        /// <summary>
        /// private because all instances come from Unpack
        /// </summary>
        private SkyObject()
        {
        }

        /// <summary>
        /// +0x004 begin_time       : Float
        /// </summary>
        public float BeginTime { get; set; }

        /// <summary>
        /// +0x008 end_time         : Float
        /// </summary>
        public float EndTime { get; set; }

        /// <summary>
        /// +0x00c begin_angle      : Float
        /// </summary>
        public float BeginAngle { get; set; }

        /// <summary>
        /// +0x010 end_angle        : Float
        /// </summary>
        public float EndAngle { get; set; }

        /// <summary>
        /// +0x014 tex_velocity     : AC1Legacy::Vector3
        /// </summary>
        public Vector3 TexVelocity;

        /// <summary>
        /// +0x020 properties       : Uint4B
        /// </summary>
        public uint Properties { get; set; }

        /// <summary>
        /// +0x024 default_gfx_object : IDClass<_tagDataID,32,0>
        /// </summary>
        public uint DefaultGraphicsObjectDid { get; set; }

        /// <summary>
        /// +0x028 default_pes_object : IDClass<_tagDataID,32,0>
        /// </summary>
        public uint DefaultPesObjectDid { get; set; }

        /// <summary>
        /// //----- (00500FD0) --------------------------------------------------------
        /// int __thiscall SkyObject::UnPack(SkyObject *this, void **addr, unsigned int *size)
        /// acclient.c 301789
        /// </summary>
        public static SkyObject Unpack(BinaryReader reader)
        {
            SkyObject s = new SkyObject();

            s.BeginTime = reader.ReadSingle();
            s.EndTime = reader.ReadSingle();

            s.BeginAngle = reader.ReadSingle();
            s.EndAngle = reader.ReadSingle();

            s.TexVelocity.X = reader.ReadSingle();
            s.TexVelocity.Y = reader.ReadSingle();

            s.DefaultGraphicsObjectDid = reader.ReadUInt32();
            s.DefaultPesObjectDid = reader.ReadUInt32();
            s.Properties = reader.ReadUInt32();

            return s;
        }
    }
}
