/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CBldPortal from the client
    /// </summary>
    public class BuildingPortal
    {
        public CellPortalState Flags { get; set; }

        /// <summary>
        ///   +0x000 portal_side      : Int4B
        /// </summary>
        public bool PortalSide
        {
            get { return Flags.HasFlag(CellPortalState.PortalSide); }
        }

        /// <summary>
        /// +0x004 other_cell_id    : Uint4B
        /// </summary>
        public uint OtherCellId { get; set; }

        /// <summary>
        /// +0x008 other_portal_id  : Int4B
        /// </summary>
        public uint OtherPortalId { get; set; }

        /// <summary>
        /// +0x00c exact_match      : Int4B
        /// </summary>
        public bool ExactMatch
        {
            get { return Flags.HasFlag(CellPortalState.ExactMatch); }
        }

        /// <summary>
        /// +0x014 stab_list        : Ptr32 Uint4B
        /// </summary>
        public List<uint> StabList { get; set; } = new List<uint>();

        /// <summary>
        /// +0x010 num_stabs        : Uint4B
        /// </summary>
        public uint NumStabs
        {
            get { return (uint)(StabList?.Count ?? 0); }
        }

        /// <summary>
        /// +0x018 sidedness        : Float
        /// </summary>
        public float Sidedness { get; set; }

        /// <summary>
        /// //----- (0053BC40) --------------------------------------------------------
        /// int __thiscall CBldPortal::UnPack(CBldPortal *this, unsigned int block_mask, void **addr, unsigned int size
        /// acclient.c 362499
        /// </summary>
        public static BuildingPortal Unpack(BinaryReader reader, uint blockMask)
        {
            BuildingPortal bp = new BuildingPortal
            {
                Flags = (CellPortalState)reader.ReadUInt16(),
                OtherCellId = blockMask + reader.ReadUInt16(),
                OtherPortalId = blockMask + reader.ReadUInt16()
            };

            uint numStabs = reader.ReadUInt16();
            for (int i = 0; i < numStabs; i++)
                bp.StabList.Add(blockMask + reader.ReadUInt16());

            // align to dword boundary
            if (numStabs % 2 != 0)
                reader.ReadBytes(2);

            return bp;
        }

        /// <summary>
        /// //----- (0053BB60) --------------------------------------------------------
        /// void __thiscall CBldPortal::add_to_stablist(CBldPortal *this, unsigned int **block_stab_list, unsigned int *max_size, unsigned int *stab_num)
        /// acclient.c 362423
        /// </summary>
        public void AddToStabList(List<uint> stabList, ref uint maxStabs)
        {
            // TODO implement BuildingPortal.AddToStabList
        }
    }
}
