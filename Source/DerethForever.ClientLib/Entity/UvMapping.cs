/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CVec2Duv in the client.  For reference, this data is how you map a flat image file
    /// to a 3D object.  See https://en.wikipedia.org/wiki/UV_mapping for conceptual explanations.
    /// </summary>
    public class UvMapping
    {
        public float U { get; set; }

        public float V { get; set; }

        /// <summary>
        /// inlined in the client - not an explicit method
        /// </summary>
        internal static UvMapping Unpack(BinaryReader reader)
        {
            UvMapping mapping = new UvMapping();

            mapping.U = reader.ReadSingle();
            mapping.V = reader.ReadSingle();

            return mapping;
        }
    }
}
