/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyObjectReplace in the client
    /// </summary>
    public class SkyObjectReplace
    {
        /// <summary>
        /// 0x000 object_index : Uint4B
        /// </summary>
        public uint Index;

        /// <summary>
        /// 0x004 object : Ptr32 SkyObject
        /// </summary>
        public SkyObject SkyObject;

        /// <summary>
        /// 0x008 gfx_obj_id : IDClass
        /// </summary>
        public uint GraphicsObjectId;

        /// <summary>
        /// 0x00c rotate : Float
        /// </summary>
        public float Rotate;

        /// <summary>
        /// 0x010 transparent : Float
        /// </summary>
        public float Transparency;

        /// <summary>
        /// 0x014 luminosity : Float
        /// </summary>
        public float Luminosity;

        /// <summary>
        /// 0x018 max_bright: Float
        /// </summary>
        public float MaxBright;

        /// <summary>
        /// //----- (00501180) --------------------------------------------------------
        /// int __thiscall SkyObjectReplace::UnPack(SkyObjectReplace *this, void **addr, unsigned int *size)
        /// acclient.c 301923
        /// </summary>
        public static SkyObjectReplace Unpack(BinaryReader reader)
        {
            SkyObjectReplace s = new SkyObjectReplace();

            s.Index = reader.ReadUInt32();
            s.GraphicsObjectId = reader.ReadUInt32();
            s.Rotate = reader.ReadSingle();
            s.Transparency = reader.ReadSingle();
            s.Luminosity = reader.ReadSingle();
            s.MaxBright = reader.ReadSingle();

            return s;
        }
    }
}
