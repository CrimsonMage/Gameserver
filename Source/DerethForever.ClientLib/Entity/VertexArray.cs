/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CVertexArray in the client
    /// </summary>
    public class VertexArray
    {
        /// <summary>
        /// because all valid instances come from either Unpack or other internal functions
        /// </summary>
        internal VertexArray()
        {
        }

        /// <summary>
        /// +0x000 vertex_memory    : Ptr32 Void
        /// </summary>
        public object VertexMemory { get; set; }

        /// <summary>
        /// +0x004 bbox             : BBox
        /// </summary>
        public BoundingBox BoundingBox { get; set; }

        /// <summary>
        /// +0x01c vertex_type      : VertexType
        /// </summary>
        public VertexType VertexType { get; set; }

        /// <summary>
        /// +0x020 num_vertices     : Uint4B
        /// </summary>
        public int NumVertices => Vertices.Length;

        /// <summary>
        /// +0x024 vertices         : Ptr32 CVertex
        /// </summary>
        public Vertex[] Vertices { get; set; }

        public Vector3 GetGroundVertex(uint x, uint y)
        {
            uint index = x * 8 + y;
            return Vertices[(int)index].Vector;
        }

        /// <summary>
        /// VertexArray has both an Unpack and a Serialize.  The serialize, however, just
        /// calls Unpack, so we'll stick with unpack.
        /// 
        /// //----- (0053C030) --------------------------------------------------------
        /// int __thiscall CVertexArray::UnPack(CVertexArray *this, void **addr, unsigned int size)
        /// acclient.c 362802
        /// </summary>
        public static VertexArray Unpack(BinaryReader reader)
        {
            VertexArray va = new VertexArray();

            va.VertexType = (VertexType)reader.ReadUInt32();
            var count = reader.ReadUInt32();
            va.Vertices = new Vertex[count];

            for (int i = 0; i < count; i++)
                va.Vertices[i] = Vertex.Unpack(reader);

            va.BoundingBox = BoundingBox.CreateBoundingBox(va);

            return va;
        }
    }
}
