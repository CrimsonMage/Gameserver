/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    public class Scene : DatabaseObject
    {
        // TODO move offset/name/type docs to property summary

        /*
        0:000> dt acclient!Scene
           +0x000 __VFN_table : Ptr32 
           +0x004 m_dataCategory   : Uint4B
           +0x008 m_bLoaded        : Bool
           +0x010 m_timeStamp      : Float
           +0x018 m_pNext          : Ptr32 DBObj
           +0x01c m_pLast          : Ptr32 DBObj
           +0x020 m_pMaintainer    : Ptr32 DBOCache
           +0x024 m_numLinks       : Int4B
           +0x028 m_DID            : IDClass<_tagDataID,32,0>
           +0x02c m_AllowedInFreeList : Bool
           +0x030 __VFN_table : Ptr32 
           +0x038 version          : Uint4B
           +0x03c num_objects      : Uint4B
           +0x040 objects          : Ptr32 ObjectDesc
        */
        
        public uint Version;
        public int NumObjects => Objects.Count;
        public List<ObjectDescription> Objects = new List<ObjectDescription>();

        /// <summary>
        /// //----- (005A6300) --------------------------------------------------------
        /// int __thiscall Scene::UnPack(Scene *this, void **addr, unsigned int size)
        /// acclient.c 463120
        /// </summary>
        public static Scene Unpack(BinaryReader reader)
        {
            Scene result = new Scene();
            uint numObjects = reader.ReadUInt32();

            for (int i = 0; i < numObjects; i++)
                result.Objects.Add(ObjectDescription.Unpack(reader));

            return result;
        }
    }
}
