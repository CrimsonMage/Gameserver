/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity.AnimationHooks
{
    public class CreateParticleHook : IHook
    {
        public uint EmitterInfoId { get; private set; }

        public uint PartIndex { get; private set; }

        public Frame Offset { get; private set; }

        public uint EmitterId { get; private set; }

        /// <summary>
        /// ----- (005278A0) --------------------------------------------------------
        /// int __thiscall CreateParticleHook::UnPack(CreateParticleHook *this, void **addr, unsigned int size)
        /// acclient.c 343213
        /// </summary>
        public static CreateParticleHook Unpack(BinaryReader reader)
        {
            CreateParticleHook hook = new CreateParticleHook
            {
                EmitterInfoId = reader.ReadUInt32(),
                PartIndex = reader.ReadUInt32(),
                Offset = Frame.Unpack(reader),
                EmitterId = reader.ReadUInt32()
            };
            return hook;
        }
    }
}
