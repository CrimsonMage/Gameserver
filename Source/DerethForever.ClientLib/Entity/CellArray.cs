/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System.Collections.Generic;
using System.Linq;

namespace DerethForever.ClientLib.Entity
{
    public class CellArray
    {
        /// <summary>
        /// +0x000 added_outside    : Int4B
        /// </summary>
        public bool AddedOutside { get; set; } // int in the client

        /// <summary>
        /// +0x004 do_not_load_cells : Int4B
        /// </summary>
        public bool DoNotLoadCells { get; set; } // int in the client

        /// <summary>
        ///  +0x00c cells            : DArray<CELLINFO>
        /// </summary>
        public List<CellInfo> Cells;

        public int NumCells
        {
            get { return Cells?.Count ?? 0; }
        }

        /// <summary>
        /// ----- (006B4FF0) --------------------------------------------------------
        /// void __thiscall CELLARRAY::add_cell(CELLARRAY*this, const unsigned int cell_id, CObjCell *cell)
        /// acclient.c 718896
        /// </summary>
        public void AddCell(uint cellId, ObjectCell cell)
        {
            Cells.Add(new CellInfo
            {
                ObjectCell = cell,
                CellId = cellId
            });
        }

        /// <summary>
        /// ----- (006B4E80) --------------------------------------------------------
        /// void __thiscall CELLARRAY::remove_cell(CELLARRAY*this, const unsigned int index)
        /// acclient.c 718762
        /// </summary>
        /// <param name="cellId"></param>
        public void RemoveCell(uint cellId)
        {
            Cells.Remove(Cells.Single(r => r.CellId == cellId));
        }
    }
}
