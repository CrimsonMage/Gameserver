/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// Subpalette in the client
    /// </summary>
    public class SubPalette
    {
        /// <summary>
        /// 0x004 subId : IDClass
        /// </summary>
        public uint Id;

        /// <summary>
        /// 0x008 Offset : Uint4B
        /// </summary>
        public uint Offset;

        /// <summary>
        /// 0x00c numcolors : Uint4B
        /// </summary>
        public uint NumColors;

        /// <summary>
        /// 0x010 prev : Ptr32 Subpalette
        /// </summary>
        public SubPalette Previous;

        /// <summary>
        /// 0x014 next : Ptr32 Subpalette
        /// </summary>
        public SubPalette Next;

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private SubPalette()
        {
        }

        /// <summary>
        /// //----- (005ADAB0) --------------------------------------------------------
        /// int __thiscall Subpalette::UnPack(Subpalette *this, void **addr, unsigned int size)
        /// acclient.c 471744
        /// </summary>
        public static SubPalette Unpack(BinaryReader reader)
        {
            SubPalette sp = new SubPalette();

            // Unpack_AsDataIDOfKnownType in the client.  did not dig deep here.
            sp.Id = 0x04000000u | reader.ReadUInt16();

            // both offset and NumColors are bytes in the client, but uints in the object model

            sp.Offset = (uint)reader.ReadByte() * 8;
            sp.NumColors = reader.ReadByte();

            if (sp.NumColors == 0)
                sp.NumColors = 256;

            sp.NumColors *= 8;

            return sp;
        }
    }
}
