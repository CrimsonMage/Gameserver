/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyDesc in the client
    /// </summary>
    public class SkyDescription
    {
        /// <summary>
        /// private because the only valid source for SkyDescription is the dat, which is handled
        /// here in Unpack
        /// </summary>
        private SkyDescription()
        {
        }

        /// <summary>
        /// +0x000 present_day_group : Uint4B
        /// </summary>
        public uint PresentDayGroup { get; set; }

        // TODO: Behemoth - Should these be doubles instead of longs?   Coral Golem
        /// <summary>
        /// +0x008 tick_size        : Float
        /// </summary>
        public ulong TickSize { get; set; }

        /// <summary>
        /// +0x010 light_tick_size  : Float
        /// </summary>
        public ulong LightTickSize { get; set; }

        /// <summary>
        /// +0x018 day_groups       : AC1Legacy::SmartArray<DayGroup*>
        /// </summary>
        public List<DayGroup> DayGroups = new List<DayGroup>();

        /// <summary>
        /// //----- (00501CD0) --------------------------------------------------------
        /// int __thiscall SkyDesc::UnPack(SkyDesc *this, void **addr, unsigned int *size)
        /// acclient.c 302822
        /// </summary>
        public static SkyDescription Unpack(BinaryReader reader)
        {
            SkyDescription sd = new SkyDescription();
            sd.TickSize = reader.ReadUInt64();
            sd.LightTickSize = reader.ReadUInt64();

            // client has an align_ptr here, but this should never matter as reading
            // unit64 values will not break alignment

            uint num = reader.ReadUInt32();
            for (uint i = 0; i < num; i++)
                sd.DayGroups.Add(DayGroup.Unpack(reader));

            return sd;
        }

        /// <summary>
        /// ----- (00500E10) --------------------------------------------------------
        /// void __thiscall SkyDesc::CalcPresentDayGroup(SkyDesc*this)
        /// acclient.c 301664
        /// </summary>
        /// <returns></returns>
        public SkyDescription CalculatePresentDayGroup()
        {
            // This may not be needed for physics but could be needed for stuff like
            // the graveyard where things change depending on the day part.   Coral Golem.
            throw new NotImplementedException();
        }
    }
}
