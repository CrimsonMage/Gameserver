/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// TimeOfDay in client
    /// </summary>
    public class TimeOfDay
    {
        // TODO move offset/name/type to property summary

        /*
        0:000> dt acclient!TimeOfDay
           +0x000 time_of_day_name : AC1Legacy::PStringBase<char>
           +0x004 begin            : Float
           +0x008 is_night         : Int4B
        */

        public string Name;
        public float Begin;
        public bool IsNight;

        /// <summary>
        /// //----- (005A6DD0) --------------------------------------------------------
        /// int __thiscall TimeOfDay::UnPack(TimeOfDay *this, void **addr, unsigned int *size)
        /// acclient.c 463952
        /// </summary>
        public static TimeOfDay Unpack(BinaryReader reader)
        {
            TimeOfDay t = new TimeOfDay();
            t.Begin = reader.ReadSingle();
            t.IsNight = (reader.ReadInt32() != 0);
            t.Name = reader.ReadPString();
            return t;
        }
    }
}
