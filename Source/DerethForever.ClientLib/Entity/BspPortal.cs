/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// BSPPORTAL in the client
    /// </summary>
    public class BspPortal : BspNode
    {
        internal BspPortal()
        {
            Type = BspNodeType.Port;
        }

        /// <summary>
        /// 0x038 num_portals : Uint4B
        /// </summary>
        public uint NumPortals => (uint)Portals.Count;

        /// <summary>
        /// 0x03c in_portals : Ptr32 Ptr32 CPortalPoly
        /// </summary>
        public List<PortalPolygon> Portals { get; set; } = new List<PortalPolygon>();

        /// <summary>
        /// //----- (0053DB70) --------------------------------------------------------
        /// int __thiscall BSPPORTAL::UnPackPortal(BSPPORTAL *this, void **addr, unsigned int size)
        /// acclient.c 364646
        /// </summary>
        public void UnpackPort(BinaryReader reader, BspTreeType treeType, List<Polygon> polygons)
        {
            SplittingPlane = reader.ReadPlane();

            PositiveNode = BspNode.UnpackChild(reader, treeType, polygons);
            NegativeNode = BspNode.UnpackChild(reader, treeType, polygons);

            if (treeType == BspTreeType.Drawing)
            {
                Sphere = Sphere.Unpack(reader);

                uint numPolys = reader.ReadUInt32();
                uint numPortals = reader.ReadUInt32();

                for (int i = 0; i < numPolys; i++)
                {
                    ushort polyIndex = reader.ReadUInt16();
                    InPolygons.Add(polygons[polyIndex]);
                }

                for (int i = 0; i < numPortals; i++)
                    Portals.Add(PortalPolygon.Unpack(reader, polygons));
            }
        }
    }
}
