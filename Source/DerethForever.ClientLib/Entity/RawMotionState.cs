/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU Lesser General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System.Collections.Generic;
using DerethForever.ClientLib.Enum;


namespace DerethForever.ClientLib.Entity
{
    public class RawMotionState
    {
        /// <summary>
        /// +0x004 actions          : LList<ActionNode>
        /// </summary>
        public List<ActionNode> Actions;

        /// <summary>
        ///  +0x00c current_holdkey  : HoldKey
        /// </summary>
        public HoldKey CurrentHoldKey { get; set; }

        /// <summary>
        /// +0x010 current_style    : Uint4B
        /// </summary>
        public MotionCommand CurrentStyle { get; set; }

        /// <summary>
        /// +0x014 forward_command  : Uint4B
        /// </summary>
        public MotionCommand ForwardCommand { get; set; }

        /// <summary>
        /// +0x018 forward_holdkey  : HoldKey
        /// </summary>
        public HoldKey ForwardHoldKey { get; set; }

        /// <summary>
        /// +0x01c forward_speed    : Float
        /// </summary>
        public float ForwardSpeed { get; set; }

        /// <summary>
        /// +0x020 sidestep_command : Uint4B
        /// </summary>
        public MotionCommand SideStepCommand { get; set; }

        /// <summary>
        /// +0x024 sidestep_holdkey : HoldKey
        /// </summary>
        public HoldKey SideStepHoldKey { get; set; }

        /// <summary>
        /// +0x028 sidestep_speed   : Float
        /// </summary>
        public float SideStepSpeed { get; set; }

        /// <summary>
        /// +0x02c turn_command     : Uint4B
        /// </summary>
        public MotionCommand TurnCommand { get; set; }

        /// <summary>
        /// +0x030 turn_holdkey     : HoldKey
        /// </summary>
        public HoldKey TurnHoldKey { get; set; }

        /// <summary>
        /// +0x030 turn_holdkey     : HoldKey
        /// </summary>
        public float TurnSpeed { get; set; }

    }
}
