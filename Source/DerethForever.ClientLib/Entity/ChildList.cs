/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CHILDLIST in the client
    /// </summary>
    public class ChildList
    {
        /// <summary>
        /// 0x000 num_objects : Uint2B
        /// </summary>
        public ushort NumObjects => (ushort)Objects.Count;

        /// <summary>
        /// 0x004 objects : SArray(CPhysicsObj *)
        /// </summary>
        public List<PhysicsObject> Objects { get; set; } = new List<PhysicsObject>();

        /// <summary>
        /// 0x00c frames : SArray(Frame)
        /// </summary>
        public List<Frame> Frames { get; set; } = new List<Frame>();

        /// <summary>
        /// 0x014 part_numbers : SArray(unsigned long)
        /// </summary>
        public List<uint> PartNumbers { get; set; } = new List<uint>();

        /// <summary>
        /// 0x01c location_ids : SArray(unsigned long)
        /// </summary>
        public List<uint> LocationIds { get; set; } = new List<uint>();
    }
}
