/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System.Collections.Generic;
using System.Linq;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CBuildingObj from the client
    /// </summary>
    public class Building : PhysicsObject
    {
        /// <summary>
        /// +0x178 num_portals      : Uint4B
        /// </summary>
        public uint NumPortals => (uint)Portals.Count;

        /// <summary>
        /// +0x17c portals          : Ptr32 Ptr32 CBldPortal
        /// </summary>
        public List<BuildingPortal> Portals { get; set; } = new List<BuildingPortal>();

        /// <summary>
        /// +0x180 num_leaves       : Uint4B
        /// </summary>
        public uint NumLeaves => (uint)LeafCells.Count;

        /// <summary>
        /// +0x184 leaf_cells       : Ptr32 Ptr32 CPartCell
        /// </summary>
        public List<PartCell> LeafCells { get; set; } = new List<PartCell>();

        /// <summary>
        /// +0x188 num_shadow       : Uint4B
        /// </summary>
        public uint NumShadow => (uint)ShadowList.Count;

        /// <summary>
        /// +0x18c shadow_list      : DArray(CShadowPart *)
        /// </summary>
        public List<ShadowPart> ShadowList { get; set; } = new List<ShadowPart>();

        /// <summary>
        /// //----- (006B53A0) --------------------------------------------------------
        /// CBuildingObj *__cdecl CBuildingObj::makeBuilding(IDClass data_id, unsigned int _num_portals, CBldPortal **_portals, unsigned int _num_leaves)
        /// acclient.c 719153
        /// </summary>
        public static Building MakeBuilding(uint id, List<BuildingPortal> portals, uint numLeaves)
        {
            Building b = new Building();

            b.InitObjectBegin(0, false);
            b.InitPartArray(id, true);

            // client initializes numLeaves here on the building, but doesn't actually set
            // anything - just allocates and initializes to 0s.  skip it in c#

            // copy the portals over.  client copies byref (pointer)
            b.Portals = portals.ToList();

            b.InitObjectEnd();
            return b;
        }

        /// <summary>
        /// //----- (006B5550) --------------------------------------------------------
        /// void __thiscall CBuildingObj::add_to_cell(CBuildingObj *this, CSortCell *new_cell)
        /// acclient.c 719257
        /// </summary>
        public void AddToCell(SortCell cell)
        {
            cell.AddBuilding(this);
            SetCellId(cell.Id);
            Cell = cell;
        }

        /// <summary>
        /// //----- (006B51B0) --------------------------------------------------------
        /// void __thiscall CBuildingObj::add_to_stablist(CBuildingObj *this, unsigned int **block_stab_list, unsigned int *max_size, unsigned int *stab_num)
        /// acclient.c 719041
        /// </summary>
        public void AddToStabList(List<uint> stabList, ref uint maxStabs)
        {
            foreach(var p in Portals)
            {
                p.AddToStabList(stabList, ref maxStabs);
            }
        }
    }
}
