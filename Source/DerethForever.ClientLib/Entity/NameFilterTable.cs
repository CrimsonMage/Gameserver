/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    public class NameFilterTable
    {
        public byte[] Unknown { get; set; }
        
        public List<NameTableFilterEntry> Table { get; set; } = new List<NameTableFilterEntry>();

        public static NameFilterTable Unpack(BinaryReader reader)
        {
            NameFilterTable nft = new NameFilterTable();

            // this seems totally arbitrary, and i'm not sure why this is an odd number
            // but there is a repeating structure of 5 bytes later on (the Entry), and
            // the DWORD starting at byte 23 is 0x00000003.
            nft.Unknown = reader.ReadBytes(23); 

            uint count = reader.ReadUInt32();

            for (int i = 0; i < count; i++)
                nft.Table.Add(NameTableFilterEntry.Unpack(reader));

            return nft;
        }
    }
}
