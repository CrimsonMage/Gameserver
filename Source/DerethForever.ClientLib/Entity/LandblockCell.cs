/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CLandCell in the client
    /// </summary>
    public class LandblockCell : SortCell
    {
        /// <summary>
        /// +0x100 polygons         : Ptr32 Ptr32 CPolygon
        /// </summary>
        public List<Polygon> Polygons { get; set; } = new List<Polygon>();

        /// <summary>
        /// +0x104 in_view          : BoundingType
        /// </summary>
        public BoundingType InView { get; set; }

        /// <summary>
        /// //----- (00532D60) --------------------------------------------------------
        /// signed int __thiscall CLandCell::find_collisions(CLandCell*this, CTransition* transition)
        /// acclient.c 354887
        /// </summary>
        public override TransitionState FindCollisions(Transition trans)
        {
            // TODO implement EnvironmentCell.FindCollisions
            return TransitionState.OK;
        }

        /// <summary>
        /// //----- (00532F20) --------------------------------------------------------
        /// signed int __thiscall CLandCell::find_env_collisions(CLandCell *this, CTransition *transition)
        /// acclient.c 354992
        /// </summary>
        public override TransitionState FindEnvironmentCollisions(Transition trans)
        {
            // TODO implement EnvironmentCell.FindEnvironmentCollisions
            return TransitionState.OK;
        }
    }
}
