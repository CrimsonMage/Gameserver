/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// TMTerrainDesc in the client
    /// </summary>
    public class TextureMergeTerrainDescription
    {
        /// <summary>
        /// 0x000 terrain_type : LandDefs::TerrainType
        /// </summary>
        public Enum.TerrainType TerrainType;

        /// <summary>
        /// technically a list in the client, but the dat files only ever have 1 entry.
        /// 0x004 terrain_tex : AC1Legacy::SmartArray(TerrainTex *)
        /// </summary>
        public List<TerrainTexture> TerrainTextures = new List<TerrainTexture>();

        /// <summary>
        /// private because Unpack is the only valid source of instances
        /// </summary>
        private TextureMergeTerrainDescription()
        {
        }

        /// <summary>
        /// //----- (00504260) --------------------------------------------------------
        /// int __thiscall TMTerrainDesc::UnPack(TMTerrainDesc *this, void **addr, unsigned int *size)
        /// acclient.c 305755
        /// </summary>
        public static TextureMergeTerrainDescription Unpack(BinaryReader reader)
        {
            TextureMergeTerrainDescription tmtd = new TextureMergeTerrainDescription();

            tmtd.TerrainType = (Enum.TerrainType)reader.ReadUInt32();
            tmtd.TerrainTextures.Add(TerrainTexture.Unpack(reader));

            return tmtd;
        }
    }
}
