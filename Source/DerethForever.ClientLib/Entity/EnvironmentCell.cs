/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using System.IO;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <inheritdoc />
    /// <summary>
    /// This reads an "indoor" cell from the client_cell.dat. This is mostly dungeons, but can also be a building interior.
    /// An EnvironmentCell is designated by starting 0x0100 (whereas all landcells are in the 0x0001 - 0x003E range.
    /// CEnvCell from the client
    /// </summary>
    public class EnvironmentCell : ObjectCell
    {
        // TODO move offset/name/type to property summary

        /*
            new properties not derived from ObjectCell

           =0081dd28 visible_cell_table : HashTable<unsigned long,CEnvCell *,0>
           =0081dda0 cell_flush_table : HashTable<unsigned long,CEnvCell *,0>
           +0x0f8 num_surfaces     : Uint4B
           +0x0fc surfaces         : Ptr32 Ptr32 CSurface
           +0x100 structure        : Ptr32 CCellStruct
           +0x104 env              : Ptr32 CEnvironment
           +0x110 num_static_objects : Uint4B
           +0x114 static_object_ids : Ptr32 IDClass<_tagDataID,32,0>
           +0x118 static_object_frames : Ptr32 Frame
           +0x11c static_objects   : Ptr32 Ptr32 CPhysicsObj
           +0x120 light_array      : Ptr32 RGBColor
           +0x124 incell_timestamp : Int4B
           =0081dcb8 master_incell_timestamp : Int4B
           +0x128 constructed_mesh : Ptr32 MeshBuffer
           +0x12c use_built_mesh   : Int4B
           +0x130 m_current_render_frame_num : Uint4B
           +0x134 num_view         : Uint4B
           +0x138 portal_view      : DArray<portal_view_type *>
         */
         
        public uint Flags { get; set; }

        // 0x08000000 textures (which contains degrade/quality info to reference the specific 0x06000000 graphics)
        public List<uint> Textures { get; set; } = new List<uint>();

        // the 0x0D000000 model of the pre-fab dungeon block
        public uint EnvironmentId { get; set; }

        /// <summary>
        /// +0x100 structure        : Ptr32 CCellStruct
        /// </summary>
        public ushort CellStructure { get; set; }

        /// <summary>
        /// +0x10c portals          : Ptr32 CCellPortal
        /// </summary>
        public List<CellPortal> CellPortals { get; set; } = new List<CellPortal>();

        public List<ushort> VisibleBlocks { get; set; } = new List<ushort>();
        
        /// <summary>
        /// ----- (0052D470) --------------------------------------------------------
        /// int __thiscall CEnvCell::UnPack(CEnvCell *this, void **addr, unsigned int size)
        /// acclient.c 349135
        /// </summary>
        public static EnvironmentCell Unpack(BinaryReader reader)
        {
            EnvironmentCell c = new EnvironmentCell();
            c.Id = reader.ReadUInt32();
            c.Flags = reader.ReadUInt32();

            var extraCellId = reader.ReadUInt32();

            byte numTextures = reader.ReadByte();
            byte numPortals = reader.ReadByte();
            ushort numVisibleBlocks = reader.ReadUInt16();
            
            for (uint i = 0; i < numTextures; i++)
                c.Textures.Add(0x08000000u + reader.ReadUInt16());

            c.EnvironmentId = (0x0D000000u + reader.ReadUInt16());
            c.CellStructure = reader.ReadUInt16();

            c.Position = Position.UnpackAsFrame(reader, c.Id);
            
            for (uint i = 0; i < numPortals; i++)
            {
                // reference: 349209 of acclient.c
                CellPortal cp = CellPortal.Unpack(reader, c.Id & 0xFFFF0000);
                c.CellPortals.Add(cp);
            }

            for (uint i = 0; i < numVisibleBlocks; i++)
            {
                c.VisibleBlocks.Add(reader.ReadUInt16());
            }

            if ((c.Flags & 2) != 0)
            {
                uint numObjects = reader.ReadUInt32();

                for (uint i = 0; i < numObjects; i++)
                {
                    c.Stabs.Add(Stab.Unpack(reader, c.Id));
                }
            }

            if ((c.Flags & 8) != 0)
            {
                c.RestrictionObject = reader.ReadUInt32();
            }

            // client line 349382
            // *((_DWORD *)v5 + 72) = operator new[](12 * *(_DWORD *)(*((_DWORD *)v5 + 64) + 36));
            // as best I can tell, this is myLandblock_ allocation

            c.CalcClipPlanes();

            return c;
        }

        /// <summary>
        /// //----- (0052C100) --------------------------------------------------------
        /// signed int __thiscall CEnvCell::find_collisions(CEnvCell *this, CTransition *transition)
        /// acclient.c 347810
        /// </summary>
        public override TransitionState FindCollisions(Transition trans)
        {
            // this was the vftable[5] call
            var result = FindEnvironmentCollisions(trans);

            if (result == TransitionState.OK)
                result = FindObjectCollisions(trans);

            return result;
        }

        /// <summary>
        /// //----- (0052C130) --------------------------------------------------------
        /// signed int __thiscall CEnvCell::find_env_collisions(CEnvCell *this, CTransition *transition)
        /// acclient.c 347823
        /// </summary>
        public override TransitionState FindEnvironmentCollisions(Transition trans)
        {
            // TODO implement EnvironmentCell.FindEnvironmentCollisions
            // note, this is where we start involving BSP trees and other fun stuff

            return TransitionState.OK;
        }

        /// <summary>
        /// //----- (0052D0F0) --------------------------------------------------------
        /// void __thiscall CEnvCell::calc_clip_planes(CEnvCell *this)
        /// acclient.c 348871
        /// </summary>
        private void CalcClipPlanes()
        {
            // TODO implement EnvironmentCell.CalcClipPlanes
        }
    }
}
