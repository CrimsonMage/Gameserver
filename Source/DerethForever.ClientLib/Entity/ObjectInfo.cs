/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class ObjectInfo
    {
        // TODO Behemoth - review not sure how to document this one.   dt info in header.

        public PhysicsObject PhysicsObject;
        public ObjectInfoState ObjectInfoState;
        public float Scale;
        public float StepUpHeight;
        public float StepDownHeight;
        public bool Ethereal; // int32 in client
        public bool StepDown; // int32 in client
        public int TargetId;

        /// <summary>
        /// this constructor is a substitute for the following method in the client:
        /// //----- (0050CF30) --------------------------------------------------------
        /// void __thiscall OBJECTINFO::init(OBJECTINFO* this, CPhysicsObj* _object, int object_state)
        /// acclient.c 314118
        /// </summary>
        public ObjectInfo(PhysicsObject physicsObject, ObjectInfoState state)
        {
            PhysicsObject = physicsObject;
            ObjectInfoState = state;
            Scale = physicsObject.Scale;
            StepUpHeight = physicsObject.GetStepUpHeight();
            StepDownHeight = physicsObject.GetStepDownHeight();
            Ethereal = physicsObject.State.HasFlag(PhysicsState.Ethereal);
            StepDown = !physicsObject.State.HasFlag(PhysicsState.Missile);

            if (physicsObject?.Instance != null)
            {
                if (physicsObject.Instance.IsImpenetrable())
                    ObjectInfoState |= ObjectInfoState.IsImpenetrable;

                if (physicsObject.Instance.IsPlayer())
                    ObjectInfoState |= ObjectInfoState.IsPlayer;

                if (physicsObject.Instance.IsPK())
                    ObjectInfoState |= ObjectInfoState.IsPK;

                if (physicsObject.Instance.IsPKLite())
                    ObjectInfoState |= ObjectInfoState.IsPkLite;
            }
        }
    }
}
