/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;
using DerethForever.ClientLib.Entity.AnimationHooks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CAnimHook in the client
    /// </summary>
    public class AnimationHook : IHook
    {
        public AnimationHookType HookType { get; set; }

        /// <summary>
        /// 0x004 next_hook : Ptr32 CAnimHook
        /// </summary>
        public int Direction { get; set; }

        private IHook _hook = null;

        /// <summary>
        /// 0x004 next_hook : Ptr32 CAnimHook
        /// </summary>
        public IHook NextHook
        {
            get { return _hook; }
            private set { _hook = value; }
        }

        /// <summary>
        /// ----- (005271D0) --------------------------------------------------------
        /// CAnimHook *__cdecl CAnimHook::UnPackHook(void **addr, unsigned int size)
        /// acclient.c 342737
        /// </summary>
        public static AnimationHook Unpack(BinaryReader reader)
        {
            AnimationHook h = new AnimationHook
            {
                HookType = (AnimationHookType)reader.ReadUInt32(),
                Direction = reader.ReadInt32()
            };

            // The following HookTypes have no additional properties:
            // AnimationHookType.AnimationDone
            // AnimationHookType.DefaultScript
            // CreateBlockingParticle

            switch (h.HookType)
            {
                case AnimationHookType.Sound:
                    h._hook = SoundHook.Unpack(reader);
                    break;
                case AnimationHookType.SoundTable:
                    h._hook = SoundTableHook.Unpack(reader);
                    break;
                case AnimationHookType.Attack:
                    h._hook = AttackHook.Unpack(reader);
                    break;
                case AnimationHookType.ReplaceObject:
                    h._hook = ReplaceObjectHook.Unpack(reader);
                    break;
                case AnimationHookType.Ethereal:
                    h._hook = EtherealHook.Unpack(reader);
                    break;
                case AnimationHookType.TransparentPart:
                    h._hook = TransparentPartHook.Unpack(reader);
                    break;
                case AnimationHookType.Luminous:
                    h._hook = LuminousHook.Unpack(reader);
                    break;
                case AnimationHookType.LuminousPart:
                    h._hook = LuminousPartHook.Unpack(reader);
                    break;
                case AnimationHookType.Diffuse:
                    h._hook = DiffuseHook.Unpack(reader);
                    break;
                case AnimationHookType.DiffusePart:
                    h._hook = DiffusePartHook.Unpack(reader);
                    break;
                case AnimationHookType.Scale:
                    h._hook = ScaleHook.Unpack(reader);
                    break;
                case AnimationHookType.CreateParticle:
                    h._hook = CreateParticleHook.Unpack(reader);
                    break;
                case AnimationHookType.DestroyParticle:
                    h._hook = DestroyParticleHook.Unpack(reader);
                    break;
                case AnimationHookType.StopParticle:
                    h._hook = StopParticleHook.Unpack(reader);
                    break;
                case AnimationHookType.NoDraw:
                    h._hook = NoDrawHook.Unpack(reader);
                    break;
                case AnimationHookType.DefaultScriptPart:
                    h._hook = DefaultScriptPartHook.Unpack(reader);
                    break;
                case AnimationHookType.CallPES:
                    h._hook = CallPESHook.Unpack(reader);
                    break;
                case AnimationHookType.Transparent:
                    h._hook = TransparentHook.Unpack(reader);
                    break;
                case AnimationHookType.SoundTweaked:
                    h._hook = SoundTweakedHook.Unpack(reader);
                    break;
                case AnimationHookType.SetOmega:
                    h._hook = SetOmegaHook.Unpack(reader);
                    break;
                case AnimationHookType.TextureVelocity:
                    h._hook = TextureVelocityHook.Unpack(reader);
                    break;
                case AnimationHookType.TextureVelocityPart:
                    h._hook = TextureVelocityPartHook.Unpack(reader);
                    break;
                case AnimationHookType.SetLight:
                    h._hook = SetLightHook.Unpack(reader);
                    break;
            }

            return h;
        }
    }
}
