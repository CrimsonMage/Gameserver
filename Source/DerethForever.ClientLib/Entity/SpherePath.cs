/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU Lesser General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SPHEREPATH in the client
    /// </summary>
    public class SpherePath
    {
        // TODO move offset/name/type to property summary

        /*
        0:000> dt acclient!SPHEREPATH
            +0x000 num_sphere       : Uint4B
            +0x004 local_sphere     : Ptr32 CSphere
            +0x008 local_low_point  : AC1Legacy::Vector3
            +0x014 global_sphere    : Ptr32 CSphere
            +0x018 global_low_point : AC1Legacy::Vector3
            +0x024 localspace_sphere : Ptr32 CSphere
            +0x028 localspace_low_point : AC1Legacy::Vector3
            +0x034 localspace_curr_center : Ptr32 AC1Legacy::Vector3
            +0x038 global_curr_center : Ptr32 AC1Legacy::Vector3
            +0x03c localspace_pos   : Position
            +0x084 localspace_z     : AC1Legacy::Vector3
            +0x090 begin_cell       : Ptr32 CObjCell
            +0x094 begin_pos        : Ptr32 Position
            +0x098 end_pos          : Ptr32 Position
            +0x09c curr_cell        : Ptr32 CObjCell
            +0x0a0 curr_pos         : Position
            +0x0e8 global_offset    : AC1Legacy::Vector3
            +0x0f4 step_up          : Int4B
            +0x0f8 step_up_normal   : AC1Legacy::Vector3
            +0x104 collide          : Int4B
            +0x108 check_cell       : Ptr32 CObjCell
            +0x10c check_pos        : Position
            +0x154 insert_type      : SPHEREPATH::InsertType
            +0x158 step_down        : Int4B
            +0x15c backup           : SPHEREPATH::InsertType
            +0x160 backup_cell      : Ptr32 CObjCell
            +0x164 backup_check_pos : Position
            +0x1ac obstruction_ethereal : Int4B
            +0x1b0 hits_interior_cell : Int4B
            +0x1b4 bldg_check       : Int4B
            +0x1b8 walkable_allowance : Float
            +0x1bc walk_interp      : Float
            +0x1c0 step_down_amt    : Float
            +0x1c4 walkable_check_pos : CSphere
            +0x1d4 walkable         : Ptr32 CPolygon
            +0x1d8 check_walkable   : Int4B
            +0x1dc walkable_up      : AC1Legacy::Vector3
            +0x1e8 walkable_pos     : Position
            +0x230 walkable_scale   : Float
            +0x234 cell_array_valid : Int4B
            +0x238 neg_step_up      : Int4B
            +0x23c neg_collision_normal : AC1Legacy::Vector3
            +0x248 neg_poly_hit     : Int4B
            +0x24c placement_allows_sliding : Int4B
        */

        public Sphere[] LocalSpheres;
        public Vector3 LocalLowPoint;
        public Sphere[] GlobalSpheres;
        public Vector3 GlobalLowPoint;
        public Sphere[] LocalSpaceSpheres;
        public Vector3 LocalSpaceLowPoint;
        public Vector3[] LocalSpaceCurrentCenter;
        public Vector3[] GlobalCurrentCenter;
        public Position LocalSpacePosition;
        public Vector3 LocalSpaceZ;
        public ObjectCell BeginCell;
        public Position BeginPosition;
        public Position EndPosition;
        public ObjectCell CurrentCell;
        public Position CurrentPosition;
        public Vector3 GlobalOffset;
        public int StepUp; // possible boolean
        public Vector3 StepUpNormal;
        public int Collide; // possible boolean
        public ObjectCell CheckCell;
        public Position CheckPosition;
        public SpherePathInsertType InsertType;
        public int StepDown; // possible boolean
        public SpherePathInsertType Backup;
        public ObjectCell BackupCell;
        public Position BackupCellPosition;
        public int ObstructionEthereal; // likely boolean
        public bool HitsInteriorCell; // int in client
        public bool BuildingCheck; // int in client
        public float WalkableAllowance;
        public float WalkInterp;
        public float StepDownAmount;
        public Sphere WalkableCheckPosition;
        public Polygon Walkable;
        public int CheckWalkable; // possible boolean
        public Vector3 WalkableUp;
        public Position WalkablePosition;
        public float WalkableScale;
        public bool CellArrayValid;
        public int NegStepUp;
        public Vector3 NegCollisionNormal;
        public int NegPolyHit;
        public bool PlacementAllowsSliding; // int in client

        /// <summary>
        /// //----- (0050C670) --------------------------------------------------------
        /// void __thiscall SPHEREPATH::init_sphere(SPHEREPATH *this, const unsigned int _num_sphere, CSphere *_sphere, const float _scale)
        /// acclient.c 313647
        /// </summary>
        public void InitializeSpheres(Sphere[] spheres, float scale)
        {
            if (spheres?.Length > 0)
            {
                LocalSpheres = new Sphere[spheres.Length];

                for (int i = 0; i < spheres.Length; i++)
                    LocalSpheres[i] = new Sphere(spheres[i].Origin * scale, spheres[i].Radius * scale);
            }

            this.LocalLowPoint = LocalSpheres[0].Origin;
            this.LocalLowPoint.Z -= LocalSpheres[0].Radius;
        }

        /// <summary>
        /// //----- (0050CE20) --------------------------------------------------------
        /// void __thiscall SPHEREPATH::init_path(SPHEREPATH*this, CObjCell* _begin_cell, Position* _begin_pos, Position* _end_pos)
        /// acclient.c 314043
        /// </summary>
        public void InitializePath(ObjectCell beginCell, Position beginPosition, Position endPosition)
        {
            BeginCell = beginCell;
            BeginPosition = beginPosition;
            EndPosition = endPosition;

            if (beginPosition != null)
            {
                CurrentPosition.CellId = beginPosition.CellId;
                CurrentPosition.Frame = new Frame(beginPosition.Frame);
                // client calls SPHEREPATH::cache_global_curr_center here
                InsertType = SpherePathInsertType.TransitionInsert;
            }
            else
            {
                CurrentPosition.CellId = endPosition.CellId;
                CurrentPosition.Frame = new Frame(endPosition.Frame);
                // client calls SPHEREPATH::cache_global_curr_center here
                InsertType = SpherePathInsertType.PlacementInsert;
            }
        }
    }
}
