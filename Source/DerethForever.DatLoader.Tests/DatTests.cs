/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using DerethForever.Common;
using System;
using System.Threading;
using DerethForever.Database;
using System.Collections.Generic;
using DerethForever.Entity.Enum;
using DerethForever.DatLoader.FileTypes;
using System.Drawing;

namespace DerethForever.DatLoader.Tests
{
    [TestClass]
    public class DatTests
    {
        private static WorldJsonDatabase WorldDb { get; set; }

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // copy config.json and initialize configuration
            File.Copy(Path.Combine(System.Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);
            ConfigManager.Initialize();
            WorldDb = new WorldJsonDatabase(ConfigManager.Config.Database.World.Folder);
            DatManager.Initialize();
        }

        /// <summary>
        /// Gets backgrounds from Dat File.
        /// </summary>
        /// <remarks>If you uncomment the code at the end of this function, you can save backgrounds to disk.</remarks>
        [TestMethod]
        public void GetBackgrounds_NoExceptions()
        {
            List<RenderSurface> backgrounds = new List<RenderSurface>();
            foreach (ItemType itemType in Enum.GetValues(typeof(ItemType)))
            {
                backgrounds.Add(ImageBuilder.GetBackgroundPngIcon(itemType));
            }

            // UNCOMMENT TO SAVE BACKGROUNDS TO DISK:
            // // next try saving:
            // foreach (var background in backgrounds.Distinct())
            // {
            //    debugSaveImage(background.GetBitmap(), background.Id);
            // }

            // filter out duplicate backgrounds:
            backgrounds = backgrounds.GroupBy(b => b.Id).Select(i => i.First()).ToList();
            Assert.IsTrue(backgrounds.Count == 14);
        }

        [TestMethod]
        public void GetFullyLayeredPngIcon_NoExceptions()
        {
            var item = WorldDb.GetWeenie(35545);
            // This is needed in the CreateIconImage class (only here to save ALOT of time in other unit tests):
            DatabaseManager.Initialize();
            var image = ImageBuilder.GetFullyLayeredPngIcon(item.WeenieClassId);

            // UNCOMMENT TO SAVE IMAGE TO DISK:
            // next try saving:
            // debugSaveImage(image.GetBitmap(), image.Id);

            Assert.IsTrue(image.GetByteArray().Length > 80);
        }

        [TestMethod]
        public void GetPrimaryIconFromDatabaseWeenie_NoExceptions()
        {
            // instance a mote
            var mote = WorldDb.GetWeenie(6353);
            var image = ImageBuilder.GetPrimaryPngIcon((uint)mote.IconDID);

            // UNCOMMENT TO SAVE IMAGE TO DISK:
            // next try saving:
            // debugSaveImage(image.GetBitmap(), image.Id);

            Assert.IsTrue(image.GetByteArray().Length > 80);
        }

        //[TestMethod]
        //public void GetRandomFullyLayeredPngIcon_NoExceptions()
        //{
        //    int count = 0;
        //    // This is needed in the CreateIconImage class (only here to save ALOT of time in other unit tests):
        //    DatabaseManager.Initialize();
        //    List<DerethForever.Entity.CachedWeenieClass> items = new List<DerethForever.Entity.CachedWeenieClass>();
        //    foreach (var type in Enum.GetValues(typeof(ItemType)))
        //    {
        //        items = WorldDb.GetRandomWeeniesOfType(Convert.ToUInt32(type), (uint)10);
        //        if (items != null)
        //        {
        //            foreach (var item in items)
        //            {
        //                var image = ImageBuilder.GetFullyLayeredPngIcon(item.WeenieClassId);
        //                if (image != null)
        //                    count++;

        //                // UNCOMMENT TO SAVE IMAGE TO DISK:
        //                // next try saving:
        //                // debugSaveImage(image.GetBitmap(), image.Id);

        //                Assert.IsTrue(image.GetByteArray().Length > 126);
        //            }
        //        }
        //    }
        //    Assert.IsTrue(count > 100);
        //    Console.WriteLine(count);
        //}

        [TestMethod]
        public void LoadCellAndPortal_NoExceptions()
        {
            // Load
            string celldat = ConfigManager.Config.Server.DatFilesDirectory + @"client_cell_1.dat";
            DatDatabase cell = new DatDatabase(celldat, DatDatabaseType.Cell);
            string portaldatfile = ConfigManager.Config.Server.DatFilesDirectory + @"client_portal.dat";
            DatDatabase portal = new DatDatabase(portaldatfile, DatDatabaseType.Portal);
            cell.ReadDat();
            portal.ReadDat();

            // Test
            int count = cell.AllFiles.Count();
            Assert.IsFalse((count < 804956));
            count = portal.AllFiles.Count();
            Assert.IsFalse((count < 78961));
        }

        /// <summary>
        /// Loads 4 dats into memory, using 4 threads.
        /// </summary>
        // [TestMethod]
        public void LoadManyDatsAsThreads_NoExceptions()
        {
            bool failed = false;
            int iterations = 4;

            // threads interact with some object - either 
            Thread thread1 = new Thread(new ThreadStart(delegate()
            {
                for (int i = 0; i < iterations; i++)
                {
                    try
                    {
                        string celldat = ConfigManager.Config.Server.DatFilesDirectory + @"client_cell_1.dat";
                        DatDatabase cell = new DatDatabase(celldat, DatDatabaseType.Cell);
                        cell.ReadDat();
                        Assert.IsFalse((cell.AllFiles.Count <= 0));
                    }
                    catch
                    {
                        failed = true;
                    }
                }
            }));
            Thread thread2 = new Thread(new ThreadStart(delegate()
            {
                for (int i = 0; i < iterations; i++)
                {
                    try
                    {
                        string portaldatfile = ConfigManager.Config.Server.DatFilesDirectory + @"client_portal.dat";
                        DatDatabase portal = new DatDatabase(portaldatfile, DatDatabaseType.Portal);
                        portal.ReadDat();
                        Assert.IsFalse((portal.AllFiles.Count <= 0));
                    }
                    catch
                    {
                        failed = true;
                    }
                }
            }));
            Thread thread3 = new Thread(new ThreadStart(delegate()
            {
                for (int i = 0; i < iterations; i++)
                {
                    try
                    {
                        string celldat = ConfigManager.Config.Server.DatFilesDirectory + @"client_cell_1.dat";
                        DatDatabase cell = new DatDatabase(celldat, DatDatabaseType.Cell);
                        cell.ReadDat();
                        Assert.IsFalse((cell.AllFiles.Count <= 0));
                    }
                    catch
                    {
                        failed = true;
                    }
                }
            }));
            Thread thread4 = new Thread(new ThreadStart(delegate()
            {
                for (int i = 0; i < iterations; i++)
                {
                    try
                    {
                        string portaldatfile = ConfigManager.Config.Server.DatFilesDirectory + @"client_portal.dat";
                        DatDatabase portal = new DatDatabase(portaldatfile, DatDatabaseType.Portal);
                        portal.ReadDat();
                        Assert.IsFalse((portal.AllFiles.Count <= 0));
                    }
                    catch
                    {
                        failed = true;
                    }
                }
            }));

            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();

            thread1.Join();
            thread2.Join();
            thread3.Join();
            thread4.Join();

            Assert.IsFalse(failed);
        }

        /// <summary>
        /// Loads 4 dats into memory, single threaded.
        /// </summary>
        // [TestMethod]
        public void LoadManyDats_NoExceptions()
        {
            // Load
            string celldat = ConfigManager.Config.Server.DatFilesDirectory + @"client_cell_1.dat";
            string portaldatfile = ConfigManager.Config.Server.DatFilesDirectory + @"client_portal.dat";
            DatDatabase cell1 = new DatDatabase(celldat, DatDatabaseType.Cell);
            DatDatabase cell2 = new DatDatabase(celldat, DatDatabaseType.Cell);
            DatDatabase portal1 = new DatDatabase(portaldatfile, DatDatabaseType.Portal);
            DatDatabase portal2 = new DatDatabase(portaldatfile, DatDatabaseType.Portal);
            cell1.ReadDat();
            portal1.ReadDat();
            cell2.ReadDat();
            portal2.ReadDat();
            var cell3 = new CellDatDatabase(celldat);
            var cell4 = new CellDatDatabase(celldat);
            var portal3 = new PortalDatDatabase(portaldatfile);
            var portal4 = new PortalDatDatabase(portaldatfile);
            var count = cell1.AllFiles.Count() + cell2.AllFiles.Count() + cell3.AllFiles.Count() + cell4.AllFiles.Count();
            count += portal1.AllFiles.Count() + portal2.AllFiles.Count() + portal3.AllFiles.Count() + portal4.AllFiles.Count();
        }

        [TestMethod]
        public void LoadCellDat_NoExceptions()
        {
            string celldat = ConfigManager.Config.Server.DatFilesDirectory + @"client_cell_1.dat";
            DatDatabase dat = new DatDatabase(celldat, DatDatabaseType.Cell);
            dat.ReadDat();
            int count = dat.AllFiles.Count();
            Assert.IsFalse((count < 804956));
        }

        [TestMethod]
        public void LoadCellDatDatabase_NoExceptions()
        {
            string celldat = ConfigManager.Config.Server.DatFilesDirectory + @"client_cell_1.dat";
            CellDatDatabase dat = new CellDatDatabase(celldat);
            int count = dat.AllFiles.Count();
            Assert.IsFalse((count < 804956));
        }

        [TestMethod]
        public void LoadPortalDat_NoExceptions()
        {
            string portaldatfile = ConfigManager.Config.Server.DatFilesDirectory + @"client_portal.dat";
            DatDatabase dat = new DatDatabase(portaldatfile, DatDatabaseType.Portal);
            dat.ReadDat();
            int count = dat.AllFiles.Count();
            Assert.IsFalse((count < 78961));
        }

        [TestMethod]
        public void LoadPortalDatDatabase_NoExceptions()
        {
            string portaldatfile = ConfigManager.Config.Server.DatFilesDirectory + @"client_portal.dat";
            PortalDatDatabase dat = new PortalDatDatabase(portaldatfile);
            int count = dat.AllFiles.Count();
            Assert.IsFalse((count < 78961));
        }

        // uncomment if you want to run this
        // [TestMethod]
        public void ExportPortalDatsWithTypeInfo()
        {
            string dat = ConfigManager.Config.Server.DatFilesDirectory + @"client_portal.dat";
            string output = ConfigManager.Config.Server.DatFilesDirectory + @"typed_portal_dat_export";
            PortalDatDatabase db = new PortalDatDatabase(dat);
            db.ExtractCategorizedContents(output);
        }

        // uncomment if you want to run this
        // [TestMethod]
        public void ExtractCellDatByLandblock()
        {
            string celldat = ConfigManager.Config.Server.DatFilesDirectory + @"client_cell_1.dat";
            string output = ConfigManager.Config.Server.DatFilesDirectory + @"cell_dat_export_by_landblock";
            CellDatDatabase db = new CellDatDatabase(celldat);
            db.ExtractLandblockContents(output);
        }

        private static void debugSaveImage(Bitmap image, uint imageId)
        {
            Bitmap debugImage = new Bitmap(image.Width, image.Height, image.PixelFormat);

            var folderPath = Path.Combine(System.Environment.CurrentDirectory, "UnitTestIconDebugImages\\");

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);
            var filePath = folderPath + imageId + ".PNG";
            image.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
        }
    }
}
