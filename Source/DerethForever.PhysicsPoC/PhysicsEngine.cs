/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using BulletSharp;
using DerethForever.DatLoader;
using DerethForever.DatLoader.FileTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Entity;
using DerethForever.DatLoader.Entity;

namespace DerethForever.PhysicsPoC
{
    public class PhysicsEngine : IDisposable
    {
        private DynamicsWorld _bulletWorld;

        const int NumVertsX = 9;
        const int NumVertsY = 9;

        Vector3 worldMin = new Vector3(0f, 0f, 0f);
        Vector3 worldMax = new Vector3(192f, 500f, 192f);
        TriangleIndexVertexArray indexVertexArrays;
        BvhTriangleMeshShape groundShape;
        RigidBody staticBody;

        protected CollisionConfiguration CollisionConf;
        protected CollisionDispatcher Dispatcher;
        protected BroadphaseInterface Broadphase;
        protected ConstraintSolver Solver;

        private List<CollisionShape> _objects { get; } = new List<CollisionShape>();

        public PhysicsEngine()
        {
            CollisionConf = new DefaultCollisionConfiguration();
            Dispatcher = new CollisionDispatcher(CollisionConf);

            Broadphase = new AxisSweep3(worldMin, worldMax);
            Solver = new SequentialImpulseConstraintSolver();

            // initialize the world
            _bulletWorld = new DiscreteDynamicsWorld(Dispatcher, Broadphase, Solver, CollisionConf);
            _bulletWorld.SolverInfo.SplitImpulse = 1;
            _bulletWorld.Gravity = new Vector3(0f, -9.8f, 0f);


        }

        public DynamicsWorld World { get { return _bulletWorld; } }

        public void LoadLandlock()
        { 
            // not a whole lot to do here - we should probably just load on demand
            // we'll load rithwic as a proof of concept though (C98C)
            uint landblockId = 0xC98C0000;

            var info = CLandblockInfo.ReadFromDat(landblockId);
            var cellInfo = CellLandblock.ReadFromDat(landblockId);

            GenerateBvhGround(cellInfo);
            
            // info will have the cell count.  theory: this applies to dungeons only.  skip for now
            for (uint i = 0; i < info.NumCells; i++)
            {
                uint cellId = landblockId & 0x0100 & i;

                // cells are loaded via EnvCell.  the tangled web continues
                if (!DatManager.CellDat.AllFiles.ContainsKey(cellId))
                    break;

                var cell = EnvCell.ReadFromDat(cellId);
                
            }

            for (int b = 0; b < info.Buildings.Count; b++)
            {
                var building = info.Buildings[b];

                var buildingModel = AddModel(building.ModelId, building.Frame);

                for (int s = 0; s < buildingModel.Spheres.Count; s++)
                {
                    CSphere sphere = buildingModel.Spheres[s];
                    
                }
            }

            for (int i = 0; i < info.ObjectIds.Count; i++)
            {
                uint objectId = info.ObjectIds[i];

                
            }
        }

        /// <summary>
        /// Adds a model to the physics world
        /// </summary>
        /// <param name="modelId">lower 3 bytes of the model id.  function will look for 0x01 and 0x02 models.</param>
        /// <param name="placement">where to put it</param>
        /// <param name="isStatic">true for immobile landscape and buildings</param>
        private SetupModel AddModel(uint modelId, Position placement, bool isStatic = true)
        {
            uint model01 = 0x01000000 & modelId;
            uint model02 = 0x02000000 & modelId;
            var model = SetupModel.ReadFromDat(model01);

            return model;
        }

        private void GenerateBvhGround(CellLandblock cellInfo)
        {
            CollisionShape groundShape = new BoxShape(50, 3, 50);
            _objects.Add(groundShape);

            CollisionConf = new DefaultCollisionConfiguration();
            Dispatcher = new CollisionDispatcher(CollisionConf);
            Solver = new SequentialImpulseConstraintSolver();

            Broadphase = new AxisSweep3(worldMin, worldMax);
            
            int i;

            //create a triangle-mesh ground
            int vertStride = Vector3.SizeInBytes;
            int indexStride = 3 * sizeof(int);
            
            const int totalVerts = NumVertsX * NumVertsY;

            const int totalTriangles = 2 * (NumVertsX - 1) * (NumVertsY - 1);

            TriangleIndexVertexArray vertexArray = new TriangleIndexVertexArray();
            IndexedMesh mesh = new IndexedMesh();
            mesh.Allocate(totalTriangles, totalVerts, indexStride, vertStride, PhyScalarType.Int32, PhyScalarType.Single);

            using (var data = mesh.LockVerts())
            {
                for (i = 0; i < NumVertsX; i++)
                {
                    for (int j = 0; j < NumVertsY; j++)
                    {
                        float height = cellInfo.HeightMap[i * 9 + j];

                        data.Write((i - NumVertsX * 0.5f) * 24);
                        data.Write(height);
                        data.Write((j - NumVertsY * 0.5f) * 24);
                    }
                }
            }

            int index = 0;
            IntArray idata = mesh.TriangleIndices;
            for (i = 0; i < NumVertsX - 1; i++)
            {
                for (int j = 0; j < NumVertsY - 1; j++)
                {
                    idata[index++] = j * NumVertsX + i;
                    idata[index++] = j * NumVertsX + i + 1;
                    idata[index++] = (j + 1) * NumVertsX + i + 1;

                    idata[index++] = j * NumVertsX + i;
                    idata[index++] = (j + 1) * NumVertsX + i + 1;
                    idata[index++] = (j + 1) * NumVertsX + i;
                }
            }

            vertexArray.AddIndexedMesh(mesh);
            groundShape = new BvhTriangleMeshShape(vertexArray, true);

            _objects.Add(groundShape);
            
            //create ground object
            RigidBody ground = LocalCreateRigidBody(0, Matrix.Identity, groundShape);
            ground.UserObject = "Ground";
            
        }

        //private void GenerateGround(CellLandblock cellInfo)
        //{
        //    // cellInfo has the heightmap in it
        //    const int totalVerts = NumVertsX * NumVertsY;
        //    const int totalTriangles = 2 * (NumVertsX - 1) * (NumVertsY - 1);
        //    const int triangleIndexStride = 3 * sizeof(int);
        //    indexVertexArrays = new TriangleIndexVertexArray();

        //    var mesh = new IndexedMesh();
        //    mesh.Allocate(totalTriangles, totalVerts, triangleIndexStride, Vector3.SizeInBytes, PhyScalarType.Int32, PhyScalarType.Single);
        //    DataStream indices = mesh.LockIndices();

        //    for (int x = 0; x < NumVertsX - 1; x++)
        //    {
        //        for (int y = 0; y < NumVertsY - 1; y++)
        //        {
        //            // this creates 2 triangles that form a square from (x,y) to (x+1,y+1) with a diagonal
        //            // between those points (as opposed to perpendicular).  that is, like this:
        //            //
        //            // * * * * *
        //            // *     * *
        //            // *   *   *
        //            // * *     *
        //            // * * * * *

        //            int row1Index = x * NumVertsX + y;
        //            int row2Index = row1Index + NumVertsX;
        //            indices.Write(row1Index);
        //            indices.Write(row1Index + 1);
        //            indices.Write(row2Index + 1);

        //            indices.Write(row1Index);
        //            indices.Write(row2Index + 1);
        //            indices.Write(row2Index);
        //        }
        //    }
        //    indices.Dispose();

        //    indexVertexArrays = new TriangleIndexVertexArray();
        //    indexVertexArrays.AddIndexedMesh(mesh);

        //    DataStream vertexBuffer, indexBuffer;
        //    int numVerts, numFaces;
        //    PhyScalarType vertsType, indicesType;
        //    int vertexStride, indexStride;
        //    indexVertexArrays.GetLockedVertexIndexData(out vertexBuffer, out numVerts, out vertsType, out vertexStride,
        //        out indexBuffer, out indexStride, out numFaces, out indicesType);

        //    for (int i = 0; i < NumVertsX; i++)
        //    {
        //        for (int j = 0; j < NumVertsY; j++)
        //        {
        //            vertexBuffer.Write((i - NumVertsX * 0.5f) * 24);
        //            vertexBuffer.Write(cellInfo.HeightMap[i * 9 + j]);
        //            vertexBuffer.Write((j - NumVertsY * 0.5f) * 24);
        //        }
        //    }

        //    vertexBuffer.Dispose();
        //    indexBuffer.Dispose();

        //    const bool useQuantizedAabbCompression = true;
        //    groundShape = new BvhTriangleMeshShape(indexVertexArrays, useQuantizedAabbCompression);
        //    _objects.Add(groundShape);

        //    staticBody = LocalCreateRigidBody(0.0f, Matrix.Identity, groundShape);
        //    staticBody.CollisionFlags |= CollisionFlags.StaticObject;
        //    staticBody.UserObject = "Ground";

        //}

        public virtual RigidBody LocalCreateRigidBody(float mass, Matrix startTransform, CollisionShape shape)
        {
            //rigidbody is dynamic if and only if mass is non zero, otherwise static
            bool isDynamic = (mass != 0.0f);

            Vector3 localInertia = Vector3.Zero;
            if (isDynamic)
                shape.CalculateLocalInertia(mass, out localInertia);

            //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
            DefaultMotionState myMotionState = new DefaultMotionState(startTransform);

            RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
            RigidBody body = new RigidBody(rbInfo);
            rbInfo.Dispose();

            _bulletWorld.AddRigidBody(body);

            return body;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ExitPhysics();
            }
        }

        public virtual void ExitPhysics()
        {
            if (_bulletWorld != null)
            {
                //remove/dispose constraints
                int i;
                for (i = _bulletWorld.NumConstraints - 1; i >= 0; i--)
                {
                    TypedConstraint constraint = _bulletWorld.GetConstraint(i);
                    _bulletWorld.RemoveConstraint(constraint);
                    constraint.Dispose();
                }

                //remove the rigidbodies from the dynamics world and delete them
                for (i = _bulletWorld.NumCollisionObjects - 1; i >= 0; i--)
                {
                    CollisionObject obj = _bulletWorld.CollisionObjectArray[i];
                    RigidBody body = obj as RigidBody;
                    if (body != null && body.MotionState != null)
                    {
                        body.MotionState.Dispose();
                    }
                    _bulletWorld.RemoveCollisionObject(obj);
                    obj.Dispose();
                }

                //delete collision shapes
                foreach (CollisionShape shape in _objects)
                    shape.Dispose();
                _objects.Clear();

                _bulletWorld.Dispose();
                Broadphase.Dispose();
                Dispatcher.Dispose();
                CollisionConf.Dispose();
            }

            if (Broadphase != null)
            {
                Broadphase.Dispose();
            }
            if (Dispatcher != null)
            {
                Dispatcher.Dispose();
            }
            if (CollisionConf != null)
            {
                CollisionConf.Dispose();
            }
        }
        
    }
}
