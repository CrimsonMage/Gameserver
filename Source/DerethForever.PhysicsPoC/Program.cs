/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Common;
using DerethForever.DatLoader.FileTypes;
using Environment = System.Environment;
using DerethForever.DatLoader;
using DerethForever.Physics;

namespace DerethForever.PhysicsPoC
{
    public class Program
    {
        private const uint _lifestone = 0x20002EE;
        private const uint _cottage = 0x01000bc3;
        private const uint _behemoth = 33556427;
        private const uint _deewain = 33556426;
        private const uint _rithwickLandblockEastBridge = 0xC98C0000;
        private const uint _rithwickLandblockWestBridge = 0xC88C0000;

        static void Main(string[] args)
        {
            // copy config over to this project
            File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);
            ConfigManager.Initialize();
            DatManager.Initialize();

            using (LandblockDemo demo = new LandblockDemo(_rithwickLandblockWestBridge))
            {
                demo.Run();
            }

            //using (ModelDemo demo = new ModelDemo(_deewain))
            //{
            //    demo.Run();
            //}
        }
    }
}
