using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DerethForever.Common;
using DerethForever.Database;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using Newtonsoft.Json;

namespace DerethForever.ConsoleHelpers
{
    public enum Commands
    {
        [Display(Name = "0) Exit")]
        Exit = 0,

        [Display(Name = "1) Export Weenies")]
        ExportWeenies = 1,

        [Display(Name = "2) Export Recipes")]
        ExportRecipes = 2,

        [Display(Name = "3) Export Spawn Maps")]
        ExportSpawnMaps = 3,

        [Display(Name = "4) Export Points of Interest")]
        ExportPointsOfInterest = 4,

        [Display(Name = "5) Load World Database")]
        LoadWorldDatabase = 5
    }

    class Program
    {

        static void Main(string[] args)
        {
            bool exit = false;
            PrintMenu();

            while (!exit)
            {
                Console.Write(@"Enter Option >> ");
                Commands? command = null;

                try
                {
                    command = (Commands)int.Parse(Console.ReadLine());
                }
                catch
                {
                    PrintMenu();
                    continue;
                }

                switch (command)
                {
                    case Commands.Exit:
                        exit = true;
                        break;
                    case Commands.ExportWeenies:
                        //ExportWeenies();
                        break;
                    case Commands.ExportRecipes:
                        //ExportRecipes();
                        break;
                    case Commands.ExportSpawnMaps:
                        //ExportSpawnMaps();
                        break;
                    case Commands.ExportPointsOfInterest:
                        //ExportPoi();
                        break;
                    case Commands.LoadWorldDatabase:
                        LoadDb();
                        break;
                    default:
                        PrintMenu();
                        break;
                }
            }
        }

        private static void LoadDb()
        {
            WorldJsonDatabase db = new WorldJsonDatabase(@"C:\dev\behemoth\Gitlab\DerethForever\GameWorld\json");
        }

        private static void PrintMenu()
        {
            Console.WriteLine("Options:");
            foreach (Commands cmd in Enum.GetValues(typeof(Commands)))
            {
                var display = GetAttribute<DisplayAttribute>(cmd);
                Console.WriteLine(display.Name);
            }
        }

        public static T GetAttribute<T>(Enum enumVal) where T : Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }

        //private static void ExportRecipes()
        //{
        //    bool exit = false;
        //    bool dirFound = false;
        //    string directory = "";

        //    while (!exit && !dirFound)
        //    {
        //        Console.WriteLine("Enter Export directory or \"exit\" to quit:");
        //        Console.Write(">> ");
        //        directory = Console.ReadLine();
        //        if (directory == "exit")
        //        {
        //            exit = true;
        //            break;
        //        }

        //        if (!Directory.Exists(directory))
        //        {
        //            Console.WriteLine("Directory not found.");
        //            continue;
        //        }
        //        else
        //        {
        //            dirFound = true;
        //            break;
        //        }
        //    }

        //    if (exit)
        //        return;

        //    if (Directory.EnumerateFiles(directory).Count() > 0)
        //    {
        //        Console.WriteLine("Directory already has files, continue? (Y/n)");
        //        var confirm = Console.ReadKey();
        //        if (confirm.Key == ConsoleKey.N)
        //            return;
        //    }
            
        //    // copy config.json
        //    File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);

        //    ConfigManager.Initialize();
        //    var worldDb = new Database.WorldDatabase();
        //    worldDb.Initialize(ConfigManager.Config.Database.World.Host,
        //        ConfigManager.Config.Database.World.Port,
        //        ConfigManager.Config.Database.World.Username,
        //        ConfigManager.Config.Database.World.Password,
        //        DatabaseSelectionOption.World);

        //    var recipes = worldDb.GetAllRecipes();

        //    Console.WriteLine($"Exporting {recipes.Count} recipes...");

        //    ThreadPool.SetMaxThreads(64, 64);

        //    JsonSerializerSettings serializer = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    Parallel.ForEach(recipes, x =>
        //    {
        //        string content = JsonConvert.SerializeObject(x, Formatting.None, serializer);
        //        string path = Path.Combine(directory, $"{x.RecipeGuid}.json");
        //        File.WriteAllText(path, content);
        //    });

        //    sw.Stop();
        //    Console.WriteLine($"{recipes.Count} recipes exported in {sw.Elapsed} seconds.");
        //}

        //private static void ExportSpawnMaps()
        //{
        //    bool exit = false;
        //    bool dirFound = false;
        //    string directory = "";

        //    while (!exit && !dirFound)
        //    {
        //        Console.WriteLine("Enter Export directory or \"exit\" to quit:");
        //        Console.Write(">> ");
        //        directory = Console.ReadLine();
        //        if (directory == "exit")
        //        {
        //            exit = true;
        //            break;
        //        }

        //        if (!Directory.Exists(directory))
        //        {
        //            Console.WriteLine("Directory not found.");
        //            continue;
        //        }
        //        else
        //        {
        //            dirFound = true;
        //            break;
        //        }
        //    }

        //    if (exit)
        //        return;

        //    if (Directory.EnumerateFiles(directory).Count() > 0)
        //    {
        //        Console.WriteLine("Directory already has files, continue? (Y/n)");
        //        var confirm = Console.ReadKey();
        //        if (confirm.Key == ConsoleKey.N)
        //            return;
        //    }
            
        //    // copy config.json
        //    File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);

        //    ConfigManager.Initialize();
        //    var worldDb = new Database.WorldDatabase();
        //    worldDb.Initialize(ConfigManager.Config.Database.World.Host,
        //        ConfigManager.Config.Database.World.Port,
        //        ConfigManager.Config.Database.World.Username,
        //        ConfigManager.Config.Database.World.Password,
        //        DatabaseSelectionOption.World);

        //    Console.WriteLine($"Exporting spawn maps...");

        //    ushort taskCount = 0xFFFF;
        //    int exportedBlocks = 0;
        //    List<ushort> landblockX = new List<ushort>();
        //    for (ushort i = 0; i <= 0xFF; i++)
        //        landblockX.Add(i);

        //    ThreadPool.SetMaxThreads(64, 64);

        //    JsonSerializerSettings serializer = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    Parallel.ForEach(landblockX, x =>
        //    {
        //        for (ushort y = 0; y <= 255; y++)
        //        {
        //            ushort block = (ushort)((x << 8) + y);
        //            var data = worldDb.Export(block);
        //            if (data != null && data.Count > 0)
        //            {
        //                string content = JsonConvert.SerializeObject(data, Formatting.None, serializer);
        //                string path = Path.Combine(directory, $"{block:X4}.json");
        //                File.WriteAllText(path, content);
        //                exportedBlocks++;
        //            }
        //        }
        //    });

        //    sw.Stop();
        //    Console.WriteLine($"{exportedBlocks} maps exported in {sw.Elapsed} seconds.");
        //}

        //private static void ExportPoi()
        //{
        //    string filename = "points-of-interest.json";
        //    bool exit = false;
        //    bool dirFound = false;
        //    string directory = "";

        //    while (!exit && !dirFound)
        //    {
        //        Console.WriteLine("Enter Export directory or \"exit\" to quit:");
        //        Console.Write(">> ");
        //        directory = Console.ReadLine();
        //        if (directory == "exit")
        //        {
        //            exit = true;
        //            break;
        //        }

        //        if (!Directory.Exists(directory))
        //        {
        //            Console.WriteLine("Directory not found.");
        //            continue;
        //        }
        //        else
        //        {
        //            dirFound = true;
        //            break;
        //        }
        //    }

        //    if (exit)
        //        return;

        //    var filePath = Path.Combine(directory, filename);
        //    if (File.Exists(filePath))
        //    {
        //        Console.WriteLine("Point-of-interest file already exists, continue? (Y/n)");
        //        var confirm = Console.ReadKey();
        //        if (confirm.Key == ConsoleKey.N)
        //            return;
        //    }

        //    // copy config.json
        //    File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);

        //    ConfigManager.Initialize();
        //    var worldDb = new Database.WorldDatabase();
        //    worldDb.Initialize(ConfigManager.Config.Database.World.Host,
        //        ConfigManager.Config.Database.World.Port,
        //        ConfigManager.Config.Database.World.Username,
        //        ConfigManager.Config.Database.World.Password,
        //        DatabaseSelectionOption.World);

        //    var poi = worldDb.GetPointsOfInterest();

        //    Console.WriteLine($"Exporting {poi.Count} points of interest...");
            
        //    JsonSerializerSettings serializer = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();
            
        //    string content = JsonConvert.SerializeObject(poi, Formatting.None, serializer);
        //    File.WriteAllText(filePath, content);

        //    sw.Stop();
        //    Console.WriteLine($"{poi.Count} points of interest exported in {sw.Elapsed} seconds.");
        //}

        //private static void ExportWeenies()
        //{
        //    bool exit = false;
        //    bool dirFound = false;
        //    string directory = "";

        //    while (!exit && !dirFound)
        //    {
        //        Console.WriteLine("Enter Export directory or \"exit\" to quit:");
        //        Console.Write(">> ");
        //        directory = Console.ReadLine();
        //        if (directory == "exit")
        //        {
        //            exit = true;
        //            break;
        //        }

        //        if (!Directory.Exists(directory))
        //        {
        //            Console.WriteLine("Directory not found.");
        //            continue;
        //        }
        //        else
        //        {
        //            dirFound = true;
        //            break;
        //        }
        //    }

        //    if (exit)
        //        return;

        //    if (Directory.EnumerateFiles(directory).Count() > 0)
        //    {
        //        Console.WriteLine("Directory already has files, continue? (Y/n)");
        //        var confirm = Console.ReadKey();
        //        if (confirm.Key == ConsoleKey.N)
        //            return;
        //    }

        //    Console.WriteLine("Enter number to export (leave empty for all)");
        //    Console.Write(">> ");
        //    string number = Console.ReadLine();
        //    if (!int.TryParse(number, out int weenieCount))
        //        weenieCount = int.MaxValue;

        //    // copy config.json
        //    File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);

        //    ConfigManager.Initialize();
        //    var worldDb = new Database.WorldDatabase();
        //    worldDb.Initialize(ConfigManager.Config.Database.World.Host,
        //        ConfigManager.Config.Database.World.Port,
        //        ConfigManager.Config.Database.World.Username,
        //        ConfigManager.Config.Database.World.Password,
        //        DatabaseSelectionOption.World);

        //    var everything = worldDb.SearchWeenies(new Entity.SearchWeeniesCriteria(), weenieCount);
            
        //    ThreadPool.SetMaxThreads(64, 64);

        //    JsonSerializerSettings serializer = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    Parallel.ForEach(everything, x =>
        //    {
        //        var w = worldDb.GetWeenie(x.WeenieClassId);
        //        string content = JsonConvert.SerializeObject(w, Formatting.None, serializer);
        //        string path = Path.Combine(directory, $"{w.WeenieClassId}.json");
        //        File.WriteAllText(path, content);
        //    });

        //    sw.Stop();
        //    Console.WriteLine($"{everything.Count} weenies exported in {sw.Elapsed} seconds.");
   
        //}
    }
}
