/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

using DerethForever.Entity.Enum;
using DerethForever.Network;
using DerethForever.Network.GameMessages.Messages;

namespace DerethForever.Command.Handlers
{
    public static class HelpCommands
    {
        // dfhelp (command)
        [CommandHandler("dfhelp", AccessLevel.Player, CommandHandlerFlag.None, 0, "Displays help.", "(command)")]
        public static void HandleDFHelp(Session session, params string[] parameters)
        {         
            if (parameters?.Length <= 0)
            {
                if (session != null)
                {
                    session.Network.EnqueueSend(new GameMessageSystemChat("Note: You may substitute a forward slash (/) for the at symbol (@).", ChatMessageType.Broadcast));
                    session.Network.EnqueueSend(new GameMessageSystemChat("Use @help to get more information about commands supported by the client.", ChatMessageType.Broadcast));
                    session.Network.EnqueueSend(new GameMessageSystemChat("Available help:", ChatMessageType.Broadcast));
                    session.Network.EnqueueSend(new GameMessageSystemChat("@dfhelp commands - Lists all commands.", ChatMessageType.Broadcast));
                    session.Network.EnqueueSend(new GameMessageSystemChat("You can also use @dfcommands to get a complete list of the supported Dereth Forever commands available to you.", ChatMessageType.Broadcast));
                    session.Network.EnqueueSend(new GameMessageSystemChat("To get more information about a specific command, use @dfhelp command", ChatMessageType.Broadcast));
                }
                else
                {
                    // TODO: DFHELP output for console
                }
                return;
            }

            if (parameters?[0] == "commands") // Mimick @help commands command
            {
                HandleDFCommands(session, parameters);
                return;
            }
                
            foreach (var command in CommandManager.GetCommandByName(parameters[0]))
            {
                if (session != null)
                {
                    if (command.Attribute.Flags == CommandHandlerFlag.ConsoleInvoke)
                        continue;
                    if (session.AccessLevel < command.Attribute.Access)
                        continue;
                    session.Network.EnqueueSend(new GameMessageSystemChat($"@{command.Attribute.Command} - {command.Attribute.Description}", ChatMessageType.Broadcast));
                    session.Network.EnqueueSend(new GameMessageSystemChat($"Usage: @{command.Attribute.Command} {command.Attribute.Usage}", ChatMessageType.Broadcast));

                    return;
                }
                else
                {
                    if (command.Attribute.Flags == CommandHandlerFlag.RequiresWorld)
                        continue;
                    Console.WriteLine($"{command.Attribute.Command} - {command.Attribute.Description}");
                    Console.WriteLine($"Usage: {command.Attribute.Command} {command.Attribute.Usage}");

                    return;
                }
            }

            if (session != null)
            {
                session.Network.EnqueueSend(new GameMessageSystemChat($"Unknown command: {parameters[0]}", ChatMessageType.Help));
                session.Network.EnqueueSend(new GameMessageSystemChat("Use @dfcommands to get a complete list of commands available for you to use.", ChatMessageType.Broadcast));
                session.Network.EnqueueSend(new GameMessageSystemChat("To get more information about a specific command, use @dfhelp command", ChatMessageType.Broadcast));
            }
            else
                Console.WriteLine($"Unknown command: {parameters[0]}");

            return;
        }

        // dfcommands
        [CommandHandler("dfcommands", AccessLevel.Player, CommandHandlerFlag.None, 0, "Lists all commands.")]
        public static void HandleDFCommands(Session session, params string[] parameters)
        {
            List<String> commandList = new List<string>();

            if (session != null)
            {
                session.Network.EnqueueSend(new GameMessageSystemChat("Note: You may substitute a forward slash (/) for the at symbol (@).", ChatMessageType.Broadcast));
                session.Network.EnqueueSend(new GameMessageSystemChat("For more information, type @dfhelp < command >.", ChatMessageType.Broadcast));
            }
            else
                Console.WriteLine("For more information, type dfhelp < command >.");

            foreach (var command in CommandManager.GetCommands())
            {
                if (session != null)
                {
                    if (command.Attribute.Flags == CommandHandlerFlag.ConsoleInvoke) // Skip Console Commands
                        continue;
                    if (session.AccessLevel < command.Attribute.Access) // Skip Commands which are higher than your current access level
                        continue;

                    commandList.Add(string.Format("@{0} - {1}", command.Attribute.Command, command.Attribute.Description));
                }
                else
                {
                    if (command.Attribute.Flags == CommandHandlerFlag.RequiresWorld) // Skip Commands that only work in game
                        continue;

                    commandList.Add(string.Format("{0} - {1}", command.Attribute.Command, command.Attribute.Description));
                }
            }

            commandList.Sort();

            for (int i = 0; i < commandList.Count; i++)
            {
                string message = commandList[i];
                if (session != null)
                    session.Network.EnqueueSend(new GameMessageSystemChat(message, ChatMessageType.Broadcast));
                else
                    Console.WriteLine(message);
            }
        }
    }
}
