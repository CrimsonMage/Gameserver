﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

namespace DerethForever.Actors.Actions
{
    public class NestedActionQueue : ActionQueue, IAction
    {
        private IActor parent;
        private LinkedListNode<IAction> node;
        private bool enqueued;
        private Tuple<IActor, IAction> nextAct;

        public NestedActionQueue()
        {
            enqueued = false;
            nextAct = null;
        }

        public NestedActionQueue(IActor parent)
        {
            this.parent = parent;
            enqueued = false;
            nextAct = null;
        }

        /// <summary>
        /// NOTE: NOT thread-safe w.r.t. addParent, action queue running
        /// Should /ONLY/ be run when old parent cannot be running
        /// </summary>
        public void RemoveParent()
        {
            lock (queueLock)
            {
                if (enqueued && parent != null)
                {
                    parent.DequeueAction(node);
                }
                parent = null;
                node = null;
            }
        }

        /// <summary>
        /// NOT Thread safe
        /// </summary>
        /// <param name="parent"></param>
        public void SetParent(IActor parent)
        {
            lock (queueLock)
            {
                this.parent = parent;
                if (enqueued && parent != null)
                {
                    node = parent.EnqueueAction(this);
                }
            }
        }

        /// <summary>
        /// Thread safe
        /// Enqueues an action in our actionQueue, and automatically enqueues our NestedActionQueue in its parent
        ///    (If the parent is a NestedActionQueue this process repeats -- woot)
        /// </summary>
        /// <param name="action"></param>
        public override LinkedListNode<IAction> EnqueueAction(IAction action)
        {
            bool needEnqueue = false;
            LinkedListNode<IAction> ret;
            lock (queueLock)
            {
                ret = queue.AddLast(action);

                if (enqueued == false)
                {
                    enqueued = true;
                    needEnqueue = parent != null;
                }
            }

            // Has to be done after I unlock to avoid cyclical dependencies
            // The race between adding an action to my queue, and adding an action to their queue is safe
            //   They will eventually run my action, even if more come along, and I will still enqueue only once (enqueued=true is locked)
            if (needEnqueue)
            {
                node = parent.EnqueueAction(this);
            }

            return ret;
        }

        public Tuple<IActor, IAction> Act()
        {
            // We're not in our parent's queue anymore
            enqueued = false;
            node = null;

            RunActions();

            var ret = nextAct;

            return ret;
        }

        /// <summary>
        /// NOT Thread safe, must be set before EnqueueAction
        /// </summary>
        /// <param name="nextActor"></param>
        /// <param name="nextAction"></param>
        public void RunOnFinish(IActor nextActor, IAction nextAction)
        {
            nextAct = new Tuple<IActor, IAction>(nextActor, nextAction);
        }
    }
}
