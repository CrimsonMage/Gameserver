﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Managers;

namespace DerethForever.Actors.Actions
{
    /// <summary>
    /// Action that will not return until WorldManager.PortalTickYears >= EndTime
    /// must only be inserted into DelayManager actor
    /// </summary>
    public class DelayAction : ActionEventBase, IComparable<DelayAction>
    {
        public double WaitTime { get; private set; }
        public double EndTime { get; private set; }

        // For breaking ties on compareto, two actions cannot be equal
        private long sequence;
        private volatile static uint glblSequence = 0;

        public DelayAction(double waitTimePortalTickYears) : base()
        {
            WaitTime = waitTimePortalTickYears;
            sequence = glblSequence++;
        }

        public void Start()
        {
            EndTime = WorldManager.PortalYearTicks + WaitTime;
        }

        public int CompareTo(DelayAction rhs)
        {
            int ret = EndTime.CompareTo(rhs.EndTime);
            if (ret == 0)
            {
                return sequence.CompareTo(rhs.sequence);
            }
            return ret;
        }
    }
}
