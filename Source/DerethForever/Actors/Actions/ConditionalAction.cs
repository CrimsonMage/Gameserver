﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Actors.Actions
{
    public class ConditionalAction : IAction
    {
        public ActionChain TrueChain { get; private set; }
        public ActionChain FalseChain { get; private set; }
        public Func<bool> Condition { get; private set; }

        public ConditionalAction(Func<bool> condition, ActionChain trueChain, ActionChain falseChain)
        {
            TrueChain = trueChain;
            FalseChain = falseChain;
        }

        public void RunOnFinish(IActor actor, IAction action)
        {
            TrueChain.AddAction(actor, action);
            FalseChain.AddAction(actor, action);
        }

        public Tuple<IActor, IAction> Act()
        {
            bool cond = Condition();

            if (cond)
            {
                return new Tuple<IActor, IAction>(TrueChain.FirstElement.Actor, TrueChain.FirstElement.Action);
            }
            else
            {
                return new Tuple<IActor, IAction>(FalseChain.FirstElement.Actor, FalseChain.FirstElement.Action);
            }
        }
    }
}
