/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using DerethForever.Database;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Factories;

namespace DerethForever.Actors
{
    /// <summary>
    /// ** Usage Data Flow **
    ///     HandleActionApproachVendor
    /// ** Buy Data Flow **
    /// Player.HandleActionBuy->Vendor.BuyItemsValidateTransaction->Player.HandleActionBuyFinalTransaction->Vendor.BuyItemsFinalTransaction
    /// ** Sell Data Flow **
    /// Player.HandleActionSell->Vendor.SellItemsValidateTransaction->Player.HandleActionSellFinalTransaction->Vendor.SellItemsFinalTransaction
    /// </summary>
    public class Vendor : Creature
    {
        private readonly Dictionary<ObjectGuid, WorldObject> defaultItemsForSale = new Dictionary<ObjectGuid, WorldObject>();
        private readonly Dictionary<ObjectGuid, WorldObject> uniqueItemsForSale = new Dictionary<ObjectGuid, WorldObject>();

        // todo : SO : Turning to player movement states  - looks at @og
        public Vendor(DataObject dataO)
            : base(dataO)
        {
            CreateVendorItems();
        }

        private void CreateVendorItems()
        {
            foreach (CreationProfile creationProfile in DataWeenie.CreateList)
            {
                if (creationProfile?.WeenieClassId == null)
                    continue;

                WorldObject item = WorldObjectFactory.CreateNewWorldObject((uint)creationProfile.WeenieClassId);
                if (item == null)
                    continue;

                if (creationProfile.Palette != null)
                    item.PaletteTemplate = (int)creationProfile.Palette;

                item.Shade = creationProfile.Shade;
                if (creationProfile.StackSize != null)
                    item.StackSize = (ushort)creationProfile.StackSize;

                if (creationProfile.Destination == null ||
                    (DestinationType)creationProfile.Destination != DestinationType.Shop)
                    continue;
                item.ContainerId = Guid.Full;
                defaultItemsForSale.Add(item.Guid, item);
            }
        }

        public double BuyRate
        {
            get { return DataObject.BuyRate ?? 0.8; }
            set { DataObject.BuyRate = value; }
        }

        public double SellRate
        {
            get { return DataObject.SellRate ?? 1.0; }
            set { DataObject.SellRate = value; }
        }

        /// <summary>
        /// Fired when Client clicks on the Vendor World Object
        /// </summary>
        /// <param name="playerId"></param>
        public override void ActOnUse(ObjectGuid playerId)
        {
            if (!(CurrentLandblock.GetObject(playerId) is Player player))
            {
                return;
            }

            if (!player.IsWithinUseRadiusOf(this))
                player.DoMoveTo(this);
            else
            {
                ApproachVendor(player);
            }
        }

        /// <summary>
        /// Used to convert Weenie based objects / not used for unique items
        /// </summary>
        private static IEnumerable<WorldObject> ItemProfileToWorldObjects(ItemProfile itemprofile)
        {
            List<WorldObject> worldobjects = new List<WorldObject>();
            while (itemprofile.Amount > 0)
            {
                WorldObject wo = WorldObjectFactory.CreateNewWorldObject(itemprofile.WeenieClassId);
                // can we stack this ?
                if (wo.MaxStackSize.HasValue)
                {
                    if ((wo.MaxStackSize.Value != 0) & (wo.MaxStackSize.Value <= itemprofile.Amount))
                    {
                        wo.StackSize = wo.MaxStackSize.Value;
                        worldobjects.Add(wo);
                        itemprofile.Amount = itemprofile.Amount - wo.MaxStackSize.Value;
                    }
                    else // we cant stack this but its not a single item
                    {
                        wo.StackSize = (ushort)itemprofile.Amount;
                        worldobjects.Add(wo);
                        itemprofile.Amount = itemprofile.Amount - itemprofile.Amount;
                    }
                }
                else
                {
                    // if there multiple items of the same  type..
                    if (itemprofile.Amount <= 0)
                        continue;
                    // single item with no stack options.
                    itemprofile.Amount = itemprofile.Amount - 1;
                    wo.StackSize = null;
                    worldobjects.Add(wo);
                }
            }
            return worldobjects;
        }

        /// <summary>
        /// Sends Vendor Inventory to player
        /// </summary>
        /// <param name="player"></param>
        private void ApproachVendor(Player player)
        {
            // default inventory
            List<WorldObject> vendorlist = defaultItemsForSale.Select(wo => wo.Value).ToList();
            vendorlist.AddRange(uniqueItemsForSale.Select(wo => wo.Value));

            // unique inventory - items sold by other players

            player.TrackInteractiveObjects(vendorlist);
            player.HandleActionApproachVendor(this, vendorlist);
        }

        /// <summary>
        /// Player has started a buy transaction
        /// Create objects, send object to player for final validation.
        /// </summary>
        /// <param name="vendorid">GUID of Vendor</param>
        /// <param name="items">Item Profile, Amount and ID</param>
        /// <param name="player"></param>
        public void BuyValidateTransaction(ObjectGuid vendorid, List<ItemProfile> items, Player player)
        {
            // que transactions.
            List<ItemProfile> filteredlist = new List<ItemProfile>();
            List<WorldObject> uqlist = new List<WorldObject>();
            List<WorldObject> genlist = new List<WorldObject>();

            uint goldcost = 0;

            // filter items out vendor no longer has in stock or never had in stock
            foreach (ItemProfile item in items)
            {
                // check default items for id
                if (defaultItemsForSale.ContainsKey(item.Guid))
                {
                    item.WeenieClassId = defaultItemsForSale[item.Guid].WeenieClassId;
                    filteredlist.Add(item);
                }
                // check unique items / add unique items to purchase list / remove from vendor list
                if (!uniqueItemsForSale.ContainsKey(item.Guid))
                    continue;

                if (!uniqueItemsForSale.TryGetValue(item.Guid, out WorldObject wo))
                    continue;
                uqlist.Add(wo);
                uniqueItemsForSale.Remove(item.Guid);
            }

            // convert profile to wold objects / stack logic does not include unique items.
            foreach (ItemProfile fitem in filteredlist)
            {
                genlist.AddRange(ItemProfileToWorldObjects(fitem));
            }

            // calculate price. (both unique and item profile)
            foreach (WorldObject wo in uqlist)
            {
                goldcost = goldcost + (uint)Math.Ceiling(SellRate * (wo.Value ?? 0) * (wo.StackSize ?? 1) - 0.1);
                wo.Value = wo.Value;        // Also set the stack's value for unique items, using the builtin WO calculations
                wo.Burden = wo.Burden;      // Also set the stack's encumbrance for unique items, using the builtin WO calculations
            }

            foreach (WorldObject wo in genlist)
            {
                goldcost = goldcost + (uint)Math.Ceiling(SellRate * (wo.Value ?? 0) * (wo.StackSize ?? 1) - 0.1);
                wo.Value = wo.Value;        // Also set the stack's value for stock items, using the builtin WO calculations
                wo.Burden = wo.Burden;      // Also set the stack's encumbrance for stock items, using the builtin WO calculations
            }

            // send transaction to player for further processing and.
            player.FinalizeBuyTransaction(this, uqlist, genlist, true, goldcost);
        }

        /// <summary>
        /// Handles the final phase of the transaction.. removing unique items and updating players local
        /// from vendors items list.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="uqlist"></param>
        /// <param name="valid"></param>
        public void BuyItemsFinalTransaction(Player player, List<WorldObject> uqlist, bool valid)
        {
            if (!valid) // re-add unique temp stock items.
            {
                foreach (WorldObject wo in uqlist)
                {
                    if (!defaultItemsForSale.ContainsKey(wo.Guid))
                        uniqueItemsForSale.Add(wo.Guid, wo);
                }
            }
            ApproachVendor(player);
        }

        public void SellItemsValidateTransaction(Player player, List<WorldObject> items)
        {
            // todo: filter rejected / accepted send item spec result back to player
            uint payout = 0;
            List<WorldObject> accepted = new List<WorldObject>();
            List<WorldObject> rejected = new List<WorldObject>();

            foreach (WorldObject wo in items)
            {
                // payout scaled by the vendor's buy rate
                payout = payout + (uint)Math.Floor(BuyRate * (wo.Value ?? 0) * (wo.StackSize ?? 1) + 0.1);

                if (!wo.MaxStackSize.HasValue & !wo.MaxStructure.HasValue)
                {
                    wo.Location = null;
                    wo.ContainerId = Guid.Full;
                    wo.Placement = null;
                    wo.WielderId = null;
                    wo.CurrentWieldedLocation = null;                    
                    // This is needed to make items lay flat on the ground.
                    wo.AnimationFrame = (int)Entity.Enum.Placement.Resting;
                    uniqueItemsForSale.Add(wo.Guid, wo);
                }
                accepted.Add(wo);
            }

            ApproachVendor(player);
            player.FinalizeSellTransaction(this, true, accepted, payout);
        }
    }
}
