/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.DatLoader.Entity;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Network;
using DerethForever.Network.GameEvent.Events;

namespace DerethForever.Actors
{
    using System;
    using System.Diagnostics;

    public class Gem : WorldObject
    {
        /// <summary>
        /// This is ace_object_property_int #349.   It links a contract weenie with the quest that it will add to your quest panel.
        /// </summary>
        public int? UseCreateContractId
        {
            get { return DataObject.UseCreateContractId; }
            set { DataObject.UseCreateContractId = value; }
        }

        public Gem(DataObject dataObject)
            : base(dataObject)
        {
        }

        /// <summary>
        /// The OnUse method for this class is to use a contract to add a tracked quest to our quest panel.
        /// This gives the player access to information about the quest such as starting and ending NPC locations,
        /// and shows our progress for kill tasks as well as any timing information such as when we can repeat the
        /// quest or how much longer we have to complete it in the case of at timed quest.   Og II
        /// </summary>
        /// <param name="session">Pass the session variable so we will have access to player and the correct sequences</param>
        public override void OnUse(Session session)
        {
            if (UseCreateContractId == null) return;
            ContractTracker contractTracker = new ContractTracker((uint)UseCreateContractId, session.Player.Guid.Full)
            {
                Stage = 0,
                TimeWhenDone = 0,
                TimeWhenRepeats = 0,
                DeleteContract = 0,
                SetAsDisplayContract = 1
            };

            DateTime lastUse;
            if (CooldownId != null && session.Player.LastUseTracker.TryGetValue(CooldownId.Value, out lastUse))
            {
                TimeSpan timeRemaining = lastUse.AddSeconds(CooldownDuration ?? 0.00).Subtract(DateTime.Now);
                if (timeRemaining.Seconds > 0)
                {
                    ChatPacket.SendServerMessage(session, "You cannot use another contract for " + timeRemaining.Seconds + " seconds", ChatMessageType.Broadcast);
                    session.Player.SendUseDoneEvent();
                    return;
                }
            }

            // We need to see if we are tracking this quest already.   Also, I cannot be used on world, so I must have a container id
            if (!session.Player.TrackedContracts.ContainsKey((uint)UseCreateContractId) && ContainerId != null)
            {
                session.Player.TrackedContracts.Add((uint)UseCreateContractId, contractTracker);

                // This will track our use for each contract using the shared cooldown server side.
                if (CooldownId != null)
                {
                    // add or update.
                    if (!session.Player.LastUseTracker.ContainsKey(CooldownId.Value))
                        session.Player.LastUseTracker.Add(CooldownId.Value, DateTime.Now);
                    else
                        session.Player.LastUseTracker[CooldownId.Value] = DateTime.Now;
                }

                GameEventSendClientContractTracker contractMsg = new GameEventSendClientContractTracker(session, contractTracker);
                session.Network.EnqueueSend(contractMsg);
                ChatPacket.SendServerMessage(session, "You just added " + contractTracker.ContractDetails.ContractName, ChatMessageType.Broadcast);

                // TODO: Add sending the 02C2 message UpdateEnchantment.   They added a second use to this existing system
                // so they could show the delay on the client side - it is not really an enchantment but the they overloaded the use. Og II
                // Thanks Slushnas for letting me know about this as well as an awesome pcap that shows it all in action.

                // TODO: there is a lot of work to do here.   I am stubbing this in for now to send the right message.   Lots of magic numbers at the moment.
                Debug.Assert(CooldownId != null, "CooldownId != null");
                Debug.Assert(CooldownDuration != null, "CooldownDuration != null");
                const uint layer = 0x10000; // FIXME: we need to track how many layers of the exact same spell we have in effect.
                const uint spellCategory = 0x8000; // FIXME: Not sure where we get this from
                SpellBase spellBase = new SpellBase
                {
                    Power = 0,
                    Duration = CooldownDuration.Value,
                    DegradeModifier = 0,
                    DegradeLimit = -666
            };
                session.Network.EnqueueSend(new GameEventMagicUpdateEnchantment(session, spellBase, layer, spellCategory, CooldownId.Value, (uint)EnchantmentTypeFlags.Cooldown));

                // Ok this was not known to us, so we used the contract - now remove it from inventory.
                // HandleActionRemoveItemFromInventory is has it's own action chain.
                session.Player.HandleActionRemoveItemFromInventory(Guid.Full, (uint)ContainerId, 1);
            }
            else
                ChatPacket.SendServerMessage(session, "You already have this quest tracked: " + contractTracker.ContractDetails.ContractName, ChatMessageType.Broadcast);

            // No mater any condition we need to send the use done event to clear the hour glass from the client.
            session.Player.SendUseDoneEvent();
        }
    }
}
