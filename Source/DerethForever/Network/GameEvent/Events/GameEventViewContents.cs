/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Actors;
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Network.GameEvent.Events
{
    using System.Diagnostics;

    public class GameEventViewContents : GameEventMessage
    {
        public GameEventViewContents(Session session, DataObject container)
            : base(GameEventType.ViewContents, GameMessageGroup.Group09, session)
        {
            Writer.Write(container.DataObjectId);

            Writer.Write((uint)container.Inventory.Count);
            foreach (DataObject inv in container.Inventory.Values)
            {
                Writer.Write(inv.DataObjectId);
                Debug.Assert(inv.WeenieType != null, "container.WeenieType != null");
                if ((WeenieType)inv.WeenieType == WeenieType.Container)
                    Writer.Write((uint)ContainerType.Container);
                else
                    Writer.Write((uint)ContainerType.NonContainer);
            }
        }
    }
}
