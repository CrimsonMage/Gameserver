/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Linq;
using DerethForever.Common.Extensions;
using System.Collections.Generic;
using System.IO;
using DerethForever.Actors;
using DerethForever.Common;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.DatUtil;
using DerethForever.Database;
using DerethForever.DatLoader.Entity;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Factories;
using DerethForever.Managers;
using DerethForever.Network.Enum;
using DerethForever.Network.GameMessages;
using DerethForever.Network.GameMessages.Messages;

namespace DerethForever.Network.Handlers
{
    public static class CharacterHandler
    {
        [GameMessage(GameMessageOpcode.CharacterEnterWorldRequest, SessionState.AuthConnected)]
        public static void CharacterEnterWorldRequest(ClientMessage message, Session session)
        {
            session.Network.EnqueueSend(new GameMessageCharacterEnterWorldServerReady());
        }

        [GameMessage(GameMessageOpcode.CharacterEnterWorld, SessionState.AuthConnected)]
        public static void CharacterEnterWorld(ClientMessage message, Session session)
        {
            ObjectGuid guid = message.Payload.ReadGuid();

            string clientString = message.Payload.ReadString16L();

            if (clientString != session.ClientAccountString)
            {
                session.SendCharacterError(CharacterError.EnterGameCharacterNotOwned);
                return;
            }

            CachedCharacter cachedCharacter = session.AccountCharacters.SingleOrDefault(c => c.Guid.Full == guid.Full);
            if (cachedCharacter == null)
            {
                session.SendCharacterError(CharacterError.EnterGameCharacterNotOwned);
                return;
            }

            session.CharacterRequested = cachedCharacter;

            session.InitSessionForWorldLogin();

            session.State = SessionState.WorldConnected;

            LandblockManager.PlayerEnterWorld(session, cachedCharacter.Guid);
        }

        [GameMessage(GameMessageOpcode.CharacterDelete, SessionState.AuthConnected)]
        public static void CharacterDelete(ClientMessage message, Session session)
        {
            string clientString = message.Payload.ReadString16L();
            uint characterSlot = message.Payload.ReadUInt32();

            if (clientString != session.ClientAccountString)
            {
                session.SendCharacterError(CharacterError.Delete);
                return;
            }

            CachedCharacter cachedCharacter = session.AccountCharacters.SingleOrDefault(c => c.SlotId == characterSlot);
            if (cachedCharacter == null)
            {
                session.SendCharacterError(CharacterError.Delete);
                return;
            }

            // TODO: check if character is already pending removal

            session.Network.EnqueueSend(new GameMessageCharacterDelete());

            DatabaseManager.Shard.DeleteOrRestore(Time.GetUnixTime() + 3600ul, cachedCharacter.Guid.Full, ((bool deleteOrRestoreSuccess) =>
            {
                if (deleteOrRestoreSuccess)
                {
                    DatabaseManager.Shard.GetCharacters(session.SubscriptionId, ((List<CachedCharacter> result) =>
                    {
                        session.UpdateCachedCharacters(result);
                        session.Network.EnqueueSend(new GameMessageCharacterList(result, session.ClientAccountString));
                    }));
                }
                else
                {
                    session.SendCharacterError(CharacterError.Delete);
                }
            }));
        }

        [GameMessage(GameMessageOpcode.CharacterRestore, SessionState.AuthConnected)]
        public static void CharacterRestore(ClientMessage message, Session session)
        {
            ObjectGuid guid = message.Payload.ReadGuid();

            CachedCharacter cachedCharacter = session.AccountCharacters.SingleOrDefault(c => c.Guid.Full == guid.Full);
            if (cachedCharacter == null)
                return;

            DatabaseManager.Shard.IsCharacterNameAvailable(cachedCharacter.Name, ((bool isAvailable) =>
            {
                if (!isAvailable)
                {
                    SendCharacterCreateResponse(session, CharacterGenerationVerificationResponse.NameInUse);
                }
                else
                {
                    DatabaseManager.Shard.DeleteOrRestore(0, cachedCharacter.Guid.Full, ((bool deleteOrRestoreSuccess) =>
                    {
                        if (deleteOrRestoreSuccess)
                            session.Network.EnqueueSend(new GameMessageCharacterRestore(guid, cachedCharacter.Name, 0u));
                        else
                            SendCharacterCreateResponse(session, CharacterGenerationVerificationResponse.Corrupt);
                    }));
                }
            }));
        }

        [GameMessage(GameMessageOpcode.CharacterCreate, SessionState.AuthConnected)]
        public static void CharacterCreate(ClientMessage message, Session session)
        {
            string clientString = message.Payload.ReadString16L();
            if (clientString != session.ClientAccountString)
                return;

            uint id = GuidManager.NewPlayerGuid().Full;

            CharacterCreateEx(message, session, id);
        }

        private static void CharacterCreateEx(ClientMessage message, Session session, uint id)
        {
            CharGen cg = CharGen.ReadFromDat();
            BinaryReader reader = message.Payload;
            DataCharacter character = new DataCharacter(id);
            
            reader.Skip(4);   /* Unknown constant (1) */
            character.Heritage = (int)reader.ReadUInt32();
            character.HeritageGroup = cg.HeritageGroups[(int)character.Heritage].Name;
            character.Gender = (int)reader.ReadUInt32();
            if (character.Gender == 1)
                character.Sex = "Male";
            else
                character.Sex = "Female";
            Appearance appearance = Appearance.FromNetwork(reader);

            // character.IconId = cg.HeritageGroups[(int)character.Heritage].IconImage;

            // pull character data from the dat file
            SexCG sex = cg.HeritageGroups[(int)character.Heritage].SexList[(int)character.Gender];

            character.MotionTableId = sex.MotionTable;
            character.SoundTableId = sex.SoundTable;
            character.PhysicsTableId = sex.PhysicsTable;
            character.SetupTableId = sex.SetupID;
            character.PaletteId = sex.BasePalette;
            character.CombatTableId = sex.CombatTable;

            // Check the character scale
            if (sex.Scale != 100u)
            {
                character.DefaultScale = (sex.Scale / 100f); // Scale is stored as a percentage
            }

            // Get the hair first, because we need to know if you're bald, and that's the name of that tune!
            HairStyleCG hairstyle = sex.HairStyleList[Convert.ToInt32(appearance.HairStyle)];
            bool isBald = hairstyle.Bald;

            // Certain races (Undead, Tumeroks, Others?) have multiple body styles available. This is controlled via the "hair style".
            if (hairstyle.AlternateSetup > 0)
                character.SetupTableId = hairstyle.AlternateSetup;

            character.EyesTexture = sex.GetEyeTexture(appearance.Eyes, isBald);
            character.DefaultEyesTexture = sex.GetDefaultEyeTexture(appearance.Eyes, isBald);
            character.NoseTexture = sex.GetNoseTexture(appearance.Nose);
            character.DefaultNoseTexture = sex.GetDefaultNoseTexture(appearance.Nose);
            character.MouthTexture = sex.GetMouthTexture(appearance.Mouth);
            character.DefaultMouthTexture = sex.GetDefaultMouthTexture(appearance.Mouth);
            character.HairTexture = sex.GetHairTexture(appearance.HairStyle);
            character.DefaultHairTexture = sex.GetDefaultHairTexture(appearance.HairStyle);
            character.HeadObject = sex.GetHeadObject(appearance.HairStyle);

            // Skin is stored as PaletteSet (list of Palettes), so we need to read in the set to get the specific palette
            PaletteSet skinPalSet = PaletteSet.ReadFromDat(sex.SkinPalSet);
            character.SkinPalette = skinPalSet.GetPaletteID(appearance.SkinHue);

            // Hair is stored as PaletteSet (list of Palettes), so we need to read in the set to get the specific palette
            PaletteSet hairPalSet = PaletteSet.ReadFromDat(sex.HairColorList[Convert.ToInt32(appearance.HairColor)]);
            character.HairPalette = hairPalSet.GetPaletteID(appearance.HairHue);

            // Eye Color
            character.EyesPalette = sex.EyeColorList[Convert.ToInt32(appearance.EyeColor)];

            if (appearance.HeadgearStyle < 0xFFFFFFFF) // No headgear is max UINT
            {
                uint headgearWeenie = sex.GetHeadgearWeenie(appearance.HeadgearStyle);
                ClothingTable headCT = ClothingTable.ReadFromDat(sex.GetHeadgearClothingTable(appearance.HeadgearStyle));
                uint headgearIconId = headCT.GetIcon(appearance.HeadgearColor);

                DataObject hat = (DataObject)DatabaseManager.World.GetDataObjectByWeenie(headgearWeenie).Clone(GuidManager.NewItemGuid().Full);
                hat.SpellIdProperties = new List<DataObjectPropertiesSpell>();
                hat.IconDID = headgearIconId;
                hat.Placement = 0;
                hat.CurrentWieldedLocation = hat.ValidLocations;
                hat.WielderIID = id;
                hat.PaletteTemplate = appearance.HeadgearColor;
                hat.Shade = appearance.HeadgearHue;

                character.WieldedItems.Add(new ObjectGuid(hat.DataObjectId), hat);
            }

            uint shirtWeenie = sex.GetShirtWeenie(appearance.ShirtStyle);
            ClothingTable shirtCT = ClothingTable.ReadFromDat(sex.GetShirtClothingTable(appearance.ShirtStyle));
            uint shirtIconId = shirtCT.GetIcon(appearance.ShirtColor);

            DataObject shirt = (DataObject)DatabaseManager.World.GetDataObjectByWeenie(shirtWeenie).Clone(GuidManager.NewItemGuid().Full);
            shirt.SpellIdProperties = new List<DataObjectPropertiesSpell>();
            shirt.IconDID = shirtIconId;
            shirt.Placement = 0;
            shirt.CurrentWieldedLocation = shirt.ValidLocations;
            shirt.WielderIID = id;
            shirt.PaletteTemplate = appearance.ShirtColor;
            shirt.Shade = appearance.ShirtHue;

            character.WieldedItems.Add(new ObjectGuid(shirt.DataObjectId), shirt);

            uint pantsWeenie = sex.GetPantsWeenie(appearance.PantsStyle);
            ClothingTable pantsCT = ClothingTable.ReadFromDat(sex.GetPantsClothingTable(appearance.PantsStyle));
            uint pantsIconId = pantsCT.GetIcon(appearance.PantsColor);

            DataObject pants = (DataObject)DatabaseManager.World.GetDataObjectByWeenie(pantsWeenie).Clone(GuidManager.NewItemGuid().Full);
            pants.SpellIdProperties = new List<DataObjectPropertiesSpell>();
            pants.IconDID = pantsIconId;
            pants.Placement = 0;
            pants.CurrentWieldedLocation = pants.ValidLocations;
            pants.WielderIID = id;
            pants.PaletteTemplate = appearance.PantsColor;
            pants.Shade = appearance.PantsHue;

            character.WieldedItems.Add(new ObjectGuid(pants.DataObjectId), pants);

            uint footwearWeenie = sex.GetFootwearWeenie(appearance.FootwearStyle);
            ClothingTable footwearCT = ClothingTable.ReadFromDat(sex.GetFootwearClothingTable(appearance.FootwearStyle));
            uint footwearIconId = footwearCT.GetIcon(appearance.FootwearColor);

            DataObject shoes = (DataObject)DatabaseManager.World.GetDataObjectByWeenie(footwearWeenie).Clone(GuidManager.NewItemGuid().Full);
            shoes.SpellIdProperties = new List<DataObjectPropertiesSpell>();
            shoes.IconDID = footwearIconId;
            shoes.Placement = 0;
            shoes.CurrentWieldedLocation = shoes.ValidLocations;
            shoes.WielderIID = id;
            shoes.PaletteTemplate = appearance.FootwearColor;
            shoes.Shade = appearance.FootwearHue;

            character.WieldedItems.Add(new ObjectGuid(shoes.DataObjectId), shoes);

            // Profession (Adventurer, Bow Hunter, etc)
            // TODO - Add this title to the available titles for this character.
            int templateOption = reader.ReadInt32();
            string templateName = cg.HeritageGroups[(int)character.Heritage].TemplateList[templateOption].Name;
            character.Title = templateName;
            character.Template = templateName;
            character.CharacterTitleId = (int)cg.HeritageGroups[(int)character.Heritage].TemplateList[templateOption].Title;
            character.NumCharacterTitles = 1;

            // stats
            uint totalAttributeCredits = cg.HeritageGroups[(int)character.Heritage].AttributeCredits;
            uint usedAttributeCredits = 0;
            // Validate this is equal to actual attribute credits (330 for all but "Olthoi", which have 60
            character.StrengthAbility.Base = ValidateAttributeCredits(reader.ReadUInt32(), usedAttributeCredits, totalAttributeCredits);
            usedAttributeCredits += character.StrengthAbility.Base;

            character.EnduranceAbility.Base = ValidateAttributeCredits(reader.ReadUInt32(), usedAttributeCredits, totalAttributeCredits);
            usedAttributeCredits += character.EnduranceAbility.Base;

            character.CoordinationAbility.Base = ValidateAttributeCredits(reader.ReadUInt32(), usedAttributeCredits, totalAttributeCredits);
            usedAttributeCredits += character.CoordinationAbility.Base;

            character.QuicknessAbility.Base = ValidateAttributeCredits(reader.ReadUInt32(), usedAttributeCredits, totalAttributeCredits);
            usedAttributeCredits += character.QuicknessAbility.Base;

            character.FocusAbility.Base = ValidateAttributeCredits(reader.ReadUInt32(), usedAttributeCredits, totalAttributeCredits);
            usedAttributeCredits += character.FocusAbility.Base;

            character.SelfAbility.Base = ValidateAttributeCredits(reader.ReadUInt32(), usedAttributeCredits, totalAttributeCredits);
            usedAttributeCredits += character.SelfAbility.Base;

            // data we don't care about
            uint characterSlot = reader.ReadUInt32();
            uint classId = reader.ReadUInt32();

            // characters start with max vitals
            character.Health.Current = character.Health.MaxValue;
            character.Stamina.Current = character.Stamina.MaxValue;
            character.Mana.Current = character.Mana.MaxValue;

            // set initial skill credit amount. 52 for all but "Olthoi", which have 68
            character.AvailableSkillCredits = (int)cg.HeritageGroups[(int)character.Heritage].SkillCredits;

            uint numOfSkills = reader.ReadUInt32();

            bool isDualWield = false;
            for (uint i = 0; i < numOfSkills; i++)
            {
                Skill skill = (Skill)i;
                SkillCostAttribute skillCost = skill.GetCost();
                SkillStatus skillStatus = (SkillStatus)reader.ReadUInt32();

                if (skillStatus == SkillStatus.Specialized)
                {
                    character.TrainSkill(skill, skillCost.TrainingCost);
                    character.SpecializeSkill(skill, skillCost.SpecializationCost);
                    // oddly enough, specialized skills don't get any free ranks like trained do
                }
                if (skillStatus == SkillStatus.Trained)
                {
                    character.TrainSkill(skill, skillCost.TrainingCost);
                    character.GetSkillProperty(skill).Ranks = 5;
                    character.GetSkillProperty(skill).ExperienceSpent = 526;
                }
                if (skillCost != null && skillStatus == SkillStatus.Untrained)
                    character.UntrainSkill(skill, skillCost.TrainingCost);               
                if (skill == Skill.DualWield && (skillStatus == SkillStatus.Trained || skillStatus == SkillStatus.Specialized)) 
                {
                    isDualWield = true;
                }
            }

            // grant starter items based on skills
            StarterGearConfiguration starterGearConfig = StarterGearFactory.GetStarterGearConfiguration();
            List<uint> grantedItems = new List<uint>();

            foreach (StarterGearSkill skillGear in starterGearConfig.Skills)
            {
                CreatureSkill charSkill = character.GetSkillProperty((Skill)skillGear.SkillId);
                if (charSkill.Status == SkillStatus.Trained || charSkill.Status == SkillStatus.Specialized)
                {
                    foreach (StarterItem item in skillGear.Gear)
                    {
                        if (grantedItems.Contains(item.WeenieId))
                        {
                            DataObject existingItem = character.Inventory.Values.FirstOrDefault(i => i.WeenieClassId == item.WeenieId);
                            if ((existingItem?.MaxStackSize ?? 1) <= 1)
                                continue;

                            existingItem.StackSize += item.StackSize;
                            continue;
                        }

                        DataObject loot = (DataObject)DatabaseManager.World.GetDataObjectByWeenie(item.WeenieId).Clone(GuidManager.NewItemGuid().Full);
                        loot.Placement = 0;
                        loot.ContainerIID = id;
                        loot.StackSize = item.StackSize > 1 ? (ushort?)item.StackSize : null;
                        character.Inventory.Add(new ObjectGuid(loot.DataObjectId), loot);
                        grantedItems.Add(item.WeenieId);
                    }

                    StarterHeritage heritageLoot = skillGear.Heritage.FirstOrDefault(sh => sh.HeritageId == character.Heritage);
                    if (heritageLoot != null)
                    {
                        foreach (StarterItem item in heritageLoot.Gear)
                        {
                            if (grantedItems.Contains(item.WeenieId))
                            {
                                DataObject existingItem = character.Inventory.Values.FirstOrDefault(i => i.WeenieClassId == item.WeenieId);
                                if ((existingItem?.MaxStackSize ?? 1) <= 1)
                                    continue;

                                existingItem.StackSize += item.StackSize;
                                continue;
                            }

                            DataObject loot = (DataObject)DatabaseManager.World.GetDataObjectByWeenie(item.WeenieId).Clone(GuidManager.NewItemGuid().Full);
                            loot.Placement = 0;
                            loot.ContainerIID = id;
                            loot.StackSize = item.StackSize > 1 ? (ushort?)item.StackSize : null;
                            character.Inventory.Add(new ObjectGuid(loot.DataObjectId), loot);
                            grantedItems.Add(item.WeenieId);

                            if (isDualWield)
                            {
                                DataObject dwloot = (DataObject)DatabaseManager.World.GetDataObjectByWeenie(item.WeenieId).Clone(GuidManager.NewItemGuid().Full);
                                dwloot.Placement = 1;
                                dwloot.ContainerIID = id;
                                dwloot.StackSize = item.StackSize > 1 ? (ushort?)item.StackSize : null;
                                character.Inventory.Add(new ObjectGuid(dwloot.DataObjectId), dwloot);
                                grantedItems.Add(item.WeenieId);
                            }
                        }
                    }

                    foreach (StarterSpell spell in skillGear.Spells)
                    {
                        character.SpellIdProperties.Add(new DataObjectPropertiesSpell() { DataObjectId = id, SpellId = spell.SpellId });
                    }
                }
            }

            character.Name = reader.ReadString16L();
            character.DisplayName = character.Name; // unsure

            // Index used to determine the starting location
            uint startArea = reader.ReadUInt32();

            character.IsAdmin = Convert.ToBoolean(reader.ReadUInt32());
            character.IsEnvoy = Convert.ToBoolean(reader.ReadUInt32());

            DatabaseManager.Shard.IsCharacterNameAvailable(character.Name, ((bool isAvailable) =>
            {
                if (!isAvailable)
                {
                    SendCharacterCreateResponse(session, CharacterGenerationVerificationResponse.NameInUse);
                    return;
                }
                BadWords.ReadFile();
                PortalDatReader.Initialize(Path.GetFullPath(Path.Combine(ConfigManager.Config.Server.DatFilesDirectory, "client_portal.dat")));
                TabooTable tt = PortalDatReader.Current.Unpack<TabooTable>(0x0E00001E);
                var list = tt.Tables[0].WordList;
                list.AddRange(BadWords.BadWordsList);
                foreach (var item in list)
                {
                    if (character.Name.ToLower().Replace(" ", string.Empty).Contains(item.Replace("*", string.Empty)))
                    {
                        SendCharacterCreateResponse(session, CharacterGenerationVerificationResponse.NameBanned);
                        return;
                    }
                }
                character.SubscriptionId = session.SubscriptionId;

                CharacterCreateSetDefaultCharacterOptions(character);
                CharacterCreateSetDefaultCharacterPositions(character, startArea);

                // We must await here --
                character.SetDirtyFlags();
                DatabaseManager.Shard.SaveObject(character, ((bool saveSuccess) =>
                {
                    if (!saveSuccess)
                    {
                        SendCharacterCreateResponse(session, CharacterGenerationVerificationResponse.DatabaseDown);
                        return;
                    }
                    // DatabaseManager.Shard.SaveCharacterOptions(character);
                    // DatabaseManager.Shard.InitCharacterPositions(character);

                    ObjectGuid guid = new ObjectGuid(character.DataObjectId);
                    session.AccountCharacters.Add(new CachedCharacter(guid, (byte)session.AccountCharacters.Count, character.Name, 0));

                    SendCharacterCreateResponse(session, CharacterGenerationVerificationResponse.Ok, guid, character.Name);
                }));
            }));
        }

        /// <summary>
        /// Checks if the total credits is more than this class is allowed.
        /// </summary>
        /// <returns>The original value or the max allowed.</returns>
        private static ushort ValidateAttributeCredits(uint attributeValue, uint allAttributes, uint maxAttributes)
        {
            if ((attributeValue + allAttributes) > maxAttributes)
            {
                return (ushort)(maxAttributes - allAttributes);
            }
            else
                return (ushort)attributeValue;
        }

        private static void CharacterCreateSetDefaultCharacterOptions(DataCharacter character)
        {
            character.SetCharacterOption(CharacterOption.VividTargetingIndicator, true);
            character.SetCharacterOption(CharacterOption.Display3dTooltips, true);
            character.SetCharacterOption(CharacterOption.ShowCoordinatesByTheRadar, true);
            character.SetCharacterOption(CharacterOption.DisplaySpellDurations, true);
            character.SetCharacterOption(CharacterOption.IgnoreFellowshipRequests, true);
            character.SetCharacterOption(CharacterOption.ShareFellowshipExpAndLuminance, true);
            character.SetCharacterOption(CharacterOption.LetOtherPlayersGiveYouItems, true);
            character.SetCharacterOption(CharacterOption.RunAsDefaultMovement, true);
            character.SetCharacterOption(CharacterOption.AutoTarget, true);
            character.SetCharacterOption(CharacterOption.AutoRepeatAttacks, true);
            character.SetCharacterOption(CharacterOption.UseChargeAttack, true);
            character.SetCharacterOption(CharacterOption.LeadMissileTargets, true);
            character.SetCharacterOption(CharacterOption.ListenToAllegianceChat, true);
            character.SetCharacterOption(CharacterOption.ListenToGeneralChat, true);
            character.SetCharacterOption(CharacterOption.ListenToTradeChat, true);
            character.SetCharacterOption(CharacterOption.ListenToLFGChat, true);
        }

        public static void CharacterCreateSetDefaultCharacterPositions(DataCharacter character, uint startArea)
        {
            character.Location = CharacterPositionExtensions.StartingPosition(startArea);
        }

        private static void SendCharacterCreateResponse(Session session, CharacterGenerationVerificationResponse response, ObjectGuid guid = default(ObjectGuid), string charName = "")
        {
            session.Network.EnqueueSend(new GameMessageCharacterCreateResponse(response, guid, charName));
        }

        [GameMessage(GameMessageOpcode.CharacterLogOff, SessionState.WorldConnected)]
        public static void CharacterLogOff(ClientMessage message, Session session)
        {
            session.LogOffPlayer();
        }
    }
}
