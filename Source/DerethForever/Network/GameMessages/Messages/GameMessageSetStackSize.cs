/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Actors;
using DerethForever.Entity;
using DerethForever.Network.Sequence;

namespace DerethForever.Network.GameMessages.Messages
{
    public class GameMessageSetStackSize : GameMessage
    {
        public GameMessageSetStackSize(SequenceManager sequence, ObjectGuid itemGuid, int amount, int newValue)
            : base(GameMessageOpcode.SetStackSize, GameMessageGroup.Group09)
        {
            // TODO: need to understand what we really need to send here for sequence it is only a byte
            // and it increments by the number of items popped off the stack in the case of comp burn. Og II
            Writer.Write(sequence.GetNextSequence(SequenceType.SetStackSize)[0]);
            Writer.Write(itemGuid.Full);
            Writer.Write(amount);
            Writer.Write(newValue);
        }
    }
}
