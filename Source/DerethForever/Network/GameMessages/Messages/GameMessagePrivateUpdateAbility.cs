/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;

namespace DerethForever.Network.GameMessages.Messages
{
    public class GameMessagePrivateUpdateAbility : GameMessage
    {
        public GameMessagePrivateUpdateAbility(Session session, Ability ability, uint ranks, uint baseValue, uint totalInvestment)
            : base(GameMessageOpcode.PrivateUpdateAttribute, GameMessageGroup.Group09)
        {
            // TODO We shouldn't be passing session. Insetad, we should pass the value after session.UpdateSkillSequence++.

            PropertyAttribute networkAbility;

            switch (ability)
            {
                case Ability.strength:
                    networkAbility = PropertyAttribute.Strength;
                    break;
                case Ability.endurance:
                    networkAbility = PropertyAttribute.Endurance;
                    break;
                case Ability.coordination:
                    networkAbility = PropertyAttribute.Coordination;
                    break;
                case Ability.quickness:
                    networkAbility = PropertyAttribute.Quickness;
                    break;
                case Ability.focus:
                    networkAbility = PropertyAttribute.Focus;
                    break;
                case Ability.self:
                    networkAbility = PropertyAttribute.Self;
                    break;
                default:
                    throw new ArgumentException("invalid ability specified");
            }

            Writer.Write(session.Player.Sequences.GetNextSequence(Sequence.SequenceType.PrivateUpdateAttribute));
            Writer.Write((uint)networkAbility);
            Writer.Write(ranks);
            Writer.Write(baseValue);
            Writer.Write(totalInvestment);
        }
    }
}
