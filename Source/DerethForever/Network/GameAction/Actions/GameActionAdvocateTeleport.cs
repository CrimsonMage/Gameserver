/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common.Extensions;
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Network.GameAction.Actions
{
    public static class GameActionAdvocateTeleport
    {
        [GameAction(GameActionType.AdvocateTeleport)]
        public static void Handle(ClientMessage message, Session session)
        {
            var target = message.Payload.ReadString16L();
            var position = new Position(message.Payload);
            // this check is also done clientside, see: PlayerDesc::PlayerIsPSR
            if (!session.Player.IsAdmin && !session.Player.IsArch && !session.Player.IsPsr)
                return;

            uint cell = position.LandblockId.Raw;
            uint cellX = (cell >> 3);

            // TODO: Wrap command in a check to confirm session.character IsAdvocate or higher access level

            // TODO: Maybe output to chat window coords teleported to.
            // ChatPacket.SendSystemMessage(session, $"Teleporting to: 0.0[N/S], 0.0[E/W]");
            ChatPacket.SendServerMessage(session, "Teleporting...", ChatMessageType.Broadcast);
            session.Player.Teleport(position);
        }
    }
}
