﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network.GameAction.Actions
{
    /// <summary>
    /// This method handles the Game Action F7B1 - 0x0055 Stackable Split to Container.   This is sent to the server when the player
    /// splits a stack in a container.   There are different messages for split to 3D (world) and to split to wield.   We get from the client
    /// the guid of the item stack we are about to split, the container Id, the place 0 - MaxPackCapacity ie the slot in the container we are trying to split to
    /// and finally we get the amount we are trying to split out. Og II
    /// </summary>
    public static class GameActionStackableSplitToContainer
    {
        [GameAction(GameActionType.StackableSplitToContainer)]
        public static void Handle(ClientMessage message, Session session)
        {
            // Read in the applicable data.
            uint stackId = message.Payload.ReadUInt32();
            uint containerId = message.Payload.ReadUInt32();
            int place = message.Payload.ReadInt32();
            ushort amount = (ushort)message.Payload.ReadUInt32();
            session.Player.HandleActionStackableSplitToContainer(stackId, containerId, place, amount);
        }
    }
}
