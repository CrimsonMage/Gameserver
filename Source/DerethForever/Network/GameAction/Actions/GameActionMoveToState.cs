/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.IO;

using DerethForever.Actors;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Network.Motion;
using DerethForever.Network.Enum;

namespace DerethForever.Network.GameAction.Actions
{
    public static class GameActionMoveToState
    {
        private class MotionStateDirection
        {
            public uint Command { get; set; }
            public uint HoldKey { get; set; }
            public float Speed { get; set; } = 1.0f;
        }

        private static MovementData DeserializeMovement(BinaryReader reader, MotionStateFlag flags)
        {
            MovementData ret = new MovementData();

            if ((flags & MotionStateFlag.CurrentStyle) != 0)
                ret.CurrentStyle = (ushort)reader.ReadUInt32();

            if ((flags & MotionStateFlag.ForwardCommand) != 0)
                ret.ForwardCommand = (ushort)reader.ReadUInt32();

            // FIXME(ddevec): Holdkey?
            if ((flags & MotionStateFlag.ForwardHoldKey) != 0)
                reader.ReadUInt32();

            if ((flags & MotionStateFlag.ForwardSpeed) != 0)
                ret.ForwardSpeed = (ushort)reader.ReadSingle();

            if ((flags & MotionStateFlag.SideStepCommand) != 0)
                ret.SideStepCommand = (ushort)reader.ReadUInt32();

            // FIXME(ddevec): Holdkey?
            if ((flags & MotionStateFlag.SideStepHoldKey) != 0)
                reader.ReadUInt32();

            if ((flags & MotionStateFlag.SideStepSpeed) != 0)
                ret.SideStepSpeed = (ushort)reader.ReadSingle();

            if ((flags & MotionStateFlag.TurnCommand) != 0)
                ret.TurnCommand = (ushort)reader.ReadUInt32();

            // FIXME(ddevec): Holdkey?
            if ((flags & MotionStateFlag.TurnHoldKey) != 0)
                reader.ReadUInt32();

            if ((flags & MotionStateFlag.TurnSpeed) != 0)
                ret.TurnSpeed = (ushort)reader.ReadSingle();
            
            return ret;
        }

        [GameAction(GameActionType.MoveToState)]
        public static void Handle(ClientMessage message, Session session)
        {
            Position position;
            HoldKey currentHoldkey = HoldKey.Invalid;
            ushort instanceTimestamp;
            ushort serverControlTimestamp;
            ushort teleportTimestamp;
            ushort forcePositionTimestamp;
            /*bool contact;
            bool longJump;*/

            MotionStateFlag flags = (MotionStateFlag)message.Payload.ReadUInt32();

            if ((flags & MotionStateFlag.CurrentHoldKey) != 0)
                currentHoldkey = (HoldKey)message.Payload.ReadUInt32();

            MovementData md = DeserializeMovement(message.Payload, flags);

            uint numAnimations = (uint)flags >> 11;
            MotionItem[] commands = new MotionItem[numAnimations];
            for (int i = 0; i < numAnimations; i++)
            {
                ushort motionCommand = message.Payload.ReadUInt16();
                ushort sequence = message.Payload.ReadUInt16();
                float speed = message.Payload.ReadSingle();
                commands[i] = new MotionItem((MotionCommand)motionCommand, speed);
            }

            position = new Position(message.Payload);
            position.LandblockId = new LandblockId(position.Cell);

            instanceTimestamp = message.Payload.ReadUInt16();
            serverControlTimestamp = message.Payload.ReadUInt16();
            teleportTimestamp = message.Payload.ReadUInt16();
            forcePositionTimestamp = message.Payload.ReadUInt16();
            message.Payload.ReadByte();
            // FIXME(ddevec): If speed values in the motion need to be updated by the player, this will likely need to be adjusted
            // Send the motion to the player

            // these 2 should probably be combined.  they have to be validated together in physics
            session.Player.RequestUpdatePosition(position);
            session.Player.RequestUpdateMotion(currentHoldkey, md, commands);
        }
    }
}
