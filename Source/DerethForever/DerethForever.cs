/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Threading;
using DerethForever.Command;
using DerethForever.Common;
using DerethForever.Database;
using DerethForever.DatLoader;
using DerethForever.Managers;
using DerethForever.Network.Managers;

using log4net;

namespace DerethForever
{
    public class DerethForever
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

            log.Info("Starting Dereth Forever...");
            Console.Title = @"Dereth Forever";

            ConfigManager.Initialize();
            ServerManager.Initialize();
            DatabaseManager.Initialize();
            AssetManager.Initialize();
            InboundMessageManager.Initialize();
            DatabaseManager.Start();
            DatManager.Initialize();
            ApiManager.Initialize();
            GuidManager.Initialize();
            SocketManager.Initialize();
            WorldManager.Initialize();
            RecipeManager.Initialize();

            if (Redeploy.RedeploymentSuccessful)
                ServerManager.RestartServer();

            Thread.Sleep(1000); // let other stuff catch up

            CommandManager.Initialize();
        }

        private static void OnProcessExit(object sender, EventArgs e)
        {
            DatabaseManager.Stop();
            ApiManager.ShutdownApi();
            Diagnostics.Diagnostics.LandBlockDiag = false;
        }
    }
}
