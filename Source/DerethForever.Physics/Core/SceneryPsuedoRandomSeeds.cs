/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.Physics.Core
{
    public class PsuedoRandomSeeds
    {
        // if prng(x; y; RND_MID_DIAG) >= 0.5; the landcell's polygon is split NE/SW
        public const uint InvertCellTriangles = 0x00000003;

        // select scene floor(prng(x; y; RND_SCENE_PICK) * num_scenes) from scene type
        public const uint SceneSubIndex = 0x00002bf9;

        // if prng(x; y; RND_SCENE_FREQ + obj_num) < freq; show object
        public const uint Frequency = 0x00005b67;

        // displaces an object prng(x; y RND_SCENE_DISP_X + obj_num) * displace_x units on the x axis
        public const uint DisplacementX = 0x0000b2cd;

        // displaces an object prng(x; y; RND_SCENE_DISP_Y + obj_num) * displace_y units on the y axis
        public const uint DisplacementY = 0x00011c0f;

        // scales an object min_scale * pow(max_scale / min_scale; prng(x; y; RND_SCENE_SCALE_1 + obj_num))
        public const uint ObjectScale = 0x00007f51;

        // ??
        public const uint AltObjectScale = 0x000096a7;

        // if prng(x; y; RND_SCENE_ROT) >= 0.75; (y; -x)
        // if prng(...) >= 0.5; (-x; -y)
        // if prng(...) >= 0.25; (-y; x)
        // else; (y; x)  (sub_5A6D40)
        public const uint SceneRotation = 0x0000e7eb;

        // rotate a scene object prng(x; y; RND_SCENE_ROT + obj_num) * max_rot degrees
        public const uint ObjectRotation = 0x0000f697;

        // @ 005005CB
        public const uint LandblockRotation = 0x0000000d;

        // @ 00500E51
        public const uint RND_LAND_TEX = 0x00000011;
        public const uint RND_LAND_ALPHA2 = 0x00012071;
        public const uint RND_LAND_ALPHA1 = 0x000002db;
        public const uint RND_SKILL_APPRAISE = 0x00000013;
        public const uint RND_TIME_WEATH1 = 0x0000a883;
        public const uint RND_TIME_WEATH2 = 0x00013255;
    }
}
