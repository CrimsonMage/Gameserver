/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Diagnostics;

namespace DerethForever.Physics.Core
{
    public class Clock
    {
        Stopwatch _physicsTimer = new Stopwatch();
        Stopwatch _renderTimer = new Stopwatch();
        Stopwatch _frameTimer = new Stopwatch();

        public long FrameCount { get; private set; }

        public float PhysicsAverage
        {
            get
            {
                if (FrameCount == 0) return 0;
                return (((float)_physicsTimer.ElapsedTicks / Stopwatch.Frequency) / FrameCount) * 1000.0f;
            }
        }

        public float RenderAverage
        {
            get
            {
                if (FrameCount == 0) return 0;
                return (((float)_renderTimer.ElapsedTicks / Stopwatch.Frequency) / FrameCount) * 1000.0f;
            }
        }

        public void StartPhysics()
        {
            _physicsTimer.Start();
        }

        public void StopPhysics()
        {
            _physicsTimer.Stop();
        }

        public void StartRender()
        {
            _renderTimer.Start();
        }

        public void StopRender()
        {
            _renderTimer.Stop();
        }

        public float GetFrameDelta()
        {
            FrameCount++;

            float delta = (float)_frameTimer.ElapsedTicks / Stopwatch.Frequency;
            _frameTimer.Restart();
            return delta;
        }

        public void Reset()
        {
            FrameCount = 0;
            _physicsTimer.Reset();
            _renderTimer.Reset();
        }
    }
}
