/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Drawing;
using BulletSharp;
using DerethForever.DatLoader;
using DerethForever.DatLoader.Entity;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity;
using DerethForever.Physics.Core;
using log4net;
using DerethForever.Entity.Enum;

namespace DerethForever.Physics
{
    /// <summary>
    /// main class for doing physics in Dereth Forever.  Relies on "Step" being called from a game loop.
    /// </summary>
    public class DerethPhysics : IPhysicsUser, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const float _offsetXZ = -24480f;

        private bool Running { get; set; } = false;

        private bool GraphicsLoading { get; set; } = false;

        /// <summary>
        /// locking object for the physics loop
        /// </summary>
        public object MultiThreadedContext { get; set; } = new object();

        /// <summary>
        /// flag that controls some behaviors - namely size restrictions.  dungeons
        /// have no real restrictions outside whatever the data has.  above-ground physics
        /// are locked into the 8x8 cell ground logic.  default to false for testing purposes.
        /// </summary>
        private bool _isDungeon = false;

        /// <summary>
        /// this is the main physics class that does the heavy lifting
        /// </summary>
        private DynamicsWorld _world { get; set; }

        /// <summary>
        /// method of entry to get into a viewer of the physics world.  object type to be added
        /// later once we have something to use.
        /// </summary>
        private Core.Graphics Viewport { get; set; }

        /// <summary>
        /// thread that the graphics are running on
        /// </summary>
        private Thread ViewPortThread { get; set; }

        /// <summary>
        /// collection of all the collision objects we want to monitor.  this is probably a rendundant
        /// thing we can get rid of - time will tell.
        /// </summary>
        private List<CollisionShape> CollisionShapes { get; } = new List<CollisionShape>();
        
        /// <summary>
        /// during each loop, some objects may need be removed.  this list is to contain objects that
        /// should be destroyed as a result of physics processing, but will occur after event
        /// processing has been completed.
        /// </summary>
        private List<CollisionObject> Zombies { get; set; } = new List<CollisionObject>();

        /// <summary>
        /// document me please
        /// </summary>
        private CollisionConfiguration CollisionConf { get; set; }

        /// <summary>
        /// document me please
        /// </summary>
        private CollisionDispatcher Dispatcher { get; set; }

        /// <summary>
        /// document me please
        /// </summary>
        private BroadphaseInterface Broadphase { get; set; }

        /// <summary>
        /// object used to track performance metrics
        /// </summary>
        public Core.Clock Clock { get; } = new Core.Clock();

        /// <summary>
        /// amount of time the last physics frame took
        /// </summary>
        public float FrameDelta { get; private set; }
        
        /// <summary>
        /// the graphfics FPS to maintain.  higher graphics FPS = more server load.  default is 30.
        /// </summary>
        public int GraphicsFramesPerSecond { get; set; } = 30;

        /*
        public bool IsDebugDrawEnabled { get; set; }

        /// <summary>
        /// debug draw mode
        /// </summary>
        public DebugDrawModes DebugDrawMode { get; set; } = DebugDrawModes.DrawWireframe;

        public IDebugDraw DebugDrawer
        {
            get { return _world.DebugDrawer; }
        }

        public bool GraphicsCullingEnabled { get; set; } = true;
        */

        /// <summary>
        /// private because this is turned on and off via functions rather than property setters     
        /// </summary>
        private bool ShowGraphics { get; set; } = false;

        public void Start()
        {
            OnInitializePhysics();

            Running = true;
        }

        private void OnInitializePhysics()
        {
            log.Info("Initializing physics...");

            RegionDesc region = RegionDesc.ReadFromDat();

            // collision configuration contains default setup for memory, collision setup
            CollisionConf = new DefaultCollisionConfiguration();
            Dispatcher = new CollisionDispatcher(CollisionConf);

            Broadphase = new DbvtBroadphase();

            _world = new DiscreteDynamicsWorld(Dispatcher, Broadphase, null, CollisionConf);
            
            GenerateAbsoluteGround();
        }
        
        /// <summary>
        /// main loop to be called at each iteration.  will process physics no matter what.  will process
        /// graphics if needed (ie, to a maximum of FPS).  If "Start" has not been previously called on this
        /// instance, Step returns without doing anything.
        /// </summary>
        public void Step()
        {
            if (!Running)
                return;

            lock (MultiThreadedContext)
            {
                Clock.StartPhysics();
                OnUpdate();
                Clock.StopPhysics();
            }
        }

        /// <summary>
        /// internal call to execute a single physics loop
        /// </summary>
        private void OnUpdate()
        {
            FrameDelta = Clock.GetFrameDelta();

            // StepSimulation allows for the engine to control motion.  unfortunately,
            // we can't use it because of all the custom stuff that has to happen with AC
            // _world.StepSimulation(FrameDelta);

            // instead, we just make a call to perform collision detection, which we don't need either
            // _world.PerformDiscreteCollisionDetection();
            
            Zombies.ForEach(z => _world.RemoveCollisionObject(z));
            Zombies.ForEach(z => z.Dispose());
            Zombies = new List<CollisionObject>();
        }
        
        public void LaunchViewer()
        {
            GraphicsLoading = true;

            if (ViewPortThread != null)
            {
                // check to see if it's already running
                if (ViewPortThread.ThreadState == ThreadState.Running)
                {
                    // attempt to close it
                    ViewPortThread.Abort();
                }
            }

            ViewPortThread = new Thread(LaunchViewerEx);
            ViewPortThread.Start();
        }

        /// <summary>
        /// private implementation to be called in a new thread
        /// </summary>
        private void LaunchViewerEx()
        {
            // create a new viewer
            Viewport = new SharpDX11Graphics(_world, this);
            Viewport.Initialize();
            Viewport.CullingEnabled = true;
            Viewport.UpdateView();

            // turn it on in the loop only after
            ShowGraphics = true;
            Viewport.Run();
        }

        public void MoveCamera(uint landblockId, bool waitForIt = false)
        {
            var t = new Thread(() => MoveCameraEx(landblockId, waitForIt));
            t.Start();
        }

        public void MoveCamera(Position position)
        {
            var t = new Thread(() => MoveCameraEx(position));
            t.Start();
        }

        private void MoveCameraEx(uint landblockId, bool waitForIt)
        {
            if (ShowGraphics && Viewport != null)
            {
                var eye = DerethToEngine(landblockId, new Vector3(0, 0, 30));
                Console.WriteLine($"Setting viewport eye to {eye.X}, {eye.Y}, {eye.Z}");
                Viewport.FreeLook.Eye = eye;
                Viewport.FreeLook.Target = DerethToEngine(landblockId, new Vector3(0, 10, 30));
            }
        }

        private void MoveCameraEx(Position position)
        {
            if (ShowGraphics && Viewport != null)
            {
                var eye = DerethToEngine(position.LandblockId.Raw, new Vector3(position.PositionX, position.PositionY, position.PositionZ));
                var target = position.InFrontOf(10);
                target.PositionZ -= 2; // looking slightly down
                Console.WriteLine($"Setting viewport eye to {eye.X}, {eye.Y}, {eye.Z}");
                Viewport.FreeLook.Eye = eye;
                Viewport.FreeLook.Target = DerethToEngine(target.LandblockId.Raw, new Vector3(target.PositionX, target.PositionY, target.PositionZ));
            }
        }

        public void CloseViewer()
        {
            ShowGraphics = false;

            Viewport.Dispose();
            Viewport = null;

            ViewPortThread = null;
        }

        // iterative repositioning logic.  can be called from the client message (in which old position matches new
        // position) or from the incremental movement logic
        public Position MoveObject(RigidBody body, Position oldPosition, Position newPosition, bool moveToGround = true)
        {
            // here we would do necessary corrections from the current position to the new position.
            // we would prevent stuff from running into a wall or through a scenery object, for example

            // dungeons will break the Z correction, currently, as it shouldn't be reading from the heightmap
            // so only do Z corrections outdoors
            try
            {
                if (!newPosition.Indoors && moveToGround)
                    newPosition.PositionZ = GetHeight(newPosition.LandblockId.Raw, newPosition.PositionX, newPosition.PositionY);
            }
            catch
            { }

            // so, the proper solution is a rewrite of physics to not use bullet.  in the interest of getting portals
            // working, we're just going to hack together a solution here.
            
            body.WorldTransform = TranslatePosition(newPosition.LandblockId.Raw, newPosition);
            return newPosition;
        }

        /// <summary>
        /// loads the terrain, structures, buildings, and static objects for the specified landblock
        /// </summary>
        public void LoadLandlock(uint landblockId)
        {
            try
            {
                lock (MultiThreadedContext)
                {
                    var info = CLandblockInfo.ReadFromDat(landblockId);
                    var cellInfo = CellLandblock.ReadFromDat(landblockId);

                    //if (cellInfo == null || cellInfo.HasObjects || info == null) // dungeon?
                    //    return;

                    GenerateBvhGround(landblockId);

                    // load interior cells
                    for (uint i = 0; i < info.NumCells; i++)
                    {
                        uint cellId = landblockId + 0x0100 + i;

                        LoadCell(cellInfo, cellId);
                    }
                    
                    // load exterior cells
                    for (uint x = 0; x < 9; x++)
                    {
                        for (uint y = 0; y < 9; y++)
                        {
                            LoadScenery(cellInfo, landblockId, x, y);
                        }
                    }

                    for (int b = 0; b < info.Buildings.Count; b++)
                    {
                        var building = info.Buildings[b];

                        LoadObjectEx(building.ModelId, building.Frame, landblockId, PhysicsState.Static, UserObjectData.ColorToUint(Color.Gray));
                    }

                    for (int i = 0; i < info.ObjectIds.Count; i++)
                    {
                        uint objectId = info.ObjectIds[i];

                        LoadObjectEx(objectId, info.ObjectFrames[i], landblockId, PhysicsState.Static, UserObjectData.ColorToUint(Color.ForestGreen));
                    }                    
                }
            }
            catch (Exception ex)
            {
                log.Info($"error loading landblock {landblockId:X}: {ex}");
            }
        }

        private void LoadCell(CellLandblock cellInfo, uint cellId)
        {
            // cells are loaded via EnvCell.  the tangled web continues
            if (!DatManager.CellDat.AllFiles.ContainsKey(cellId))
                return;

            EnvCell cell;

            try
            {
                cell = EnvCell.ReadFromDat(cellId);
            }
            catch (Exception ex)
            {
                log.Info($"Physics: Error loading cell {cellId} from the dat files\r\n{ex}");
                return;
            }

            uint landblockId = cellId & 0xFFFF0000;
            
            DatLoader.FileTypes.Environment env = DatLoader.FileTypes.Environment.ReadFromDat(cell.EnvironmentId);
            var cellKeys = env.Cells.Keys.ToArray();

            CompoundShape cellShape = new CompoundShape();
            var offsetTransform = TranslatePosition(landblockId, cell.Position);

            // these are things like the floors of upstairs/downstairs in a building
            for (uint j = 0; j < env.Cells.Count; j++)
            {
                var envCell = env.Cells[cellKeys[j]];
                var shape = GetShape(envCell);

                cellShape.AddChildShape(Matrix.Identity, shape);

                LocalCreateRigidBody(0f, offsetTransform, cellShape, CollisionFlags.StaticObject);
                CollisionShapes.Add(cellShape);
            }

            // these are re-usable objects just like appear on the landblock at large
            for (int j = 0; j < cell.StabList.Count; j++)
            {
                var stab = cell.StabList[j];
                LoadObjectEx(stab.Model, stab.Position, landblockId, PhysicsState.Static);
            }
        }

        private void LoadScenery(CellLandblock cellInfo, uint landblockId, uint x, uint y)
        {
            try
            {
                Position p = new Position();
                p.Cell = landblockId;
                uint cellX = p.LandblockId.LandblockX * 8u + x;
                uint cellY = p.LandblockId.LandblockY * 8u + y;

                uint rawTerrain = cellInfo.Terrain[x * 9 + y];
                uint terrainType = (rawTerrain >> 2) & 0x1f;
                uint terrainSceneType = (rawTerrain >> 11) & 0x1f;

                // log.Info($"landcell {landblockId:X} ({x}, {y}) has terrain {terrainType} and terrain scene {terrainSceneType} ({x}, {y})");

                RegionDesc region = RegionDesc.ReadFromDat();
                var terrain = region.TerrainInfo.TerrainTypes[(int)terrainType];
                var sceneIndex = terrain.SceneTypes[(int)terrainSceneType];

                if (sceneIndex >= region.SceneInfo.SceneTypes.Count)
                {
                    // log.Warn($"landblock {landblockId:X} ({x}, {y}) has invalid scene index {sceneIndex}");
                    return; // should proabably be an error.
                }

                var sceneType = region.SceneInfo.SceneTypes[(int)sceneIndex];

                if (sceneType.Scenes.Count == 0)
                {
                    // log.Warn($"landblock {landblockId:X} ({x}, {y}) has scene index {sceneIndex} has no scene types.");
                    return;
                }

                uint subScene = (uint)(SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.SceneSubIndex) * sceneType.Scenes.Count);
                if (subScene >= sceneType.Scenes.Count)
                    subScene = 0;

                // log.Info($"attempting to load scene index {sceneIndex}");

                // log.Info($"attempting to load sub scene {subScene}");
                uint sceneResourceId = sceneType.Scenes[(int)subScene];

                Scene scene = Scene.ReadFromDat(sceneResourceId);

                double sceneRot = SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.SceneRotation);

                for (uint i = 0; i < scene.Objects.Count; i++)
                {
                    if (i >= 300) // max objects
                        break;

                    var sceneObject = scene.Objects[(int)i];

                    if (sceneObject.WeenieObj > 0)
                        continue;

                    double f = SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.Frequency + i);
                    if (sceneObject.Freq <= f)
                        continue;
                    
                    var loc = sceneObject.BaseLoc;
                    Position placement = new Position(landblockId, loc.PositionX, loc.PositionY, loc.PositionZ, loc.RotationX, loc.RotationY, loc.RotationZ, loc.RotationW);
                    
                    if (sceneObject.DisplaceX > 0.0f)
                        placement.PositionX = loc.PositionX + ((float)SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.DisplacementX + i) * sceneObject.DisplaceX);
                    if (sceneObject.DisplaceY > 0.0f)
                        placement.PositionY = loc.PositionY + ((float)SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.DisplacementY + i) * sceneObject.DisplaceY);

                    Position tempPos = new Position(landblockId, placement.PositionX, placement.PositionY, placement.PositionZ, 0, 0, 0, 0);

                    float delta = 0.0f;
                    if (sceneRot >= 0.75)
                    {
                        placement.PositionX = tempPos.PositionY;
                        placement.PositionY = delta - tempPos.PositionX;
                    }
                    else if (sceneRot >= 0.5)
                    {
                        placement.PositionX = delta - tempPos.PositionX;
                        placement.PositionY = delta - tempPos.PositionY;
                    }
                    else if (sceneRot >= 0.25)
                    {
                        placement.PositionX = delta - tempPos.PositionY;
                        placement.PositionY = tempPos.PositionX;
                    }
                    else
                    {
                        placement.PositionX = tempPos.PositionX;
                        placement.PositionY = tempPos.PositionY;
                    }

                    // coming into this point, positions are 0 <= p <= 8, so math them up for the cell offsets
                    placement.PositionX += (x * 24.0f);
                    placement.PositionY += (y * 24.0f);

                    // exclude anything that falls out of bounds
                    if (placement.PositionX < 0 ||
                        placement.PositionY < 0 ||
                        placement.PositionX >= 192 ||
                        placement.PositionY >= 192)
                        continue;

                    // no stuff goes on roads
                    if (IsRoadAtPoint(cellInfo, placement.PositionX, placement.PositionY))
                    {
                        // log.Info($"object id {sceneObject.ObjId} omitted: road in the way");
                        continue;
                    }

                    // no stuff goes on/in buildings.  note, this check is probably not aggressive enough as it only considers the
                    // cell of the building's center point.

                    uint landCellX = (uint)(placement.PositionX / 24);
                    uint landCellY = (uint)(placement.PositionY / 24);

                    var info = CLandblockInfo.ReadFromDat(landblockId);
                    var buildingCount = info.Buildings.Count(b =>
                    {
                        uint buildingCellX = (uint)(b.Frame.PositionX / 24);
                        uint buildingCellY = (uint)(b.Frame.PositionY / 24);

                        return buildingCellX == landCellX && buildingCellY == landCellY;
                    });
                    if (buildingCount > 0)
                    {
                        // log.Info($"object id {sceneObject.ObjId} omitted: building in the way at ({placement.PositionX}, {placement.PositionY})");
                        continue;
                    }

                    // slope check
                    var plane = GetPlane(placement.LandblockId.Raw, placement.PositionX, placement.PositionY);
                    if (plane.Normal.Z < sceneObject.MinSlope || plane.Normal.Z > sceneObject.MaxSlope)
                    {
                        // log.Info($"object id {sceneObject.ObjId} omitted: bad slope.  plane Z: {plane.Normal.Z}, min {sceneObject.MinSlope}, max {sceneObject.MaxSlope}");
                        continue;
                    }

                    // no scenery on psuedo-vertical walls
                    if (plane.Normal.Z < 0.0002f)
                    {
                        // log.Info($"object id {sceneObject.ObjId} omitted: vertical ground");
                        continue;
                    }

                    // get the height
                    placement.PositionZ += GetHeight(plane, placement.PositionX, placement.PositionY);

                    // psuedo-randomly scale it
                    double scale = sceneObject.MaxScale / sceneObject.MinScale;
                    var factor = Math.Pow(scale, SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.ObjectScale + i));
                    scale = sceneObject.MinScale * factor;
                    
                    // psuedo-randomly rotate it
                    factor = SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.ObjectRotation + i);
                    var rotation = sceneObject.MaxRotation * factor; // max rotation is in degrees
                    rotation = rotation * Math.PI / 180f; // convert to radians

                    // do quaternion math
                    Quaternion q = new Quaternion(placement.RotationX, placement.RotationY, placement.RotationZ, placement.RotationW);
                    var q2 = q * new Quaternion(new Vector3(0, 0, 1), (float)-rotation);
                    placement.RotationW = q2.W;
                    placement.RotationZ = q2.Z;

                    // log.Info($"adding scenery object {sceneObject.ObjId:X} at {placement.PositionX}, {placement.PositionY}, {placement.PositionZ}");
                    LoadObject(sceneObject.ObjId, placement, PhysicsState.Static, Color.LimeGreen, 0f, scale: (float)scale);
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error loading land cell {landblockId:X} ({x}, {y}): {ex}");
            }
        }
        
        public static bool IsRoadAtPoint(CellLandblock data, float x, float y)
        {
            // (0052FFF0)
            // int __thiscall CLandBlock::on_road(CLandBlock *this, AC1Legacy::Vector3 *obj_vector)
            // line 352238 of decomp client

            // pretty much every source for this function is wrong in one way or another.  i went
            // back to the decompiled client to cover a lot of the diagonal edge cases, but even then
            // i had to do some hackery to make it match what i saw logged in (my c++ aint so hot).

            // basic logic: roads are 10f wide.  this method examines 4 cells, starting with the cell
            // being referenced as the bottom left.  roads will traverse in any pattern of the 4 cells.

            const float roadHalfWidth = 5.0f;
            const float diagonalTolerance = 7.07f; // 5.0f * sqrt(2)
            float xf, yf;
            int tileX, tileY;
            bool result = false;

            tileX = (int)(x / 24.0f);
            xf = ((x / 24.0f) % 1f) * 24;
            tileY = (int)(y / 24.0f);
            yf = ((y / 24.0f) % 1f) * 24;

            // log.Info($"IsRoadAtPoint ({x}, {y}): tile {tileX}, {tileY}, point {xf}, {yf}");
            
            bool r0 = IsRoadTile(data, tileX, tileY);
            bool r1 = IsRoadTile(data, tileX + 1, tileY); 
            bool r2 = IsRoadTile(data, tileX, tileY + 1); 
            bool r3 = IsRoadTile(data, tileX + 1, tileY + 1);

            // solid road blocks
            if (r0 && r1 && r2 && r3)
                return true;

            // solid nonroad blocks
            if (!(r0 || r1 || r2 || r3))
                return false;

            // x,y
            if (r0)
            {
                if (r1 && (yf < roadHalfWidth)) // road along the bottom row
                    return true;
                if (r2 && (xf < roadHalfWidth)) // road up the left side
                    return true;

                // this check is technically not in the client code as i was able to decipher it.  however,
                // it yields results consitent with what is seen when logged in.  i used the diagonal road
                // from rithwic to eastham to test extensively
                if (r3 && Math.Abs(xf - yf) < diagonalTolerance) // up the middle of the diagonal 
                    return true;

                if ((xf < roadHalfWidth) && (yf < roadHalfWidth)) // this is part of a diagonal opposite our search direction
                    return true;

                return false; // this block is a road, but the points on it are not
            }

            // x+1, y
            if (r1)
            {
                // no sense in checking r0, already covered r1 && r0

                if (r3 && (xf > (19f))) // up the right side
                    return true;

                if (r2 && xf > 19f && yf > 19f) // diagonal, and we're in the corner of it
                    return true;

                if ((xf > 19f) && (yf < roadHalfWidth)) // part of a diagonal not in this grid
                    return true;
            }

            // x, y+1
            if (r2)
            {
                // no sense in checking r0, as r0 && r2 and r1 && r2 are already coverd

                if (r3 && (yf > 19f)) // road across the top
                    return true;

                if ((xf < roadHalfWidth) && (yf > 19f)) // part of a diagonal not in this grid
                    return true;
            }

            // x+1, y+1
            if (r3)
            {
                if ((xf > 19f) && (yf > 19f)) // part of a diagonal not in this grid
                    return true;
            }

            return false;

            // implementation derived from decompiled client is here (saving it for reference)
            // https://pastebin.com/tmHkGvXR
        }

        private static bool IsRoadTile(CellLandblock data, int tileX, int tileY)
        {
            // log.Info($"IsRoadTile({tileX}, {tileY})");
            return (data.Terrain[tileX * 9 + tileY] & 0x3) > 0;
        }

        /// <summary>
        /// unloads all the resources of this landblock.
        /// </summary>
        public void UnloadLandblock()
        {
            // TODO: implement.  we will probably need to keep track of objects by landblock, as many overlap or are even
            // duplicated if they span blocks

        }

        /// <summary>
        /// adds an object to the physics world.  should probably not move for now
        /// </summary>
        public RigidBody LoadObject(uint modelId, Position offset, PhysicsState physicsState, Color color, float mass, CollisionFlags flags = CollisionFlags.StaticObject, float scale = 1.0f, bool moveToGround = true)
        {
            return LoadObjectEx(modelId, offset, offset.LandblockId.Raw, physicsState, UserObjectData.ColorToUint(color), mass, flags, scale, moveToGround);
        }

        /// <summary>
        /// called when a player logs out of an object times out (loot on the ground, a temporary portal, etc)
        /// </summary>
        public void RemoveObject(RigidBody body)
        {
            _world.RemoveCollisionObject(body);
            body.Dispose();
        }

        /// <summary>
        /// requires a lock on MultiThreadedContext to already be established
        /// </summary>
        private RigidBody LoadObjectEx(uint objectId, Position offset, uint landblockId, PhysicsState physicsState, uint? color = null, float mass = 0f, CollisionFlags flags = CollisionFlags.StaticObject, float scale = 1.0f, bool moveToGround = true)
        {
            var fullTransform = TranslatePosition(landblockId, offset);

            if (objectId > 0x02000000)
            {
                SetupModel model = SetupModel.ReadFromDat(objectId);

                // model.spheres and model.cylspheres are the physics objects
                // model.parts has the rendered objects, which are also the physics if spheres and cylspheres are empty
                CompoundShape fullObject = new CompoundShape();
                
                if (model.Spheres.Count > 0)
                {
                    for (int i = 0; i < model.Spheres.Count; i++)
                    {
                        float x = model.Spheres[i].Origin.X;
                        float y = model.Spheres[i].Origin.Z;
                        float z = model.Spheres[i].Origin.Y;

                        fullObject.AddChildShape(Matrix.Translation(x, y, z), new SphereShape(model.Spheres[i].Radius));
                    }
                }

                if (model.CylSpheres.Count > 0)
                {
                    for (int i = 0; i < model.CylSpheres.Count; i++)
                    {
                        float x = model.CylSpheres[i].Origin.RawVector.X;
                        float y = model.CylSpheres[i].Origin.RawVector.Z;
                        float z = model.CylSpheres[i].Origin.RawVector.Y;
                        float radius = model.CylSpheres[i].Radius;
                        float height = model.CylSpheres[i].Height;
                        
                        fullObject.AddChildShape(Matrix.Translation(x, y, z), new CylinderShape(radius, height, radius));
                    }
                }

                for (int i = 0; i < model.Parts.Count; i++)
                {
                    var part = GfxObj.ReadFromDat(model.Parts[i]);

                    if ((part.Flags & GfxObjFlags.HasPhysicsBsp) == 0)
                        continue;
                    
                    BvhTriangleMeshShape shape = GetShape(part);
                    shape.LocalScaling = new Vector3(scale);

                    if ((model.Bitfield & SetupModelFlags.SpecifiesScale) > 0 && scale == 1.0f)  
                         shape.LocalScaling = model.DefaultScale[i].GetBulletVector(); // ?????  maybe ?????                    

                    if (shape == null)
                        continue;

                    float x = model.PlacementFrames[0].AnimFrame.Locations[i].PositionX;
                    float y = model.PlacementFrames[0].AnimFrame.Locations[i].PositionZ;
                    float z = model.PlacementFrames[0].AnimFrame.Locations[i].PositionY;

                    float qx = model.PlacementFrames[0].AnimFrame.Locations[i].RotationX;
                    float qy = -model.PlacementFrames[0].AnimFrame.Locations[i].RotationZ;
                    float qz = model.PlacementFrames[0].AnimFrame.Locations[i].RotationY;
                    float qw = model.PlacementFrames[0].AnimFrame.Locations[i].RotationW;

                    fullObject.AddChildShape(Matrix.RotationQuaternion(new Quaternion(qx, qy, qz, qw)) * Matrix.Translation(x, y, z), shape);
                }

                if (fullObject.ChildList.Count < 1)
                    return null;
                
                var body = LocalCreateRigidBody(mass, fullTransform, fullObject, flags);
                body.UserObject = new UserObjectData() { Color = color ?? UserObjectData.ColorToUint(Color.Beige), ObjectId = objectId, Physics = physicsState };
                CollisionShapes.Add(fullObject);

                return body;
            }
            else
            {
                var part = GfxObj.ReadFromDat(objectId);

                if ((part.Flags & GfxObjFlags.HasPhysicsBsp) == 0)
                    return null;

                var shape = GetShape(part);
                if (shape == null)
                    return null;

                var body = LocalCreateRigidBody(mass, fullTransform, shape, flags);
                body.UserObject = new UserObjectData() { Color = color ?? UserObjectData.ColorToUint(Color.Beige) };
                CollisionShapes.Add(shape);

                return body;
            }
            
            // log.Info($"{landblockId:X}: added object at {fullTransform.Origin.X}, {fullTransform.Origin.Y}, {fullTransform.Origin.Z}");
        }

        public BvhTriangleMeshShape GetShape(IPhysicsPart part)
        {
            Vector3[] vertices = new Vector3[part.VertexArray.Vertices.Count];
            for (short v = 0; v < part.VertexArray.Vertices.Count; v++)
            {
                vertices[v] = new Vector3(part.VertexArray.Vertices[v].X, part.VertexArray.Vertices[v].Z, part.VertexArray.Vertices[v].Y);
            }

            var trimesh = new TriangleMesh();
            
            if (part.PhysicsPolygons?.Count > 0)
            {
                for (ushort p = 0; p < part.PhysicsPolygons.Count; p++)
                {
                    var poly = part.PhysicsPolygons[p];

                    // create trifans from the polygon
                    for (ushort pt = 3; pt <= poly.NumPts; pt++)
                    {
                        Vector3 vertex0 = vertices[poly.VertexIds[0]];
                        Vector3 vertex1 = vertices[poly.VertexIds[pt - 1]];
                        Vector3 vertex2 = vertices[poly.VertexIds[pt - 2]];

                        if (poly.NegSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex1, ref vertex2);

                        if (poly.PosSurface != 0)
                            trimesh.AddTriangleRef(ref vertex0, ref vertex2, ref vertex1);
                    }
                }
            }

            if (trimesh.NumTriangles > 0)
            {
                BvhTriangleMeshShape shape = new BvhTriangleMeshShape(trimesh, true);

                return shape;
            }

            return null;
        }

        /// <summary>
        /// creates ground at height 0. required for outdoor areas just in the height map
        /// omits height data.  also a useful default/saftey net for missing terrain
        /// </summary>
        private void GenerateAbsoluteGround()
        {
            StaticPlaneShape groundShape = new StaticPlaneShape(new Vector3(0, 1, 0), 1f);
            RigidBody ground = LocalCreateRigidBody(0, Matrix.Identity, groundShape, CollisionFlags.StaticObject);
            ground.UserObject = new UserObjectData() { Color = UserObjectData.ColorToUint(Color.SteelBlue), Label = "Absolute Ground / Water" };

            // also add points at the extremes to (attempt to?) force the correct size
            BoxShape minimum = new BoxShape(DerethToEngine(0, Vector3.Zero));
            LocalCreateRigidBody(0, Matrix.Identity, minimum, CollisionFlags.None);
            BoxShape maximum = new BoxShape(DerethToEngine(0xFFFF, new Vector3(192f, 0f, 192f)));
            LocalCreateRigidBody(0, Matrix.Identity, maximum, CollisionFlags.None);
        }

        private void GenerateBvhGround(uint landblockId)
        {
            var trimesh = new TriangleMesh();

            Vector3[] vertices = new Vector3[81];
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    float height = GetVertexHeight(landblockId, i, j);
                    vertices[i * 9 + j] = new Vector3(i * 24, height, j * 24);
                }
            }

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    // i and j are the coords for the lower left vertex of a triangle.  i+1,j is top left, and so on.
                    // draw 2 triangles to form the square

                    Vector3 v00 = vertices[i * 9 + j];
                    Vector3 v01 = vertices[(i + 1) * 9 + j];
                    Vector3 v10 = vertices[i * 9 + j + 1];
                    Vector3 v11 = vertices[(i + 1) * 9 + j + 1];

                    if (IsNESW(landblockId, (uint)i, (uint)j))
                    {
                        // X---X
                        // |\  |
                        // | \ |
                        // |  \|
                        // X---X
                        // lower left to top right diagonal
                        trimesh.AddTriangleRef(ref v00, ref v10, ref v01);
                        trimesh.AddTriangleRef(ref v10, ref v11, ref v01);
                    }
                    else
                    {
                        // X---X
                        // |  /|
                        // | / |
                        // |/  |
                        // X---X
                        // lower left to top right diagonal
                        trimesh.AddTriangleRef(ref v00, ref v11, ref v01);
                        trimesh.AddTriangleRef(ref v00, ref v10, ref v11);
                    }
                }
            }

            BvhTriangleMeshShape shape = new BvhTriangleMeshShape(trimesh, true);
            
            //create ground object
            var location = DerethToEngine(landblockId, new Vector3(0f, 0f, 0f));
            // log.Info($"{landblockId:X}: adding ground at {location.X}, {location.Y}, {location.Z}");

            RigidBody ground = LocalCreateRigidBody(0f, Matrix.Translation(location), shape, CollisionFlags.StaticObject);
            ground.UserObject = new UserObjectData() { Color = UserObjectData.ColorToUint(Color.SaddleBrown), Label = "Ground" };
        }
        
        private RigidBody LocalCreateRigidBody(float mass, Matrix startTransform, CollisionShape shape, CollisionFlags customCollisionFlags)
        {
            //rigidbody is dynamic if and only if mass is non zero, otherwise static
            bool isDynamic = (mass != 0.0f);

            Vector3 localInertia = Vector3.Zero;
            if (isDynamic)
                shape.CalculateLocalInertia(mass, out localInertia);

            //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
            DefaultMotionState myMotionState = new DefaultMotionState(startTransform);

            RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
            RigidBody body = new RigidBody(rbInfo);
            body.CollisionFlags |= customCollisionFlags;
            rbInfo.Dispose();

            _world.AddRigidBody(body);

            return body;
        }

        private Vector3 DerethToEngine(uint landblockId, Vector3 landblockXyz)
        {
            byte landblockX = (byte)((landblockId & 0xFF000000) >> 24);
            byte landblockY = (byte)((landblockId & 0x00FF0000) >> 16);

            float newX = landblockX * 192f + landblockXyz.X + _offsetXZ;
            float newY = landblockXyz.Z;
            float newZ = landblockY * 192f + landblockXyz.Y + _offsetXZ;

            return new Vector3(newX, newY, newZ);
        }

        private Matrix TranslatePosition(uint landblockId, Entity.Position position)
        {
            Vector3 location = DerethToEngine(landblockId, new Vector3(position.PositionX, position.PositionY, position.PositionZ));

            Quaternion placement = new Quaternion(position.RotationX, -position.RotationZ, position.RotationY, position.RotationW);
            Matrix fullTransform = Matrix.RotationQuaternion(placement) * Matrix.Translation(location);

            return fullTransform;
        }


        private Vector3 EngineToDereth(Vector3 engineCoords, out uint landblockId)
        {
            float transposedX = engineCoords.X - _offsetXZ;
            float transposedY = engineCoords.Y - _offsetXZ;

            byte landblockX = (byte)((int)transposedX / 255);
            byte landblockY = (byte)((int)transposedY / 255);
            landblockId = ((uint)landblockX << 24) + ((uint)landblockY << 16);

            float newX = transposedX % 192;
            float newY = engineCoords.Z;
            float newZ = transposedY % 192;

            return new Vector3(newX, newY, newZ);
        }

        private static double SceneryPsuedoRandom(uint cellX, uint cellY, uint seed)
        { 
            uint ival = 0x6C1AC587 * cellY - 0x421BE3BD * cellX - seed * (0x5111BFEF * cellY * cellX + 0x70892FB7);

            return ((double)ival / (double)uint.MaxValue);
        }

        private static float GetHeight(System.Numerics.Plane plane, float x, float y)
        {
            // z = (-D - Ax - By) / C
            return ((-plane.D - plane.Normal.X * x - plane.Normal.Y * y) / plane.Normal.Z);
        }

        private static float GetHeight(uint landblockId, float x, float y)
        {
            var plane = GetPlane(landblockId, x, y);
            return GetHeight(plane, x, y);
        }

        private static System.Numerics.Plane GetPlane(uint landblockId, float x, float y)
        {
            int tileX = (int)(x / 24.0f);
            var xf = x - tileX;
            int tileY = (int)(y / 24.0f);
            var yf = y - tileY;

            int xv0 = tileX * 24;
            int yv0 = tileY * 24;
            int xv1 = (tileX + 1) * 24;
            int yv1 = (tileY + 1) * 24;

            var v00 = new System.Numerics.Vector3(xv0, yv0, GetVertexHeight(landblockId, tileX, tileY));
            var v10 = new System.Numerics.Vector3(xv1, yv0, GetVertexHeight(landblockId, tileX + 1, tileY));
            var v01 = new System.Numerics.Vector3(xv0, yv1, GetVertexHeight(landblockId, tileX, tileY + 1));
            var v11 = new System.Numerics.Vector3(xv1, yv1, GetVertexHeight(landblockId, tileX + 1, tileY + 1));
            
            System.Numerics.Plane plane;
            
            if (IsNESW(landblockId, (uint)tileX, (uint)tileY))
            {
                if (yf > (1.0 - xf))    
                {
                    // NE tri
                    plane = System.Numerics.Plane.CreateFromVertices(v10, v11, v01);
                }
                else                    
                {
                    // SW tri
                    plane = System.Numerics.Plane.CreateFromVertices(v00, v10, v01);
                }
            }
            else
            {
                if (yf > xf)            
                {
                    // NW tri
                    plane = System.Numerics.Plane.CreateFromVertices(v00, v11, v01);
                }
                else                    
                {
                    // SE tri
                    plane = System.Numerics.Plane.CreateFromVertices(v00, v10, v11);
                }
            }
            
            return plane;
        }
        
        private static float GetVertexHeight(uint landblockId, int x, int y)
        {
            var cellInfo = CellLandblock.ReadFromDat(landblockId & 0xFFFF0000);
            RegionDesc region = RegionDesc.ReadFromDat();
            return region.LandDefs.LandHeightTable[cellInfo.HeightMap[x * 9 + y]];
        }

        /// <summary>
        /// true if the cell is split into NE and SW triangles
        /// </summary>
        private static bool IsNESW(uint landblockId, uint x, uint y)
        {
            uint cellX = (landblockId >> 24 & 0xFF) * 8 + x;
            uint cellY = (landblockId >> 16 & 0xFF) * 8 + y;

            return SceneryPsuedoRandom(cellX, cellY, PsuedoRandomSeeds.InvertCellTriangles) >= 0.5;
        }

        public void Dispose()
        {
            _world.Dispose();
        }
    }
}
