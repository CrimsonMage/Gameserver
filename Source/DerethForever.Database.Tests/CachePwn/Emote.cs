/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DerethForever.Database.Tests.CachePwn
{
    public class Emote
    {
        [JsonProperty("category")]
        public uint Category { get; set; }

        [JsonProperty("emotes")]
        public List<EmoteAction> Emotes { get; set; }

        [JsonProperty("probability")]
        public double? Probability { get; set; }

        [JsonProperty("vendorType")]
        public uint? VendorType { get; set; }

        [JsonProperty("quest")]
        public string Quest { get; set; }

        [JsonProperty("classID")]
        public uint? ClassId { get; set; }

        [JsonProperty("style")]
        public uint? Style { get; set; }

        [JsonProperty("substyle")]
        public uint? SubStyle { get; set; }

        [JsonProperty("minhealth")]
        public float? MinHealth { get; set; }

        [JsonProperty("maxhealth")]
        public float? MaxHealth { get; set; }
    }
}
