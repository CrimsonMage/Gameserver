/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;

namespace DerethForever.Database.Tests.CachePwn
{
    public class EmoteAction
    {
        [JsonProperty("type")]
        public uint EmoteActionType { get; set; }

        [JsonProperty("delay")]
        public float? Delay { get; set; }

        [JsonProperty("extent")]
        public float? Extent { get; set; }

        [JsonProperty("amount")]
        public uint? Amount { get; set; }

        [JsonProperty("motion")]
        public long? Motion { get; set; }

        [JsonProperty("msg")]
        public string Message { get; set; }

        [JsonProperty("amount64")]
        public ulong? Amount64 { get; set; }

        [JsonProperty("heroxp64")]
        public ulong? HeroXp64 { get; set; }

        [JsonProperty("cprof")]
        public CreateItem Item { get; set; }

        [JsonProperty("min64")]
        public long? Minimum64 { get; set; }

        [JsonProperty("max64")]
        public long? Maximum64 { get; set; }

        [JsonProperty("percent")]
        public float? Percent { get; set; }

        [JsonProperty("display")]
        public byte? Display_Binder { get; set; }

        public bool? Display
        {
            get { return ((Display_Binder == null) ? (bool?)null : (Display_Binder.Value == 0 ? true : false)); }
            set { Display_Binder = (value == null) ? (byte?)null : (value.Value ? (byte)0 : (byte)1); }
        }

        [JsonProperty("max")]
        public uint? Max { get; set; }

        [JsonProperty("min")]
        public uint? Min { get; set; }

        [JsonProperty("fmax")]
        public float? FMax { get; set; }

        [JsonProperty("fmin")]
        public float? FMin { get; set; }

        [JsonProperty("stat")]
        public uint? Stat { get; set; }

        [JsonProperty("pscript")]
        public uint? PScript { get; set; }

        [JsonProperty("sound")]
        public uint? Sound { get; set; }

        [JsonProperty("frame")]
        public Frame Frame { get; set; }

        [JsonProperty("spellid")]
        public uint? SpellId { get; set; }

        [JsonProperty("teststring")]
        public string TestString { get; set; }

        [JsonProperty("wealth_rating")]
        public uint? Wealth_Rating { get; set; }

        [JsonProperty("treasure_class")]
        public uint? treasure_class { get; set; }

        [JsonProperty("treasure_type")]
        public int? treasure_type { get; set; }

        [JsonProperty("mPosition")]
        public Position MPostion { get; set; }
    }
}
