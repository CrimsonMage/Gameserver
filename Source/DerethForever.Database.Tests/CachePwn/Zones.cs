/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Database.Tests.CachePwn
{
    public class Zones
    {
        public double? HLF { get; set; }

        public double? MLF { get; set; }

        public double? LLF { get; set; }

        public double? HRF { get; set; }

        public double? MRF { get; set; }

        public double? LRF { get; set; }

        public double? HLB { get; set; }

        public double? MLB { get; set; }

        public double? LLB { get; set; }

        public double? HRB { get; set; }

        public double? MRB { get; set; }

        public double? LRB { get; set; }
    }
}
