/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using DerethForever.Common;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;

namespace DerethForever.Database.Tests
{
    [TestClass]
    public class WeenieSearchTests
    {
        private static WorldJsonDatabase worldDb;

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // copy config.json
            File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);

            ConfigManager.Initialize();
            worldDb = new WorldJsonDatabase(ConfigManager.Config.Database.World.Folder);
        }

        [TestMethod]
        public void WeenieSearch_NullCriteria_ReturnsLotsOfWeenies()
        {
            var results = worldDb.SearchWeenies(null);
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void WeenieSearch_ByName_ReturnsKnownWeenies()
        {
            SearchWeeniesCriteria criteria = new SearchWeeniesCriteria();
            criteria.PartialName = "Pyreal Mote";
            var results = worldDb.SearchWeenies(criteria);
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void WeenieSearch_ByContent_DoesntExplode()
        {
            SearchWeeniesCriteria criteria = new SearchWeeniesCriteria();
            criteria.ContentGuid = Guid.NewGuid();
            var results = worldDb.SearchWeenies(criteria);
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void WeenieSearch_ByWeenieClassId_DoesntExplode()
        {
            SearchWeeniesCriteria criteria = new SearchWeeniesCriteria();
            criteria.WeenieClassId = 6353; // Pyreal Mote
            var results = worldDb.SearchWeenies(criteria);
            Assert.IsNotNull(results);
            Assert.AreEqual(1, results.Count);
        }

        [TestMethod]
        public void WeenieSearch_ByWeenieType_DoesntExplode()
        {
            SearchWeeniesCriteria criteria = new SearchWeeniesCriteria();
            criteria.WeenieType = Entity.Enum.WeenieType.LifeStone;
            var results = worldDb.SearchWeenies(criteria);
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0, "no lifestones (weenietype) in the database is bad.");
        }

        [TestMethod]
        public void WeenieSearch_ByItemType_DoesntExplode()
        {
            SearchWeeniesCriteria criteria = new SearchWeeniesCriteria();
            criteria.ItemType = Entity.Enum.ItemType.LifeStone;
            var results = worldDb.SearchWeenies(criteria);
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0, "no lifestones (itemtype) in the database is bad.");
        }

        [TestMethod]
        public void WeenieSearch_ByFloatProperty_ReturnsObject()
        {
            SearchWeeniesCriteria criteria = new SearchWeeniesCriteria();
            criteria.PartialName = "Peerless Healing Kit";
            criteria.PropertyCriteria.Add(new SearchWeenieProperty()
            {
                PropertyType = ObjectPropertyType.PropertyDouble,
                PropertyId = (uint)PropertyDouble.HealkitMod,
                PropertyValue = "1.75"
            });
            var results = worldDb.SearchWeenies(criteria);
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count > 0, "no Peerless Healing Kit in the database is bad.");
        }

        [TestMethod]
        public void GetAndSaveWeenie_ById_DoesNotThrow()
        {
            var mote = worldDb.GetWeenie(6353);

            mote.IsDirty = true;

            worldDb.SaveWeenie(mote);
        }
    }
}
