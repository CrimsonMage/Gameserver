/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DerethForever.Common;
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Database.Tests
{
    [TestClass]
    public class ContentTests
    {
        private static WorldJsonDatabase worldDb;

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // copy config.json
            File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"), ".\\Config.json", true);

            ConfigManager.Initialize();
            worldDb = new WorldJsonDatabase(ConfigManager.Config.Database.World.Folder);
        }
        
        [TestMethod]
        public void CreateContent_NoChildData_SavesContent()
        {
            Content c = new Content { ContentGuid = Guid.NewGuid() };
            if (c.ContentGuid == null)
                return;
            c.ContentName = "CreateContent_NoChildData_SavesContent " + c.ContentGuid.Value;

            worldDb.CreateContent(c);

            // clean up after ourselves
            worldDb.DeleteContent(c.ContentGuid.Value);
        }

        [TestMethod]
        public void UpdateContent_NoChildData_SavesContent()
        {
            Content c = new Content { ContentGuid = Guid.NewGuid() };
            if (c.ContentGuid == null)
                return;
            c.ContentName = "UpdateContent_NoChildData_SavesContent 1  " + c.ContentGuid.Value;

            worldDb.CreateContent(c);

            c.ContentName = "UpdateContent_NoChildData_SavesContent 2 " + c.ContentGuid.Value.ToString();

            worldDb.UpdateContent(c);

            List<Content> allContent = worldDb.GetAllContent();
            Content cCopy = allContent.FirstOrDefault(x => x.ContentGuid == c.ContentGuid);

            Assert.IsNotNull(cCopy, "cCopy is null");
            Assert.AreEqual(c.ContentName, cCopy.ContentName, "contentName is incorrect after an update.");

            // clean up after ourselves
            worldDb.DeleteContent(c.ContentGuid.Value);
        }

        [TestMethod]
        public void CreateContent_WithWeenieData_SavesContent()
        {
            Content c = new Content { ContentGuid = Guid.NewGuid() };
            if (c.ContentGuid == null)
                return;
            c.ContentName = "CreateContent_WithWeenieData_SavesContent " + c.ContentGuid.Value;
            c.ContentType = Entity.Enum.ContentType.Patch;
            c.Weenies.Add(new ContentWeenie()
            {
                ContentWeenieGuid = Guid.NewGuid(),
                WeenieId = 6353,
                Comment = "Pyreal Mote"
            });
            c.Weenies.Add(new ContentWeenie()
            {
                ContentWeenieGuid = Guid.NewGuid(),
                WeenieId = 6354,
                Comment = "Pyreal Nugget"
            });
            worldDb.CreateContent(c);

            List<Content> allContent = worldDb.GetAllContent();
            Content cCopy = allContent.FirstOrDefault(x => x.ContentGuid == c.ContentGuid);

            Assert.IsNotNull(cCopy, "cCopy is null");
            Assert.IsTrue(cCopy.Weenies.Count == 2, "cCopy.Weenies is missing");

            // clean up after ourselves
            worldDb.DeleteContent(c.ContentGuid.Value);
        }

        [TestMethod]
        public void CreateContent_WithLandblock_SavesContent()
        {
            Content c = new Content { ContentGuid = Guid.NewGuid() };
            if (c.ContentGuid == null)
                return;
            c.ContentName = "CreateContent_WithLandblock_SavesContent " + c.ContentGuid.Value;
            c.ContentType = Entity.Enum.ContentType.Patch;
            c.AssociatedLandblocks.Add(new ContentLandblock()
            {
                ContentLandblockGuid = Guid.NewGuid(),
                LandblockId = new LandblockId(5, 5),
                Comment = "a landblock"
            });
            worldDb.CreateContent(c);

            List<Content> allContent = worldDb.GetAllContent();
            Content cCopy = allContent.FirstOrDefault(x => x.ContentGuid == c.ContentGuid);

            Assert.IsNotNull(cCopy, "cCopy is null");
            Assert.IsTrue(cCopy.AssociatedLandblocks.Count > 0, "cCopy.AssociatedLandblocks is missing");

            // clean up after ourselves
            worldDb.DeleteContent(c.ContentGuid.Value);
        }

        [TestMethod]
        public void CreateContent_WithAssociatedContent_SavesContent()
        {
            Content c1 = new Content { ContentGuid = Guid.NewGuid() };
            c1.ContentName = "CreateContent_WithAssociatedContent_SavesContent 1 " + c1.ContentGuid.Value.ToString();
            c1.ContentType = Entity.Enum.ContentType.Patch;
            worldDb.CreateContent(c1);

            Content c2 = new Content { ContentGuid = Guid.NewGuid() };
            c2.ContentName = "CreateContent_WithAssociatedContent_SavesContent 2 " + c2.ContentGuid.Value.ToString();
            c2.ContentType = Entity.Enum.ContentType.Quest;
            c2.AssociatedContent.Add(new ContentLink() { AssociatedContentGuid = c1.ContentGuid.Value });
            worldDb.CreateContent(c2);

            List<Content> allContent = worldDb.GetAllContent();
            Content c1Copy = allContent.FirstOrDefault(c => c.ContentGuid == c1.ContentGuid);
            Content c2Copy = allContent.FirstOrDefault(c => c.ContentGuid == c2.ContentGuid);

            Assert.IsNotNull(c1Copy, "c1Copy is null");
            Assert.IsNotNull(c2Copy, "c2Copy is null");
            Assert.IsTrue(c2Copy.AssociatedContent.Count > 0, "c2Copy.AssociatedContent is missing");

            // clean up after ourselves
            worldDb.DeleteContent(c1.ContentGuid.Value);
            worldDb.DeleteContent(c2.ContentGuid.Value);
        }

        [TestMethod]
        public void CreateContent_WithExternalResource_SavesContent()
        {
            Content c = new Content { ContentGuid = Guid.NewGuid() };
            c.ContentName = "CreateContent_WithExternalResource_SavesContent " + c.ContentGuid.Value;
            c.ContentType = Entity.Enum.ContentType.Patch;
            c.ExternalResources.Add(new ContentResource() { ContentResourceGuid = Guid.NewGuid(), Name = "ACPedia link", ResourceUri = "http://acpedia.com/something" });
            worldDb.CreateContent(c);

            List<Content> allContent = worldDb.GetAllContent();
            Content cCopy = allContent.FirstOrDefault(x => x.ContentGuid == c.ContentGuid);

            Assert.IsNotNull(cCopy, "cCopy is null");
            Assert.IsTrue(cCopy.ExternalResources.Count > 0, "cCopy.ExternalResources is missing");

            // clean up after ourselves
            // worldDb.DeleteContent(c.ContentGuid);
        }
    }
}
