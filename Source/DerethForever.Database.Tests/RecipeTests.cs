/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Database.Tests
{
    [TestClass]
    public class RecipeTests
    {
        private static WorldJsonDatabase _worldDb;

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            _worldDb = new WorldJsonDatabase(Path.GetFullPath(@"..\..\..\..\..\..\GameWorld\json"));
        }

        [TestMethod]
        public void Recipe_Search_DoesntThrowExceptions()
        {
            SearchRecipesCriteria criteria = new SearchRecipesCriteria
            {
                RecipeType = RecipeType.CreateItem,
                SourceWcid = 1,
                TargetWcid = 2,
                Skill = Skill.ArcaneLore
            };

            List<Recipe> results = _worldDb.SearchRecipes(criteria);

            // this search shouldn't return anything, but we want to check for a not-null list either way
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public void Recipe_CreateGetUpdateDelete_WorksAsExpected()
        {
            Recipe r = new Recipe
            {
                RecipeType = (byte) RecipeType.CreateItem,
                ModifiedBy = "yo bro",
                LastModified = DateTime.UtcNow
            };
            _worldDb.CreateRecipe(r);

            Assert.IsNotNull(r.RecipeGuid);
            Assert.AreEqual((byte)RecipeType.CreateItem, r.RecipeType);
            Assert.IsTrue(r.UserModified);

            r.RecipeType = (byte)RecipeType.Healing;
            _worldDb.UpdateRecipe(r);

            Recipe r2 = _worldDb.GetRecipe(r.RecipeGuid.Value);
            Assert.AreEqual(r.ModifiedBy, r2.ModifiedBy);
            Assert.AreEqual((byte)RecipeType.Healing, r2.RecipeType, "update failed to change the recipe type");

            if (r2.RecipeGuid != null)
                _worldDb.DeleteRecipe(r2.RecipeGuid.Value);
        }
    }
}
