/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using DerethForever.Common;
using DerethForever.Database.Tests.CachePwn;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Emote = DerethForever.Entity.Emote;

namespace DerethForever.Database.Tests
{
    [TestClass]
    public class EmoteTests
    {
        private static WorldJsonDatabase _worldDb;

        private const uint TestWeenieId = 2;

        private const uint TestStackSize = 100;

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // copy config.json
            File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"),
                ".\\Config.json", true);

            ConfigManager.Initialize();
            _worldDb = new WorldJsonDatabase(ConfigManager.Config.Database.World.Folder);
        }

        public DataObject CreateTestingWeenie()
        {
            DataObject dataWeenie = _worldDb.GetWeenie(TestWeenieId) ?? new DataObject
            {
                DataObjectId = TestWeenieId,
                WeenieClassId = TestWeenieId,
                UserModified = true,
                DataObjectDescriptionFlags = (uint)ObjectDescriptionFlag.None
            };

            _worldDb.SaveWeenie(dataWeenie);
            return dataWeenie;
        }

        [TestMethod]
        public void Weenie_ManipulateAllTheThings_CreatesGetsAndDeletes()
        {
            // create a weenie
            CreateTestingWeenie();

            // see if it is there
            DataObject testWeenie = _worldDb.GetWeenie(TestWeenieId);
            testWeenie.EmoteTable = null;
            _worldDb.SaveWeenie(testWeenie);

            // refetch, assert no emotes
            testWeenie = _worldDb.GetWeenie(TestWeenieId);
            Assert.IsTrue(testWeenie.EmoteTable == null || testWeenie.EmoteTable.Count == 0);

            // add an emote directly
            testWeenie.EmoteTable = new List<EmoteSet>();
            testWeenie.EmoteTable.Add(new EmoteSet());
            testWeenie.EmoteTable[0].Category = EmoteCategory.Death;

            testWeenie.CreateList = new List<CreationProfile>();
            testWeenie.CreateList.Add(new CreationProfile());
            testWeenie.CreateList[0].Shade = 44f;
            testWeenie.CreateList[0].WeenieClassId = 1; // obviously not real data, but weenie should be there

            testWeenie.BodyParts = new List<Entity.BodyPart>();
            testWeenie.BodyParts.Add(new Entity.BodyPart() { BodyPartType = BodyPartType.Head, DamageType = (int)DamageType.Slashing });

            // INTENTIONALLY not set.  DB should handle setting it when it's saved.
            // testWeenie.EmoteTable[0].WeenieClassId

            testWeenie.EmoteTable[0].Emotes = new List<Emote>();
            testWeenie.EmoteTable[0].Emotes.Add(new Emote());

            // INTENTIONALLY not set.  DB should handle it on save
            // testWeenie.EmoteTable[0].Emotes[0].WeenieEmoteSetId
            testWeenie.EmoteTable[0].Emotes[0].Delay = 5f;
            testWeenie.EmoteTable[0].Emotes[0].EmoteType = EmoteType.DeleteSelf;
            testWeenie.EmoteTable[0].Emotes[0].Extent = 1f; // no idea what this is
            testWeenie.EmoteTable[0].Emotes[0].Message = "Hello World!"; // of course
            testWeenie.EmoteTable[0].Emotes[0].CreationProfile.StackSize = 1234;

            testWeenie.ModifiedBy = Guid.NewGuid().ToString();
            testWeenie.LastModified = DateTime.UtcNow;
            _worldDb.SaveWeenie(testWeenie);

            var savedWeenie = _worldDb.GetWeenie(TestWeenieId);

            Assert.AreEqual(1, savedWeenie.EmoteTable.Count);
            Assert.AreEqual(1, savedWeenie.EmoteTable[0].Emotes.Count);
            Assert.AreEqual(testWeenie.WeenieClassId, savedWeenie.EmoteTable[0].WeenieClassId);
            Assert.AreEqual(testWeenie.EmoteTable[0].EmoteSetGuid, savedWeenie.EmoteTable[0].Emotes[0].EmoteSetGuid);
            Assert.AreEqual(testWeenie.EmoteTable[0].Emotes[0].Delay, savedWeenie.EmoteTable[0].Emotes[0].Delay);
            Assert.AreEqual(testWeenie.EmoteTable[0].Emotes[0].CreationProfile.StackSize,
                savedWeenie.EmoteTable[0].Emotes[0].CreationProfile.StackSize);

            Assert.AreEqual(testWeenie.ModifiedBy, savedWeenie.ModifiedBy);
            Assert.AreEqual(testWeenie.LastModified.Value, savedWeenie.LastModified);
            Assert.AreEqual(1, savedWeenie.CreateList.Count);
            Assert.AreEqual(testWeenie.WeenieClassId, savedWeenie.CreateList[0].OwnerId);
            Assert.AreEqual(testWeenie.CreateList[0].Shade, savedWeenie.CreateList[0].Shade);

            Assert.AreEqual(testWeenie.BodyParts.Count, savedWeenie.BodyParts.Count);

            // add some more creation profiles
            testWeenie.CreateList.Add(new CreationProfile() { WeenieClassId = 1 });
            testWeenie.CreateList.Add(new CreationProfile() { WeenieClassId = 1 });
            testWeenie.CreateList.Add(new CreationProfile() { WeenieClassId = 1 });
            testWeenie.CreateList.Add(new CreationProfile() { WeenieClassId = 1 });
            _worldDb.SaveWeenie(testWeenie);

            savedWeenie = _worldDb.GetWeenie(TestWeenieId);

            Assert.AreEqual(testWeenie.CreateList.Count, savedWeenie.CreateList.Count);
        }

        // uncomment this when finished work. Coral Golem.
        [TestMethod]
        public void Weenie_UpdateFromJson()
        {
            const string path = @"..\..\..\..\DerethForever.Database.Tests\";
            // const string path = @"C:\DerethForever\weenies\";
            string[] files = Directory.GetFiles(path, "*.json", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                CachePwnWeenie importedWeenie = JsonConvert.DeserializeObject<CachePwnWeenie>(File.ReadAllText(file));
                if (importedWeenie == null)
                    continue;

                DataObject testWeenie = _worldDb.GetWeenie(importedWeenie.WeenieId);

                if (testWeenie == null || testWeenie.DataObjectId == 0)
                {
                    testWeenie = new DataObject();
                    // This is a new json - only populate items that exist so clear only 2nd level
                    testWeenie.StringProperties?.RemoveAll(x => x == null);
                    testWeenie.IntProperties?.RemoveAll(x => x == null);
                    testWeenie.Int64Properties?.RemoveAll(x => x == null);
                    testWeenie.DoubleProperties?.RemoveAll(x => x == null);
                    testWeenie.BoolProperties?.RemoveAll(x => x == null);
                    testWeenie.DataIdProperties?.RemoveAll(x => x == null);
                    testWeenie.InstanceIdProperties?.RemoveAll(x => x == null);
                    testWeenie.SpellsInSpellBars?.RemoveAll(x => x == null);
                    testWeenie.BookProperties?.RemoveAll(x => x == null);
                    testWeenie.Positions?.RemoveAll(x => x == null);
                    testWeenie.EmoteTable.ForEach(es => es.Emotes.RemoveAll(x => x == null));
                    testWeenie.EmoteTable?.RemoveAll(x => x == null);
                    testWeenie.BodyParts?.RemoveAll(x => x == null);

                    if (importedWeenie.Attributes == null)
                    {
                        testWeenie.DataObjectPropertiesAttributes = null;
                        testWeenie.DataObjectPropertiesAttributes2nd = null;
                    }
                }
                testWeenie = importedWeenie.UpdateWeenie(testWeenie);
                testWeenie.IsDirty = true;
                _worldDb.SaveWeenie(testWeenie);
            }
        }
    }
}
