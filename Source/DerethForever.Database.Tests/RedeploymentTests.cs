/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using DerethForever.Entity.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DerethForever.Database.Tests
{
    [TestClass]
    public class RedeploymentTests
    {
        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // copy config.json
            File.Copy(Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\DerethForever\\Config.json"),
                ".\\Config.json", true);

            ConfigManager.Initialize();
            Redeploy.SetToken();
        }

        [TestMethod]
        public void GetGitlabFiles_NoExceptions()
        {
            var dataFiles = Redeploy.GetResources(ResourceSelectionOption.Gitlab);
            Assert.IsNotNull(dataFiles, "No data files returned!");
        }

        [TestMethod]
        public void GetLocalFiles_NoExceptions()
        {
            var dataFiles = Redeploy.GetResources(ResourceSelectionOption.LocalDisk);
            Assert.IsNotNull(dataFiles, "No data files returned!");
        }

        [TestMethod]
        public void GetWebsiteFiles_NoExceptions()
        {
            var dataFiles = Redeploy.GetResources(ResourceSelectionOption.Website);
            Assert.IsNotNull(dataFiles, "No data files returned!");
        }

        [TestMethod]
        public void GetWorldArchiveChecksum()
        {
            var dataFiles = Redeploy.GetResources(ResourceSelectionOption.LocalDisk);
            Assert.IsFalse(dataFiles == null || dataFiles?.Count == 0);
            string checksum = null;
            foreach (var file in dataFiles)
            {
                if (file.Type == RemoteResourceType.WorldReleaseJsonFile)
                {
                    checksum = Redeploy.CalculateFileChecksum(file.FilePath);
                }
            }
            Assert.IsNotNull(checksum);
        }
    }
}
